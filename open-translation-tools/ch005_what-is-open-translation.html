<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Open Translation Tools</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/open-translation-tools/open-translation-tools.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/open-translation-tools/open-translation-tools.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Open Translation Tools
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch005_what-is-open-translation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch005_what-is-open-translation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>The Vision for Open Translation</h1>

  <p>Open Translation describes a nascent field of practice emerging at the crossroads of three dynamic movements of the information and internet eras:</p>

  <ul>
    <li>Open Content</li>

    <li>Free/Libre/Open Source Software (FLOSS)</li>

    <li>Open/Peer Production&nbsp;</li>
  </ul>

  <p><strong>Open Content</strong> encompasses a diverse range of knowledge resources available under open licenses such as Creative Commons (CC) and Free Document License (FDL), from books to manuals to documents to blog posts to multimedia. These resources are published on terms that encourage their redistribution, modification, and broad re-use. Open content resources like Wikipedia have dramatically changed the way knowledge is authored, maintained, and accessed.</p>

  <p>The <strong>Free/Libre/Open Source Software (FLOSS)</strong> movement is a vibrant global phenomenon which has, over the past 30-plus years, generated a sprawling ecology of software tools that are freely and openly available to anyone who wants them. This movement has established an alternative to proprietary, corporate-controlled software and corresponding closed data formats, which greatly benefits translators and localization practitioners. Given access to the underlying source code, they can create new versions of tools to support underserved audiences.<br></p>

  <p><strong>Open- or peer-production models</strong> use the internet's connected-but-distributed nature to bring broad human resources to bear on specific tasks or problems. Wikipedia is the flagship example of this, existing today as the single largest knowledge set in history. Other examples of peer production include Project Gutenberg's distributed proofreading community (www.gutenberg.org) and the FLOSSManuals authoring platform (www.flossmanuals.net) on which this book is produced.<br></p>

  <p>Open Translation synthesizes these three models of open production and open collaboration into a new discipline. It is the set of practices and work processes for translating and maintaining open content using FLOSS tools, and using the the internet to make that content and those tools and processes available to the largest number of writers and readers. Open Translation tools comprise a body of software that supports or performs language translation and is distributed under a FLOSS license.&nbsp;<br></p>

  <p>Open Translation's open components are fundamentally interrelated.</p>

  <ul>
    <li>If translation of open content depends on non-free or non-open software, it creates a critical bottleneck in the open knowledge ecosystem. When translation access and flow are controlled by proprietary tool vendors, those vendors can charge high prices and can disregard the best interests of open publishers and their readers.</li>

    <li>Because FLOSS projects are open source, FLOSS translation tools can always be further localized and customized to support new language pairs and locales.<br></li>

    <li>For free and open source software, open content is appropriate. Using FLOSS tools to translate and manage non-open content is prevalent and mostly license-compliant (with the GNU General Public License version 3 a notable exception). Free and open tools should ideally operate on free and open data.<br></li>

    <li>The open production models of Open Translation lower the barriers to participation in cross-language knowledge exchange, and help avoid replication of the "expert culture" that permeates the professional translation industry.</li>
  </ul>

  <p>Open translation can be viewed as translation's movement from an individual sport to a team sport. Additionally, social translation on the Internet is, as Ethan Zuckerman has suggested, a way for communities of translators to become journalists, deciding which content to move between language communities. Journalism on the web, as a social practice, is as much about curating, annotating, rating, and linking as it is about writing. This is a powerful and emergent form of journalism, encyclopedia creation, social networking, and much more.</p>

  <h2>The Promise of Open Translation<br></h2>

  <p>Open content projects like Wikipedia have rewritten conventional wisdom on who can publish knowledge. Global Voices Online has dramatically prefixed the role of 'journalist' with the adjective 'citizen'. The Free and Open Source software movements have inverted software production models from centralized, opaque and often lurching processes into decentralized, transparent and frequently agile endeavors.</p>

  <p>Open Translation promises to profoundly broaden access to knowledge across language barriers. Wikipedia may exist in hundreds of languages, but many language versions lag in terms of coverage. General cross-lingual access to open content and digital knowledge is still the exception rather the rule. The future of Open Translation lies in establishing richer, better-connected sets of online and offline tools while growing a global network of volunteer translators who understand and follow best practices for translating content and building open translation memories.<br></p>

  <p>The vision for Open Translation is predicated on the notion that anyone can be a translator by contributing to Open Translation projects. Just as FLOSS projects have project managers, testers, community moderators, and documenters in addition to developers, Open Translation projects welcome the efforts of proofreaders, editors, and project managers in addition to actual translators.</p>

  <p>But there are also opportunity costs to adhering to a vision of Open Translation. Open Translation tools are in many instances not as mature or full-featured as their proprietary counterparts. Those wishing to blaze the trail of an all-open approach to translation face a worse-before-better situation, where near-term sacrifice is necessary to support the improvement and evolution of the open tool set.<br></p>

  <h2>Getting to Open Translation<br></h2>

  <p>As the field of Open Translation continues to emerge and evolve, there are a number of projects, networks, issues and trends driving and gating that evolution.</p>

  <p>As much as open content, FLOSS, and peer production models have profoundly impacted our world and culture, they are not yet well integrated for the purposes of Open Translation. Open Translation tool coverage is incomplete, and existing tools rarely inter-operate or share standards for data interchange. Ubiquitous web publishing platforms like Drupal and Wordpress have minimal built-in support for maintaining multi-lingual sites. Add to these facts that open content license publishers like Creative Commons have not fully resolved licensing implications for translated works, and it is clear there is still much work to be done.</p>

  <p>Open Translation is synonymous with a new ecology of participation, and one for which the roles are still being established. What is known is that there are two under-tapped human resources which can be brought to bear: translators who want to volunteer their skills, and poly-lingual individuals who want to serve as volunteer translators. But leveraging such contributions is dependent on having well-defined ways in which to get involved. Global Voices and Wikipedia have fundamentally different models for volunteer translation, and are still evolving their community processes. Most open content projects have no idea how to establish sustainable volunteer translation models, and many that do utilize rudimentary processes centered on exchanging large email attachments.</p>

  <p>Scaling the pool of volunteer translators is its own challenge. Bi-lingual abilities are but a pre-requisite to being an effective translator; practice and experiential learning are required to effectively translate. Establishing community hubs for open translators is also an unsolved problem; while several professional translation communities such as ProZ thrive on the internet, open translator communities are only now beginning to gain momentum, and individuals who translate for open content projects are usually operating in project-specific networks.<br></p>

  <p>A fundamental challenge in an open environment is quality control. Traditional, centralized translation models have dedicated editors and proofreaders whose job it is to verify accuracy and consistency of translation. It remains for the Open Translation movement to establish quality processes and transparent mechanisms for reputation measurement.</p>

  <p>Also, the significance of regional and cultural issues in translation work can not be overstated; as norms and values vary, a range of secondary connotations and associations must be considered in crafting appropriate translations. While professional translators spend years learning the nuances of idiom and linguistic mapping in specific language pairs, open translators will not as often have the benefit of such learning curves. Creating better open repositories of essential empirical knowledge and best practices will further accelerate the ramp-up of volunteer translators and the quality of translations.<br></p>

  <h2>The Vision Turns on When, Not Whether<br></h2>

  <p>An openly translated internet is ultimately a matter of time, and the great unknown is how long it will take to realize the vision. Open Translation will scale in proportion to the open tools and open content upon which it rests, and on the corresponding efforts of those leading the way.&nbsp;</p>

  <p>This book exists as a step along the path, an attempt to both capture essential knowledge and take measure of the tools, processes and learnings of Open Translation to date. As you read on, consider yourself a part of this movement by virtue of your very interest. We invite you to contribute to the Open Translation movement in any way that taps your passion and inspires your participation.<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">OPEN TRANSLATION</li>

  <li><a href="ch004_why-translate.html">WHY TRANSLATE</a></li>

  <li><a href="ch005_what-is-open-translation.html">WHAT IS OPEN TRANSLATION?</a></li>

  <li><a href="ch006_current-state.html">CURRENT STATE</a></li>

  <li class="booki-section">BASIC CONCEPTS</li>

  <li><a href="ch008_translation.html">TRANSLATION</a></li>

  <li><a href="ch009_localisation.html">LOCALISATION</a></li>

  <li><a href="ch010_interpreting.html">INTERPRETING</a></li>

  <li><a href="ch011_dictionaries.html">DICTIONARIES</a></li>

  <li><a href="ch012_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch013_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch014_standards.html">STANDARDS</a></li>

  <li><a href="ch015_technical-concepts.html">TECHNICAL CONCEPTS</a></li>

  <li class="booki-section">TRANSLATION PROCESSES</li>

  <li><a href="ch017_translation.html">TRANSLATION</a></li>

  <li><a href="ch018_workflow.html">WORKFLOW</a></li>

  <li><a href="ch019_the-translation-industry.html">THE TRANSLATION INDUSTRY</a></li>

  <li><a href="ch020_open-translation.html">OPEN TRANSLATION</a></li>

  <li class="booki-section">COMMUNITY</li>

  <li><a href="ch022_roles-in-open-translation.html">ROLES IN OPEN TRANSLATION</a></li>

  <li><a href="ch023_community-management.html">COMMUNITY MANAGEMENT</a></li>

  <li class="booki-section">QUALITY CONTROL</li>

  <li><a href="ch025_quality-control.html">QUALITY CONTROL</a></li>

  <li><a href="ch026_reputation-metrics.html">REPUTATION METRICS</a></li>

  <li class="booki-section">CASE STUDIES</li>

  <li><a href="ch028_global-voices.html">GLOBAL VOICES</a></li>

  <li><a href="ch029_wikipedia.html">WIKIPEDIA</a></li>

  <li><a href="ch030_floss-manuals.html">FLOSS MANUALS</a></li>

  <li><a href="ch031_olpc-sugar.html">OLPC &amp; SUGAR</a></li>

  <li><a href="ch032_yeeyan.html">YEEYAN</a></li>

  <li class="booki-section">TRANSLATING TEXT</li>

  <li><a href="ch034_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch035_web-translation-systems.html">WEB TRANSLATION SYSTEMS</a></li>

  <li><a href="ch036_preparing-content.html">PREPARING CONTENT</a></li>

  <li><a href="ch037_translation-tips.html">TRANSLATION TIPS</a></li>

  <li><a href="ch038_cms.html">CMS</a></li>

  <li><a href="ch039_wikis.html">WIKIS</a></li>

  <li class="booki-section">TRANSLATING VIDEO</li>

  <li><a href="ch041_subtitles.html">SUBTITLES</a></li>

  <li><a href="ch042_file-formats.html">FILE FORMATS</a></li>

  <li><a href="ch043_finding.html">FINDING</a></li>

  <li><a href="ch044_creating.html">CREATING</a></li>

  <li><a href="ch045_playing.html">PLAYING</a></li>

  <li><a href="ch046_distribution.html">DISTRIBUTION</a></li>

  <li class="booki-section">TRANSLATING IMAGES</li>

  <li><a href="ch048_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch049_formats.html">FORMATS</a></li>

  <li><a href="ch050_tools.html">TOOLS</a></li>

  <li><a href="ch051_svg.html">SVG</a></li>

  <li><a href="ch052_translating-svg.html">TRANSLATING SVG</a></li>

  <li><a href="ch053_scripting-svg.html">SCRIPTING SVG</a></li>

  <li class="booki-section">INTELLECTUAL PROPERTY</li>

  <li><a href="ch055_free-software.html">FREE SOFTWARE</a></li>

  <li><a href="ch056_licensing.html">LICENSING</a></li>

  <li><a href="ch057_machine-translation-and-copyright.html">MACHINE TRANSLATION AND COPYRIGHT</a></li>

  <li class="booki-section">OPEN SOURCE TOOLS</li>

  <li><a href="ch059_anaphraseus.html">ANAPHRASEUS</a></li>

  <li><a href="ch060_apertium.html">APERTIUM</a></li>

  <li><a href="ch061_gaupol.html">GAUPOL</a></li>

  <li><a href="ch062_glossmaster.html">GLOSSMASTER</a></li>

  <li><a href="ch063_moses.html">MOSES</a></li>

  <li><a href="ch064_okapi-framework.html">OKAPI FRAMEWORK</a></li>

  <li><a href="ch065_omegat.html">OMEGAT+</a></li>

  <li><a href="ch066_omegat.html">OMEGAT</a></li>

  <li><a href="ch067_openoffice.html">OPENOFFICE</a></li>

  <li><a href="ch068_pootle.html">POOTLE</a></li>

  <li><a href="ch069_translate-toolkit.html">TRANSLATE TOOLKIT</a></li>

  <li><a href="ch070_virtaal.html">VIRTAAL</a></li>

  <li><a href="ch071_wwl.html">WWL</a></li>

  <li class="booki-section">ADVANCED TECH ISSUES</li>

  <li><a href="ch073_transliteration.html">TRANSLITERATION</a></li>

  <li><a href="ch074_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch075_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch076_character-encoding.html">CHARACTER ENCODING</a></li>

  <li><a href="ch077_web-fonts.html">WEB FONTS</a></li>

  <li><a href="ch078_scoring.html">SCORING</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch080_glossary.html">GLOSSARY</a></li>

  <li><a href="ch081_credits.html">CREDITS</a></li>

  <li><a href="ch082_preparingconent.html">PreparingConent</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch005_what-is-open-translation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch005_what-is-open-translation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

