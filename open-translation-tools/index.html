<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Open Translation Tools</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/open-translation-tools/open-translation-tools.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/open-translation-tools/open-translation-tools.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Open Translation Tools
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Introduction<br></h1>

  <p>The first wave of the internet revolution changed expectations about the availability of information a great deal. Information that was stored in libraries, locked in government vaults or available only to subscribers suddenly became accessible to anyone with an internet connection. A second wave has changed expectations about <em>who</em> creates information online. Tens of millions of people are contributing content to the modern internet, publishing photos, videos, and blog posts to a global audience.</p>

  <p>The globalization of the internet has brought connectivity to almost 1.6 billion people. The internet that results from globalization and user-authorship is profoundly polyglot. Wikipedia is now available in more than 210 languages, which implies that there are communities capable of authoring content in those tongues. Weblog search engine Technorati sees at least as many blog posts in Japanese as in English, and some scholars speculate that there may be as much Chinese content created on sites like Sina and QQ as on all English-language blogs combined.</p>

  <p>A user who joins the internet today is far more likely to encounter content in her own language than had she joined ten years ago. But each internet user is able to participate in a smaller percentage of the total interactions and conversations than an English-speaking internet user could have in 1997, when English was the dominant language of the Net.</p>

  <p>There's a danger of linguistic isolation in today's internet. In an earlier, English-dominated internet, users were often forced to cross linguistic barriers and interact in a common language to share ideas with a wider audience. In today's internet, there's more opportunity for Portuguese, Chinese, or Arabic speakers to interact with one another, and perhaps less incentive to interact with speakers of other languages. This in turn may fulfill some of the predictions put forth by those who see the internet acting as an echo chamber for like-minded voices, not as a powerful tool to encourage interaction and understanding across barriers of nation, language and culture.</p>

  <p>For the the internet to fulfill its most ambitious promises, we need to recognize translation as one of the core challenges to an open, shared, and collectively governed internet. Many of us share a vision of the internet as a place where the good ideas of any person in any country can influence thought and opinion around the world. This vision can only be realized if we accept the challenge of a polyglot internet and build tools and systems to bridge and translate between the hundreds of languages represented online.</p>

  <p>Machine translation will not solve all our problems. While machine translation systems continue to improve, they are well below the quality threshold necessary to enable readers to participate in conversations and debates with speakers of other languages. The best machine translation systems still have difficulty with colloquial and informal language, and are most reliable in translating between romance languages. The dream of a system that creates fully automated, high quality translations in important language pairs like English-Hindi still appears long off.</p>

  <p>While there is profound need to continue improving machine translation, we also need to focus on enabling and empowering human translators. Professional translation continues to be the gold standard for the translation of critical documents. But this method is too expensive to be used by web surfers simply interested in participating in discussions with peers in China or Colombia.</p>

  <p>The polyglot internet demands that we explore the possibility and power of distributed human translation. Hundreds of millions of internet users speak multiple languages; some percentage of these users are capable of translating between these. These users could be the backbone of a powerful, distributed peer production system able to tackle the audacious task of translating the internet.</p>

  <p>We are at the very early stages of the emergence of a new model for translation of online content -- "peer production" models of translation. Yochai Benkler uses the term "peer production" to describe new ways of organizing collaborative projects beyond such conventional arrangements as corporate firms. Individuals have a variety of motives for participation in translation projects, sometimes motivated by an explicit interest in building intercultural bridges, sometimes by fiscal reward or personal pride. In the same way that open source software is built by programmers fueled both by personal passion and by support from multinational corporations, we need a model for peer-produced translation that enables multiple actors and motivations.</p>

  <p>To translate the internet, we need both tools and communities. Open source translation memories will allow translators to share work with collaborators around the world; translation marketplaces will let translators and readers find each other through a system like Mechanical Turk, enhanced with reputation metrics; browser tools will let readers seamlessly translate pages into the highest-quality version available and request future human translations. Making these tools useful requires building large, passionate communities committed to bridging a polyglot web, preserving smaller languages, and making tools and knowledge accessible to a global audience.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">OPEN TRANSLATION</li>

  <li><a href="ch004_why-translate.html">WHY TRANSLATE</a></li>

  <li><a href="ch005_what-is-open-translation.html">WHAT IS OPEN TRANSLATION?</a></li>

  <li><a href="ch006_current-state.html">CURRENT STATE</a></li>

  <li class="booki-section">BASIC CONCEPTS</li>

  <li><a href="ch008_translation.html">TRANSLATION</a></li>

  <li><a href="ch009_localisation.html">LOCALISATION</a></li>

  <li><a href="ch010_interpreting.html">INTERPRETING</a></li>

  <li><a href="ch011_dictionaries.html">DICTIONARIES</a></li>

  <li><a href="ch012_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch013_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch014_standards.html">STANDARDS</a></li>

  <li><a href="ch015_technical-concepts.html">TECHNICAL CONCEPTS</a></li>

  <li class="booki-section">TRANSLATION PROCESSES</li>

  <li><a href="ch017_translation.html">TRANSLATION</a></li>

  <li><a href="ch018_workflow.html">WORKFLOW</a></li>

  <li><a href="ch019_the-translation-industry.html">THE TRANSLATION INDUSTRY</a></li>

  <li><a href="ch020_open-translation.html">OPEN TRANSLATION</a></li>

  <li class="booki-section">COMMUNITY</li>

  <li><a href="ch022_roles-in-open-translation.html">ROLES IN OPEN TRANSLATION</a></li>

  <li><a href="ch023_community-management.html">COMMUNITY MANAGEMENT</a></li>

  <li class="booki-section">QUALITY CONTROL</li>

  <li><a href="ch025_quality-control.html">QUALITY CONTROL</a></li>

  <li><a href="ch026_reputation-metrics.html">REPUTATION METRICS</a></li>

  <li class="booki-section">CASE STUDIES</li>

  <li><a href="ch028_global-voices.html">GLOBAL VOICES</a></li>

  <li><a href="ch029_wikipedia.html">WIKIPEDIA</a></li>

  <li><a href="ch030_floss-manuals.html">FLOSS MANUALS</a></li>

  <li><a href="ch031_olpc-sugar.html">OLPC &amp; SUGAR</a></li>

  <li><a href="ch032_yeeyan.html">YEEYAN</a></li>

  <li class="booki-section">TRANSLATING TEXT</li>

  <li><a href="ch034_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch035_web-translation-systems.html">WEB TRANSLATION SYSTEMS</a></li>

  <li><a href="ch036_preparing-content.html">PREPARING CONTENT</a></li>

  <li><a href="ch037_translation-tips.html">TRANSLATION TIPS</a></li>

  <li><a href="ch038_cms.html">CMS</a></li>

  <li><a href="ch039_wikis.html">WIKIS</a></li>

  <li class="booki-section">TRANSLATING VIDEO</li>

  <li><a href="ch041_subtitles.html">SUBTITLES</a></li>

  <li><a href="ch042_file-formats.html">FILE FORMATS</a></li>

  <li><a href="ch043_finding.html">FINDING</a></li>

  <li><a href="ch044_creating.html">CREATING</a></li>

  <li><a href="ch045_playing.html">PLAYING</a></li>

  <li><a href="ch046_distribution.html">DISTRIBUTION</a></li>

  <li class="booki-section">TRANSLATING IMAGES</li>

  <li><a href="ch048_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch049_formats.html">FORMATS</a></li>

  <li><a href="ch050_tools.html">TOOLS</a></li>

  <li><a href="ch051_svg.html">SVG</a></li>

  <li><a href="ch052_translating-svg.html">TRANSLATING SVG</a></li>

  <li><a href="ch053_scripting-svg.html">SCRIPTING SVG</a></li>

  <li class="booki-section">INTELLECTUAL PROPERTY</li>

  <li><a href="ch055_free-software.html">FREE SOFTWARE</a></li>

  <li><a href="ch056_licensing.html">LICENSING</a></li>

  <li><a href="ch057_machine-translation-and-copyright.html">MACHINE TRANSLATION AND COPYRIGHT</a></li>

  <li class="booki-section">OPEN SOURCE TOOLS</li>

  <li><a href="ch059_anaphraseus.html">ANAPHRASEUS</a></li>

  <li><a href="ch060_apertium.html">APERTIUM</a></li>

  <li><a href="ch061_gaupol.html">GAUPOL</a></li>

  <li><a href="ch062_glossmaster.html">GLOSSMASTER</a></li>

  <li><a href="ch063_moses.html">MOSES</a></li>

  <li><a href="ch064_okapi-framework.html">OKAPI FRAMEWORK</a></li>

  <li><a href="ch065_omegat.html">OMEGAT+</a></li>

  <li><a href="ch066_omegat.html">OMEGAT</a></li>

  <li><a href="ch067_openoffice.html">OPENOFFICE</a></li>

  <li><a href="ch068_pootle.html">POOTLE</a></li>

  <li><a href="ch069_translate-toolkit.html">TRANSLATE TOOLKIT</a></li>

  <li><a href="ch070_virtaal.html">VIRTAAL</a></li>

  <li><a href="ch071_wwl.html">WWL</a></li>

  <li class="booki-section">ADVANCED TECH ISSUES</li>

  <li><a href="ch073_transliteration.html">TRANSLITERATION</a></li>

  <li><a href="ch074_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch075_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch076_character-encoding.html">CHARACTER ENCODING</a></li>

  <li><a href="ch077_web-fonts.html">WEB FONTS</a></li>

  <li><a href="ch078_scoring.html">SCORING</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch080_glossary.html">GLOSSARY</a></li>

  <li><a href="ch081_credits.html">CREDITS</a></li>

  <li><a href="ch082_preparingconent.html">PreparingConent</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

