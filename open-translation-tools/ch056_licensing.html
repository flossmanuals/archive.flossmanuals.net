<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Open Translation Tools</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/open-translation-tools/open-translation-tools.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/open-translation-tools/open-translation-tools.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Open Translation Tools
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch056_licensing.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch056_licensing.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>LICENSING</h1>

  <p>Intellectual Property (IP) is an analogy for describing legal ownership of ideas. Common types of intellectual property include copyrights, trademarks, patents, industrial design rights and trade secrets. Copyrights are the legal area that apply most to Open Translation, as in the United States translations are deemed "derivative works," which may be violations of copyright. However, software patents for translation tools or machine translation algorithms may also be relevant, depending on a number of factors.</p>

  <p>Outside the United States, whether or not translations are deemed "derivative works" is hotly debated and varies widely by jurisdiction. Note that part of the debate centers on the nature of translations themselves: are translations simply word-for-word substitutions from one language into another, or are they nuanced interpretations of the meaning of texts (as well as faithfulness where possible to specific words used) from one language to another? In the former case, an argument could be made that a translation is not a derivative work; in the latter case, it is virtually certain that the translation is a derivative work. This distinction matters because your perspective has an impact on the tools (including licenses) and goals of any open translation project. Note that many people have an expansive view of "translation" which includes format-shifting (such as rendering the content for disabled users) and other types of changes to the original work, which again suggests that translations are best understood as derivative works from an intellectual property perspective.<br></p>

  <h2>Why License?</h2>

  <p>Licensing is controlling the rights for use and redistribution of your created work. <em>Licensing is very important.</em> In the United States, by default you own an <em>"all rights reserved"</em> copyright to any work that you create. In regards to translation, republication, and sharing over the web, it becomes very important to understand exactly what licensing rights you have, and the details of which rights you want to waive in order to further your project.</p>

  <p>For example, if you waive certain aspects of your copyright (from an <em>"all rights reserved"</em> to a <em>"some rights reserved"</em> model), it can be beneficial to the quantity, quality, scale and success of your translation project.</p>

  <p>Not only does copyright apply to translatable content, but also to the software tools that aid in the translation of that content. When we refer to 'Open Translation Tools', for example, we are generally referring to software tools licensed in a way that allows the free use, reuse, and alteration of the software.<br></p>

  <h2>Common Types of Licenses</h2>

  <p><em>Choosing the proper license for your software tool or translatable content is extremely important.</em> Below are some common types of licenses and their distinctions.</p>

  <h3>Full Copyright</h3>

  <p><img alt="354px_Copyright_serif.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-354px_Copyright_serif-svg-en.png" width="128" align="right" height="128"></p>

  <p><em>The default "all rights reserved."</em></p>

  <p>Copyright was designed to allow for legal protection of authors, allowing them to hold exclusive rights to their work for a limited time. While copyright term was only designed to last 14 years in the United States, corporations have pushed forward legislation that has now expanded the copyright term to last the lifetime of the author plus 70 years from the original date of publication. After this time, the work falls into the public domain--the free zone of sharing where everyone is free to use the work. Note that although all signatories of the Berne Convention on Literary and Artistic works have a limited (if overly long) duration for copyright, not all countries unambiguously deposit works out of copyright into the public domain.<br></p>

  <p>Copyright protects content owners from having others steal their work and treat it as their own. However, many content creators can unintentionally prevent their work from being spread by not properly communicating (through licenses) which specific uses of their work they wish to allow (such as the right to save an image, document, or other file from the Internet.)<br></p>

  <h3>Permissive Licenses</h3>

  <p><img alt="600px_NoCopyright.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-600px_NoCopyright-en.png" width="128" align="right" height="128"></p>

  <p><em>"All wrongs reserved."</em></p>

  <p>Permissive licenses allow the work to be re-purposed. From a legal perspective, permissive licenses often exist merely to disclaim warranty. This removes all liability by the copyright holder. <em>If it breaks, you can't say it's our fault.</em> By default a copyright has "all rights reserved." An easy way to think about permissive licenses is "all wrongs reserved." This kind of license says "we don't care about copyright." Some examples of software projects that use a permissive license are Apache Web Server and the Berkeley Software Distribution (BSD).</p>

  <p>Permissive licenses can be good because they provide free and open source software, and free content, all of which are unencumbered by any significant restrictions on uses or users. Permissive licenses can be bad because modified versions of the software (for example), often improvements, do not have to be free like the original. For example, the proprietary, closed-source operating system Macintosh OS X is based off the Berkeley Software Distribution. Apple Macintosh is basically "free-riding" on the backs of the diligent coders who create BSD.</p>

  <p>Some important free tools released under this license are Optical Character Recognition (OCR) tools such as Tesseract and OCRopus, both released under the Apache license. Google currently funds and uses both of these tools in its large book scanning project. Note that the decision steps regarding the use of permissive licenses often vary considerably depending on the nature of the content--the differences among software and other types of content are particularly important.<br></p>

  <h3>Copyleft Licenses</h3>

  <p><img alt="512px_Copyleft.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-512px_Copyleft-svg-en.png" width="127" align="right" height="127"></p>

  <p><em>"Sharing is caring."</em></p>

  <p>The "problem" of future versions not being free is solved by copyleft licenses. 'Copyleft' as an idea is a largely ethical, philosophical, and political movement that seeks to free ideas from the constraints of intellectual property law. According to the proponents of copyleft, duplication via communication is part of the very essence of what it means to have an idea, that is to say, what it means to share an idea. As they say, "sharing is the nature of creation."</p>

  <p>The earliest example of a copyleft license is the GPL. Written in 1989 by a computer scientist at MIT named Richard Stallman, the license was designed to ensure the future success of Stallman's project: GNU. GNU, or <em>GNU's Not Unix</em> (a snarky recursive acronym I might add), is a massive collaboration project in free, community-developed, community-maintained software.</p>

  <p>The GNU project has grown to enormity in its success, with the GPL quickly being adopted by free software projects across the world. Today, most serious and self-respecting open source projects publish their code under the GPL, which simply stands for GNU Public License. However, the freest license on the Internet today is not in fact the GPL, but the Affero GPL or AGPL. This license is only used for a few projects, such as Laconica.<br></p>

  <p>Since FLOSS Manuals is designed as a documentation service for Free (as in Libre) and Open Source Software, our text is published under the GPL so that it can be redistributed with the free software that it is a reference for. This text, since it covers some open source tools, is released under the GPL as well.<br></p><img alt="63px_Cc.logo.circle.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-63px_Cc-logo-circle-svg-en.png" width="63" align="right" height="63">

  <h3>Creative Commons Licenses</h3>

  <p><em>"Saving the world from failed sharing."</em></p>

  <p>Creative Commons (CC) licenses are several copyright licenses first published on December 16, 2002 by the Creative Commons organization, a U.S. non-profit corporation founded in 2001 by a Stanford law professor named Lawrence Lessig. "Larry" has written several books on US copyright law--his latest is called <em>Remix: Making Art and Commerce Thrive in the Hybrid Economy.</em> The goal of the Creative Commons organization is to enable effective sharing of copyrighted content. Larry is also featured in a fantastic documentary called <em>RiP: a remix manifesto</em>. If you're too young to drink in the states, watch the movie. If you're too old to get smashed like a kid in the states, read the book. If you are in neither category do both. Then you will understand.<br></p><strong>Attributes of CC Licenses</strong>

  <p>The Creative Commons organization sought to create a common language for licenses, so that they were readable and immediately understandable by the everyday man. They created a spectrum of free licenses, with varying freedoms controlled by various attributes. <em>These attributes can be mixed and matched to create a custom license.</em> Not all combinations are possible since some attributes are mutually exclusive. Here are the various licenses composed of the four basic attributes:<br></p><strong>Attribution (CC-BY)</strong><br>
  <img alt="64px_Cc_by_new.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-64px_Cc_by_new-svg-en.png" width="64" align="right" height="64">

  <p>Attribution (BY) comes by default with all CC licenses, with the only exception being the CC0 license. This attribute is to say <em>"give credit where credit is due."</em> Copying is only permitted when the author or authors of the original work are properly attributed. You may choose to have attribution be the only stipulation (CC BY license), in which case the work can then be repurposed for anything. <em>This is essentially a permissive license.</em><br></p>

  <p><strong>Attribution Share-Alike (CC-BY-SA)</strong></p>

  <p><strong><img alt="64px_Cc_sa.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-64px_Cc_sa-svg-en.png" width="64" align="right" height="64"></strong></p>

  <p>Share-Alike (SA) means that any derivatives of the original work <em>must be shared under the same exact license as the original</em>. This option allows for the freedom to 'remix,' but backed by a principle of reciprocity. I share with you, you share with everyone else. The CC BY-SA license also requires that you properly attribute the original author, like all CC licenses. <em>This is a copyleft license.</em><br></p>

  <p><strong>Attribution No-Derivatives (CC-BY-ND)</strong></p>

  <p><strong><img alt="64px_Cc_nd.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-64px_Cc_nd-svg-en.png" width="64" align="right" height="64"></strong></p>

  <p>No-Derivatives (ND), like the rest of these terms, is fairly straightforward. If present, you may not make derivative works of the original work. The right to 'remix' is reserved by the original author, who may wish that his work is not mutilated or misrepresented. While this term can be used in a free license as it allows for copying, <em>licenses using the ND term are less free than permissive or a copyleft licenses.</em> Licenses using the ND term are less free is because they make for works that are sterile and not generative. I use the terms "sterile" and "generative" in the Jonathan Zittrain sense.</p>

  <p>Because no derivatives are allowed, the ND and SA terms cannot be combined. Note that the lack of permission to make derivative works means that the work may not be translated (under most circumstances). As such, Open Translation projects are not likely to either use or recommend the use of the ND term.<br></p>

  <p><strong>Attribution Non-Commercial (CC-BY-NC-[ND/SA])</strong></p>

  <p><img alt="64px_Cc_nc.svg.png" src="_booki/open-translation-tools/static/OpenTranslationTools-Licenses-64px_Cc_nc-svg-en.png" width="64" align="right" height="64"></p>

  <p>The Non-Commercial (NC) attribute is a stipulation that you may reuse the work provided that it is only used for non-commercial purposes. The author reserves the right to monetize and commercialize the work. This means the work cannot be sold by any person or corporate entity, not even a non-profit corporation. However, the work may be shared freely and, depending on the actual license chosen, adapted (including translated) or modifed without permission. If the share-alike (SA) term is also used, then those modifications must be relicensed and made available using exactly the same license. If the no derivatives (ND) term is used, then the original work can only be copied and distributed, not adapted, and then only non-commercially. <em>As with the ND term, licenses with this attribute are less free than permissive or copyleft licenses.</em></p>

  <p>It is worth noting that the interpretation of the non-commercial restriction is not clear-cut. In addition, most proponents of copyleft licensing for software (generally with the GPL) find the commercial constraint to be highly discriminatory to legitimate open business models and believe that the NC term should be avoided at all costs.<br></p><strong>The CC0 Waiver</strong>

  <p><em>"No rights reserved."</em><br></p>

  <p>The CC0 waiver is <em>not</em> a license, but is rather a tool that allows a copyright holder, to the extent possible, to release all restrictions on a copyrighted work worldwide. CC0 was created (at least in part) in response to new database rights ("moral rights") in the European Union. Because copyright law is different for databases in the European Union, the Creative Commons organization combined a public domain dedication with a waiver that released any and all ownership rights, including those on data. The CC0 waiver is the closest thing to a public domain dedication, <em>as you waive any possible rights.</em><br></p>

  <h2>The Public Domain</h2>

  <p>The public domain is the free realm of sharing where the original author, if known, contains no rights at all over his work. Some freedom-lovers who seek no recognition for their work simply release it into the public domain with a tool called a <em>public domain dedication.</em> Additionally, when the copyright term expires for a particular work, that work enters the public domain (at least in the United States). Since work in the public domain lacks any copyright protection, it may be leveraged by corporations hoping to profit from the work, as well as anyone else. This is common for reprinting of out-of-copyright texts like Alice in Wonderland, Shakespeare, and other classics. Once a work is in the public domain, the original work can never be copyrighted by anyone, regardless of any copyright claims on new renditions.<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">OPEN TRANSLATION</li>

  <li><a href="ch004_why-translate.html">WHY TRANSLATE</a></li>

  <li><a href="ch005_what-is-open-translation.html">WHAT IS OPEN TRANSLATION?</a></li>

  <li><a href="ch006_current-state.html">CURRENT STATE</a></li>

  <li class="booki-section">BASIC CONCEPTS</li>

  <li><a href="ch008_translation.html">TRANSLATION</a></li>

  <li><a href="ch009_localisation.html">LOCALISATION</a></li>

  <li><a href="ch010_interpreting.html">INTERPRETING</a></li>

  <li><a href="ch011_dictionaries.html">DICTIONARIES</a></li>

  <li><a href="ch012_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch013_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch014_standards.html">STANDARDS</a></li>

  <li><a href="ch015_technical-concepts.html">TECHNICAL CONCEPTS</a></li>

  <li class="booki-section">TRANSLATION PROCESSES</li>

  <li><a href="ch017_translation.html">TRANSLATION</a></li>

  <li><a href="ch018_workflow.html">WORKFLOW</a></li>

  <li><a href="ch019_the-translation-industry.html">THE TRANSLATION INDUSTRY</a></li>

  <li><a href="ch020_open-translation.html">OPEN TRANSLATION</a></li>

  <li class="booki-section">COMMUNITY</li>

  <li><a href="ch022_roles-in-open-translation.html">ROLES IN OPEN TRANSLATION</a></li>

  <li><a href="ch023_community-management.html">COMMUNITY MANAGEMENT</a></li>

  <li class="booki-section">QUALITY CONTROL</li>

  <li><a href="ch025_quality-control.html">QUALITY CONTROL</a></li>

  <li><a href="ch026_reputation-metrics.html">REPUTATION METRICS</a></li>

  <li class="booki-section">CASE STUDIES</li>

  <li><a href="ch028_global-voices.html">GLOBAL VOICES</a></li>

  <li><a href="ch029_wikipedia.html">WIKIPEDIA</a></li>

  <li><a href="ch030_floss-manuals.html">FLOSS MANUALS</a></li>

  <li><a href="ch031_olpc-sugar.html">OLPC &amp; SUGAR</a></li>

  <li><a href="ch032_yeeyan.html">YEEYAN</a></li>

  <li class="booki-section">TRANSLATING TEXT</li>

  <li><a href="ch034_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch035_web-translation-systems.html">WEB TRANSLATION SYSTEMS</a></li>

  <li><a href="ch036_preparing-content.html">PREPARING CONTENT</a></li>

  <li><a href="ch037_translation-tips.html">TRANSLATION TIPS</a></li>

  <li><a href="ch038_cms.html">CMS</a></li>

  <li><a href="ch039_wikis.html">WIKIS</a></li>

  <li class="booki-section">TRANSLATING VIDEO</li>

  <li><a href="ch041_subtitles.html">SUBTITLES</a></li>

  <li><a href="ch042_file-formats.html">FILE FORMATS</a></li>

  <li><a href="ch043_finding.html">FINDING</a></li>

  <li><a href="ch044_creating.html">CREATING</a></li>

  <li><a href="ch045_playing.html">PLAYING</a></li>

  <li><a href="ch046_distribution.html">DISTRIBUTION</a></li>

  <li class="booki-section">TRANSLATING IMAGES</li>

  <li><a href="ch048_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch049_formats.html">FORMATS</a></li>

  <li><a href="ch050_tools.html">TOOLS</a></li>

  <li><a href="ch051_svg.html">SVG</a></li>

  <li><a href="ch052_translating-svg.html">TRANSLATING SVG</a></li>

  <li><a href="ch053_scripting-svg.html">SCRIPTING SVG</a></li>

  <li class="booki-section">INTELLECTUAL PROPERTY</li>

  <li><a href="ch055_free-software.html">FREE SOFTWARE</a></li>

  <li><a href="ch056_licensing.html">LICENSING</a></li>

  <li><a href="ch057_machine-translation-and-copyright.html">MACHINE TRANSLATION AND COPYRIGHT</a></li>

  <li class="booki-section">OPEN SOURCE TOOLS</li>

  <li><a href="ch059_anaphraseus.html">ANAPHRASEUS</a></li>

  <li><a href="ch060_apertium.html">APERTIUM</a></li>

  <li><a href="ch061_gaupol.html">GAUPOL</a></li>

  <li><a href="ch062_glossmaster.html">GLOSSMASTER</a></li>

  <li><a href="ch063_moses.html">MOSES</a></li>

  <li><a href="ch064_okapi-framework.html">OKAPI FRAMEWORK</a></li>

  <li><a href="ch065_omegat.html">OMEGAT+</a></li>

  <li><a href="ch066_omegat.html">OMEGAT</a></li>

  <li><a href="ch067_openoffice.html">OPENOFFICE</a></li>

  <li><a href="ch068_pootle.html">POOTLE</a></li>

  <li><a href="ch069_translate-toolkit.html">TRANSLATE TOOLKIT</a></li>

  <li><a href="ch070_virtaal.html">VIRTAAL</a></li>

  <li><a href="ch071_wwl.html">WWL</a></li>

  <li class="booki-section">ADVANCED TECH ISSUES</li>

  <li><a href="ch073_transliteration.html">TRANSLITERATION</a></li>

  <li><a href="ch074_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch075_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch076_character-encoding.html">CHARACTER ENCODING</a></li>

  <li><a href="ch077_web-fonts.html">WEB FONTS</a></li>

  <li><a href="ch078_scoring.html">SCORING</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch080_glossary.html">GLOSSARY</a></li>

  <li><a href="ch081_credits.html">CREDITS</a></li>

  <li><a href="ch082_preparingconent.html">PreparingConent</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch056_licensing.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch056_licensing.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

