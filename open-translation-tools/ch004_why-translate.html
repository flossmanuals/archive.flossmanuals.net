<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Open Translation Tools</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/open-translation-tools/open-translation-tools.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/open-translation-tools/open-translation-tools.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Open Translation Tools
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch004_why-translate.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch004_why-translate.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Why Translate?</h1>

  <p>A foundational question in explaining and advocating Open Translation is "why translate?". Though reasons vary, there is a range of inspirations and mandates for translation and broader access to content.<br></p>

  <h2>Motivations for translating&nbsp;</h2>

  <p>For those in the open knowledge and open education fields, translation is about access to information and associated issues of social justice. Until all peoples have access to the most up-to-date and complete knowledge, a globally equitable world is not possible.</p>

  <p>For those in the FLOSS (Free/Libre Open Source Software) universe, key motivators are expanding access to documentation and training resources, as well as creating localized versions of software tools. A framing value of the Free Software movement is the right of individuals to modify software to meet their specific needs, and language-specific versions of software certainly exemplify this freedom.</p>

  <p>Other motivations to translate are:<br></p>

  <ul>
    <li><em>Creating new audiences and markets</em> -- Making content available in additional languages expands the potential pool of consumers for that material. For instance, making an article available in Chinese (and getting it through the Great Firewall) invites potential attention from an audience that numbers in the billions.&nbsp;<br></li>

    <li><em>Promoting mother tongues</em> -- In a related dynamic, translation allows communities to promote their mother tongue, both by translating relevant new information into the language, as well as publishing information available only in that language into other languages. Translation into just a few widespread languages, like Hindi, Spanish, Arabic, Russian, and Japanese, can reach about half the people on the planet. Enabling translation into about 300 additional languages could reach just about everybody else <a href="http://www.ethnologue.com">(</a><a href="http://www.ethnologue.com" target="_top">http://www.ethnologue.com</a>). With cell phone technology expanding around the globe, many millions are poised to come online, driving demand for content and communications in their native tongues.<br></li>
  </ul>

  <p><img alt="Long_tail_languages.jpg" src="_booki/open-translation-tools/static/OpenTranslationTools-Long_tail_languages-en.jpg"><br></p>

  <ul>
    <li><em>Revitalizing endangered languages</em> -- UNESCO (United Nations Educational, Scientific and Cultural Organization) estimates that nearly half of the approximately 6,000 languages spoken on earth are endangered, and many more are in a precarious state (<a href="http://www.unesco.org/culture/ich/index.php?pg=00206" target="_top">http://www.unesco.org/culture/ich/index.php?pg=00206</a>). With the loss of a language comes incalculable cultural loss, as well as loss to all humanity of our knowledge about the planet and how to live sustainably in its myriad environments. Enabling translation into endangered languages empowers speakers and adds vitality to their languages by allowing them a voice in a new, modern online domain. Translation in both directions generates language-pair corpora that support subsequent translations, and new tools powered by machine analysis.<br></li>

    <li><em>Cross-pollinating ideas</em> -- Translated content provides perspective and knowledge from different cultures, experiences, and intellectual frames. Even in the internet era, much information still resides in language-specific silos. Making such resources available to broader audiences propagates ideas and drives the consideration of different schools of thought and belief.<br></li>

    <li><em>Stimulating local content creation</em> -- Translated content can be a catalyst for additions, responses, and new creation in the local language. As YeeYan (www.yeeyan.com) translates content into Chinese for publication within China, they are engendering a new generation of Chinese content producers, both those who translate and those who comment and collaborate around the content itself.&nbsp;</li>

    <li><em>Aligning with national objectives or legislation</em> -- In Canada, products must be released in both French and English, and similar situations exist in many other countries. Sometimes it's not just a good idea, it's the law.</li>

    <li><em>Matching content with community language needs</em> -- There are specific efforts in South Africa aimed at producing HIV/AIDS content for impacted communities. In such situations, translation is a life-saving and community-sustaining service.<br></li>
  </ul>

  <h2>The broader case for translation&nbsp;</h2>

  <p>In general terms, translation enables us to understand our fellow global citizens. The ongoing globalization of knowledge, communication, and information networks has the potential to be a hopeful story. There is vast untapped potential for diverse collaborations between the 1.8 billion users who are on line -- the potential to share knowledge and converse through an increasingly capable infrastructure of open translation tools, open translation data, and online translation communities. There are also many&nbsp; examples of a lack of translation foilng the best intentions. For example, the name of the online invitation and social event site, Evite, tells Spanish-speaking users, "avoid". Translation is crucial for a global society to function together.<br></p>

  <p>When we think about the role of translation in transforming the global web, we can consider several major areas of impact: media, education, health resources, and software.<br></p>

  <p>The media environment is changing. Increasingly, the way we communicate with the people around us is an act of publication. Every moment there is a voice in the world that most needs to be understood, whose poetic insight or dramatic experience or unique knowledge is potentially world-changing. So-called "citizen journalism" offers real-time and real-place access to understanding our world. Translation is the missing ingredient in a participatory global media ecosystem that could lead to a world with a more complete and more nuanced understanding of the events that shape our shared circumstances. This becomes all the more true as dialog scales about our responses to climate change and other global challenges.</p>

  <p>Open Educational Resources (OER) offer an opportunity to scale learning across the globe. With ever more learning material available under open licenses and with increasing global internet penetration, the primary remaining barrier to making this material accessible is translation. In one early proof of concept, Meedan.net implemented a pilot project for Teachers Without Borders that combined machine translation with crowd-sourced (social) error correction on top of OER for Arab region secondary school learners.</p>

  <p>As the world becomes increasingly connected and interdependent, diseases and health concerns move faster, while continuing to ignore borders and language barriers. Global health efforts against illnesses such as HIV/AIDS, tuberculosis, and malaria depend on successful transmission of accurate prevention and care information. This can only occur with proper translation. Health information is an area where translation is essential to saving lives.&nbsp;<br></p>

  <p>While participatory media and open knowledge networks offer global citizens better access to content, the free software movement enables emergent knowledge economies around the world. Projects like <a href="http://translate.org.za">translate.org.za</a> empower language communities with tools to localize open source software into local languages.</p>

  <h2>But what about <strong><em>my</em></strong> content?<br></h2>Why translate your website or blog? There are many reasons to consider publishing a multilingual website, among them reaching local readers who speak other languages, engaging international readership, and increasing search engine visibility for your content in other languages.<br>

  <p><strong>Local Readership</strong></p>Most cities are multi-ethnic and multi-lingual. Even in the United States, which many people consider an English speaking country, a sizable minority of the population speaks English as a second language or not at all. In most major US cities, the Spanish-speaking audience alone is significant. If you publish a local or regional website, for example an online newspaper or local events blog, you should consider targeting the most important secondary languages in your market, which might otherwise never hear your message.<br>

  <p><strong>International Readership</strong></p>If your website has an international audience (this is easy to see with services like Google Analytics) or covers a topic that is not tied to a region, you can expand your readership and visibility by targeting important international languages (for example, English, Spanish, French, or Chinese). Once your content is routinely translated to these languages, you'll become visible and linkable in these languages and countries, and should start receiving traffic from these regions that you would otherwise never have received.<br>

  <p><strong>Search Engine Visibility</strong></p>

  <p>People search for sites or terms in their language, not yours. Translating your site into other languages makes your site more visible to search engines, which will index your site and relevant search terms in those languages. You will soon become visible to people doing keyword searches on those terms, and to other websites, which may begin to link to you as well. Blogs are an especially important source of "side door" links in other languages, and as more of them link to you, your search engine ranking will improve.</p>

  <h2>Translation is a local decision&nbsp;</h2>

  <p>In the end, each creator of content makes his or her own decision about its readiness and availability for translation. This book is published on the dual premises that translation is an imperative for better global understanding in an increasingly complicated world, and that open translation is the most appropriate, scalable and sustainable approach to making content and knowledge available to the broadest set of communities and citizens.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">OPEN TRANSLATION</li>

  <li><a href="ch004_why-translate.html">WHY TRANSLATE</a></li>

  <li><a href="ch005_what-is-open-translation.html">WHAT IS OPEN TRANSLATION?</a></li>

  <li><a href="ch006_current-state.html">CURRENT STATE</a></li>

  <li class="booki-section">BASIC CONCEPTS</li>

  <li><a href="ch008_translation.html">TRANSLATION</a></li>

  <li><a href="ch009_localisation.html">LOCALISATION</a></li>

  <li><a href="ch010_interpreting.html">INTERPRETING</a></li>

  <li><a href="ch011_dictionaries.html">DICTIONARIES</a></li>

  <li><a href="ch012_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch013_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch014_standards.html">STANDARDS</a></li>

  <li><a href="ch015_technical-concepts.html">TECHNICAL CONCEPTS</a></li>

  <li class="booki-section">TRANSLATION PROCESSES</li>

  <li><a href="ch017_translation.html">TRANSLATION</a></li>

  <li><a href="ch018_workflow.html">WORKFLOW</a></li>

  <li><a href="ch019_the-translation-industry.html">THE TRANSLATION INDUSTRY</a></li>

  <li><a href="ch020_open-translation.html">OPEN TRANSLATION</a></li>

  <li class="booki-section">COMMUNITY</li>

  <li><a href="ch022_roles-in-open-translation.html">ROLES IN OPEN TRANSLATION</a></li>

  <li><a href="ch023_community-management.html">COMMUNITY MANAGEMENT</a></li>

  <li class="booki-section">QUALITY CONTROL</li>

  <li><a href="ch025_quality-control.html">QUALITY CONTROL</a></li>

  <li><a href="ch026_reputation-metrics.html">REPUTATION METRICS</a></li>

  <li class="booki-section">CASE STUDIES</li>

  <li><a href="ch028_global-voices.html">GLOBAL VOICES</a></li>

  <li><a href="ch029_wikipedia.html">WIKIPEDIA</a></li>

  <li><a href="ch030_floss-manuals.html">FLOSS MANUALS</a></li>

  <li><a href="ch031_olpc-sugar.html">OLPC &amp; SUGAR</a></li>

  <li><a href="ch032_yeeyan.html">YEEYAN</a></li>

  <li class="booki-section">TRANSLATING TEXT</li>

  <li><a href="ch034_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch035_web-translation-systems.html">WEB TRANSLATION SYSTEMS</a></li>

  <li><a href="ch036_preparing-content.html">PREPARING CONTENT</a></li>

  <li><a href="ch037_translation-tips.html">TRANSLATION TIPS</a></li>

  <li><a href="ch038_cms.html">CMS</a></li>

  <li><a href="ch039_wikis.html">WIKIS</a></li>

  <li class="booki-section">TRANSLATING VIDEO</li>

  <li><a href="ch041_subtitles.html">SUBTITLES</a></li>

  <li><a href="ch042_file-formats.html">FILE FORMATS</a></li>

  <li><a href="ch043_finding.html">FINDING</a></li>

  <li><a href="ch044_creating.html">CREATING</a></li>

  <li><a href="ch045_playing.html">PLAYING</a></li>

  <li><a href="ch046_distribution.html">DISTRIBUTION</a></li>

  <li class="booki-section">TRANSLATING IMAGES</li>

  <li><a href="ch048_introduction.html">INTRODUCTION</a></li>

  <li><a href="ch049_formats.html">FORMATS</a></li>

  <li><a href="ch050_tools.html">TOOLS</a></li>

  <li><a href="ch051_svg.html">SVG</a></li>

  <li><a href="ch052_translating-svg.html">TRANSLATING SVG</a></li>

  <li><a href="ch053_scripting-svg.html">SCRIPTING SVG</a></li>

  <li class="booki-section">INTELLECTUAL PROPERTY</li>

  <li><a href="ch055_free-software.html">FREE SOFTWARE</a></li>

  <li><a href="ch056_licensing.html">LICENSING</a></li>

  <li><a href="ch057_machine-translation-and-copyright.html">MACHINE TRANSLATION AND COPYRIGHT</a></li>

  <li class="booki-section">OPEN SOURCE TOOLS</li>

  <li><a href="ch059_anaphraseus.html">ANAPHRASEUS</a></li>

  <li><a href="ch060_apertium.html">APERTIUM</a></li>

  <li><a href="ch061_gaupol.html">GAUPOL</a></li>

  <li><a href="ch062_glossmaster.html">GLOSSMASTER</a></li>

  <li><a href="ch063_moses.html">MOSES</a></li>

  <li><a href="ch064_okapi-framework.html">OKAPI FRAMEWORK</a></li>

  <li><a href="ch065_omegat.html">OMEGAT+</a></li>

  <li><a href="ch066_omegat.html">OMEGAT</a></li>

  <li><a href="ch067_openoffice.html">OPENOFFICE</a></li>

  <li><a href="ch068_pootle.html">POOTLE</a></li>

  <li><a href="ch069_translate-toolkit.html">TRANSLATE TOOLKIT</a></li>

  <li><a href="ch070_virtaal.html">VIRTAAL</a></li>

  <li><a href="ch071_wwl.html">WWL</a></li>

  <li class="booki-section">ADVANCED TECH ISSUES</li>

  <li><a href="ch073_transliteration.html">TRANSLITERATION</a></li>

  <li><a href="ch074_machine-translation.html">MACHINE TRANSLATION</a></li>

  <li><a href="ch075_translation-memory.html">TRANSLATION MEMORY</a></li>

  <li><a href="ch076_character-encoding.html">CHARACTER ENCODING</a></li>

  <li><a href="ch077_web-fonts.html">WEB FONTS</a></li>

  <li><a href="ch078_scoring.html">SCORING</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch080_glossary.html">GLOSSARY</a></li>

  <li><a href="ch081_credits.html">CREDITS</a></li>

  <li><a href="ch082_preparingconent.html">PreparingConent</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch004_why-translate.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch004_why-translate.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

