<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>Evergreen in Action</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/evergreen-in-action/evergreen-in-action.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/evergreen-in-action/evergreen-in-action.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Evergreen in Action
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="maximizing-the-performance-of-evergreen.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="maximizing-the-performance-of-evergreen.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content"><h1>Maximizing performance
</h1>
<p>Now that you have ensured that the data for your Evergreen system is safe and recoverable in the event of a disaster, you can begin to improve its performance with confidence. While you can improve performance in many ways, this chapter will give you an overview of tuning your ejabberd XMPP server, optimizing the performance of your PostgreSQL database server, and enhancing the configuration of your OpenSRF application servers. 
</p>
<h2>Making your ejabberd XMPP server faster
</h2>
<p>When you installed Evergreen, you had to change a number of <em>ejabberd</em> configuration settings to increase its performance. However, some Linux distributions prevent <em>ejabberd</em> from using more than one CPU core at a time by default. On a production system running <em>ejabberd </em>on Debian or Ubuntu, you can enable the <em>ejabberd</em> XMPP server to use more than one CPU core at a time to process requests.
</p>
<ol><li>Edit <em>/etc/default/ejabberd</em> and change the <em>SMP</em> setting from <font face="courier new,courier,monospace">#SMP=disable</font> to <font face="courier new,courier,monospace">SMP=auto</font></li>
  <li>Restart <em>ejabberd</em>.</li>
  <li>Restart the OpenSRF services.</li>
</ol><h2>Improving the performance of your PostgreSQL database server 
</h2>
<p>Almost every Evergreen operation requires communicating with a PostgreSQL database. If the PostgreSQL database is slow, then Evergreen is slow. This section describes how to ensure that PostgreSQL is as fast as it can be, given the resources that you have available for your Evergreen system.
</p>
<h3>Install your PostgreSQL database onto its own separate, dedicated hardware
</h3>
<p>The performance of your database server depends on having access to large amounts of RAM so that it can cache frequently-accessed data in memory and avoid having to read from the disk, which is a much slower operation. Correspondingly, if other software on the system blocks PostgreSQL from accessing the hard drives for read or write operations, then the performance of Evergreen will suffer. Therefore, one of the best options for maximizing the performance of Evergreen is to dedicate a separate server to the PostgreSQL database.
</p>
<h3>Write transaction logs to a separate physical drive 
</h3>
<p>If your database server has enough disk drives, put the transaction log on a separate physical drive or volume from your data. This prevents writes to the transaction log from blocking reads and writes of your data.
</p>
<h3>Add RAM to your database server
</h3>
<p>If your server can cache the entire contents of your database in RAM, then the performance of your database will be many times faster than if it has to occasionally access data from disk. Your database size may not allow this, however; for example, an Evergreen system with 2.25 million bibliographic records currently uses over 120 GB of data. Alternately,  your performance can still benefit if you have enough RAM to keep the largest, most frequently accessed database tables cached in memory. The same Evergreen system holds approximately 20 GB of data in the core index tables.
</p>
<h3>Add CPU cores to your database server
</h3>
<p>If you have the luxury of having enough RAM to cache your database in memory, then adding CPU cores to your database server can improve performance as each CPU core can handle one concurrent query. For example, if 10 users submit search requests at the same time on your Evergreen server, a database server with only 1 CPU core will finish the first query before it can handle the next query, while a database server with 16 CPU cores would be able to process all of the queries simultaneously.
</p>
<h3>Tune your PostgreSQL database
</h3>
<p>In the interests of conserving RAM and CPU resources, the default configuration for PostgreSQL on Linux distributions tends to be quite conservative. While that configuration can support a freshly installed Evergreen system, production Evergreen systems must adjust that configuration to maximize their performance. On most Linux distributions, the PostgreSQL configuration is kept in a file named <em>postgresql.conf</em>. The <em>pgtune</em> utility can be used to generate some suggestions for your system, or you can start with the following rules of thumb:
</p>
<ul><li><em>shared_buffers</em>: This setting dedicates memory to caching frequently accessed data and blocks the operating system from accessing that memory. Set this to 1/4 of the available RAM on your server.</li>
  <li><em>max_connections</em>: This setting determines how many concurrent physical connections your database server will support. If you are not using a connection pooling solution such as pgBouncer (<a href="http://wiki.postgresql.org/wiki/PgBouncer">http://wiki.postgresql.org/wiki/PgBouncer</a>), this setting needs to be more than the total number of maximum children defined in the <em>opensrf.xml</em> configuration file for all services that connect to the database, plus extra connections for manual connections to the database and any scripts you might need to run. If this number is too low, your system may run out of available database connections and return random errors. As a caveat, each physical connection increases the maximum amount of memory consumed by PostgreSQL and can lead to running out of memory on the database server.</li>
  <li><em>default_statistics_target</em>: This setting determines how much of the data in each table PostgreSQL samples to determine how to access that data when it processes a query. If the statistics target is too low, PostgreSQL may choose a bad (slow) plan. Set this to 1000.</li>
  <li><em>effective_cache_size</em>: This setting represents how much memory is available to the operating system to cache frequently accessed data, and affects PostgreSQL's choice of access plans. Set this to approximately 60% of the available RAM on your server.</li>
</ul><h3>Running reports against a replica database
</h3>
<p>If you have the luxury of having a replica database server, you can use it for more than just disaster recovery. PostgreSQL versions 9.0 and later support reads against replica databases. In this scenario, you can point the Evergreen reporter service at your replica database for an easy performance win:
</p>
<ul><li>Reports cannot tie up your production database server with long-running queries, which can be a severe problem if a report template contains a particularly complicated set of relationships</li>
  <li>You can reduce the <em>max_connections</em> required in your database configuration, freeing up memory for caching data</li>
  <li>Your production database server can cache the data that is most necessary for searches and circulation transactions rather than the data required by arcane reports.</li>
</ul><p>To run your reports against a replica database:
</p>
<ol><li>In your <em>opensrf.xml</em> file, find the <em>&lt;reporter&gt;</em> section. When you installed Evergreen, <em>eg_db_config.pl </em>will have set the <em>&lt;database&gt;</em> and <em>&lt;state_store&gt;</em> connection information to connect to your production database server.</li>
  <li>Change the <em>&lt;database&gt;</em> connection information to connect to the replica database.</li>
  <li>Keep the <em>&lt;state_store&gt;</em> connection information pointing at your production database server. The reporter process needs to be able to write to the database to update the status of each scheduled report, and a replica database built on streaming replication is, by definition, a read-only database.
  <div class="note">Using a replication tool like Slony can result in a writable replica database, but that is outside the scope of this document.
  </div></li>
  <li>Restart the Perl OpenSRF services to load the changed configuration.</li>
  <li>Run <em>clark-kent.pl</em> for the report generator to load the new database connection information.</li>
</ol><h2>Adding another Evergreen application server
</h2>
<p>If you have the resources to add another server to your Evergreen system, consider using it as an additional Evergreen application server to run selected OpenSRF services. At the same time, you can use the divide and conquer strategy to distribute the ejabberd, Apache, and memcached services between your two Evergreen application servers. In the following scenario, we refer to the first Evergreen application server in your system as <em>app_server_1</em> and the second Evergreen application server in your system as <em>app_server_2</em>.
</p>
<h3>app_server_1 configuration
</h3>
<p>We will use <em>app_server_1</em> for the following services:
</p>
<ul><li>Apache</li>
  <li>All OpenSRF applications defined in <em>opensrf.xml</em></li>
  <li>A network share serving up <em>/openils</em> (typically via NFS)</li>
  <li>A network share serving up a writable location for MARC batch import (typically via NFS)</li>
</ul><h3>app_server_2 configuration
</h3>
<p>We will use <em>app_server_2</em> for the following services:
</p>
<ul><li>ejabberd</li>
  <li>memcached</li>
  <li>OpenSRF router</li>
  <li>All OpenSRF applications defined in <em>opensrf.xml</em> </li>
  <li>A read-only network share from <em>app_server_1</em> mounted at <em>/openils</em></li>
  <li>A writable network share from <em>app_server_1 </em>mounted at <em>/nfs-cluster</em></li>
</ul><h3>Enabling Apache to access the correct memcached server 
</h3>
<p>When the Apache server is not on the same server as your memcached<em> </em>server, you must update the Apache configuration in <em>eg_vhost.conf</em>  to set the <em>OSRFTranslatorCacheServer</em> value to the IP address (or hostname) and port on which the memcached server is available.
</p>
<h3>Starting and stopping OpenSRF services across multiple servers
</h3>
<p>Within a single cluster of Evergreen servers, you can only have one OpenSRF router process running at a time. To stop the OpenSRF Perl and C services cleanly, perform the following steps:
</p>
<ol><li><em>app_server_1</em>: Stop the OpenSRF C  services.</li>
  <li><em>app_server_2</em>: Stop the OpenSRF C  services.</li>
  <li><em>app_server_1</em>: Stop the OpenSRF Perl  services.</li>
  <li><em>app_server_2</em>: Stop the OpenSRF Perl  services.</li>
  <li><em>app_server_2</em>: Stop the OpenSRF Router.</li>
</ol><p>To start the OpenSRF services, perform the following steps:
</p>
<ol><li> <em>app_server_2</em>: Start the OpenSRF Router.</li>
  <li><em>app_server_2</em>: Start the OpenSRF Perl  services.</li>
  <li><em>app_server_1</em>: Start the OpenSRF Perl  services.</li>
  <li><em>app_server_2</em>: Start the OpenSRF C  services.</li>
  <li><em>app_server_1</em>: Start the OpenSRF C services.</li>
</ol><h3>Providing access to the network shares
</h3>
<p>The <em>/openils</em> directory on a single-server Evergreen instance is used to read and write many different files. However, on a multiple-server Evergreen instance, administrators typically turn the <em>/openils</em> directory into a network share that is writable by only one Evergreen application server and read-only for other application servers. This can complicate your configuration significantly, as a default install of OpenSRF and Evergreen attempts to write log and process ID (PID) files to the <em>/openils/var/log</em> directory.
</p>
<p>Similarly, the <em>MARC Batch Importer </em>(<em>open-ils.vandelay</em>) service needs to be able to store files temporarily in a common directory, such as <em>/nfs-cluster</em>.
</p>
<h3>Defining which services to run on each server
</h3>
<p>The stock <em>opensrf.xml</em>  configuration file includes a <em>&lt;hosts&gt;</em> section at the very end that lists:
</p>
<ul><li>One or more hostnames as XML elements; for example, <em>&lt;localhost&gt;...&lt;/localhost&gt;</em> in the stock configuration file.</li>
  <li>Each hostname contains one <em>&lt;activeapps&gt; </em>element, which in turn contains a set of one or more <em>&lt;appname&gt;</em> elements naming the OpenSRF applications that should be started on that given host.</li>
</ul></div><ul Class="menu-goes-here"><li Class="booki-section">About this book</li><li><a href="index.html">Acknowledgements</a></li>
<li><a href="overview-of-daily-and-ongoing-operation.html">Overview of daily and ongoing operation</a></li>
<li Class="booki-section">Initial set up</li><li><a href="getting-your-library-into-evergreen-initial-setup.html">Describing your organization</a></li>
<li><a href="characterizing-the-people-who-will-use-the-system.html">Describing your people</a></li>
<li><a href="sending-gentle-reminders-to-your-users-setting-up-notifications-and-triggers.html">Migrating from a legacy system</a></li>
<li><a href="getting-your-materials-into-evergreen.html">Importing materials in the staff client</a></li>
<li><a href="adding-materials-associated-with-a-budget.html">Ordering materials</a></li>
<li Class="booki-section">Finding and borrowing materials</li><li><a href="enabling-users-to-find-what-theyre-looking-for.html">Designing your catalog</a></li>
<li><a href="defining-who-gets-to-borrow-what-for-how-long-circulation-policies.html">Borrowing items: who, what, for how long</a></li>
<li><a href="defining-how-materials-are-requested-and-requests-are-fulfilled-hold-policies.html">Controlling how holds are fulfilled</a></li>
<li><a href="managing-patrons.html">Managing patron penalties and alerts</a></li>
<li><a href="sending-gentle-reminders-to-your-users.html">Sending reminders to your users</a></li>
<li Class="booki-section">Care and feeding of Evergreen</li><li><a href="care-and-feeding-of-evergreen.html">Preparing for disaster: saving your data</a></li>
<li><a href="maximizing-the-performance-of-evergreen.html">Maximizing performance</a></li>
<li><a href="keeping-evergreen-current-and-secure.html">Staying current and secure</a></li>
<li Class="booki-section">Resources</li><li><a href="getting-more-help-in-the-community.html">Getting more help in the community</a></li>
<li><a href="glossary.html">Glossary</a></li>
<li><a href="appendix-a-suggested-profile-permissions.html">Appendix A: Suggested Profile Permissions</a></li>
</ul>
  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="maximizing-the-performance-of-evergreen.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="maximizing-the-performance-of-evergreen.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

