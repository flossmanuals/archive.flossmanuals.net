<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>Font Forge</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/fontforge/fontforge.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/fontforge/fontforge.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Font Forge
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="making-sure-your-font-works-validation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="making-sure-your-font-works-validation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Making sure your font works: Validation</h1>

  <p>In a perfect world, your font would be ready to build and install on any modern computer without any special effort, but reality is messier -- particularly during the design process. Fonts can have technical errors that prevent them from working or displaying correctly. For example, curves that intersect themselves will not render correctly because they do not have a "inside" and "outside." The various font file formats also expect glyphs to adhere to certain rules that simplify placing the text on screen, and fonts that break the rules can cause unexpected problems. An example of this type of issue is that all of the points on a curve should have coordinates that are integers. Finally, there are stylistic errors that are not technically incorrect, but that you will still want to repair -- such as lines that are intended to be perfectly horizontal or vertical, but are accidentally slightly off-kilter.</p>

  <p>FontForge offers tools that you can use to locate (and, in many cases, repair) all three categories of problem. Validating your font to eliminate these errors will thus not only ensure that it can be installed and enjoyed by users, but will ensure that finished project exhibits polish.</p>

  <h2>Find Problems</h2>

  <p>The first tool is called <em>Find Problems</em>, and is found under the Element menu. You must first select one or more glyphs -- either from in the font view, the outline view, or the metrics view -- then open the Find Problems tool. The tool presents you with an assortment of potential problems in eight separate tabs.</p>

  <p><img src="_booki/fontforge/static/findproblemswindow.png" alt=""></p>

  <p>You can select which problems you are interested in looking for by checking the checkbox next to each, and in some cases providing a numeric value to check the font against. When you click the OK button, the tool will examine all of the selected glyphs, and report any problems it finds in a dialog box.</p>

  <p>The problems that the Find Problems tool can look for are sorted into these eight groups:</p>

  <ul>
    <li>Problems related to points</li>

    <li>Problems with paths and curves</li>

    <li>Problems with references</li>

    <li>Problems with hinting</li>

    <li>Problems with ATT</li>

    <li>Problems specific to CID-keyed fonts</li>

    <li>Problems with bounding boxes</li>

    <li>Miscellaneous other problems</li>
  </ul>

  <p>Not every check is necessary; some apply only to specific scripts or languages (such as those in the "CID" tab), while others apply only to specific, optional font features (such as the checks in the references tab). But you should check that your font passes those tests that examine the glyphs for required features, and several tests that look for optional but commonly-expected behavior. Several of the other tests provide feedback and guidance to you during the design process, and are worth exploring for that reason.</p>

  <h3>First things first: test for required features</h3>

  <p>In the "Points" tab, select the <em>Non-Integral Coordinates</em> test. This test makes sure that all of the points in each glyph (including both on-curve points and control points) have integer coordinates. Not every font output format requires this behaviour, but some do.</p>

  <p>In the "Paths" tab, select the options <em>Open paths</em> and <em>Check outermost paths clockwise</em>. These are both mandatory features in all fonts; the first looks for any curves that are not closed shapes, and the second makes sure that the outer curves of every glyph are traced in clockwise order. It is a very good idea to check <em>Intersecting paths</em> as well; although modern font formats can support two intersecting paths, curves that insect with themselves are not allowed. In addition, if a glyph has any self-intersecting paths then FontForge cannot perform the <em>Check outermost paths clockwise</em> test.</p>

  <p>In the "Refs" tab, select all six tests. These checks all relate to references, in which a glyph includes paths from another glyph. For example, an accented letter includes a reference to the original (unaccented) letter, plus a reference to the accent character. All of the tests in the "Refs" tab are mandatory for at least one common output format, and all are good ideas.</p>

  <p>Similarly, select all of the tests in the "ATT" tab. These tests look for missing glyph names, substitution rules that refer to non-existent glyphs, and other problems related to glyph names or OpenType features. The problems they guard against are uncommon, but all will cause the font to be considered invalid by one or more computer system, so they are worth including.</p>

  <h3>Make life easier for your users: test for good behaviour</h3>

  <p>The tests listed above will ensure that your font installs and renders correctly according to the rules set out by the various font formats, but there are a handful of others tests you should consider adding -- especially at the end of the design process -- simply because they check for common conventions followed by most modern typography.</p>

  <p>In the "Points" tab, select <em>Control points beyond spline</em>. This test will look for control points lying beyond the endpoints of the curve segment on which they reside. There is rarely a reason that a control point should lie outside of the curve, so these instances usually signify accidents. It is also a good idea to select <em>Points too far apart</em>, which will look for points that are more than 32767 units away from the next nearest point. That distance is larger than most computers can deal with internally, and a point that far away is almost certainly unintentional (for comparison, a single glyph tends to be drawn on a grid of about 1000 units), so removing such points is important.</p>

  <p>In the "Paths" tab, both the <em>Check Missing Extrema</em> and <em>More Points Than [val]</em> tests can be valuable. The first looks for points at the extrema -- that is, the uppermost point, lowest point, and leftmost and rightmost points of the glyph. Modern font formats strongly suggest that each path have a point at each of its horizontal and vertical extrema; this makes life easier when the font is rendered on screen or on the page. check will look for missing extrema points. The second test is a sanity check on the number of points within any one glyph. FontForge's default value for this check is 1,500 points, which is the value suggested by the PostScript documentation, and it is good enough for almost all fonts.</p>

  <p>As its name suggests, the "Random" tab lists miscellaneous tests that do not fit under the other categories. Of these, the final three are valuable: <em>Check Multiple Unicode</em>, <em>Check Multiple Names</em>, and <em>Check Unicode/Name mismatch</em>. They look for metadata errors in the mapping between glyph names and Unicode slots.</p>

  <h3>Help yourself: run tests that can aid design</h3>

  <p>Many of the other tests in the Find Problems tool can be useful to find and locate inconsistencies in your collection of glyphs; things that are not wrong or invalid, but that you as a designer will want to polish. For example, the <em>Y near standard heights</em> test in the "Points" tab compares glyphs to a set of useful vertical measurements: the baseline, the height of the "x" glyph, the lowest point of the descender on the letter "p", and so on. In a consistent typeface, most letters will adhere to at least a couple of these standard measurements, so the odds are that a glyph that is nowhere near any of them needs a lot of work.</p>

  <p>The <em>Edges near horizontal/vertical/italic</em> test in the "Paths" tab looks for line segments that are almost exactly horizontal, vertical, or at the font's italic angle. Making your almost-vertical lines perfectly vertical means that shapes will render sharply when the font is used, and this test is a reliable way to track down the not-quite-right segments that might be hard to spot with the unaided eye.</p>

  <p>You can use other tests to locate on-curve points that are too close to each other to be meaningful, to compare the side bearings of similarly-shaped glyphs, and to perform a range of other tests that reveal when you have glyphs with oddities. Part of the refinement process is taking your initial designs and making them more precise; like other aspects of font design, this is an iterative task, so using the built-in tools reduces some of the repetition.</p>

  <h2>Validate font</h2>

  <p>FontForge's other validation tool is the whole-font validator, which runs a battery of tests and checks on the entire font. Because the validator is used to examine a complete font, you can only start it up from the font view window; you will find it in the Element menu, under the Validation submenu. The validator is deigned to run just those tests that examine the font for technical correctness -- essentially the tests described in the "test for required features" section above. But it does execute the tests against the entire font, and it does so far more rapidly than you can step through the process yourself using the Find problems tool.</p>

  <p><img src="_booki/fontforge/static/validator-integral-question.png" alt=""></p>

  <p>The first time you run the validator during a particular editing session, it will pop up a dialog box asking you whether or not it should flag non-integer point coordinates to be an error. The safe answer is to choose "Report as an error," since sticking with integral coordinates is good design practice.&nbsp; When the validator completes its scan of the font (which will be mere seconds later), it will open up a new dialog box named Validation of <em>Whatever Your Font Name Is</em>. This window will list every problem the validator found, presented in a list sorted by glyph.</p>

  <p><img src="_booki/fontforge/static/valiator-output.png" alt=""></p>

  <p>But this window is not merely a list of errors: you can double-click on each item in the list, and FontForge will jump to the relevant glyph and highlight the exact problem, complete with a text explanation in its own window. You can then fix the problem in the glyph editor, and the associated error item will immediately disappear from the validator's error list. In many cases, the error will be something FontForge can automatically repair; in those cases the explanation window will have a "Fix" button at the bottom. You can click it and perform the repair without additional effort.</p>

  <p><img src="_booki/fontforge/static/validator-fix-problem.png" alt=""></p>

  <p>For some problems, there is no automatic fix, but seeing the issue on-screen will help you fix it immediately. For example, a self-intersecting curve has a specific place where the path crosses over itself -- it may have been too small for you to notice at a glance, but zooming in will allow you to reshape the path and eliminate the problem.</p>

  <p>For other problems, there may not be one specific point at which the error is located. For example, if a curve is traced in the wrong direction (that is, counterclockwise when it should be clockwise), the entire curve is affected. In those instances where FontForge cannot automatically fix the problem and there is no specific point on the glyph for the validator to highlight, you may have to hunt around in order to manually correct the problem.</p>

  <p>Finally, there are some tests performed by the validator that might not be a problem from the final output format you have in mind -- for example, the non-integral coordinates test mentioned earlier.&nbsp; In those cases, you can click on the "ignore this problem in the future" checkbox in the error explanation window, and suppress that particular error message in future validation runs.</p>

  <h2>Fix problems as you edit</h2>

  <p>Most of the errors that the Find problem tool and the whole font validator look for can be corrected during the editing process, so do not feel any need to defer troubleshooting while you work. For example, View &gt; Show submenu has options that highlight problem areas during editing; the Element menu hold commands like <em>Add Extrema</em> that will add the extrema points expected in most output file formats, and checkboxes to indicate whether the selected path is oriented in the clockwise or counterclockwise direction. If you flip a shape (horizontally or vertically) in the glyph editor, you will notice that its direction is automatically reversed as well. If you click on the <em>Correct Direction</em> command in the Element menu, FontForge will fix the clockwise/counterclockwise orientation immediately. Getting in the habit of doing small fixes like this as you work will save you a bit of time during the validation stage later.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Before you begin</li>

  <li><a href="index.html">Introduction</a></li>

  <li><a href="what-is-a-font.html">What is a font?</a></li>

  <li><a href="trusting-your-eyes.html">Trusting your eyes</a></li>

  <li><a href="planning-your-project.html">Planning your project</a></li>

  <li class="booki-section">Getting to know FontForge</li>

  <li><a href="installing-fontforge.html">Installing FontForge</a></li>

  <li><a href="using-the-fontforge-drawing-tools.html">Using the FontForge drawing tools</a></li>

  <li><a href="drawing-with-spiro.html">Drawing with Spiro</a></li>

  <li class="booki-section">Workflow</li>

  <li><a href="creating-o-and-n.html">Creating 'o' and 'n'</a></li>

  <li><a href="word-space.html">Word space</a></li>

  <li><a href="creating-your-types-dna.html">Creating your type's DNA</a></li>

  <li><a href="capital-letters.html">Capital letters</a></li>

  <li><a href="line-spacing.html">Line spacing</a></li>

  <li><a href="punctuation-and-symbols.html">Punctuation and symbols</a></li>

  <li><a href="completing-the-lower-case.html">Completing the lower case</a></li>

  <li><a href="numerals.html">Numerals</a></li>

  <li><a href="bold-and-other-weights.html">Bold and other weights</a></li>

  <li><a href="italic.html">Italic</a></li>

  <li><a href="spacing-metrics-and-kerning.html">Spacing, metrics, and kerning</a></li>

  <li><a href="making-sure-your-font-works-validation.html">Making sure your font works: validation</a></li>

  <li><a href="the-final-output-generating-font-files.html">The final output: generating font files</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="when-things-go-wrong-with-fontforge-itself.html">When things go wrong with FontForge itself</a></li>

  <li><a href="glossary.html">Glossary</a></li>

  <li><a href="further-reading.html">Further reading</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="making-sure-your-font-works-validation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="making-sure-your-font-works-validation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

