<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Ardour</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/ardour/ardour.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/ardour/ardour.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Ardour
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch024_understanding-routing.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch024_understanding-routing.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Understanding Routing</h1>

  <p>Routing an audio signal is sending it from somewhere to somewhere else. In addition to getting audio signals to and from Ardour, routing plays an important part inside Ardour itself. Examples of using routing inside Ardour include routing audio from Tracks to the Master Bus or to other Busses, creating 'sends', routing the outputs from Busses to the Master Bus, etc. (see chapter on <strong>Creating a Track</strong> for an explanation of Tracks and Busses). All routing, both internal and external to Ardour, is handled by JACK.</p>

  <h3>Routing in Ardour</h3>

  <p>The standard routing of inputs, tracks and busses in Ardour is determined when a new Session is created in the Advanced Options of the New Session dialog box (see <strong>Starting a Session</strong> chapter). By default, the routing is as follows:</p>

  <ul>
    <li>The audio device inputs are routed to the Track inputs.</li>

    <li>All outputs from Tracks and Busses are routed to the master bus inputs.</li>

    <li>The Master Bus outputs are routed to the audio device outputs.</li>
  </ul>

  <p>Note that when a new Bus is created, nothing is routed to its input.</p>

  <p>This routing setup makes sense for sessions containing only Tracks, but to make use of any Busses (other than the Master Bus) or to get creative with the paths of the audio signals inside Ardour, we need to be able to change the routing. This can be done in JACK's Connections Manager window, but it is much more convenient to do this from within Ardour.</p>

  <p>We can edit the routing of each track or bus by clicking on the corresponding <em>Input</em> or <em>Output</em> buttons at the top and bottom of their Mixer Strip. Hovering the cursor over a Routing button will make a tool-tip appear showing the current routing.</p>

  <p><img alt="a Routing-button tooltip" src="_booki/ardour/static/Ardour-UnderstandingRouting-RoutingButton_Tooltip_1-en.png" height="127" width="241"></p>

  <p>When you click the Routing button, a pop-up menu appears which allows you to edit the routing, disconnect it altogether, or select a standard routing scheme from a short list.</p>

  <p><img alt="Routing-button menu" src="_booki/ardour/static/Ardour-UnderstandingRouting-RoutingButton_Menu_1-en.png" height="136" width="240"></p>

  <p>When you select 'Edit', a <strong>Routing Editor</strong> window appears.</p>

  <p><img title="RoutingEditor_1_1" alt="RoutingEditor_1_1" src="_booki/ardour/static/Ardour-UnderstandingRouting-RoutingEditor_1_1-en.png" height="301" width="329"></p>

  <p>This window is functionally very similar to JACK's Connections Manager. It just looks and works a bit differently. It is important to realize that any routings that you make or disconnect from within Ardour are in fact JACK routings.</p>

  <p>The title bar of the Routing Editor shows what is being routed (in this case, the inputs of the track called BackBeat). The left-hand pane ("<em>Inputs"</em>) shows how many input ports this track currently has (two, in this case: <em>"in 1</em>" and "<em>in 2</em>"), and from which ports these inputs are receiving signals (in the example, "<em>system:capture_1</em>" and <em>"system:capture_2</em>"). Clicking on an existing connection will disconnect it.</p>

  <p>The right-hand pane ("<em>Available connections</em>") shows the available ports from within Ardour, displayed under the <em>"ardour"</em> tab, and from outside Ardour, displayed under the <em>"system"</em> tab. All these are available for connecting to the input ports of the track we are currently editing.</p>

  <p>You will find a similar window for routing the output of a Track or Bus by clicking on the <em>"Output"</em> button at the bottom of each Mixer Strip.</p>

  <p>In this example session, there are two guitar Tracks and one unused Bus called Guitar Bus. Suppose you want to send the output from the two guitar Tracks to the Guitar Bus instead of the Master Bus. This can be useful to control the volume of both guitars with just one Fader (in this case the Guitar Bus fader). Then the output of the Guitar Bus, which is the sum of the two guitars, goes directly to the Master Bus.</p>

  <p>Here is how to edit the output routing of one guitar Track in order to send its outputs to the Bus inputs.</p>

  <p><img title="RoutingEditor_GuitarSolo_to_Master_1" alt="RoutingEditor_GuitarSolo_to_Master_1" src="_booki/ardour/static/Ardour-UnderstandingRouting-RoutingEditor_GuitarSolo_to_Master_1-en.png" height="301" width="328"></p>

  <p>We disconnect the outputs of the GuitarSolo Track from the <em>"ardour:master/in</em>" ports by clicking on them. Then we click on the desired "<em>ardour:Guitar Bus/in"</em> ports in order to connect them. Then we do the same for the 2nd Guitar Track. This is how the connection window of each guitar track will look like afterwards:<br></p>

  <p><img title="RoutingEditor_GuitarSolo_to_GuitarBus_1" alt="RoutingEditor_GuitarSolo_to_GuitarBus_1" src="_booki/ardour/static/Ardour-UnderstandingRouting-RoutingEditor_GuitarSolo_to_GuitarBus_1-en.png" height="300" width="329"></p>

  <p>Now both guitar tracks are routed to the Guitar Bus, and no longer directly to the Master Bus. We then make sure that the Guitar Bus is, by its turn, routed to the Master Bus (the output routing of a Bus is edited in the same way as for a Track), so that we can still hear the sound from both guitar Tracks. Now we can control the volume of both guitar Tracks together by changing the Fader of the Guitar Bus. What's more, we can now add Plugins to the Guitar Bus to process the sound of both guitar Tracks together.</p>

  <h2>Continuing</h2>

  <p>In this chapter, we covered how to manage Routing inside Ardour, or between Ardour and the sound card. However, one of the strengths of using the JACK system is that it can also manage connections between applications on the same computer. To gain a better understanding of how this works, please continue to the chapter <strong>Routing Between JACK Applications</strong>. If you would prefer to work only with Ardour, then skip ahead to the section on <strong>Editing Sessions</strong>.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li><a href="ch003_what-is-digital-audio.html">WHAT IS DIGITAL AUDIO?</a></li>

  <li class="booki-section">INSTALLATION OS X</li>

  <li><a href="ch005_installing-jackosx.html">INSTALLING JACKOSX</a></li>

  <li><a href="ch006_installing-ardour.html">INSTALLING ARDOUR</a></li>

  <li><a href="ch007_powerbooks-macbooks.html">POWERBOOKS &amp; MACBOOKS</a></li>

  <li class="booki-section">INSTALLATION LINUX</li>

  <li><a href="ch009_ubuntu.html">UBUNTU</a></li>

  <li><a href="ch010_other-linux.html">OTHER LINUX</a></li>

  <li><a href="ch011_advanced-linux-configuration.html">ADVANCED LINUX CONFIGURATION</a></li>

  <li class="booki-section">GETTING STARTED</li>

  <li><a href="ch013_starting-jackosx.html">STARTING JACKOSX</a></li>

  <li><a href="ch014_starting-ardour-on-osx.html">STARTING ARDOUR ON OSX</a></li>

  <li><a href="ch015_starting-jack-on-ubuntu.html">STARTING JACK ON UBUNTU</a></li>

  <li><a href="ch016_starting-ardour-on-ubuntu.html">STARTING ARDOUR ON UBUNTU</a></li>

  <li><a href="ch017_an-overview-of-the-interface.html">AN OVERVIEW OF THE INTERFACE</a></li>

  <li class="booki-section">STARTING SESSIONS</li>

  <li><a href="ch019_starting-a-session.html">STARTING A SESSION</a></li>

  <li><a href="ch020_setting-up-the-timeline.html">SETTING UP THE TIMELINE</a></li>

  <li><a href="ch021_creating-a-track-or-bus.html">CREATING A TRACK OR BUS</a></li>

  <li><a href="ch022_importing-audio.html">IMPORTING AUDIO</a></li>

  <li><a href="ch023_recording-audio.html">RECORDING AUDIO</a></li>

  <li><a href="ch024_understanding-routing.html">UNDERSTANDING ROUTING</a></li>

  <li><a href="ch025_routing-between-jack-applications.html">ROUTING BETWEEN JACK APPLICATIONS</a></li>

  <li class="booki-section">EDITING SESSIONS</li>

  <li><a href="ch027_arranging-tracks.html">ARRANGING TRACKS</a></li>

  <li><a href="ch028_setting-up-the-meter.html">SETTING UP THE METER</a></li>

  <li><a href="ch029_using-ranges.html">USING RANGES</a></li>

  <li><a href="ch030_working-with-regions.html">WORKING WITH REGIONS</a></li>

  <li><a href="ch031_further-region-operations.html">FURTHER REGION OPERATIONS</a></li>

  <li><a href="ch032_changing-edit-modes.html">CHANGING EDIT MODES</a></li>

  <li><a href="ch033_creating-looped-sections.html">CREATING LOOPED SECTIONS</a></li>

  <li><a href="ch034_stretching-shrinking-regions.html">STRETCHING / SHRINKING REGIONS</a></li>

  <li><a href="ch035_adjusting-the-rhythm-of-a-region.html">ADJUSTING THE RHYTHM OF A REGION</a></li>

  <li class="booki-section">MIXING SESSIONS</li>

  <li><a href="ch037_using-the-mixer-strip.html">USING THE MIXER STRIP</a></li>

  <li><a href="ch038_mixing-levels.html">MIXING LEVELS</a></li>

  <li><a href="ch039_panning.html">PANNING</a></li>

  <li><a href="ch040_using-plugins.html">USING PLUGINS</a></li>

  <li><a href="ch041_using-sends.html">USING SENDS</a></li>

  <li><a href="ch042_dynamics.html">DYNAMICS</a></li>

  <li><a href="ch043_equalizing.html">EQUALIZING</a></li>

  <li><a href="ch044_using-automation.html">USING AUTOMATION</a></li>

  <li><a href="ch045_moving-automation.html">MOVING AUTOMATION</a></li>

  <li class="booki-section">EXPORTING SESSIONS</li>

  <li><a href="ch047_exporting-a-region.html">EXPORTING A REGION</a></li>

  <li><a href="ch048_exporting-a-range.html">EXPORTING A RANGE</a></li>

  <li><a href="ch049_exporting-a-session.html">EXPORTING A SESSION</a></li>

  <li class="booki-section">SAVING SESSIONS</li>

  <li><a href="ch051_saving-a-session.html">SAVING A SESSION</a></li>

  <li><a href="ch052_saving-a-snapshot.html">SAVING A SNAPSHOT</a></li>

  <li><a href="ch053_saving-a-template.html">SAVING A TEMPLATE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch055_further-help.html">FURTHER HELP</a></li>

  <li><a href="ch056_glossary.html">GLOSSARY</a></li>

  <li><a href="ch057_links.html">LINKS</a></li>

  <li><a href="ch058_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch024_understanding-routing.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch024_understanding-routing.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

