<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Digital Foundations</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/digital-foundations/digital-foundations.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/digital-foundations/digital-foundations.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Digital Foundations
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch021_hello-world.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch021_hello-world.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Hello World</h1>

  <p>Every web page is code made visual by the web browser. Open any page in a web browser and use the View menu (View &gt; Page Source) to see the source code used to create that page. This code tells the browser how to render the layout, images, links, and interactivity of the page. Whereas in the previous applications we have clicked and dragged our way to good design, with web pages we design interface elements and then write the code that describes what we want our interface to look like in the browser.</p>

  <p>While web sites are usually programmed to hide the presence of code in a seamless graphic facade, experimental artists often revel in exposing it. The artist group Jodi (Joan Heemskerk and Dirk Paesmans) works extensively with the materiality of code. In the early moments of the dot-com boom, when corporations began to stake out an aesthetic and functional claim online (1995, 1996), Jodi hosted a series of confrontational pages. Theirs responded to the corporate attempt to professionalize the aesthetics of online media and conceal the presence of code. The site wwwwwwwww.jodi.org looks like disorganized text in the browser, and viewing the source reveals a rough diagram of a nuclear bomb made with the letters of the source code.</p>

  <table class="infobox" bgcolor="#F8F8F8">
    <tbody>
      <tr>
        <td>Note: Code is language. It can be thought of as a material that hides its own materiality.</td>
      </tr>
    </tbody>
  </table>

  <p>Jodi.org reversed the order of the medium. Where design elements are organized to create visual messages in the browser, Jodi appears random and frantic. In the source code Jodi reveals the message of the work in a simple illustration. Jodi throws a bomb at clean design. Web pages are written in Hypertext Markup Language (HTML), which ignores extra "white space" characters. Jodi took advantage of this feature to make a brilliant visual and conceptual argument for breaking the rules of web design.</p>

  <p><img title="jodi" alt="jodi" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-jodi-en.png"><a href="http://wwwwwwwww.jodi.org"></a></p>

  <p><em><a href="http://wwwwwwwww.jodi.org">wwwwwwwww.jodi.org,</a>&nbsp; Jodi (Joan Heemskerk and Dirk Paesmans), 1995, website and HTML source code, used by permission of the artists.</em></p>

  <p>In the early years of the World Wide Web, bandwidth was limited, so a large amount of communication took place on text-only listservs, chat rooms, multi-user dungeons (MUDs), and bulletin board servers (BBSs). Web pages were designed with few images because they took too long to download. Because of these limits on the use of high-resolution images, people found creative ways of drawing with text.</p>

  <p>Loosely defined, ASCII art is art made by arranging the 128 glyphs that are part of the American Standard Code for Information Interchange, usually in the form of figurative drawings. The shapes and densities of the characters are treated purely as formal elements to construct line, form, and shading. As an example, Heath Bunting's 1998 portrait of Natalie Bookchin, an early net artist, can be seen here: <a href="http://www.irational.org/heath/imaging_natalie/" class="external free" title="http://www.irational.org/heath/imaging_natalie/" rel="nofollow">http://www.irational.org/heath/imaging_natalie/</a></p>

  <p><img title="nathalie_bunting_1" alt="nathalie_bunting_1" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-nathalie_bunting_1-en.png" height="640" width="600"></p>

  <p><em><a href="http://www.irational.org/heath/imaging_natalie/" class="external free" title="http://www.irational.org/heath/imaging_natalie/" rel="nofollow">http://www.irational.org/heath/imaging_natalie/</a>&nbsp; Portrait of Natalie Bookchin, Heath Bunting, 1998.</em></p>

  <h2>Exercise 1: Hello World!</h2>

  <p>With the materiality of code in mind, we will construct our first web page.</p>

  <p>For this chapter, create a folder on your desktop or hard drive and commit to saving everything that you make from or related to this chapter into that one folder. Do not make sub-folders. Do not make more than one folder. Our folder is named <em>chapterfifteen</em>.<br></p>

  <p>1. Open a text editing application, such as TextEdit, Text Wrangler, TextMate, Smultron, or BBEdit. On a PC, use Notepad or WordPad. In the following examples we used the Ubuntu Operating System with Gedit installed, so our screen shots show the Ubuntu and Gedit interfaces. The first, very important step when writing HTML in any text editing application is to make sure that you are working in plain text format. Fortunately Gedit works in plain text by default, so for the following examples no extra setup is necessary.</p>

  <p>2. Close any open documents in Gedit and click File &gt; New to start a new document.</p>

  <p>3. Type the opening and closing HTML tags.</p>

  <p><img title="15_01" alt="15_01" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_01-en.png" height="216" width="208"></p>

  <p>The &lt;html&gt; opening tag tells the browser that we are writing in Hypertext Markup Language (HTML), and the same tag with a slash, as shown, tells the browser to stop rendering HTML. All HTML tags follow the same scheme. Other markup languages that a browser understands will have similar rules, with some variation.</p>

  <p>4. Position your cursor after the opening HTML tag. Press the Enter key and then the Tab key. Tabbing is used to add visible structure to the code so that it is easier to read. (Tabs and other extra white space are ignored by the browser and do not affect the display or functionality of the code.) Then add the opening and closing body tags. All media placed inside the body tags (and only that media) will be displayed on the web page when it is viewed in a browser.</p>

  <p><img title="15_02" alt="15_02" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_02-en.png" height="216" width="208">&nbsp;</p>

  <p>5. Inside the body tags type:</p>

  <p><strong>&lt;p&gt;Hello World!&lt;/p&gt;</strong></p>

  <p><img title="helloworld2" alt="helloworld2" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-helloworld2-en.png" height="226" width="492">&nbsp;</p>

  <p>6. Save the file as helloworld.html. Make sure that you add the .html extension to the end of the file name. The file extension is important as it communicates to the browser that this is an HTML file.&nbsp;</p>

  <p><img title="15_04" alt="15_04" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_04-en.png" height="71" width="405">&nbsp;</p>

  <p><em>Watch Out: When saving files for the web, do not use capital letters, spaces, or reserved characters. Only use a-z, 1-9, -, and _. Stay away from reserved characters like ( )&nbsp;! + @ &amp; =&nbsp;?</em></p>

  <p>7. Open a web browser, click File &gt; Open and find the helloworld.html file. We opened FireFox, clicked File &gt; Open File... and browsed to Desktop &gt; chapterfifteen&gt; helloworld.html. Notice that the message "Hello World!" is the only part of the code that is displayed in the browser, because it is the only code inside the body tags of our HTML file.</p>

  <p><img title="15_05" alt="15_05" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_05-en.png" height="132" width="576"></p>

  <p>In the browser, choose View &gt; Page Source to see all of the code.&nbsp;</p>

  <p><img title="15_06" alt="15_06" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_06-en.png" height="134" width="497"><br></p>

  <p>In Exercise 2 we will be returning to this file in the web browser, so leave it open if you are going to work on that exercise next.</p>

  <p><em>Watch Out: Double-clicking on the HTML file in your folder may not open the file in a web browser. If your intent is to view the file in a web browser, be sure to launch the browser and choose File &gt; Open File, or drag the HTML file to the browser.</em></p>

  <h2>Exercise 2: Hello KompoZer</h2>

  <p>In the previous exercise, we wrote the Hello World! code using a text editing program. A text editor is the most basic application required for writing code, but hand-typing lots of code can be very time-consuming. Most artists and designers prefer to use a WYSIWYG ("What You See Is What You Get") application such as the FLOSS program KompoZer to develop code. KompoZer actually writes the code for you, which makes creating the HTML file much easier.</p>

  <p>In this exercise, we will modify and replace the file we just made, meaning we will use the Save command rather than Save As. At the end of this exercise you should still have only one file in your folder.</p>

  <p>1. Open KompoZer from the Applications menu. In KompoZer choose File &gt; Open and open the helloworld.html file. We will click File &gt; Open then Desktop &gt; chapterfifteen &gt; helloworld.html.</p><img title="kompozer" alt="kompozer" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-kompozer-en.png" height="349" width="600">

  <p>2. Click the Source tab at the bottom of the screen, then click the Preview button. Preview displays the result of the code as it would appear in a browser, and Source displays the code itself. Another way to think about this is that the Source view is the set of instructions to the browser and the Preview view is what the browser displays when it follows those instructions.</p>

  <p><img title="15_08" alt="15_08" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_08-en.png" height="344" width="600">&nbsp;</p>

  <p>3. Click the Source tab. Only the code is displayed. Note that your HTML commands are color-coded, with tags displayed in purple and text in black.</p>

  <p>4. Click Preview (or you can also try the Normal view which is the view you generally use for creating HTML). Now we can see the result of the code, or what will be displayed in a browser when you load your file.</p>

  <p>5. In Normal or Preview view, place your cursor at the end of "Hello World!" Press the Return key and type "This is my first HTML page." Save the document using File &gt; Save. Switch to Source view and note how KompoZer has changed the code for you. A new line (with the paragraph tag &lt;p&gt;) has been added to the code to format the new paragraph we requested when we pressed the Return key in Normal or Preview view.</p>

  <p><em>Note: KompoZer is a WYSIWYG editor because you can change the code by using KompoZer pull down menus and buttons in Normal view. It should be called a WYSIWYGMOTT, or "What You See Is What You Get Most Of The Time." The Normal/Preview view is going to be about 95% accurate. When designing for the web, always preview your work in the browser to confirm that the HTML code is correct.</em></p>

  <p><img title="15_09" alt="15_09" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_09-en.png" height="458" width="561">&nbsp;</p>

  <p>&nbsp;<img title="15_10" alt="15_10" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_10-en.png" height="251" width="557"></p>

  <p>The paragraph tag is opened and closed around the new line of text. This is an example of nesting. Nesting is when a set of open and closed tags are placed inside of another open and closed tag. The relationship between where each set opens and closes is important. One set of tags is structured around the other so that they never overlap.</p>

  <p><img title="html_diagram" alt="html_diagram" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-resized_560x131_html_diagram-en.jpg" height="131" width="560">&nbsp;</p>

  <p>6. Return to the browser where you viewed helloworld.html in Exercise 1. Refresh the browser page (Command+R in most browsers) and observe the changes we made to the file in KompoZer. If the file was closed, use File &gt; Open and select the helloworld.html file. In this exercise we modified the helloworld.html file. We did not save a new file, but replaced the existing file. The browser displays changes to the file when changes have been made to the original file (File &gt; Save) in KompoZer and the page is reloaded in the browser .</p>

  <p><img title="15_12" alt="15_12" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_12-en.png" height="155" width="565"></p>

  <p>7. Go back to KompoZer and type a new paragraph in Normal view: "Hello World is the first exercise you learn in any code or markup language." Save the file and refresh the browser to see the new text display on the web page.&nbsp;</p>

  <p><img title="15_13" alt="15_13" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_13-en.png" height="164" width="600"></p>

  <h2>Exercise 3: Hyperlinks</h2>

  <p>Hyperlinks, or links as they are commonly called, are a one-click route from one HTML file to another. Links are the simplest form of interactivity on the web.</p>

  <p>1. Open a new web browser tab or window and search for the phrase "Hello World! Collection." Click on the Hello World Collection which is at this URL at the time of writing: <a href="http://www.roesler-ac.de/wolfram/hello.htm" class="external free" title="http://www.roesler-ac.de/wolfram/hello.htm" rel="nofollow">http://www.roesler-ac.de/wolfram/hello.htm</a>.</p>

  <p>This collection of Hello World! examples started in 1994, although "Hello World!" first appeared in a programming book in 1978. Copy the site's URL from the browser's address bar and return to the file we modified in Exercise 2 in KompoZer.</p>

  <p>2. In Normal view, select the text "the first exercise you learn in any code or markup language" by highlighting it with your mouse. If it isn’t already active, click on the Link button on the center of the toolbar. Paste the URL you just copied into the field labeled Link Location.</p>

  <p><img title="15_14" alt="15_14" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_14-en.png" height="558" width="600">&nbsp;</p>

  <p>3. Press the Return key and it will change the selected text from body copy to a hyperlink. This is important: save the file. If the file is not saved, the updated work will not load in the browser.&nbsp;</p>

  <p>4. Go to the browser and refresh the helloworld.html page. The link should function in the browser. Return to KompoZer and inspect the code that was created.&nbsp;</p>

  <p>In the code, the letter "a" stands for anchor and the string "href" tells the browser that this is a hypertext reference, while the URL surrounded by quote marks is the value that tells the browser where the hypertext reference points. The anchor tag can be read as "close a" or &lt;/a&gt;. Notice that the tag starts just before the word "the" and closes just after the period at the end of the sentence. This part of the sentence becomes the link.</p>

  <p>.<img title="15_15" alt="15_15" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_15-en.png" height="323" width="532"></p>

  <p>5. Move the closing anchor tag to just after the word "learn." When the web page is viewed in a browser, the link will be shortened to include only the text that is between the opening and closing tags.</p>

  <p><img title="15_16" alt="15_16" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_16-en.png" height="295" width="534"></p>

  <h2>Exercise 4: Images</h2>

  <p>To add an image to the HTML page, you need to have an image prepared for online viewing. For this exercise, we will search for an image on Flickr. Flickr is an online photo sharing website where viewers can search for images by tags. In this exercise we will use an image that has been placed in the public domain with a Creative Commons license.</p>

  <p>1. Go to Flickr.com. If you are not signed in, the search field is vertically centered on the right half of the page. If you are signed in, use the search field in the top left corner. Search for "Hello World."</p>

  <p>2. Click on the Advanced Search button on the results page.</p>

  <p><img title="flickr_search1" alt="flickr_search1" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-flickr_search1-en.png" height="194" width="600"></p>

  <p>3. Scroll down and check the box next to "only show Creative Commons licensed photos." Creative Commons is a licensing schema that presents an alternative to the standard US copyright laws. All photographs uploaded to Flickr are automatically copyrighted, preventing other people from using or building upon them. Creative Commons allows you to post your work online and license it openly, allowing others to use it in their work. Online culture is a culture of sharing, remixing, and collaboration. Creative Commons licensing enables and empowers this culture. Reference Chapter 2 for a more detailed synopsis of copyright laws, fair use, and alternative licensing with Creative Commons.</p>

  <p><img title="flickr_search_advanced" alt="flickr_search_advanced" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-flickr_search_advanced-en.png" height="282" width="372"></p>

  <p>4. Click on an image that you found in the Creative Commons "Hello World" Flickr search. Now the image appears on the Flickr page maintained by its author.</p>

  <p><img title="15_17" alt="15_17" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_17-en.png" height="255" width="566"></p>

  <p>5. Contol-click or right-click on the image and select Copy Image Address or Copy Image Location. This copies the URL. The URL is the path to the location where the image file is saved on the server. The next time you use Edit &gt; Paste in any text field, this address will be pasted. We will use this in KompoZer in the next step.</p>

  <p><img title="copy_image_location" alt="copy_image_location" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-copy_image_location-en.png" height="256" width="376"></p>

  <p>6. Go back to KompoZer and view the helloworld.html file in Source view. Type a new paragraph tag. Note that KompoZer automatically closes the tag. Now add an image tag like this, &lt;img src="../url" /&gt;. Replace the letters "url" with the URL that you copied from flickr.com by pasting it into that area. Leave the quote marks in the tag, and the space before the closing bracket.&nbsp;</p>

  <p><img title="15_21" alt="15_21" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_21-en.png" height="260" width="600"></p>

  <p><img title="15_22" alt="15_22" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_22-en.png" height="445" width="500"></p>

  <p>The image tag closes itself. The combination of space, slash, and bracket at the end of the tag signifies a closing tag.</p>

  <p>7. Save the file and refresh the page in your browser. The image appears on the page, with a paragraph break between it and the link we made in Exercise 3.</p>

  <h2>Exercise 5: Formatting type</h2>

  <p>If you have printed documents from OpenOffice.org to a laser printer, you have used a markup language. The difference between working in KompoZer and printing from OpenOffice.org is that you are aware that you are creating the markup language in the HTML code. In essence, the B button in almost any word processor (for example, Microsoft Word or OpenOffice.org) is a user interface component that marks up the selected type so it displays and prints as bold. The printer reads the file sent by the program and formats the typography. In KompoZer, you use the interface to add formatting, and you see the code that is being written for the browser. Thinking of the browser as a printer (and the web as the page to be printed) can be helpful for understanding markup language. You will discover that it is not always the perfect analogy, as user interaction varies from the printed page to the web browser. The media environment always affects the audience.</p>

  <p>1. The h1 tag will transform the words "Hello World!" into a headline. Insert the tag as demonstrated.&nbsp;</p>

  <p><img title="15_23_use" alt="15_23_use" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_23_use-en.png" height="94" width="275"></p>

  <p>2. Formatting type in KompoZer is like formatting type in other text processing programs. Bold and italic styles are one click away, but notice that when you press the I or B button tags are added to the HTML so that the styles will be displayed properly in a browser.</p>

  <p>Click on Normal view. Make the word "first" bold by selecting it and clicking the B button in the Toolbar. KompoZer will surround the word with the span tag and bold style so it will be displayed as bold when your page is viewed in a browser.</p>

  <p>3. Select the word "any" and click the I button in the Toolbar. KompoZer uses the span tag and italic style to italicize the word.</p>

  <p><img title="15_24" alt="15_24" src="_booki/digital-foundations/static/DigitalFoundations-chapterfifteen-15_24-en.png" height="515" width="585"></p>

  <p><em>In this final image, you can see the code used to create the page in the browser.</em></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li class="booki-section">INKSCAPE</li>

  <li><a href="ch003_the-metaphor-of-graphics-applications.html">THE METAPHOR OF GRAPHICS APPLICATIONS</a></li>

  <li><a href="ch004_symmetry.html">SYMMETRY</a></li>

  <li><a href="ch005_type-on-the-grid.html">TYPE ON THE GRID</a></li>

  <li><a href="ch006_color-theory.html">COLOR THEORY</a></li>

  <li><a href="ch007_line-art-flat-graphics.html">LINE ART &amp; FLAT GRAPHICS</a></li>

  <li class="booki-section">FIREFOX</li>

  <li><a href="ch009_searching-and-sampling.html">SEARCHING AND SAMPLING</a></li>

  <li class="booki-section">GIMP</li>

  <li><a href="ch011_scanning.html">SCANNING</a></li>

  <li><a href="ch012_tonal-scale.html">TONAL SCALE</a></li>

  <li><a href="ch013_layering-and-collage.html">LAYERING AND COLLAGE</a></li>

  <li><a href="ch014_repetition-and-cloning.html">REPETITION AND CLONING</a></li>

  <li><a href="ch015_non-destructive-editing.html">NON-DESTRUCTIVE EDITING</a></li>

  <li><a href="ch016_graphics-for-the-web.html">GRAPHICS FOR THE WEB</a></li>

  <li class="booki-section">SCRIBUS</li>

  <li><a href="ch018_multiples-creating-of-unity.html">MULTIPLES: CREATING OF UNITY</a></li>

  <li><a href="ch019_multiples-creating-tension.html">MULTIPLES: CREATING TENSION</a></li>

  <li class="booki-section">KOMPOZER</li>

  <li><a href="ch021_hello-world.html">HELLO WORLD</a></li>

  <li><a href="ch022_files-and-servers.html">FILES AND SERVERS</a></li>

  <li><a href="ch023_separating-form-and-content.html">SEPARATING FORM AND CONTENT</a></li>

  <li class="booki-section">PROCESSING</li>

  <li><a href="ch025_drawing-with-code.html">DRAWING WITH CODE</a></li>

  <li><a href="ch026_motion-and-framerate.html">MOTION AND FRAMERATE</a></li>

  <li><a href="ch027_basic-interaction.html">BASIC INTERACTION</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch029_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch021_hello-world.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch021_hello-world.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

