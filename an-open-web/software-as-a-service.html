<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>An Open Web</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/an-open-web/an-open-web.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/an-open-web/an-open-web.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	An Open Web
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="software-as-a-service.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="software-as-a-service.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Other People's Computers</h1>

  <p>Much of what we call collaboration occurs on web sites generally running software services. This is particularly true of collaboration among many distributed users. Direct support for collaboration, and more broadly for social features, is simply easier in a centralized context. It is possible to imagine a decentralized Wikipedia or Facebook, but building such services with sufficient ease of use, features, and robustness to challenge centralized web sites is a very difficult task.</p>

  <p>Why does this matter? Making it relatively easy for people to work together in the specific way offered by a web site owner is a rather impoverished vision of what the web and digital networks could enable, just as merely allowing people to run programs on their computers in the way program authors intended is an impoverished vision of personal computing.</p>

  <p>Free software allows users control their own computing and to help other users by retaining the ability to run, modify, and share software for any purpose. Whether the value of this autonomy is primarily ethical, as often framed by advocates of the term free software, or primarily practical, as often framed by advocates of the term open source, any threat to these freedoms has to be of deep concern to anyone interested in the future of collaboration, both in terms of what collaborations are possible and what interests control and benefit from those collaborations. Kragen Sitaker frames the problem with these threats to freedom:</p>

  <blockquote>
    <p>"Web sites and special-purpose hardware […] do not give me the same freedoms general-purpose computers do. If the trend were to continue to the extent the pundits project, more and more of what I do today with my computer will be done by special-purpose things and remote servers.</p>

    <p>What does freedom of software mean in such an environment? Surely it's not wrong to run a Web site without offering my software and databases for download. (Even if it were, it might not be feasible for most people to download them. IBM's patent server has a many-terabyte database behind it.)</p>

    <p>I believe that software-open-source software, in particular-has the potential to give individuals significantly more control over their own lives, because it consists of ideas, not people, places, or things. The trend toward special-purpose devices and remote servers could reverse that.</p>

    <p>-Kragen Sitaker, "people, places, things, and ideas"<span class="InsertNoteMarker" id="InsertNoteID_6_marker7"><sup><a href="software-as-a-service.html#InsertNoteID_6">1</a></sup></span></p>
  </blockquote>

  <p>What are the prospects and strategies for keeping the benefits of free software in an age of collaboration mediated by software services? One strategy, argued for in "The equivalent of free software for online services" by Kragen Sitaker,<span class="InsertNoteMarker" id="InsertNoteID_8_marker9"><sup><a href="software-as-a-service.html#InsertNoteID_8">2</a></sup></span> is that centralized services need to be re-implemented as peer-to-peer services that can run on computers as free software under users' control. This is an extremely interesting strategy, but a very long term one, for it is both a computer science challenge and a social one.</p>

  <p>Abstinence from software services may be a naive and losing strategy in both the short and long term. Instead, we can both work on decentralization as well as attempt to build services that respect user's autonomy:</p>

  <blockquote>
    <p>"Going places I don't individually control-restaurants, museums, retail stores, public parks-enriches my life immeasurably. A definition of "freedom" where I couldn't leave my own house because it was the only space I had absolute control over would not feel very free to me at all. At the same time, I think there are some places I just don't want to go-my freedom and physical well-being wouldn't be protected or respected there.</p>

    <p>Similarly, I think that using network services makes my computing life fuller and more satisfying. I can do more things and be a more effective person by spring-boarding off the software on other peoples' computers than just with my own. I may not control your email server, but I enjoy sending you email, and I think it makes both of our lives better.</p>

    <p>And I think that just as we can define a level of personal autonomy that we expect in places that belong to other people or groups, we should be able to define a level of autonomy that we can expect when using software on other people's computers. Can we make working on network services more like visiting a friends' house than like being locked in a jail?</p>

    <p>We've made a balance between the absolute don't-use-other-people's-computers argument and the maybe-it's-OK-sometimes argument in the Franklin Street Statement. Time will tell whether we can craft a culture around Free Network Services that is respectful of users' autonomy, such that we can use other computers with some measure of confidence."</p>

    <p>-Evan Prodromou, "RMS on Cloud Computing: 'Stupidity'"<span class="InsertNoteMarker" id="InsertNoteID_10_marker11"><sup><a href="software-as-a-service.html#InsertNoteID_10">3</a></sup></span></p>
  </blockquote>

  <p>The Franklin Street Statement on Freedom and Network Services is an initial attempt to distill actions that users, service providers (the "other people" here), and developers should take to retain the benefits of free software in an era of software services:</p>

  <blockquote>
    <p>"The current generation of <strong>network services</strong> or <strong>Software as a Service</strong> can provide advantages over traditional, locally installed software in ease of deployment, collaboration, and data aggregation. Many users have begun to rely on such services in preference to software provisioned by themselves or their organizations. This move toward centralization has powerful effects on software freedom and user autonomy.</p>

    <p>On March 16, 2008, a working group convened at the Free Software Foundation to discuss issues of freedom for users given the rise of network services. We considered a number of issues, among them what impacts these services have on user freedom, and how implementers of network services can help or harm users. We believe this will be an ongoing conversation, potentially spanning many years. Our hope is that free software and open source communities will embrace and adopt these values when thinking about user freedom and network services. We hope to work with organizations including the FSF to provide moral and technical leadership on this issue.</p>

    <p>We consider network services that are <strong>Free Software</strong> and which share <strong>Free Data</strong> as a good starting-point for ensuring users' freedom. Although we have not yet formally defined what might constitute a 'Free Service', we do have suggestions that developers, service providers, and users should consider:</p>

    <p><strong>Developers</strong> of network service software are encouraged to:</p>

    <ul>
      <li>Use the GNU Affero GPL, a license designed specifically for network service software, to ensure that users of services have the ability to examine the source or implement their own service.</li>

      <li>Develop freely-licensed alternatives to existing popular but non-Free network services.</li>

      <li>Develop software that can replace centralized services and data storage with distributed software and data deployment, giving control back to users.</li>
    </ul>

    <div style="page-break-after: always;"></div>

    <p><strong>Service providers</strong> are encouraged to:</p>

    <ul>
      <li>Choose Free Software for their service.</li>

      <li>Release customizations to their software under a Free Software license.</li>

      <li>Make data and works of authorship available to their service's users under legal terms and in formats that enable the users to move and use their data outside of the service. This means:<br>
      * Users should control their private data.<br>
      * Data available to all users of the service should be available under terms approved for Free Cultural Works or Open Knowledge.</li>
    </ul><strong>Users</strong> are encouraged to:

    <ul>
      <li>Consider carefully whether to use software on someone else's computer at all. Where it is possible, they should use Free Software equivalents that run on their own computer. Services may have substantial benefits, but they represent a loss of control for users and introduce several problems of freedom.</li>

      <li>When deciding whether to use a network service, look for services that follow the guidelines listed above, so that, when necessary, they still have the freedom to modify or replicate the service without losing their own data."</li>
    </ul>

    <p>-Franklin Street Statement on Freedom and Network Services<span class="InsertNoteMarker" id="InsertNoteID_12_marker13"><sup><a href="software-as-a-service.html#InsertNoteID_12">4</a></sup></span></p>
  </blockquote>

  <p>As challenging as the Franklin Street Statement appears, additional issues must be addressed for maximum autonomy, including portable identifiers:</p>

  <blockquote>
    <p>"A Free Software Definition for the next decade should focus on the user's overall autonomy- their ability not just to use and modify a particular piece of software, but their ability to bring their data and identity with them to new, modified software.</p>

    <p>Such a definition would need to contain something like the following minimal principles:</p>

    <ol>
      <li>data should be available to the users who created it without legal restrictions or technological difficulty.</li>

      <li>any data tied to a particular user should be available to that user without technological difficulty, and available for redistribution under legal terms no more restrictive than the original terms.</li>

      <li>source code which can meaningfully manipulate the data provided under 1 and 2 should be freely available.</li>

      <li>if the service provider intends to cease providing data in a manner compliant with the first three terms, they should notify the user of this intent and provide a mechanism for users to obtain the data.</li>

      <li>a user's identity should be transparent; that is, where the software exposes a user's identity to other users, the software should allow forwarding to new or replacement identities hosted by other software."</li>
    </ol>

    <p>-Luis Villa, "Voting With Your Feet and Other Freedoms"<span class="InsertNoteMarker" id="InsertNoteID_25_marker26"><sup><a href="software-as-a-service.html#InsertNoteID_25">5</a></sup></span></p>
  </blockquote>

  <p>Fortunately the oldest, and at least until recently, the most ubiquitous network service-email-accommodates portable identifiers. (Not to mention that email is the lowest common denominator for much collaboration-sending attachments back and forth.) Users of a centralized email service like Gmail <em>can</em> retain a great deal of autonomy <em>if</em> they use an email address at a domain they control and merely route delivery to the service-though of course most users use the centralized provider's domain.</p>

  <p>Making email address portability available on a wider scale could be cheaper and technically easier. As an example, a democratically-run non-profit <em>The Internet Users Forever IKI</em> has worked to make this a reality in Finland. Since 1995, more than 24000 individuals have paid the one-time membership fee and received an @iki.fi address they can route to a provider of their choosing. The fees earn interests that are used to finance the routing service.<span class="InsertNoteMarker" id="InsertNoteID_27_marker28"><sup><a href="software-as-a-service.html#InsertNoteID_27">6</a></sup></span></p>

  <p>It is worth noting that the more recent and widely used, if not ubiquitous, instant messaging protocol XMPP as well as the brand new and little used Wave protocol have an architecture similar to email, though use of non-provider domains seems even less common, and in the case of Wave, Google is currently the only service provider.</p>

  <p>It may be valuable to assess software services from the respect of community autonomy as well as user autonomy. The former may explicitly note requirements for the product of collaboration-non-private data, roughly-as well as service governance:</p>

  <blockquote>
    In cases where one accepts a centralized web application, should one demand that application be somehow constitutionally open? Some possible criteria:

    <ul>
      <li>All source code for the running service should be published under an open source license and developer source control available for public viewing.</li>

      <li>All private data available for on-demand export in standard formats.</li>

      <li>All collaboratively created data available under an open license (e.g., one from Creative Commons), again in standard formats.</li>

      <li>In some cases, I am not sure how rare, the final mission of the organization running the service should be to provide the service rather than to make a financial profit, i.e., beholden to users and volunteers, not investors and employees. Maybe. Would I be less sanguine about the long term prospects of Wikipedia if it were for-profit? I don't know of evidence for or against this feeling.</li>
    </ul>

    <p>-Mike Linksvayer, "Constitutionally open services"<span class="InsertNoteMarker" id="InsertNoteID_29_marker30"><sup><a href="software-as-a-service.html#InsertNoteID_29">7</a></sup></span></p>
  </blockquote>

  <p>Software services are rapidly developing and subjected to much hype, often referred to as the buzzword Cloud Computing. However, some of the most potent means of encouraging autonomy may be relatively boring-for example, making it easier to maintain one's own computer and deploy slightly customized software in a secure and foolproof fashion. Any such development helps traditional users of free software as well as makes doing computing on one's own computer (which may be a "personal server" or virtual machine that one controls) more attractive.</p>

  <p>Perhaps one of the most hopeful trends is relatively widespread deployment by end users of free software web applications like WordPress and MediaWiki. StatusNet, free software for microblogging, is attempting to replicate this adoption success. StatusNet also includes technical support for a form of decentralization (remote subscription) and a legal requirement for service providers to release modifications as free software via the AGPL.</p>

  <p>This section barely scratches the surface of the technical and social issues raised by the convergence of so much of our computing, in particular computing that facilitates collaboration, to servers controlled by "other people", especially when these "other people" are a small number of large service corporations. The challenges of creating autonomy-respecting alternatives should not be understated.</p>

  <p>One of those challenges is only indirectly technical: decentralization can make community formation more difficult. To the extent the collaboration we are interested in requires community, this is a challenge. However, easily formed but inauthentic and controlled community also will not produce the kind of collaboration we are interested in.</p>

  <p>We should not limit our imagination to the collaboration facilitated by the likes of Facebook, Flickr, Google Docs, Twitter, or other "Web 2.0" services. These are impressive, but then so was AOL two decades ago. We should not accept a future of collaboration mediated by centralized giants now, any more than we should have been, with hindsight, happy to accept information services dominated by AOL and its near peers.</p>

  <p>Wikipedia is both held up as an exemplar of collaboration and is a free-as-in-freedom service: both the code and the content of the service are accessible under free terms. It is also a huge example of community governance in many respects. And it is undeniably a category-exploding success: vastly bigger and useful in many more ways than any previous encyclopedia. Other software and services enabling autonomous collaboration should set their sights no lower-not to merely replace an old category, but to explode it.</p>

  <p>However, Wikipedia (and its MediaWiki software) are not the end of the story. Merely using MediaWiki for a new project, while appropriate in many cases, is not magic pixie dust for enabling collaboration. Affordances for collaboration need to be built into many different types of software and services. Following Wikipedia's lead in autonomy is a good idea, but many experiments should be encouraged in every other respect. One example could be the young and relatively domain-specific collaboration software that this book is being written with, Booki.</p>

  <p>Software services have made "installation" of new software as simple as visiting a web page, social features a click, and provide an easy ladder of adoption for mass collaboration. They also threaten autonomy at the individual and community level. While there are daunting challenges, meeting them means achieving "world domination" for freedom in the most important means of production-computer-mediated collaboration-something the free software movement failed to approach in the era of desktop office software.</p>

  <ol id="InsertNote_NoteList">
    <li id="InsertNoteID_6"><a href="http://lists.canonical.org/pipermail/kragen-tol/1999-January/000322.html">http://lists.canonical.org/pipermail/kragen-tol/1999-January/000322.html</a><span id="InsertNoteID_6_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_6_marker7">^</a></sup></span></li>

    <li id="InsertNoteID_8"><a href="http://lists.canonical.org/pipermail/kragen-tol/2006-July/000818.html">http://lists.canonical.org/pipermail/kragen-tol/2006-July/000818.html</a><span id="InsertNoteID_8_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_8_marker9">^</a></sup></span></li>

    <li id="InsertNoteID_10">CC BY-SA <a href="http://autonomo.us/2008/09/rms-on-cloud-computing-stupidity">http://autonomo.us/2008/09/rms-on-cloud-computing-stupidity</a><span id="InsertNoteID_10_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_10_marker11">^</a></sup></span></li>

    <li id="InsertNoteID_12">CC BY-SA <a href="http://autonomo.us/2008/07/franklin-street-statement">http://autonomo.us/2008/07/franklin-street-statement</a><span id="InsertNoteID_12_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_12_marker13">^</a></sup></span></li>

    <li id="InsertNoteID_25">CC BY-SA <a href="http://tieguy.org/blog/2007/12/06/voting-with-your-feet-and-other-freedoms">http://tieguy.org/blog/2007/12/06/voting-with-your-feet-and-other-freedoms</a><span id="InsertNoteID_25_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_25_marker26">^</a></sup></span></li>

    <li id="InsertNoteID_27"><a href="http://www.iki.fi/iki/statistics.html">http://www.iki.fi/iki/statistics.html</a><span id="InsertNoteID_27_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_27_marker28">^</a></sup></span></li>

    <li id="InsertNoteID_29">CC0 <a href="http://gondwanaland.com/mlog/2006/07/06/constitutionally-open-services">http://gondwanaland.com/mlog/2006/07/06/constitutionally-open-services</a><span id="InsertNoteID_29_LinkBacks"><sup><a href="software-as-a-service.html#InsertNoteID_29_marker30">^</a></sup></span></li>
  </ol>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">The Web is Closed</a></li>

  <li><a href="heading-towards-a-commons.html">The Future is Open</a></li>

  <li class="booki-section">Your Battleground</li>

  <li><a href="your-battleground.html">You are the Battleground; It’s Your Battleground</a></li>

  <li><a href="your-rights-and-freedoms.html">Your Rights and Freedoms</a></li>

  <li><a href="the-browser.html">The Browser and the Web are Magic</a></li>

  <li><a href="the-contet-should-be-under-open-liscense.html">Content is Your Knowledge</a></li>

  <li><a href="devices.html">Hardware is Physical Software</a></li>

  <li><a href="the-browser-should-be-open-source.html">Software is a Global Interface to Hardware</a></li>

  <li><a href="social-networks.html">Network Services Connect People</a></li>

  <li><a href="distribution-how-to.html">10 Things You Can Do Now</a></li>

  <li class="booki-section">Global Battlegrounds</li>

  <li><a href="the-open-web-stack.html">The Open Web Stack</a></li>

  <li><a href="apis.html">Standard but not Standards</a></li>

  <li><a href="introduction-excisions.html">Tiers of the Cloud</a></li>

  <li><a href="file-sharing.html">Edges of Autonomy</a></li>

  <li><a href="software-as-a-service.html">Other People’s Computers</a></li>

  <li><a href="global-battlefield-stragies.html">5 Battlefield Tactics</a></li>

  <li class="booki-section">Conclusion</li>

  <li><a href="conclusion.html">The Web is Open‽</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="the-myth-of-openness.html">The Myth of Openness</a></li>

  <li><a href="about-this-book.html">About This Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="attribution-and-license.html">Attribution and License</a></li>

  <li><a href="colofon.html">Colophon</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="software-as-a-service.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="software-as-a-service.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

