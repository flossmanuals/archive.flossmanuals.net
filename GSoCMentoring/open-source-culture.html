<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>Mentors Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/GSoCMentoring/GSoCMentoring.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/GSoCMentoring/GSoCMentoring.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="../gsocmentoring/_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Mentors Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="open-source-culture.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="open-source-culture.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Open Source Culture</h1>

  <p>When you encounter an open source group for the first time, it may be a bewildering experience. Whether posting to a mailing list for the first time, blogging about the project you're taking on or hanging out on an IRC channel - the way people interact, and what they expect from each other is pretty different than in classroom or with friends and family.</p>

  <h2>Openness and Sharing</h2>

  <p>Open source communication can vary a lot. A core value held in common is that sharing code is good. Regardless of license, language or indentation style, open source developers create, share and modify source code together.</p>

  <p>Working on code together means a lot of things: transparency, directness and cooperation are words that are often mentioned by developers when describing the process. It can involve bug reports, code refactoring, implementing new features, documentation, project management and advocacy.</p>

  <p>Amazingly, the ways in which people actually share code are as varied as the individuals involved. Even if you have previous experience with other open source projects, keep in mind that you still need to take the time to learn how the new open source project works, and acquaint yourself with their particular brand of sharing.</p>

  <p>One aspect of "open culture" is that people are informal. People address each other by first name. They tend to speak directly to one another, regardless of social status or formal title. Disagreements about code, whether as profound as which algorithm is most appropriate, or as seemingly mundane as how many spaces are used for indentation, are aired in public. This process is very intimidating to newcomers, who might be concerned about having their words immortalized on the Internet, and worse, saying something wrong or embarrassing. The only way to get over this fear is to practice and participate publicly.</p>

  <p>Although "open culture" is generally informal, it is important to remember that you still need to mind your manners when participating in conversations.&nbsp;</p>

  <div style="page-break-after: always;" contenteditable="false" title="Page Break">
    <h2>Remote Communication</h2>
  </div>

  <p>Many projects involve individuals who are working not only in different cities, countries and continents, but collaborating across major cultural and language differences. And rather than having procedures or policies on how to interact with one another handed down from HR departments or other authorities, communication practices evolve between individuals over time.</p>

  <p>Because there are few rules around working together on open source projects, there is a lot of freedom to share directly with one another. This freedom is at the core of what attracts many people to open source software. Multi-cultural projects offer an incredible opportunity to share knowledge between individuals directly. With sharing, however, comes a responsibility to inform new participants about expectations for communication and ways to solve misunderstandings to the benefit of all.</p>

  <p>Be mindful of cultural assumptions about race, gender, sexual orientation and disability. People often make assumptions, have stereotypes or are biased in ways that no one can control. The best practice is to assume that people don't mean any harm, and when they're told respectfully that they've offended or hurt someone, that they'll stop whatever it is that they are doing that is harmful.</p>

  <p>All that tolerant rhetoric aside, it is never productive for an open source project to allow individuals who consistently try to cause harm to others to do so. If you are causing undue problems don't be surprised if you are asked to discontinue your participation regardless of your contributions. It's often better for an open source project to ask someone to leave, than to allow them to harm others and in turn, cause other productive members of your team to depart.</p>

  <h2>Abbreviations and Slang</h2>

  <p>People come up with abbreviations and slang that are meaningful inside the group, but not necessarily to outsiders. Ask questions when you don't understand a term, a joke or some arcane bit of project lore.</p>

  <p>Here are a few useful resources for teasing out meaning from initialisms, acronyms and abbreviations:</p>

  <ul>
    <li>Urban Dictionary (<a target="_blank" href="http://www.urbandictionary.com/">http://www.urbandictionary.com</a>)&nbsp;<br></li>

    <li>Webster's Online Dictionary (<a target="_blank" href="http://www.websters-online-dictionary.org/">http://www.websters-online-dictionary.org/</a>)&nbsp;<br></li>

    <li>Acronym Finder (<a target="_blank" href="http://www.acronymfinder.com/">http://www.acronymfinder.com/</a>)&nbsp;<br></li>

    <li>A to Z word finder&nbsp;<a target="_blank" href="http://www.a2zwordfinder.com/)">(</a><a href="http://www.a2zwordfinder.com/" target="_top">http://www.a2zwordfinder.com/</a>) &nbsp;</li>
  </ul>

  <h2>Volunteerism and Gift Economies</h2>

  <p>Donated time are the life blood of open source projects. Many individuals contribute their time and energy without any expectation of compensation or even a simple "thank you" in return.</p>

  <p>Contributions to projects are often self-directed, with developers having a personal itch to scratch in the form of a new feature, by correcting a bug that they've encountered or by implementing something from a TODO list. Projects operating in this way may seem chaotic if you are only familiar with top-down management - where teachers, professors or bosses tell you what to do and when. Of course, some projects do have clearly defined project management, with milestones and tasks meted out carefully.</p>

  <p>Because many (or in some cases all) contributors are volunteering, methods of coercion available to businesses are not available. The best way to collaborate is to behave in a way that encourages others, and ultimately, makes people want to contribute. Some easy ways to encourage volunteerism include:</p>

  <ul>
    <li>Saying thank you</li>

    <li>Giving compliments when they are deserved, regularly and in public</li>

    <li>Publicizing cool hacks and features as they are implemented, in blog posts and on the mailing lists</li>

    <li>Promptly committing useful code</li>

    <li>Responding promptly to requests for information</li>

    <li>Clearly defining ways to contribute to your project (TODO lists are great!)</li>
  </ul>

  <p>Consider treating every patch like it is a gift. Being grateful is good for both the giver and the receiver, and invigorates the cycle of virtuous giving.</p>

  <p>Overall, your goal is to help create and maintain an atmosphere around contribution that is enjoyable. What that means will vary significantly depending on the project, but certainly the above points apply to any project.&nbsp;<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">About This Manual</a></li>

  <li><a href="what-is-gsoc.html">What Is GSoC?</a></li>

  <li><a href="why-gsoc-matters.html">Why GSoC Matters</a></li>

  <li class="booki-section">GETTING STARTED</li>

  <li><a href="notes-for-first-year-organizations.html">Notes for First Year Organizations</a></li>

  <li><a href="what-makes-a-good-mentor.html">What Makes a Good Mentor</a></li>

  <li><a href="defining-a-project.html">Defining a Project</a></li>

  <li><a href="selecting-a-student.html">Selecting a Student</a></li>

  <li class="booki-section">COMMUNICATION</li>

  <li><a href="community-basics.html">Community Basics</a></li>

  <li><a href="best-practices.html">Best Practices</a></li>

  <li><a href="warning-signs.html">Warning Signs</a></li>

  <li><a href="open-source-culture.html">Open Source Culture</a></li>

  <li class="booki-section">MENTORING</li>

  <li><a href="mind-the-gap.html">Mind the Gap</a></li>

  <li><a href="setting-expectations.html">Setting Expectations</a></li>

  <li><a href="workflow.html">Workflow</a></li>

  <li><a href="managing-the-plan.html">Managing the Plan</a></li>

  <li><a href="evaluations.html">Evaluations</a></li>

  <li class="booki-section">WRAPPING UP</li>

  <li><a href="upstream-integration.html">Upstream Integration</a></li>

  <li><a href="building-a-lifetime-contributor.html">Building a Lifetime Contributor</a></li>

  <li class="booki-section">ORG ADMIN</li>

  <li><a href="making-your-ideas-page.html">Making Your Ideas Page</a></li>

  <li><a href="selecting-projects-and-mentors.html">Selecting Projects and Mentors</a></li>

  <li><a href="org-application.html">Org Application</a></li>

  <li><a href="managing-the-mentors.html">Managing the Mentors</a></li>

  <li><a href="student-mentor-facilitation.html">Student - Mentor Facilitation</a></li>

  <li><a href="other-possible-issues.html">Other Possible Issues</a></li>

  <li><a href="end-of-year-report.html">End of Year Report</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="the-quick-guide.html">The Quick Guide</a></li>

  <li><a href="history-of-gsoc.html">History of GSoC</a></li>

  <li><a href="additional-resources.html">Additional Resources</a></li>

  <li><a href="glossary.html">Glossary</a></li>

  <li><a href="credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="open-source-culture.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="open-source-culture.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

