<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Implementers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-guide/openmrs-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-guide/openmrs-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Implementers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="openmrs-information-model.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="openmrs-information-model.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>OpenMRS Information Model</h1>

  <p><img src="_booki/openmrs-guide/static/infomodel.png"></p>

  <address>
    Reference books line a shelf in a rural African clinic.
  </address>

  <p>This chapter explains terms and concepts which are useful to understand as you install and use OpenMRS.</p>

  <h2>Data</h2>

  <p>The actual information you want to record in OpenMRS is called <strong>Data</strong>.&nbsp;Examples of Data in OpenMRS are Patients, Encounters, and Observations.&nbsp;To support this data, and describe its meaning, you need additional <strong>Metadata</strong>.</p>

  <p>When a user deletes a piece of data in OpenMRS, the information actually remains in the database. It is marked as <strong>voided</strong>, so that it will not show up in the interface, but it is not immediately deleted from the database. If a user deletes a piece of data by accident, an administrator can <strong>unvoid</strong>&nbsp;it to return it to the system. To permanently delete data from the database, an administrator must&nbsp;<strong>purge&nbsp;</strong>that data. Typically, this should never be done in a production system.</p>

  <h2>Metadata</h2>

  <p>The fundamental expectation of OpenMRS's design is that you will customize it for your clinical program's use case. The system has no built-in idea of the patient's weight or seeing the patient in an outpatient visit. Instead, you can configure these things yourself, to match your project's workflow. Generally speaking, the things that you need to configure in order to describe the real patient information you will be capturing are referred to as <strong>Metadata</strong>. An example of a piece of metadata is a Location that represents a hospital.</p>

  <p>An administrator may also <strong>retire</strong>&nbsp;metadata in OpenMRS. This does not mean that the metadata is deleted, but rather that it is not intended to be used going forwards. Old information that refers to the retired metadata remains valid. An administrator may <strong>unretire</strong>&nbsp;metadata if it becomes relevant to active use again. If no actual data refers to a piece of metadata, an administrator may <strong>purge</strong>&nbsp;the metadata to permanently remove it from the database.</p>

  <p>For example, the hospital you refer patients to closes. Therefore, you can no longer refer patients there. This Location can now be retired in OpenMRS. This would not invalidate the fact that many patients were referred there in the past.</p>

  <h2>Concepts and Concept Dictionary</h2>

  <p>The most important part of the system's metadata is the <strong>Concept Dictionary</strong>, which is a list of all the medical and program-related terms that you will use as questions and answers in Observations. This dictionary does not need to be complete when you begin using OpenMRS. You should expect new terms to be added and old terms to be retired as your use of the system evolves. It is better to start with a pre-populated Concept Dictionary, rather than starting from scratch yourself. See the chapter "Sharing Concepts and Metadata" for more details.</p>

  <p>Every question you ask about a patient needs to be defined by a <strong>Concept</strong>. (For example, to record a patient's weight you need a Concept like <strong>Weight in kilograms</strong>.)</p>

  <p>If you want to ask a question that has a fixed set of coded answers, those answers are also Concepts. (For example, the question concept&nbsp;<strong>Blood Type</strong> may have 4 different answer concepts:&nbsp;<strong>A</strong>,&nbsp;<strong>B</strong>, <strong>AB</strong>, and <strong>O</strong>)</p>

  <h2>Persons</h2>

  <p>Every individual who is referred to in any patient's record in OpenMRS must be stored in the system as a <strong>Person</strong>. Most persons will also be Patients or Users.</p>

  <h3>Names</h3>

  <p>A person can have one or more names,&nbsp;one&nbsp;of which must be marked as the <strong>preferred</strong>&nbsp;name. The preferred name will be displayed in search results and patient screens.</p>

  <h3>Addresses</h3>

  <p>A person may have zero or more contact addresses. You may configure the format of these addresses for your particular locale.</p>

  <h3>Person Attributes</h3>

  <p>To support your local needs, you can define additional pieces of information&nbsp;about the people in your system, on top of those&nbsp;that are natively supported by OpenMRS. You can define the datatype of a Person Attribute, as well as any constraints on the possible values, using metadata. This metadata is called a Person Attribute Type.</p>

  <p>Person Attributes are suitable for storing other information. But historical values of person attributes are not retained. For example, you should use a person attribute to record a patient's contact telephone number. This information may change, but if it does so, the system need only store the most recent value, and need not retain previous values. It is not&nbsp;appropriate to use a person attribute to store something like the patient's height, which is recorded at a given point in time, but can be expected to change and should be tracked as it does so.</p>

  <h2>Patients</h2>

  <p>Anyone who receives care in OpenMRS&nbsp;must be a&nbsp;<strong>Patient&nbsp;</strong>(for example, anyone who has an Encounter or who is enrolled in a Program).&nbsp;Every Patient must have at least one Identifier, which is explained below.</p>

  <p>A Patient is also a Person, meaning they must have at least one name and they may have addresses.</p>

  <h3>Patient Identifier</h3>

  <p>The Patient Identifier is a medical record number assigned by your facility, used to identify and re-identify the patient on subsequent visits.</p>

  <p>A <strong>Patient Identifier Type</strong>&nbsp;defines the format of a particular kind of patient identifier. For example, you might define that <strong>Amani ID</strong>&nbsp;is an identifier type that is required for every patient; the format is 2 letters followed by 6 digits and uses a particular check digit<span id="InsertNoteID_6_marker7" class="InsertNoteMarker"><sup><a href="openmrs-information-model/openmrs-information-model.html#InsertNoteID_6">1</a></sup></span>&nbsp;algorithm.</p>

  <h2>Relationships</h2>

  <p><img src="_booki/openmrs-guide/static/case-study.png" align="right">A <strong>Relationship</strong> is a bidirectional link between two Persons in OpenMRS.</p>

  <p>The metadata describing a particular kind of relationship is a <strong>Relationship Type</strong>&nbsp;which defines the names of each direction of the relationship.</p>

  <p>At the Amani Clinic, it is necessary to use relationships to link a mother's patient record to the patient record of her children. One might also use relationships to record the link between a patient and their primary care provider.</p>

  <h2>Encounters</h2>

  <p>A moment in time where a patient is seen by a provider at a location, and data is captured. Generally speaking, every time you enter a form in OpenMRS this creates an <strong>Encounter</strong>.</p>

  <p>If a patient visits a clinic, checks in at registration, is seen by a doctor, and has meds dispensed in the pharmacy, this would be recorded as 3 Encounters.</p>

  <p>The metadata that describes a kind of encounter is an <strong>Encounter Type</strong>. These are displayed in the user interface, and you may also search against them.</p>

  <h2>Locations</h2>

  <p>A <strong>Location</strong> is a physical place where a patient may be seen.</p>

  <p>Locations may have a hierarchy, for example <strong>Children's Ward</strong>&nbsp;might be a location within the location <strong>Amani Clinic</strong>.</p>

  <p>You might also store physical areas (for example <strong>Eastern Province</strong>, or <strong>California</strong>) as Locations. You should&nbsp;not&nbsp;use Locations to represent logical ideas like <strong>All District Hospitals</strong>.</p>

  <h2>Observations</h2>

  <p>An <strong>Observation</strong> is one single piece of information that is recorded about a person at a moment in time.</p>

  <p>Every observation has a Concept as its question, and depending on the datatype of the concept, it has a value that is a number, date, text, Concept, etc.</p>

  <p>Most of the information you store in OpenMRS is in the form of Observations, and most Observations happen in an Encounter. When you enter a form in OpenMRS, typically one Encounter is created with anywhere between tens or hundreds of Observations.</p>

  <p>Note that an individual Observation is valid only at one moment in time, and it does not carry forward. You may query the system for the last observation for <strong>pregnancy status</strong>&nbsp;but this does not tell you whether or not the patient is pregnant at any point after&nbsp;the moment of that observation.</p>

  <p>Examples of observations include <strong>Serum Creatinine of 0.9mg/dL</strong>&nbsp;or <strong>Review of cardiopulmonary system is normal</strong>.</p>

  <h3>Observation Groups</h3>

  <p>Sometimes a single Observation is not sufficient to capture an entire piece of patient information, and you need to use multiple Observations that are grouped together.</p>

  <p>For example recording that a patient had a rash as an allergic reaction to penicillin would need to be stored as two observations plus a third one that groups the previous two together:</p>

  <ol>
    <li>Concept = "Allergen", coded value = "Penicillin", group = (3)</li>

    <li>Concept = "Reaction", coded value = "Rash", group = (3)</li>

    <li>Concept = "Allergic Reaction Construct", group members = (1), (2)</li>
  </ol>

  <h2>Orders</h2>

  <p>An <strong>Order</strong> is an action that a provider requests be taken regarding a patient.</p>

  <p>For example a provider could order a Complete Blood Count laboratory panel for a patient.</p>

  <p>An Order only records an intention, not whether or not the action is carried out. The results of an Order are typically recorded later as Observations.</p>

  <p>Prescribing a medication is a <strong>Drug Order</strong>. A drug order can be placed for a generic drug, represented by a Concept (for example,&nbsp;<strong>500mg of Ciprofloxacin, twice a day</strong>). If you are using OpenMRS to manage a formulary of specific medications (i.e., <strong>Drugs</strong>&nbsp;in OpenMRS), you may also record <strong>Drug Orders</strong> against those. For example, a drug order might be <strong>one 500mg tablet of Ciprofloxacin, twice a day</strong>.</p>

  <h2>Allergy Lists</h2>

  <p>OpenMRS lets you manually maintain an <strong>Allergy List</strong> for a patient, including the allergen, reaction, severity, etc.</p>

  <p>This list is managed separately from Observations: observing an allergic reaction to a drug does not automatically add an Allergy to the list.</p>

  <p>Unlike an Observation (which happens at one moment in time), an Allergy is longitudinal data, with start and end dates.</p>

  <h2>Problem Lists</h2>

  <p>OpenMRS lets you manually maintain a Problem List for a patient. This list is managed separated from Observations: observing that the patient has "Diagnosis Present = Diabetes" does not automatically add a Problem to the list.&nbsp;Unlike an Observation (which happens at one moment in time), a Problem is longitudinal data, with start and end dates.</p>

  <h2>Program Enrollments, Workflows, and States</h2>

  <p>A <strong>Program</strong> represents an administrative program or study that a patient may be enrolled in (for example,&nbsp;<strong>Child Nutrition Study</strong>&nbsp;or <strong>DOTS Tuberculosis Treatment Program</strong>).</p>

  <p>A <strong>Program Enrollment</strong> represents the fact that a patient is enrolled in one of these Programs over a time period at a Location. This is longitudinal data with a start date and end date.</p>

  <p>A Program can also define administrative <strong>Workflows</strong>, and possible <strong>States</strong> the Patient may have within those workflows.&nbsp;An <strong>initial state</strong>&nbsp;is one that a Patient is allowed to start in when they are first enrolled in a Program. A <strong>terminal state</strong>&nbsp;is one that closes the Program enrollment if the Patient is placed in it.</p>

  <p>For example a research study on infant nutrition might have a workflow called <strong>Study Enrollment Status</strong>&nbsp;with the states:</p>

  <ul>
    <li>Patient Identified (initial)</li>

    <li>Mother Consented to Study</li>

    <li>Study Complete (terminal)</li>

    <li>Lost to Followup (terminal)</li>
  </ul>

  <p>These states are meant to represent&nbsp;administrative&nbsp;statuses, not clinical ones. For example putting a patient in a <strong>Loss to Followup</strong>&nbsp;state represents an&nbsp;official&nbsp;declaration and will not happen automatically even if no encounters are entered for the patient for several months.</p>

  <h2>Forms</h2>

  <p>A <strong>Form</strong> represents an electronic form that may be used for entering or viewing data. The basic OpenMRS system does not define a specific technology for entering forms. You will need to use one of the community-developed form entry modules. See the chapter "Data Entry" for more details.</p>

  <p>The Form Entry (Infopath) and XForms modules rely on a <strong>Form Schema</strong>, where you define which Concepts are used on the Form.&nbsp;The HTML Form Entry module does not require you to manage the schema.</p>

  <h2>Users, Roles, and Privileges</h2>

  <p>A <strong>User</strong> in OpenMRS is an account that a person may use to log into the system.</p>

  <p>The real-life person must be represented by a Person record in OpenMRS, and a person may have more than one user account. If you want a patient to be able to view her own record in OpenMRS, then you need to attach User account to the Patient record.</p>

  <p>A <strong>Role</strong> represents a group of privileges in the system. Roles may inherit privileges from other roles, and Users may have one or more Roles.</p>

  <p>A <strong>Privilege</strong> is an authorization to perform a particular action in the system. The list of available privileges are defined by the core system and by add-on modules (for example,&nbsp;<strong>Delete Patients</strong>&nbsp;and&nbsp;<strong>Manage Encounter Types</strong>), but you need to&nbsp;configure which Roles have which Privileges while you are configuring your system.</p>

  <h2>The Information Model in use at Amani Clinic</h2>

  <p><img src="_booki/openmrs-guide/static/case-study.png" align="right">A patient named Asaba arrives at Amani Clinic, where the registration clerk James creates her electronic record and stores her contact phone number as 312-555-7890. On paper the Nurse, Kissa, records Asaba's weight as 61.5kg and orders a pregnancy test. James enters these onto an electronic screen.</p>

  <p>From the perspective of the OpenMRS model, we have the following metadata:</p>

  <ul>
    <li>The nurse, Kissa (a Person)</li>

    <li>The registration clerk, James (a User)</li>

    <li>Contact Phone Number (a&nbsp;Person Attribute Type)</li>

    <li>Weight, in kilograms (a Concept, with class <strong>Finding</strong>&nbsp;and datatype <strong>Numeric</strong>)</li>

    <li>Urine Pregnancy Test (a Concept, with class <strong>Test</strong>)</li>

    <li>Amani Clinic (a Location)</li>

    <li>Outpatient Visit (an&nbsp;Encounter Type)</li>

    <li>Outpatient Triage Form (a Form)</li>
  </ul>

  <p>When Asaba is first seen at the registration desk, James creates the following data:</p>

  <ul>
    <li>A Patient (Asaba)</li>

    <li>A Person Attribute (type = Contact Phone Number, value = 312-456-7890).</li>
  </ul>

  <p>After Asaba sees the nurse, who gives a paper form to James, he creates more data:</p>

  <ul>
    <li>An Encounter with:</li>

    <li style="list-style: none; display: inline">
      <ul>
        <li>patient = Asaba</li>

        <li>type = Outpatient Visit</li>

        <li>form = Outpatient Triage Form</li>

        <li>location = Amani Clinic</li>

        <li>provider = Nurse Kissa</li>

        <li>creator = Registration Clerk James</li>
      </ul>
    </li>

    <li>An Observation (in that encounter), of <strong>Weight in kilograms</strong>&nbsp;= 61.5.</li>

    <li>An Order (in that encounter), for <strong>Urine Pregnancy Test</strong></li>
  </ul>

  <h2>Check digits</h2>

  <p>A <strong>check digit</strong>&nbsp;is an extra digit that is added to the end of an identifier, and depends on the rest of identifier. It allows OpenMRS to determine whether an identifier has been mistyped. For example using a Luhn check digit, "1234-1" is valid, but "1234-5" is incorrect.&nbsp;It is a strongly-recommended best practice to use check digits in all patient identifiers that you assign.</p>

  <p><a href="http://en.wikipedia.org/wiki/Check_digit" target="" title="">http://en.wikipedia.org/wiki/Check_digit</a></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">OpenMRS Around the World</a></li>

  <li><a href="a-brief-history.html">A Brief History</a></li>

  <li><a href="example-amani-clinic.html">Example: Amani Clinic</a></li>

  <li class="booki-section">Planning</li>

  <li><a href="is-openmrs-for-you.html">Is OpenMRS for You?</a></li>

  <li><a href="identifying-your-needs.html">Identifying Your Needs</a></li>

  <li><a href="transitioning-to-openmrs.html">Transitioning to OpenMRS</a></li>

  <li class="booki-section">Getting Started</li>

  <li><a href="installation-and-initial-setup.html">Installation and Initial Setup</a></li>

  <li><a href="openmrs-information-model.html">OpenMRS Information Model</a></li>

  <li><a href="getting-around-the-user-interface.html">Getting Around the User Interface</a></li>

  <li class="booki-section">Configuration</li>

  <li><a href="customizing-openmrs-with-plug-in-modules.html">Customizing OpenMRS with Plug-in Modules</a></li>

  <li><a href="managing-concepts-and-metadata.html">Managing Concepts and Metadata</a></li>

  <li><a href="sharing-concepts-and-metadata.html">Sharing Concepts and Metadata</a></li>

  <li class="booki-section">Collecting Data</li>

  <li><a href="registering-patients.html">Registering Patients</a></li>

  <li><a href="data-entry.html">Data Entry</a></li>

  <li class="booki-section">Using Data</li>

  <li><a href="cohort-builder.html">Cohort Builder</a></li>

  <li><a href="reporting.html">Reporting</a></li>

  <li><a href="patient-alerts-and-flags.html">Patient Alerts and Flags</a></li>

  <li class="booki-section">Administering OpenMRS</li>

  <li><a href="user-management-and-access-control.html">User Management and Access Control</a></li>

  <li><a href="maintenance.html">Maintenance</a></li>

  <li><a href="troubleshooting.html">Troubleshooting</a></li>

  <li><a href="getting-help-from-the-openmrs-community.html">Getting Help from the OpenMRS Community</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="leaving-amani-clinic.html">Leaving Amani Clinic</a></li>

  <li><a href="about-this-book.html">About this Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-glossary.html">Appendix A: Glossary</a></li>

  <li><a href="appendix-b-example-html-form-source.html">Appendix B: Example HTML Form Source</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="openmrs-information-model.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="openmrs-information-model.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

