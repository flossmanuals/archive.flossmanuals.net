<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Implementers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-guide/openmrs-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-guide/openmrs-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Implementers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="cohort-builder.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="cohort-builder.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Cohort Builder</h1>

  <p>The Cohort Builder is a tool in the <strong>Reporting Compatibility</strong> module&nbsp;(included with most OpenMRS installations) that lets you perform ad-hoc queries for patients with defined characteristics, and combines multiple queries into more complex ones.</p>

  <p>A cohort query returns a list of patients matching the specified criteria. It is not possible to create lists of data elements other than patients. For example, you can use the cohort builder to search for all patients with any weight observation &gt; 70, but it is not possible to create a list of all observations of weight &gt; 70.</p>

  <p>To use this tool, click <strong>Cohort Builder</strong>&nbsp;at the top of any page.</p>

  <h2>Cohort definitions, cohorts, and search history</h2>

  <p>Each Patient Search is added to&nbsp;your search history.&nbsp;This history is preserved until you choose to clear it or the web application is restarted. You may also save your search history to preserve it for future re-use.</p>

  <p>You may save any search (simple or combined) as a "Cohort Definition" to make it easier to re-run that same search in the future. When you save a combined search, it includes copies of all its component searches.</p>

  <p>You may also save the list of patients resulting from a query as a "Cohort". The list of members in a saved Cohort will never change. On the other hand, re-running a saved search may produce new results.</p>

  <p>The initial screen of the cohort builder contains several sections:</p>

  <ol>
    <li>The top tabs allow you to run different kinds of queries.</li>

    <li>Each query you perform goes into the search history.</li>

    <li>The save, load, and clear buttons help keep your entire search organized.</li>

    <li>After running a query, cohort members are displayed here.</li>

    <li>Click this save button to save this cohort for future re-use.</li>

    <li>Click these save buttons to save a previous query as a cohort definition for future re-use.</li>

    <li>Use the link at the top of the cohort builder to load saved cohorts and cohort definitions.</li>
  </ol>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;overview.png"></p>

  <h2>Searching by observation</h2>

  <p>To search for patients who have observations matching certain criteria, choose the <strong>Concept/Observation</strong> tab. Start typing the name of a concept that you want to search for [1], and choose that concept from the search results [2].</p>

  <p>If you choose a concept whose datatype is anything other than N/A, you can search for observations whose question&nbsp;is the concept you selected [3]. Depending on the datatype, you can limit this to a numeric or date range, or to specific coded answers. You can also choose which observations you are looking for (first, last, min, max, any, none) or combine (average), and you can specify date ranges.</p>

  <p>This example will build a cohort of patients whose last systolic blood pressure measurement was above 130 mmHg:</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;obs&#32;question.png"></p>

  <p>You can also search for any observations that have your chosen concept as an <em>answer</em>. (You'd typically use this for doing a highly selective search, which you'll later filter down to something more specific.)</p>

  <p>In this example we search for patients who have any observation whose answer is Hypertension, which might include both confirmed diagnoses of hypertension as well as consults to rule out Hypertension:</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;obs&#32;answer.png"></p>

  <h2>Searching by demographics</h2>

  <p>Select the <strong>Patient Attributes</strong>&nbsp;tab to search based on simple demographic characteristics: gender, age, birthdate, and vital status.</p>

  <p>In this example, we search for living male patients between 45 and 65 years old:&nbsp;</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;demographics.png"></p>

  <h2>Searching by encounters</h2>

  <p>Select the <strong>Encounters</strong>&nbsp;tab to search for patients based on encounters they have had. You can search by encounter type (control-click to select multiple types), location, the form with which the encounter was recorded, date ranges, and the number of matching encounters to look for.</p>

  <p>In this example we search for patients who have had at least 3 encounters whose types were either ADULTINITIAL or ADULTRETURN:</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;encounters.png"></p>

  <h2>Searching by program enrollments</h2>

  <p>Select the <strong>Program Enrollment</strong>&nbsp;tab to search for patients enrolled in a particular program, or patients who have a particular status.</p>

  <p>In this example, we search for patients who have ever been in the Hypertension Program:</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;program.png"></p>

  <h2>Combining searches</h2>

  <p>After you have done several searches, the <strong>Composition</strong>&nbsp;tab allows you to combine them using Boolean algebra.&nbsp;You can use AND, OR, NOT, or parentheses to build complex combinations of the other searches in your history. Refer to your previous searches using the number next to them in the <strong>Search History</strong> section.</p>

  <p>Here, we search for patients who match a combination of the previous example queries:</p>

  <p><img src="_booki/openmrs-guide/static/cohort&#32;builder&#32;composition_1.png"></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">OpenMRS Around the World</a></li>

  <li><a href="a-brief-history.html">A Brief History</a></li>

  <li><a href="example-amani-clinic.html">Example: Amani Clinic</a></li>

  <li class="booki-section">Planning</li>

  <li><a href="is-openmrs-for-you.html">Is OpenMRS for You?</a></li>

  <li><a href="identifying-your-needs.html">Identifying Your Needs</a></li>

  <li><a href="transitioning-to-openmrs.html">Transitioning to OpenMRS</a></li>

  <li class="booki-section">Getting Started</li>

  <li><a href="installation-and-initial-setup.html">Installation and Initial Setup</a></li>

  <li><a href="openmrs-information-model.html">OpenMRS Information Model</a></li>

  <li><a href="getting-around-the-user-interface.html">Getting Around the User Interface</a></li>

  <li class="booki-section">Configuration</li>

  <li><a href="customizing-openmrs-with-plug-in-modules.html">Customizing OpenMRS with Plug-in Modules</a></li>

  <li><a href="managing-concepts-and-metadata.html">Managing Concepts and Metadata</a></li>

  <li><a href="sharing-concepts-and-metadata.html">Sharing Concepts and Metadata</a></li>

  <li class="booki-section">Collecting Data</li>

  <li><a href="registering-patients.html">Registering Patients</a></li>

  <li><a href="data-entry.html">Data Entry</a></li>

  <li class="booki-section">Using Data</li>

  <li><a href="cohort-builder.html">Cohort Builder</a></li>

  <li><a href="reporting.html">Reporting</a></li>

  <li><a href="patient-alerts-and-flags.html">Patient Alerts and Flags</a></li>

  <li class="booki-section">Administering OpenMRS</li>

  <li><a href="user-management-and-access-control.html">User Management and Access Control</a></li>

  <li><a href="maintenance.html">Maintenance</a></li>

  <li><a href="troubleshooting.html">Troubleshooting</a></li>

  <li><a href="getting-help-from-the-openmrs-community.html">Getting Help from the OpenMRS Community</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="leaving-amani-clinic.html">Leaving Amani Clinic</a></li>

  <li><a href="about-this-book.html">About this Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-glossary.html">Appendix A: Glossary</a></li>

  <li><a href="appendix-b-example-html-form-source.html">Appendix B: Example HTML Form Source</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="cohort-builder.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="cohort-builder.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

