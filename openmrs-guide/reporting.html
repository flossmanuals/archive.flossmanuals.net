<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Implementers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-guide/openmrs-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-guide/openmrs-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Implementers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="reporting.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="reporting.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Reporting</h1>

  <p>This chapter describes how to use the <strong>Reporting</strong> module to produce a simple report on several indicators--the type you might use for monitoring and evaluating a program.</p>

  <p>Although this chapter will cover the basics, as your OpenMRS implementation grows, you'll want to take advantage of the Reporting module's additional features like:</p>

  <ul>
    <li>Multiple types of indicator-based reports,</li>

    <li>Quick ways to break down&nbsp;indicators&nbsp;based on gender, age groups, etc.,</li>

    <li>Several kinds of patient reports,</li>

    <li>The ability to schedule regular reporting,</li>

    <li>Easy formatting options for printed output using Excel templates, and</li>

    <li>An API that Java developers can extend to add custom reports, indicators, and displays.</li>
  </ul>

  <p>The module's full functionality is beyond the scope of this book. You can find further documentation on the OpenMRS Wiki:</p>

  <p><a href="http://go.openmrs.org/book-reporting">http://go.openmrs.org/book-reporting</a></p>

  <p>This chapter follows after the ones on Data Entry, because you cannot actually build reports without some data to run them on. But while planning the project you should follow the best practice of determining what <em>outputs</em>&nbsp;you want, and working backwards from there to determine the minimal set of data that you need to collect to produce those outputs.</p>

  <h2>Background and terminology</h2>

  <p>The reporting module is built around the idea of <strong>Definitions</strong> that are evaluated to produce output.</p>

  <h3>Reports and data sets&nbsp;</h3>

  <p>In general a <strong>Report Definition</strong> can have multiple <strong>Data Set Definitions</strong>. When run, this will produce a report with multiple data sets, which is rendered to a format chosen by the user.</p>

  <h3>Cohorts</h3>

  <p>Almost all reports produced with OpenMRS refer to groups of patients. A report may be run on different patient groups, or require identifying or counting sub-groups of patients. The module lets you define cohort queries (as discussed in the chapter "Cohort Builder"). When the report is run, these queries will be evaluated to produce actual cohorts of patients.</p>

  <h3>Indicators</h3>

  <p>In this chapter, we look at a report that is based on <strong>Indicators</strong>, and specifically indicators that look at the count of patients in a cohort in a period of time.</p>

  <h3>Parameters and mapping</h3>

  <p>Unlike in the OpenMRS Cohort Builder, reports and their underlying queries are intended to be created once, and reused. To support this idea, reports and queries usually take parameters. For example, a report intended to be run monthly would have <strong>Start Date</strong>&nbsp;and <strong>End Date</strong>&nbsp;parameters, and the user would be asked for these when they generate the report.</p>

  <p>The underlying queries in the report also typically take parameters. If the report is going to display the number of patients enrolled in the Child Nutrition Study at the end of a given month, it would need to have an underlying Cohort Query for "patients enrolled in Child Nutrition Study on a date". That date would be an&nbsp;<strong>Effective Date</strong>&nbsp;parameter.</p>

  <p>When the user runs the report, they are asked for a <strong>Start Date</strong>&nbsp;and an <strong>End Date</strong>, but they are not&nbsp;asked to specify an <strong>Effective Date</strong>. When designing the report, you will need to define how parameters in the underlying queries obtain their values, based on the values provided by the user when running the report. This process is called mapping.</p>

  <p>The idea of mapping parameters is complicated. The following resources include more information about why it is necessary, and how to do it:</p>

  <ul>
    <li><a href="http://go.openmrs.org/book-mapping">http://go.openmrs.org/book-mapping</a></li>

    <li><a href="http://go.openmrs.org/book-mapvid">http://go.openmrs.org/book-mapvid</a></li>
  </ul>

  <h2>Amani Clinic's weekly report</h2>

  <p><img src="_booki/openmrs-guide/static/case-study.png" align="right">Before adopting OpenMRS, Amani Clinic used to spend significant time at the end of every month tabulating paper registers and patient charts to produce a monthly report for the Ministry of Health. When planning their OpenMRS implementation, they decided that to improve their program, they needed more immediate feedback. The clinic and&nbsp;Ministry&nbsp;of Health met and decided on five indicators on which they wanted a report every week. They modified their paper data collection forms to make sure that they were capturing the right data to produce those indicators, as well as the periodic Ministry of Health reports.</p>

  <p>We'll focus on two of the indicators they calculated:</p>

  <ol>
    <li>Number of female patients seen during the week, and</li>

    <li>The percentage of those who were&nbsp;&gt;16 years old, not pregnant, and&nbsp;using appropriate family planning&nbsp;</li>
  </ol>

  <h2>Defining the underlying cohort queries</h2>

  <p>Calculating the first of those indicators was very straightforward: they defined this to be any female patient having an encounter between the start and end of the week.</p>

  <p>The second indicator was more complicated: they had to break down both the numerator and the denominator into multiple Cohort Queries. For the denominator they needed:</p>

  <ul>
    <li>Not pregnant ("no obs for Estimated Date of Confinement with a value in the future")</li>

    <li>Female</li>

    <li>Age &gt; 16 at the end of the week</li>

    <li>Had an encounter during the week (same as the query for the first indicator)</li>
  </ul>

  <p>The numerator required just one more Cohort Query, for patients who self-reported use of&nbsp;contraceptive methods other than "Natural Planning / Rhythm" during the week.</p>

  <h2>Building the report in the user interface</h2>

  <p>Having determined how to calculate their indicators, they proceeded to build them in the Reporting module's user interface. First, they build the low-level queries [1]. They then composed the two indicator definitions [2] from those cohort queries. Finally, they created a report definition [3] that included the two indicators.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;admin.png"></p>

  <h2>Building cohort queries</h2>

  <p>The <strong>Cohort Query</strong> management page shows you the different types of queries available. Clicking on any of the <strong>[+]</strong> links lets you create a new query of that type.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;cohort&#32;queries.png"></p>

  <p>The simplest query built by Amani Clinic included only female patients:</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;females.png"></p>

  <p>The rest of the queries needed to include parameters. For example, the query to find patients with any encounter between two given dates, the "on or after" and "on or before" fields were set as a <strong>Parameter</strong> [1] and a user-friendly names "Start Date" and "End Date" were provided.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;encounters.png"></p>

  <p>Some of the queries built in this example included parameters that weren't directly equivalent to the <strong>Start Date</strong>&nbsp;and <strong>End Date</strong>&nbsp;of the report. The "not pregnant" query was a <strong>Date Observation Query</strong>&nbsp;that included a single parameter, which they later mapped to the <strong>End Date</strong>&nbsp;of the report.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;pregnant.png"></p>

  <h3>Combining cohort queries</h3>

  <p>After Amani Clinic created the underlying queries that their report required, they built several Composition Cohort Queries to tie them together.&nbsp;The most complicated query calculated the denominator of the second indicator, "non-pregnant women, age &gt; 16, seen during the week".</p>

  <p>This is their composition query, which includes the two parameters&nbsp;<strong>Start Date</strong>&nbsp;and&nbsp;<strong>End Date</strong>.&nbsp;It includes four underlying queries, with values in those queries mapped to these two parameters. Finally, the queries are combined by AND-ing them all together.&nbsp;</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;composition.png"></p>

  <p>Here, we see the seven cohort queries they built:</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;all&#32;cohort&#32;queries.png"></p>

  <h3>Indicators</h3>

  <p>Having built cohort queries to do the underlying calculations, they used these to build the two indicators. The <strong>Indicators</strong> page is accessed from the <strong>Manage Report Definitions</strong>&nbsp;section of the <strong>Administration</strong> page.</p>

  <p>Since indicators are generally calculated over a time period, at a particular location, the indicators they created contain the default <strong>Start Date</strong>, <strong>End Date</strong>, and <strong>Location</strong>&nbsp;parameters. (Since the Amani Clinic was only managing a single site in OpenMRS, they ignored the <strong>Location</strong>&nbsp;parameter.)</p>

  <h4>Count indicators</h4>

  <p>The simplest type of indicator is a <strong>Count</strong>&nbsp;indicator, which counts the number of patients who match a Cohort Query.</p>

  <p>They used a <strong>Count</strong> indicator to build their first indicator, shown below. The underlying cohort query is a composition query including "Females" and "Any Encounter Between Dates".&nbsp;</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;seen&#32;indicator.png"></p>

  <h4>Fraction indicators</h4>

  <p>The most useful type of indicator for monitoring program progress is the <strong>Fraction</strong>&nbsp;indicator, which takes two cohort definitions representing a numerator and a denominator, and displays this as a fraction. (It ensures that the numerator patients are a subset of the denominator.)</p>

  <p>Amani Clinic built their second indicator as a fraction indicator.&nbsp;The underlying cohort query for the numerator was a simple Coded Observation Query, while the denominator was the Composition Query described above.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;indicator&#32;contraception.png"></p>

  <h3>Period indicator report</h3>

  <p>Having created their indicators, they built a report that combined them. They used a <strong>Period Indicator Report</strong>, which a simple way to show the indicators you have already defined.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;indicator&#32;report.png"></p>

  <h2>Running the report</h2>

  <p>To run this report, the Amani Clinic data manager clicks the <strong>Reporting</strong>&nbsp;link on the top of the screen and selects the <strong>Program Monitoring Report</strong>.&nbsp;They must enter the start and end date of the week for which to generate the report.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;run.png"></p>

  <p>The output of the report includes clickable links to the lists of patients matching each indicator.</p>

  <p><img src="_booki/openmrs-guide/static/report&#32;run&#32;results.png"></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">OpenMRS Around the World</a></li>

  <li><a href="a-brief-history.html">A Brief History</a></li>

  <li><a href="example-amani-clinic.html">Example: Amani Clinic</a></li>

  <li class="booki-section">Planning</li>

  <li><a href="is-openmrs-for-you.html">Is OpenMRS for You?</a></li>

  <li><a href="identifying-your-needs.html">Identifying Your Needs</a></li>

  <li><a href="transitioning-to-openmrs.html">Transitioning to OpenMRS</a></li>

  <li class="booki-section">Getting Started</li>

  <li><a href="installation-and-initial-setup.html">Installation and Initial Setup</a></li>

  <li><a href="openmrs-information-model.html">OpenMRS Information Model</a></li>

  <li><a href="getting-around-the-user-interface.html">Getting Around the User Interface</a></li>

  <li class="booki-section">Configuration</li>

  <li><a href="customizing-openmrs-with-plug-in-modules.html">Customizing OpenMRS with Plug-in Modules</a></li>

  <li><a href="managing-concepts-and-metadata.html">Managing Concepts and Metadata</a></li>

  <li><a href="sharing-concepts-and-metadata.html">Sharing Concepts and Metadata</a></li>

  <li class="booki-section">Collecting Data</li>

  <li><a href="registering-patients.html">Registering Patients</a></li>

  <li><a href="data-entry.html">Data Entry</a></li>

  <li class="booki-section">Using Data</li>

  <li><a href="cohort-builder.html">Cohort Builder</a></li>

  <li><a href="reporting.html">Reporting</a></li>

  <li><a href="patient-alerts-and-flags.html">Patient Alerts and Flags</a></li>

  <li class="booki-section">Administering OpenMRS</li>

  <li><a href="user-management-and-access-control.html">User Management and Access Control</a></li>

  <li><a href="maintenance.html">Maintenance</a></li>

  <li><a href="troubleshooting.html">Troubleshooting</a></li>

  <li><a href="getting-help-from-the-openmrs-community.html">Getting Help from the OpenMRS Community</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="leaving-amani-clinic.html">Leaving Amani Clinic</a></li>

  <li><a href="about-this-book.html">About this Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-glossary.html">Appendix A: Glossary</a></li>

  <li><a href="appendix-b-example-html-form-source.html">Appendix B: Example HTML Form Source</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="reporting.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="reporting.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

