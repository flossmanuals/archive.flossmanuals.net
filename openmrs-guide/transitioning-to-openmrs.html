<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Implementers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-guide/openmrs-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-guide/openmrs-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Implementers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="transitioning-to-openmrs.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="transitioning-to-openmrs.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Transitioning to OpenMRS</h1>

  <p><img src="_booki/openmrs-guide/static/transitioning.png"></p>

  <address>
    A paper-based patient register book at an African OpenMRS clinic.
  </address>

  <p>This chapter outlines steps that typically make up a OpenMRS project, and should be read by people about to embark on a OpenMRS project.&nbsp;Some of this information may be obvious to experienced project managers.&nbsp;A comprehensive guide to project management is beyond the scope of this book, but we have included some high-level process considerations to get you started thinking about what needs to happen.</p>

  <p>We recommend you try to build a structured implementation process. It's important to plan carefully--the decisions you make during this process require substantial investments of resources, and you will be living with your choice for the foreseeable future.</p>

  <p>When you start out on a new OpenMRS project, you should spend time thinking about (at minimum):</p>

  <ul>
    <li>Which people will be involved in the project</li>

    <li>Business goals of using OpenMRS</li>

    <li>How you will approach the initial configuration</li>

    <li>What ongoing support you will need</li>

    <li>Costs associated with ICT infrastructure</li>

    <li>Training and documentation</li>

    <li>Change management</li>
  </ul>

  <h2>People and the project team</h2>

  <p>Your project implementation team should include clinic staff:</p>

  <ol>
    <li><strong>Management</strong>&nbsp;are aware of funding obligations and 3rd party reporting requirements.</li>

    <li><strong>Health care providers</strong>&nbsp;are focused on improving patient care.</li>

    <li><strong>Administrative staff</strong>&nbsp;are specialists of workflow issues and clinic processes.</li>
  </ol>

  <p>The team could also include the following people that may or may not be from the clinic:</p>

  <ol>
    <li>A <strong>system administrator</strong>&nbsp;is in charge of installing and maintaining OpenMRS inside of the clinic's ICT infrastructure.</li>

    <li><strong>Medical informatics expert(s)</strong>&nbsp;create clinical documentation and ensure that data is managed properly in the system. Develop reports.</li>

    <li>(Optional) A <strong>project manager or coordinator</strong>. For larger implementations, this person works to hold people accountable to finishing their work in a timely manner, and ensures the project is on track.</li>

    <li>(Optional)&nbsp;<strong>Software developers</strong> may be needed&nbsp;for locations that decide to customize the system.</li>
  </ol>

  <p>It's very important to include clinical staff (for example nurses, data entry clerks, and others) in your implementation team from the earliest phases of the project so that the resulting deployment is ultimately useful for them and easy for them to use.</p>

  <p>Managing an OpenMRS project will require a major time investment from people within your organization, even if you employ an external consultant. Organizations often under-estimate the amount of time that will be required from their staff in implementing an enterprise ICT project. This time investment includes items such as training, modifying existing processes, and providing new or updated information to relevant people. Deploying OpenMRS is no different. It's not something that can be added to the end of an already busy schedule--we urge you to keep this in mind and take it into consideration when planning.</p>

  <h2>Goals</h2>

  <p>By this point in the project, you should have a good idea of what indicates a successful OpenMRS implementation for your clinic. This could be something like reduction of time to prepare month-end reports by 50%, or increase antiretroviral treatment (ART) in HIV-infected pregnant women by 25%. Your goals should be specific, measurable, attainable, relevant, timely--or SMART.</p>

  <p>These goals will help you in directing and managing your project. For example, if the project group wants some customization that requires budget and effort, your overall goals will help you decide whether or not to consider that customization. Your goals will help you to focus on why you are implementing OpenMRS and what you want to achieve in the long run.</p>

  <h2>Incremental adoption</h2>

  <p>It often makes sense to divide the implementation process into smaller, more manageable sections, which can be implemented in discrete stages or iterations.&nbsp;Implementing in stages allows people to get used to changes gradually without feeling overwhelmed, and allows your implementation team to be responsive to feedback from users during the process.</p>

  <p>Another reason people choose to develop iteratively is that it is very hard for users to correctly or fully explain their requirements at the beginning of the project. Giving people hands-on experience of an early version of the system helps them understand how it works and what might be possible. They can then provide you with valuable feedback, and they might articulate requirements that they not previously identified.</p>

  <p><img src="_booki/openmrs-guide/static/case-study.png" align="right">The Amani Clinic chose to introduce change iteratively. First they started using the system for patient registration. This affected only the administrative staff without impacting the clinical staff. Later they started doing retrospective data entry, which included paper forms for clinicians that had minor changes, as well as training a new data entry clerk.</p>

  <h3>Pilot projects</h3>

  <p>Larger multi-site implementations may wish to develop a pilot approach to help reduce risk. In this scenario, you would only deploy OpenMRS at one site and learn about the process in a more controlled way. You can then incorporate what you've learned into a&nbsp;coordinated&nbsp;implementation process for other sites.</p>

  <h2>Ongoing support and development</h2>

  <p>It is a mistake to think about an OpenMRS project as a one-off installation that will meet the needs of your organization for the foreseeable future. Organizations are always changing and evolving. Your medical record system should evolve with you, otherwise it will eventually become out-of-sync with the organization.</p>

  <p>Once you have been using OpenMRS for a while and staff are comfortable with it, you will likely want to take advantage of additional functionality. Each improvement or new piece of functionality that you decide to implement in OpenMRS will take resources, so you'll want to plan ahead for these.</p>

  <p>Even if your organizational needs don't change, you need to plan for ongoing support of OpenMRS, including:</p>

  <ul>
    <li>Keeping your system up-to-date with security patches</li>

    <li>Upgrading to the latest version of OpenMRS (not always necessary, but OpenMRS is improving all the time and your users will thank you for the improved usability and functionality each time you upgrade)</li>

    <li>Upgrading the modules you use to fix bugs and improve features</li>

    <li>Maintenance of your server and network infrastructure</li>
  </ul>

  <p>For more information, see the "Maintenance" chapter.</p>

  <h2>Training</h2>

  <p>Training is also an important part of any OpenMRS implementation project. Your training could take many forms depending on the needs of your users, but it often makes sense to spend resources (e.g., time and money) on formal and reusable training resources such as user guides, lesson plans, and other materials.</p>

  <p>Trying to cover everything in one training session probably won't be effective. People will want and need time to digest the new ideas they learn and use them in their daily work, and you must anticipate staff turnover. Instead, consider holding smaller training sessions that introduce concepts and specific functionality, followed by periods of testing, piloting and feedback. Customize your training for your audience--not everyone needs to sit through a two-hour training session on data entry if only a single person is responsible for this role. When possible, train people to become trainers. This increases peoples' sense of ownership in your OpenMRS implementation, and helps people to better remember what they learn.</p>

  <p>Training is an ongoing process. New employees will need to be trained when they start, and people familiar with the system can benefit from learning about more advanced topics. Sometimes, people will need further training when there are significant upgrades or new functionality is added to OpenMRS or a module you use.</p>

  <h2>Change management</h2>

  <p>Introducing an electronic medical record system will cause changes in workflow and processes at your organization. These changes may be "political" and cause challenges in your organization, or they may be more practical and technical changes. Either way, too much change at the same time is often difficult and stressful.</p>

  <p>To help, give people time to accept and support each change so that they share in ownership of the new system, rather than feeling as if something has been forced on them. Focus on simple tasks at the beginning of deployment and introduce more difficult tasks as people start to better understanding OpenMRS. Show staff how the new system will make their work easier and where their feedback has been incorporated.</p>

  <p>Good planning can minimize the risks around change, but it is important to be flexible within your plan. Unforeseen things often occur, and a plan that is too rigid could prevent you from reaching the best solution.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">OpenMRS Around the World</a></li>

  <li><a href="a-brief-history.html">A Brief History</a></li>

  <li><a href="example-amani-clinic.html">Example: Amani Clinic</a></li>

  <li class="booki-section">Planning</li>

  <li><a href="is-openmrs-for-you.html">Is OpenMRS for You?</a></li>

  <li><a href="identifying-your-needs.html">Identifying Your Needs</a></li>

  <li><a href="transitioning-to-openmrs.html">Transitioning to OpenMRS</a></li>

  <li class="booki-section">Getting Started</li>

  <li><a href="installation-and-initial-setup.html">Installation and Initial Setup</a></li>

  <li><a href="openmrs-information-model.html">OpenMRS Information Model</a></li>

  <li><a href="getting-around-the-user-interface.html">Getting Around the User Interface</a></li>

  <li class="booki-section">Configuration</li>

  <li><a href="customizing-openmrs-with-plug-in-modules.html">Customizing OpenMRS with Plug-in Modules</a></li>

  <li><a href="managing-concepts-and-metadata.html">Managing Concepts and Metadata</a></li>

  <li><a href="sharing-concepts-and-metadata.html">Sharing Concepts and Metadata</a></li>

  <li class="booki-section">Collecting Data</li>

  <li><a href="registering-patients.html">Registering Patients</a></li>

  <li><a href="data-entry.html">Data Entry</a></li>

  <li class="booki-section">Using Data</li>

  <li><a href="cohort-builder.html">Cohort Builder</a></li>

  <li><a href="reporting.html">Reporting</a></li>

  <li><a href="patient-alerts-and-flags.html">Patient Alerts and Flags</a></li>

  <li class="booki-section">Administering OpenMRS</li>

  <li><a href="user-management-and-access-control.html">User Management and Access Control</a></li>

  <li><a href="maintenance.html">Maintenance</a></li>

  <li><a href="troubleshooting.html">Troubleshooting</a></li>

  <li><a href="getting-help-from-the-openmrs-community.html">Getting Help from the OpenMRS Community</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="leaving-amani-clinic.html">Leaving Amani Clinic</a></li>

  <li><a href="about-this-book.html">About this Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-glossary.html">Appendix A: Glossary</a></li>

  <li><a href="appendix-b-example-html-form-source.html">Appendix B: Example HTML Form Source</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="transitioning-to-openmrs.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="transitioning-to-openmrs.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

