<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Implementers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-guide/openmrs-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-guide/openmrs-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Implementers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="identifying-your-needs.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="identifying-your-needs.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Identifying Your Needs</h1>

  <p><img src="_booki/openmrs-guide/static/needs.png"></p>

  <address>
    Discussing requirements and needs at TRAC Plus clinic in Kigali.
  </address>

  <p>This chapter covers some basic strategies for identifying your organizational needs, and how OpenMRS might help. It does not go into detail about what OpenMRS does or how it stores data -- you will find that in other chapters. Instead, we encourage you to first take a step back and think about your organization.</p>

  <h2>Your organizational goals and practices</h2>

  <p>For now, forget about technology and instead think about your organizational goals and processes. Here's a list of questions to start:</p>

  <ul>
    <li>What are the high level goals of your organization?</li>

    <li>What are the teams and staff in the clinic? What roles exists? What functions does each role perform?</li>

    <li>What tasks are staff involved with on a day to day basis?</li>

    <li>What services does the clinic provide to your patients? What activities are involved?</li>

    <li>What other 3rd-party or government organizations do you report to? What information is included in each of these reports?</li>
  </ul>

  <p>Answering these questions will probably help you think of more related questions. Make sure you consider them thoroughly.</p>

  <h2>Take advantage of institutional knowledge</h2>

  <p>As you think about your patients and how they interact with your organization, talk to your clinical and administrative staff--both those who have been around a long time, and those who have just joined. Talk to as many people as you possibly can to get a complete picture of every service provided to patients.</p>

  <p>People generally want to be positive in describing their work places, so you may need to ask some people multiple times. Get physical or electronic copies, or pictures of all paper forms if possible. Figure out where (e.g., specific rooms and desks) data is recorded onto paper and by whom. Write an overview of current practices and define specific shortcomings that could be addressed by using an electronic medical records system.</p>

  <p>Note that practices may vary seasonally, for example if the hospital is much busier due to increased malaria during rainy season or malnutrition before harvest.</p>

  <h2>Map your needs to OpenMRS</h2>

  <p>OpenMRS has been designed to be flexible and adaptable, based on input from many different partners, but it may not be an exact fit for the ways that your organization currently works. Doing things the "OpenMRS way" could mean adapting your workflow and adopting best practices in medical informatics. Be pragmatic and flexible, and think about whether your current working practices might need to change.</p>

  <p>Remember that OpenMRS offers many opportunities to capture and analyze information in new ways not previously possible. Taking advantage of these new possibilities might possibly lead to positive changes and improvements for your organization.</p>

  <h2>Do not "reinvent the wheel"</h2>

  <p>The open source ethos of OpenMRS extends beyond just the application, to a much larger open community where ideas and experiences are shared.&nbsp;There are many existing resources available in the form of pre-built OpenMRS features (modules) and content that a new implementer should take advantage of. You should explore the following resources before building anything new.</p>

  <h3>Reuse an existing concept dictionary</h3>

  <p>A well-constructed, mature concept dictionary (see the "OpenMRS Information Model" chapter) is a strong foundation for any OpenMRS Implementation.</p>

  <p>The Millenium Villages Project&nbsp;(MVP) maintains a well-curated concept dictionary. If this dictionary is&nbsp;applicable to your domain of care, you should strongly consider using it. The best way to learn about this dictionary is through a partner project, the Maternal Concept Lab.</p>

  <p><a href="http://go.openmrs.org/book-mcl">http://go.openmrs.org/book-mcl</a></p>

  <p>Other OpenMRS implementers can also help advise you about other concept references for your domain. Read the "Getting Help from the OpenMRS Community" chapter for more information.</p>

  <h3>Adapt existing forms</h3>

  <p>Implementers should evaluate data collection forms built by other OpenMRS users before creating new custom forms for their specific needs.</p>

  <p>Implementers across the OpenMRS community have invested a lot of resources in ensuring that their forms reflect clinical best practices, international standards, and current research. These forms have already been optimized for electronic data entry. Many OpenMRS partners develop forms using medical informatics experts that may not be available to all projects. Finally, creating forms is time consuming--those resources could be redirected to other efforts.</p>

  <p>The OpenMRS Form Bank is a new community-driven project which is beginning to collect existing forms from other users. Visit&nbsp;<a href="http://go.openmrs.org/book-formbank">http://go.openmrs.org/book-formbank</a>&nbsp;for details, or contact other implementers for help. Read the "Getting Help from the OpenMRS Community" chapter for more information.</p>

  <h3>Explore the module repository</h3>

  <p>Implementers should consult the OpenMRS Module Repository at <a href="http://modules.openmrs.org">http://modules.openmrs.org/</a>&nbsp;before considering customization through software development.</p>

  <p>There is a good chance that someone has created a module to address needs you may have. Read the "Customizing OpenMRS with Plug-in Modules" chapter for a list of recommended modules.</p>

  <h2>Amani discovers their specific needs</h2>

  <p><img src="_booki/openmrs-guide/static/case-study.png" align="right">Once the clinic determined they would indeed use OpenMRS, they began thinking specifically about how they would integrate their existing processes into the workflow supported by the software. As the newly-hired medical informatics manager, Claudine knew she should speak with everyone working in the clinic and watch them during a typical day to understand how they work. When she spoke to them, she assured them that OpenMRS would help to make their work easier, and they would still be using the same overall processes they were familiar with.</p>

  <p>Claudine found many resources within the OpenMRS community, including pre-existing concept dictionaries and forms that had been used in other clinics. She was able to take these artifacts and adapt them to Amani's paper forms that were already in use. Starting out with the work of others saved quite a bit of time.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">OpenMRS Around the World</a></li>

  <li><a href="a-brief-history.html">A Brief History</a></li>

  <li><a href="example-amani-clinic.html">Example: Amani Clinic</a></li>

  <li class="booki-section">Planning</li>

  <li><a href="is-openmrs-for-you.html">Is OpenMRS for You?</a></li>

  <li><a href="identifying-your-needs.html">Identifying Your Needs</a></li>

  <li><a href="transitioning-to-openmrs.html">Transitioning to OpenMRS</a></li>

  <li class="booki-section">Getting Started</li>

  <li><a href="installation-and-initial-setup.html">Installation and Initial Setup</a></li>

  <li><a href="openmrs-information-model.html">OpenMRS Information Model</a></li>

  <li><a href="getting-around-the-user-interface.html">Getting Around the User Interface</a></li>

  <li class="booki-section">Configuration</li>

  <li><a href="customizing-openmrs-with-plug-in-modules.html">Customizing OpenMRS with Plug-in Modules</a></li>

  <li><a href="managing-concepts-and-metadata.html">Managing Concepts and Metadata</a></li>

  <li><a href="sharing-concepts-and-metadata.html">Sharing Concepts and Metadata</a></li>

  <li class="booki-section">Collecting Data</li>

  <li><a href="registering-patients.html">Registering Patients</a></li>

  <li><a href="data-entry.html">Data Entry</a></li>

  <li class="booki-section">Using Data</li>

  <li><a href="cohort-builder.html">Cohort Builder</a></li>

  <li><a href="reporting.html">Reporting</a></li>

  <li><a href="patient-alerts-and-flags.html">Patient Alerts and Flags</a></li>

  <li class="booki-section">Administering OpenMRS</li>

  <li><a href="user-management-and-access-control.html">User Management and Access Control</a></li>

  <li><a href="maintenance.html">Maintenance</a></li>

  <li><a href="troubleshooting.html">Troubleshooting</a></li>

  <li><a href="getting-help-from-the-openmrs-community.html">Getting Help from the OpenMRS Community</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="leaving-amani-clinic.html">Leaving Amani Clinic</a></li>

  <li><a href="about-this-book.html">About this Book</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-glossary.html">Appendix A: Glossary</a></li>

  <li><a href="appendix-b-example-html-form-source.html">Appendix B: Example HTML Form Source</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="identifying-your-needs.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="identifying-your-needs.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

