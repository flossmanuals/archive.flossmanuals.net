<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>How to Bypass Internet Censorship</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/bypassing-censorship/bypassing-censorship.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/bypassing-censorship/bypassing-censorship.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	How to Bypass Internet Censorship
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Introduction</h1>

  <p>On 10 December 1948, the adoption by the General Assembly of the Universal Declaration of Human Rights launched a new era. Lebanese scholar Charles Habib Malik described it to the assembled delegates as follows:</p>

  <blockquote>
    <p><em>Every member of the United Nations has solemnly pledged itself to achieve respect for and observance of human rights. But, precisely what these rights are we were never told before, either in the Charter or in any other national instrument. This is the first time the principles of human rights and fundamental freedoms are spelled out authoritatively and in precise detail. <strong>I now know what my government pledged itself to promote, achieve, and observe. … I can agitate against my government, and if she does not fulfill her pledge, I shall have and feel the moral support of the entire world.</strong></em></p>
  </blockquote>

  <p>One of the fundamental rights the Universal Declaration described, in Article 19, was the right to freedom of speech:</p>

  <blockquote>
    <p><em>Everyone has the right to freedom of opinion and expression; this right includes freedom to hold opinions without interference and <strong>to seek, receive, and impart information and ideas through any media and regardless of frontiers.</strong></em></p>
  </blockquote>

  <p>When those words were written sixty years ago, no one imagined how the global phenomenon of the Internet would expand people's ability to "seek, receive and impart information", not only across borders but at amazing speeds and in forms that can be copied, edited, manipulated, recombined and shared with small or large audiences in ways fundamentally different than the communications media available in 1948.</p>

  <h2>More information in more places than ever imagined</h2>

  <p>The unbelievable growth in the past several years of what is on the Internet and where it is available has the effect of making an unimaginably vast portion of human knowledge and activity suddenly present in unexpected places: a hospital in a remote mountain village, your 12-year-old's bedroom, the conference room where you are showing your closest colleagues the new product design that will put you ahead of the competition, your grandmother's house.</p>

  <p>In all of these places, the possibility of connecting to the world opens up many wonderful opportunities for improving people's lives. When you contract a rare disease on vacation, the remote village hospital may save your life by sending your test results to a medical specialist in the capital, or even in another country; your 12-year-old can research her school project or make friends with kids in other countries; you can present your new product design simultaneously to top managers in offices around the world, who can help you improve it; your grandmother can send you her special apple pie recipe by e-mail in time for you to bake it for dessert tonight.</p>

  <p>But the Internet does not contain only relevant and helpful educational information, friendship and apple pie. Like the world itself, it is vast, complex and often scary. It is just as available to people who are malicious, greedy, unscrupulous, dishonest or merely rude as it is to you and your 12-year-old child and your grandmother.</p>

  <h2>Not everyone wants to let the whole world in</h2>

  <p>With all of the best and worst of human nature reflected on the Internet and certain kinds of deception and harassment made much easier by the technology, it should not surprise anyone that the growth of the Internet has been paralleled by attempts to control how people use it. There are many different motivations for these attempts. The goals include:</p>

  <ul>
    <li>Protecting children from material perceived as inappropriate, or limiting their contact with people who may harm them.</li>

    <li>Reducing the barrage of unwanted commercial offers by e-mail or on the Web.</li>

    <li>Controlling the size of the flow of data any one user is able to access at one time.</li>

    <li>Preventing employees from sharing information that is viewed as the property of their employer, or from using their work time or an employer's technical resources for personal activities.</li>

    <li>Restricting access to materials or online activities that are banned or regulated in a specific jurisdiction (for example a country or an organization like a school) such as explicit sexual or violent materials, drugs or alcohol, gambling and prostitution, and information about religious, political or other groups or ideas that are deemed to be dangerous.</li>
  </ul>

  <p>Some of these concerns involve allowing people to control <em>their own</em> experience of the Internet (for instance, letting people use spam-filtering tools to prevent spam from being delivered to their own e-mail accounts), but others involve restricting how <em>other people</em> can use the Internet and what those <em>other people</em> can and can't access. The latter case causes significant conflicts and disagreements when the people whose access is restricted don't agree that the blocking is appropriate or in their interest.</p>

  <h2>Who is filtering or blocking the Internet?</h2>

  <p>The kinds of people and institutions who try to restrict the Internet use of specific people are as varied as their goals. They include parents, schools, commercial companies, operators of Internet cafés or Internet Service Providers (ISPs), and governments at different levels.</p>

  <p>The extreme end of the spectrum of Internet control is when a national government attempts to restrict the ability of its entire population to use the Internet to access whole categories of information or to share information freely with the outside world. Research by the OpenNet Initiative (<a href="http://opennet.net/">http://opennet.net</a>) has documented the many ways that countries filter and block Internet access for their citizens. These include countries with pervasive filtering policies, who have been found to routinely block access to human rights organizations, news, blogs, and Web services that challenge the <em>status quo</em> or are deemed threatening or undesirable. Others block access to single categories of Internet content, or intermittently to specific websites or network services to coincide with strategic events, such as elections or public demonstrations. Even countries with generally strong protections for free speech sometimes try to limit or monitor Internet use in connection with suppressing pornography, so-called "hate speech", terrorism and other criminal activities, leaked military or diplomatic communications, or the infringement of copyright laws.</p>

  <h2>Filtering leads to monitoring</h2>

  <p>Any of these official or private groups may also use various techniques to monitor the Internet activity of people they are concerned about, to make sure that their attempts at restriction are working. This ranges from parents looking over their child's shoulder or looking at what sites were visited on the child's computer, to companies monitoring employees' e-mail, to law enforcement agencies demanding information from ISPs or even seizing the computer in your home looking for evidence that you have engaged in "undesirable" activities.</p>

  <h2>When is it censorship?</h2>

  <p>Depending on who is restricting access to the Internet and/or monitoring its use, and the perspective of the person whose access is being restricted, nearly any of these goals and any of the methods used to achieve them may be seen as legitimate and necessary or as unacceptable censorship and a violation of fundamental human rights. A teenage boy whose school blocks access to his favorite online games or to social networking sites such as Facebook feels his personal freedom to be abridged just as much as someone whose government prevents him from reading an online newspaper about the political opposition.</p>

  <h2>Who exactly is blocking my access to the Internet?</h2>

  <p>Who is able to restrict access to the Internet on any given computer in any given country depends on who has the ability to control specific parts of the technical infrastructure. This control may be based on legally established relationships or requirements, or on the ability of governmental or other bodies to pressure those who have legal control over the technical infrastructure to comply with requests to block, filter or collect information. Many parts of the international infrastructure that supports the Internet are under the control of governments or government-controlled agencies, any of which may assert control, in accordance with local law or not.</p>

  <p>Filtering or blocking of parts of the Internet may be heavy-handed or very light, clearly defined or nearly invisible. Some countries openly admit to blocking and publish blocking criteria, as well as replacing blocked sites with explanatory messages. Other countries have no clear standards and sometimes rely on informal understandings and uncertainty to pressure ISPs to filter. In some places, filtering comes disguised as technical failures and governments don't openly take responsibility or confirm when blocking is deliberate. Different network operators even in the same country and subject to the same regulations may execute filtering in quite different ways out of caution, technical ignorance, or commercial competition.</p>

  <p>At all levels of possible filtering, from individual to national, the technical difficulties of blocking precisely what is viewed as undesirable may have unexpected and often ridiculous consequences. "Family-friendly" filters meant to block sexual materials prevent access to useful health information. Attempts to block spam may filter out important business correspondence. Attempts to block access to specific news sites may also cut off educational resources.</p>

  <h2>What methods exist to bypass filtering?</h2>

  <p>Just as many individuals, corporations and governments see the Internet as a source of dangerous information that must be controlled, there are many individuals and groups who are working hard to ensure that the Internet, and the information on it, is freely available to everyone who wants it. These people have as many different motivations as those seeking to control the Internet. However, for someone whose Internet access is restricted and who wants to do something about it, it may not matter whether the tools were developed by someone who wanted to chat with a girlfriend, write a political manifesto, or send spam.</p>

  <p>There is a vast amount of energy, from commercial, non-profit and volunteer groups, devoted to creating tools and techniques to bypass Internet censorship, resulting in a number of methods to bypass Internet filters. Collectively, these are called&nbsp;<strong>circumvention</strong> methods, and can range from simple work-arounds, protected pathways, to complex computer programs. However, they nearly all work in approximately the same manner. They instruct your Web browser to take a detour through an intermediary computer, called a <strong>proxy</strong>, that:</p>

  <ul>
    <li>is located somewhere that is not subject to Internet censorship</li>

    <li>has not been blocked from your location</li>

    <li>knows how to fetch and return content for users like you.<br></li>
  </ul>

  <p><img src="_booki/bypassing-censorship/static/01circumvention_a.png"><br></p><img src="_booki/bypassing-censorship/static/02circumvention_b.png">&nbsp;

  <h2>What are the risks of using circumvention tools?</h2>

  <p>Only you, the person who hopes to bypass restrictions on your Internet access, can decide whether there are significant risks involved in accessing the information you want; and only you can decide whether the benefits outweigh the risks. There may be no law specifically banning the information you want or the act of accessing it. On the other hand, the lack of legal sanctions does not mean you are not risking other consequences, such as harassment, losing your job, or worse.</p>The following chapters discuss how the Internet works, describe various forms of online censorship, and elaborate on a number of tools and techniques that might help you circumvent these barriers to free expression. The overarching issue of digital privacy and security is considered throughout the book, which begins by covering the basics, then addresses a few advanced topics before closing with a brief section intended for&nbsp;webmasters and computer specialists who want to help others bypass Internet censorship.
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">QUICK START</li>

  <li><a href="ch004_quickstart-from-lg.html">QUICKSTART</a></li>

  <li class="booki-section">BACKGROUND</li>

  <li><a href="ch006_chapter-1-how.html">HOW THE NET WORKS</a></li>

  <li><a href="ch007_chapter-2-censorship.html">CENSORSHIP AND THE NET</a></li>

  <li><a href="ch008_chapter-3-safe-circumvention.html">CIRCUMVENTION AND SAFETY</a></li>

  <li class="booki-section">BASIC TECHNIQUES</li>

  <li><a href="ch010_simple-tricks.html">SIMPLE TRICKS</a></li>

  <li><a href="ch011_get-creative.html">GET CREATIVE</a></li>

  <li><a href="ch012_using-a-web-proxy.html">WEB PROXIES</a></li>

  <li><a href="ch013_using-psiphon2.html">PSIPHON</a></li>

  <li><a href="ch014_using-sabzproxy.html">SABZPROXY</a></li>

  <li class="booki-section">FIREFOX AND ITS ADD-ONS</li>

  <li><a href="ch016_introduction-to-firefox.html">INTRODUCTION TO FIREFOX</a></li>

  <li><a href="ch017_noscript.html">ADBLOCK PLUS AND NOSCRIPT</a></li>

  <li><a href="ch018_https-everywhere.html">HTTPS EVERYWHERE</a></li>

  <li><a href="ch019_proxy-settings-and-foxyproxy.html">PROXY SETTINGS AND FOXYPROXY</a></li>

  <li class="booki-section">TOOLS</li>

  <li><a href="ch021_tool-matrix.html">INTRODUCTION</a></li>

  <li><a href="ch022_freegate.html">FREEGATE</a></li>

  <li><a href="ch023_puff.html">SIMURGH</a></li>

  <li><a href="ch024_ultrasurf.html">ULTRASURF</a></li>

  <li><a href="ch025_what-is-vpn.html">VPN SERVICES</a></li>

  <li><a href="ch026_air-vpn.html">VPN ON UBUNTU</a></li>

  <li><a href="ch027_hotspot-shield.html">HOTSPOT SHIELD</a></li>

  <li><a href="ch028_alkasir.html">ALKASIR</a></li>

  <li><a href="ch029_tor-the-onion-router.html">TOR: THE ONION ROUTER</a></li>

  <li><a href="ch030_using-jon-do.html">JONDO</a></li>

  <li><a href="ch031_yourfreedom.html">YOUR-FREEDOM</a></li>

  <li class="booki-section">ADVANCED TECHNIQUES</li>

  <li><a href="ch033_playing-with-dns.html">DOMAINS AND DNS</a></li>

  <li><a href="ch034_http-proxies.html">HTTP PROXIES</a></li>

  <li><a href="ch035_command-line.html">THE COMMAND LINE</a></li>

  <li><a href="ch036_openvpn.html">OPENVPN</a></li>

  <li><a href="ch037_ssh-tunnelling.html">SSH TUNNELLING</a></li>

  <li><a href="ch038_socks-proxies.html">SOCKS PROXIES</a></li>

  <li class="booki-section">HELPING OTHERS</li>

  <li><a href="ch040_researching-and-documenting-censorship.html">RESEARCHING AND DOCUMENTING CENSORSHIP</a></li>

  <li><a href="ch041_diagnosing-port-blocking.html">DEALING WITH PORT BLOCKING</a></li>

  <li><a href="ch042_installing-web-proxies.html">INSTALLING WEB PROXIES</a></li>

  <li><a href="ch043_setting-up-a-tor-relay.html">SETTING UP A TOR RELAY</a></li>

  <li><a href="ch044_risks-of-operating-a-proxy.html">RISKS OF OPERATING A PROXY</a></li>

  <li><a href="ch045_best-practices-for-webmasters.html">BEST PRACTICES FOR WEBMASTERS</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch047_glossary.html">GLOSSARY</a></li>

  <li><a href="ch048_assessing-and-comparing-circumvention-tools.html">TEN THINGS</a></li>

  <li><a href="ch049_further-resources.html">FURTHER RESOURCES</a></li>

  <li><a href="ch050_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

