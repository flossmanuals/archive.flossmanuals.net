<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>How to Bypass Internet Censorship</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/bypassing-censorship/bypassing-censorship.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/bypassing-censorship/bypassing-censorship.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	How to Bypass Internet Censorship
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch021_tool-matrix.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch021_tool-matrix.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Introduction</h1>

  <p>The basic idea of circumventing Internet censorship is to route the requests over a third server which is not blocked and is connected to the Internet through a non filtered connection. This chapter explains some of the tools which make it possible to use such a server in order to defeat Internet blocking, filtering, and monitoring. The choice of which tool might best accomplish your objectives should be based on an initial assessment on the type of content you want to access, your available resources, and the risks of doing so.</p>

  <p>Tools to defeat Internet blocking, filtering and monitoring are designed to deal with different obstacles and threats. They may facilitate:</p>

  <ul>
    <li><strong>Circumventing censorship</strong>: enabling you to read or author content, send or receive information, or communicate with particular people, sites or services by bypassing attempts to prevent you from doing so. Similar to the operation of the Google cache or an RSS aggregator which can be used to access a blocked Web site indirectly.</li>

    <li><strong>Preventing eavesdropping</strong>: keeping communications private, so that nobody can see or hear the content of what you're communicating (even if they might still be able to see with whom you're communicating). Tools that try to circumvent censorship without also preventing eavesdropping may remain vulnerable to censorship by keyword filters that block all communications containing certain prohibited words. For example, various forms of encryption, such as HTTPS or SSH, make the information unreadable to anyone other than the sender and receiver. An eavesdropper will see which user is connecting to which Web server, but from the content he can only see a string of characters that looks like nonsense.</li>

    <li><strong>Remaining anonymous</strong>: the ability to communicate so that no one can connect you to the information or people you are connecting with – neither the operator of your Internet connection nor the sites or people with whom you're communicating. Many proxy servers and proxy tools don't offer perfect, or <em>any</em>, anonymity: the proxy operator is able to observe the traffic going into and out of the proxy and easily determine who is sending it, when they're sending it, and how often they're sending it; a malicious observer on either side of the connection is able to gather the same information. Tools like Tor are designed to make it difficult for attackers to gather this kind of information about users by limiting the amount of information any node in the network can have about the user's identity or location.</li>

    <li><strong>Concealing what you are doing</strong>: disguising the communications you send so that someone spying on you will not be able to tell that you are trying to circumvent censorship. For example, steganography, the hiding of text messages within an ordinary image file, may conceal that you are using a circumvention tool at all. Using a network with many kinds of users means that an adversary can not tell what you are doing because of your choice of software. This is especially good when others are using the same system to get to uncontroversial content.</li>
  </ul>

  <p>Some tools protect your communications in only one of these ways. For example, many proxies can circumvent censorship but don't prevent eavesdropping. It's important to understand that you may need a combination of tools to achieve your goal.</p>

  <p>Each kind of protection is relevant to different people in different situations. When you choose tools that bypass Internet censorship, you should keep in mind what kind of protection you need and whether the particular set of tools you're using can provide that sort of protection. For example, what will happen if someone detects that you are attempting to circumvent a censorship system? Is accessing your main concern, or do you need to remain anonymous while doing so?</p>

  <p>Sometimes, one tool can be used to defeat censorship and protect anonymity, but the steps for each are different. For instance, Tor software is commonly used for both purposes, but Tor users who are most concerned with one or the other will use Tor differently. For anonymity reasons, it is important that you use the Web browser bundled with Tor, since it has been modified to prevent leaking of your real identity.</p>

  <h2>An important warning</h2>

  <p>Most circumvention tools can be detected with sufficient effort by network operators or government agencies, since the traffic they generate may show distinctive patterns. This is certainly true for circumvention methods that don't use encryption, but it can also be true for methods that do. It's very difficult to keep secret the fact that you're using technology to circumvent filtering, especially if you use a fairly popular technique or continue using the same service or method for a long period of time. Also, there are ways to discover your behavior that do not rely on technology: in-person observation, surveillance, or many other forms of traditional human information-gathering.</p>

  <p>We cannot provide specific advice on threat analysis or the choice of tools to meet the threats. The risks are different in each situation and country, and change frequently. You should always expect that those attempting to restrict communications or activities will continue to improve their methods.</p>

  <p>If you are doing something that may put you at risk in the location where you are, you should make your own judgments about your security and (if possible) consult experts.</p>

  <ul>
    <li>Most often, you will have to rely on a service provided by a stranger. Be aware that they may have access to information about where you are coming from, the sites you are visiting and even the passwords you enter on unencrypted Web sites. Even if you know and trust the person running a single-hop proxy or VPN, they may be hacked or forced to compromise your information.</li>

    <li>Remember that the promises of anonymity and security made by different systems may not be accurate. Look for independent confirmation. Open source tools can be evaluated by tech-savvy friends. Security flaws in open source tools can be discovered and fixed by volunteers. It is difficult to do the same with proprietary software.</li>

    <li>Achieving anonymity or security may require you to be disciplined and carefully obey certain security procedures and practices. Ignoring security procedures may dramatically reduce the security protections you receive. It is dangerous to think that it is possible to have a "one click solution" for anonymity or security. For instance, routing your traffic through a proxy or through Tor is not enough. Be sure to use encryption, keep your computer safe and avoid leaking your identity in the content you post.</li>

    <li>Be aware that people (or governments) may set up honeypots – fake Web sites and proxies that pretend to offer secure communication or censorship circumvention but actually capture the communications from unwitting users.</li>

    <li>Sometimes even "Policeware" may be installed on users' computers – either remotely or directly – that acts like malware, monitoring all activities on the computer even when it is not connected to the Internet and undermining most other preventive security measures.</li>

    <li>Pay attention to non-technical threats. What happens if someone steals your computer or mobile phone or that of your best friend? What if an Internet café staff member looks over your shoulder or points a camera to your screen or keyboard? What happens if someone sits down at a computer in a café somewhere where your friend has forgotten to log out and sends you a message pretending to be from her? What if someone in your social network is arrested and forced to give up passwords?</li>

    <li>If there are laws or regulations that restrict or prohibit the materials you are accessing or the activities you are undertaking, be aware of the possible consequences.</li>
  </ul>

  <p>To learn more about digital security and privacy, read:</p>

  <p><a href="http://www.frontlinedefenders.org/manual/en/esecman/intro.html" target="_top">http://www.frontlinedefenders.org/manual/en/esecman/intro.html</a><br>
  <a href="http://security.ngoinabox.org/html/en/index.html" target="_top">http://security.ngoinabox.org/html/en/index.html</a>&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">QUICK START</li>

  <li><a href="ch004_quickstart-from-lg.html">QUICKSTART</a></li>

  <li class="booki-section">BACKGROUND</li>

  <li><a href="ch006_chapter-1-how.html">HOW THE NET WORKS</a></li>

  <li><a href="ch007_chapter-2-censorship.html">CENSORSHIP AND THE NET</a></li>

  <li><a href="ch008_chapter-3-safe-circumvention.html">CIRCUMVENTION AND SAFETY</a></li>

  <li class="booki-section">BASIC TECHNIQUES</li>

  <li><a href="ch010_simple-tricks.html">SIMPLE TRICKS</a></li>

  <li><a href="ch011_get-creative.html">GET CREATIVE</a></li>

  <li><a href="ch012_using-a-web-proxy.html">WEB PROXIES</a></li>

  <li><a href="ch013_using-psiphon2.html">PSIPHON</a></li>

  <li><a href="ch014_using-sabzproxy.html">SABZPROXY</a></li>

  <li class="booki-section">FIREFOX AND ITS ADD-ONS</li>

  <li><a href="ch016_introduction-to-firefox.html">INTRODUCTION TO FIREFOX</a></li>

  <li><a href="ch017_noscript.html">ADBLOCK PLUS AND NOSCRIPT</a></li>

  <li><a href="ch018_https-everywhere.html">HTTPS EVERYWHERE</a></li>

  <li><a href="ch019_proxy-settings-and-foxyproxy.html">PROXY SETTINGS AND FOXYPROXY</a></li>

  <li class="booki-section">TOOLS</li>

  <li><a href="ch021_tool-matrix.html">INTRODUCTION</a></li>

  <li><a href="ch022_freegate.html">FREEGATE</a></li>

  <li><a href="ch023_puff.html">SIMURGH</a></li>

  <li><a href="ch024_ultrasurf.html">ULTRASURF</a></li>

  <li><a href="ch025_what-is-vpn.html">VPN SERVICES</a></li>

  <li><a href="ch026_air-vpn.html">VPN ON UBUNTU</a></li>

  <li><a href="ch027_hotspot-shield.html">HOTSPOT SHIELD</a></li>

  <li><a href="ch028_alkasir.html">ALKASIR</a></li>

  <li><a href="ch029_tor-the-onion-router.html">TOR: THE ONION ROUTER</a></li>

  <li><a href="ch030_using-jon-do.html">JONDO</a></li>

  <li><a href="ch031_yourfreedom.html">YOUR-FREEDOM</a></li>

  <li class="booki-section">ADVANCED TECHNIQUES</li>

  <li><a href="ch033_playing-with-dns.html">DOMAINS AND DNS</a></li>

  <li><a href="ch034_http-proxies.html">HTTP PROXIES</a></li>

  <li><a href="ch035_command-line.html">THE COMMAND LINE</a></li>

  <li><a href="ch036_openvpn.html">OPENVPN</a></li>

  <li><a href="ch037_ssh-tunnelling.html">SSH TUNNELLING</a></li>

  <li><a href="ch038_socks-proxies.html">SOCKS PROXIES</a></li>

  <li class="booki-section">HELPING OTHERS</li>

  <li><a href="ch040_researching-and-documenting-censorship.html">RESEARCHING AND DOCUMENTING CENSORSHIP</a></li>

  <li><a href="ch041_diagnosing-port-blocking.html">DEALING WITH PORT BLOCKING</a></li>

  <li><a href="ch042_installing-web-proxies.html">INSTALLING WEB PROXIES</a></li>

  <li><a href="ch043_setting-up-a-tor-relay.html">SETTING UP A TOR RELAY</a></li>

  <li><a href="ch044_risks-of-operating-a-proxy.html">RISKS OF OPERATING A PROXY</a></li>

  <li><a href="ch045_best-practices-for-webmasters.html">BEST PRACTICES FOR WEBMASTERS</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch047_glossary.html">GLOSSARY</a></li>

  <li><a href="ch048_assessing-and-comparing-circumvention-tools.html">TEN THINGS</a></li>

  <li><a href="ch049_further-resources.html">FURTHER RESOURCES</a></li>

  <li><a href="ch050_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch021_tool-matrix.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch021_tool-matrix.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

