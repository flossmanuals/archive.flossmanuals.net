<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch033_text-editors.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch033_text-editors.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Text Editors</h1>

  <p>Besides running simple commands like <code>ls</code> and <code>grep</code>, you can use the command line to start large, complex programs. Before graphical interfaces were common, programs were designed to use plain text and take up the screen. Now these programs run within the same window that you use for the command line.</p>

  <p>In this book we'll focus on text editors, because you need them to save your commands for later use and to write scripts. They're handy for lots of other things too; for instance, you may want to edit HTML files on a web server using a text editor.</p>

  <h2>Word Processing vs Text Editing</h2>

  <p>Almost everybody who uses a computer is familiar with a word processor. The free software world provides several powerful ones, including OpenOffice.org and KWrite. The text editors we show in this book manipulate text, like word processors, but there's a fundamental difference.</p>

  <ul>
    <li>Word processors store a lot more than a stream of text characters you see on the screen. They want to provide "rich text," with italic and bold, numbering and bullets, colors--you name it. This is obviously valuable for many purposes. A plain text resume does not impress many employers.<br></li>

    <li>On the other hand, word processors are showing their limitations these days: some word processors have proprietary formats that make it hard to use documents in other programs. In fact, sometimes you cannot open a document with another version of the same word processor, or the document has display problems.</li>

    <li>Many people find online collaboration tools (such as the wiki software we used to write this manual) and content management systems (such as many weblog sites use) easier for modern document production than a word processor. But word processors are also evolving to do a better job supporting collaboration; probably all these tools will merge over time and evolve into something better than any of them offered before.</li>
  </ul>

  <p>Word processors, wikis, content management systems, and text editors all have their place. The tasks in this book require a text editor. If you want to use a word processor to edit these files, you can do so, but make sure to choose a plain text form when you save the file.<br></p>

  <h2>Why do you need a text editor?</h2>

  <p>GNU/Linux is a very file-centric operating system: everything is (or looks like) a file. All basic configuration is done via carefully crafted text files, in the right place with the right contents. You can find many graphical tools to configure your GNU/Linux box, but most of them just tweak text files on your behalf.</p>

  <p>Those text files have an exact syntax that you must follow. A simple misplaced character could jeopardize your system, so using a word processor for this matter is not only a bad idea but could corrupt your files with extra formatting information. Configuration files don't need italic or bold, they only need the right information.</p>

  <p>With source code it's the same thing. Compilers (programs that turn code into other programs) are very strict with syntax. Some of them even care about where in the line a specific command is. Word processors mess up the position of text in lines far too much for compiler to like them. What you need is a clean view of what's in the source code or the configuration file to know that what you're writing is exactly what your system will get.<br></p>

  <p>Some editors go even further: they became Integrated Development Environments (IDEs), that not only understand what you're typing (be it an Apache configuration or Java code) but can predict what you want to type, suggest modifications, or show your mistakes. They can color specific keywords, automatically place things in the right place, and so on.</p>

  <p>But the most important is that all those colors and highlighting are done <em>only</em> within the display. Those fancy changes are <strong>not</strong> propagated to the text files, which are meant to be plain text. This is one particular useful feature that word processing programs can't do and is most essential to text editing.</p>

  <h2>Why are most text editors command-line programs?</h2>

  <p>In the beginning... was the command line (Neil Stephenson). Twenty years ago there weren't many graphical interfaces around and Unix was already a grown-up operating system running on a whole lot of very important computers. All configuration was already stored in text files because of the KISS principle (keep it short and simple). Unix made the most of KISS and plain text by helping programs work together on text files. Pipes (using the | character) are one powerful method of working together that you've seen in this book.<br></p>

  <p>Nowadays, computers have thousands of times more power than those early ones, but keeping configuration in text files still gives a big advantage when the only connection you have to your server is through a 56-Kbit modem line and it's in a different country. Having to open a graphical interface might not be possible and if that's the only way you have to fix a problem, you're in big trouble.</p>

  <p>Making graphical programs that deal with configuration was a big plus, as the average user can now change things without reading tons of documentation and isn't likely to break the system by inserting one wrong character to a point where it's irrecoverable, but providing text files and the command-line editor is fundamental to any operating system.</p>

  <p>Although most text editors came from the command-line world, most also have a graphical interface today. Menus and buttons do help a lot when using Gvim or Emacs. GEdit and Kate (which are purely graphical) are short and simple, still providing the same basic functionality and the same important features for text editing.</p>

  <h2>Setting a default text editor</h2>

  <p>Because the terminal and command line are so tied in with the text editors, many commands open up a text editor for you. We saw one example <code>sudoedit</code>, in the "Useful Customizations" section. You can set the default editor though by setting either the EDITOR or the VISUAL environment variable. For instance:<br></p>
  <pre>
$<strong> export EDITOR=emacs</strong>
</pre>

  <p>Put this in a startup file such as <em>./bashrc</em>, and commands will use your chosen editor when they present a file for editing.<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="ch004_getting-started.html">GETTING STARTED</a></li>

  <li><a href="ch005_beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="ch006_moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="ch008_basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="ch009_standard-files.html">STANDARD FILES</a></li>

  <li><a href="ch010_cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="ch011_superusers.html">SUPERUSERS</a></li>

  <li><a href="ch012_redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="ch014_multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="ch015_searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="ch016_piping.html">PIPING</a></li>

  <li><a href="ch017_processes.html">PROCESSES</a></li>

  <li><a href="ch018_file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="ch019_command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="ch021_permissions.html">PERMISSIONS</a></li>

  <li><a href="ch022_interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="ch023_checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="ch024_sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="ch025_moving-again.html">MOVING AGAIN</a></li>

  <li><a href="ch026_customisation.html">CUSTOMISATION</a></li>

  <li><a href="ch027_parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="ch028_gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ch029_ssh.html">SSH</a></li>

  <li><a href="ch030_installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="ch031_making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="ch033_text-editors.html">TEXT EDITORS</a></li>

  <li><a href="ch034_nano.html">NANO</a></li>

  <li><a href="ch035_vim.html">VIM</a></li>

  <li><a href="ch036_emacs.html">EMACS</a></li>

  <li><a href="ch037_kedit.html">KEDIT</a></li>

  <li><a href="ch038_gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="ch040_scripting.html">SCRIPTING</a></li>

  <li><a href="ch041_maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="ch042_other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="ch043_sed.html">SED</a></li>

  <li><a href="ch044_awk.html">AWK</a></li>

  <li><a href="ch045_regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">Click here to edit chapter title</li>

  <li><a href="ch047_perl.html">PERL</a></li>

  <li><a href="ch048_python.html">PYTHON</a></li>

  <li><a href="ch049_ruby.html">RUBY</a></li>

  <li><a href="ch050_kjksajdsa.html">kjksajdsa</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch052_glossary.html">GLOSSARY</a></li>

  <li><a href="ch053_command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="ch054_outline.html">OUTLINE</a></li>

  <li><a href="ch055_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch033_text-editors.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch033_text-editors.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

