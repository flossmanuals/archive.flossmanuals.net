<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="checking-exit.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="checking-exit.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Exit status</h1>

  <p>When you type commands, you can usually tell whether they worked or not. Commands that are unable to do what you asked usually print an error message. This is sufficient if you are typing in each command by hand and looking at the output, but sometimes (for example, if you are writing a script) you want to have your commands react differently when a command fails.</p>

  <p>To facilitate this, when a command finishes it returns an <em>exit status</em>. The exit status is not normally displayed; instead it is placed in a variable (a named memory slot) named "<strong>$?</strong>". The exit status is a number between 0 and 255 (inclusive); zero means success, and any other value means a failure.</p>

  <p>One way to see the exit status of a command is to use the <code>echo</code> command to display it:<br></p>
  <pre>
$<strong> echo "this works fine"</strong>
this works fine
$<strong> echo $?</strong>
0
$<strong> hhhhhh</strong>
bash: hhhhhh: command not found
$<strong> echo $?</strong>
127
</pre>

  <p>Now we'll look at various ways to handle errors.<br></p>

  <h2>if/then</h2>

  <p>Handling an error is an example of something you do conditionally: <em>if</em> something happens, <em>then</em> you want to take action.&nbsp;&nbsp; The shell provides a compound command--a command that runs other commands--called <code>if</code>.&nbsp; The most basic form is:</p>
  <pre>
<strong>if</strong>
<strong><em>  &lt;command&gt;</em></strong>
<strong>then</strong>
<strong><em>  &lt;commands-if-successful&gt;</em></strong>
<strong>fi</strong>
</pre>

  <p>We will start with a basic example, then improve it to make it more useful.&nbsp; After we type <code>if</code> and press the <strong>Enter</strong> key, the shell knows we're in the middle of a compound command, so it displays a different prompt (<strong>&gt;</strong>) to remind us of that.</p>
  <pre class="SCREEN">
$ <strong>if</strong>
&gt; <strong>man ls</strong>
&gt; <strong>then</strong>
&gt; <strong>echo "You now know more about ls"</strong>
&gt; <strong>fi</strong>
<em>The manual page for ls scrolls by</em>
You now know more about ls
</pre>

  <p>Running this command brings up the manual page for <code>ls</code>.&nbsp; Upon quitting with the <strong>q</strong> key, the <code>man</code> command exits successfully and the <code>echo</code> command runs.</p>

  <h3><strong>Handling command failure</strong></h3>

  <p>Adding an <code>else</code> clause allows us to specify what to run on failure:</p>
  <pre>
<strong>if</strong>
<em>  <strong>&lt;command&gt;</strong></em>
<strong>then</strong>
<em>  <strong>&lt;commands-if-successful&gt;</strong></em>
<strong>else</strong>
<em>  <strong>&lt;commands-if-failed&gt;</strong></em>
<strong>fi</strong>
</pre>

  <p>Let's run <code>apropos</code> if the <code>man</code> command fails.</p>
  <pre class="SCREEN">
$<strong> if</strong>
&gt;<strong> man draw</strong>
&gt;<strong> then</strong>
&gt;<strong> echo "You now know more about draw"</strong>
&gt;<strong> else</strong>
&gt;<strong> apropos draw</strong>
&gt;<strong> fi</strong>
<em>...
list of results for apropos draw
...
</em>
</pre>

  <p>This time the <code>man</code> command failed because there is no <code>draw</code> command, activating the else clause.</p>

  <h2>&amp;&amp; and ||</h2>

  <p>The if-then construct is very useful, but rather verbose for chaining together dependent commands.&nbsp; The "<em>&amp;&amp;</em>" <em>(and)</em> and "<em>||</em>" <em>(or)</em> operators provide a more compact format.</p>
  <pre>
<strong>command1 &amp;&amp; command2 [&amp;&amp; command3]...</strong>
</pre>

  <p>The &amp;&amp; operator links two commands together.&nbsp; The second command will run only if the first has an exit status of zero, that is, if the first command was successful.&nbsp; Multiple instances of the &amp;&amp; operator can be used on the same line.</p>
  <pre>
$<strong> mkdir mylogs &amp;&amp; cd mylogs &amp;&amp; touch mail.log &amp;&amp; chmod 0660 mail.log</strong>
</pre>

  <p>Here is an example of multiple commands, each of which assume the prior one has run successfully.&nbsp; If we were to use the if-then construct to do this, we would have ended up with an unwieldy mass of ifs and thens.</p>

  <p>Note that the &amp;&amp; operator <em>short circuits</em>, that is, if one command fails, no subsequent command is run.&nbsp; We take advantage of this property to prevent unwanted effects (like creating <em>mail.log</em> in the wrong directory in the above example).</p>

  <p>If <em>&amp;&amp;</em> is the equivalent of <code>then</code>, the <em>|| operator</em> is the equivalent of <code>else</code>.&nbsp; It provides us a mechanism to specify what command to run if the first fails.</p>
  <pre>
<strong>command1 || command2 || command3 || ...</strong>
</pre>

  <p>Each command in the chain will be run only if the previous command did not succeed (that is, had a nonzero exit status).</p>
  <pre>
$<strong> cd Desktop || mkdir Desktop || echo "Desktop directory not found and could not be created"</strong>
</pre>

  <p>In this example we try to enter the <em>Desktop</em> directory, failing which we create it, failing which we inform the user with an error message.</p>

  <p>With this knowledge we can write an efficient and compact <code>helpme</code> function.&nbsp; Our previous examples have shown the two operators used in isolation, but they can be mixed as well.</p>
  <pre>
$<strong> function helpme() {
  man  &amp;&amp; echo "you now know more about " || apropos 
}</strong>
</pre>

  <p>As you probably suspect, the "you now know..." echo is not exactly the most useful command.&nbsp; (It might not even be accurate, perhaps the <code>man</code> page introduced so many options and confused the poor user).&nbsp; We heartily confess we threw it in just to match the original if-then syntax.&nbsp; Now that we know about the <em>|| operator</em>, we can simplify the function to:</p>
  <pre>
$<strong> function helpme() {
  man  || apropos 
}</strong>
</pre>

  <h2>What does an exit status mean?</h2>

  <p>Up to now, we considered only the difference between zero and non-zero exit statuses.&nbsp; We know that zero means success, and non-zero means a failure.&nbsp; But what kind of failure?&nbsp; Some exit values may be used for user-specified exit parameters, so that their meaning may vary for one command to another.&nbsp; However, some widely accepted meanings for particular values do exist.</p>

  <p>For example, if you invoke an inexistent command (e.g. by wrongly typing an existing one or by omitting its correct path), you should expect to receive the standard notification of "command not found" which is generally associated with the exit status 127.&nbsp; We encountered this value at the very beginning of this chapter:<br></p>
  <pre>
$<strong> hhhhhh</strong>
bash: hhhhhh: command not found
$<strong> echo $?</strong>
127
</pre>

  <p>Another exit status to which attention should be drawn is "permission denied", usually the code 126.&nbsp; When you encounter this value, it may be worth enhancing the level of attention.&nbsp; The command you are trying to execute requires permissions you do not have.&nbsp; There are some frequent cases.</p>

  <p>First, you attempted to execute as a normal user a command which requires root privileges.&nbsp; In this case, if you know what are you doing, you should log in as root and then invoke the command again.&nbsp; However, if you are plenty of doubts about that command, it would probably be a good idea to spend some time documenting yourself about.&nbsp; If that command requires root privileges, it is likely to be potentially harmful if not <span id="main"><span id="search"> used correctly</span></span>.</p>

  <p>Second, you may be trying to run a software which has been installed with the wrong privileges.&nbsp; For instance, you may be collaborating with other people in developing an application which is hosted in the home path of another user and that user may have missed to allow you to run the executable.&nbsp; The <code>chown</code> and <code>chmod</code> commands can help.</p>

  <p>There is even the possibility you are trying to obtain information about your system without modifying the system status, so you may think this should be possible without the need to be root.&nbsp; It is often possible, but not always.&nbsp; For example, you may check the status of the <code>ssh</code> and <code>at</code> services: the first one is normally readable while the second one may not:<br></p>
  <pre>
$<strong> /etc/init.d/sshd status</strong>
sshd (pid 1234) is running...
<strong>
</strong>$<strong> /etc/init.d/atd status</strong>
bash: /etc/init.d/atd: Permission denied
$<strong> echo $?</strong>
126
</pre>

  <p>It may happen that an invoked command takes a long time to complete.&nbsp; If that command is needed to generate or modify information which is required by your subsequent commands, then you should check that the time-demanding command you've started has not been unexpectedly terminated its execution.&nbsp; This is different from checking that the command correctly terminated (exit status 0).&nbsp; That command may encounter an error condition and decide to terminate with a non-zero exit code.&nbsp; However it is very different the case in which the command cannot choose its exit status because it cannot terminate at all.&nbsp; Whether an infinite loop inside the command requires an extern intervention or a premature extern intervention overrides the correctly running command,&nbsp; an INT signal (which is a user interrupt) may be sent to the command by hitting <strong>ctrl + c</strong> or killing the command via:<br></p>
  <pre>
$<strong> kill -int <em>pid-of-the-command</em></strong>
</pre>

  <p>Such kind of termination may alter the expected output of the interrupted command and break some subsequent manipulation of that output.&nbsp; When an INT signal terminates a command, a 130 exit status is returned.&nbsp; For instance, consider the <code>yes</code> command which requires a <strong>ctrl + c</strong> to terminate:<br></p>
  <pre>
$ <strong>yes</strong>
y
y
y
...
# press ctrl+c <strong>
$ echo $?
</strong>130
</pre>

  <p>That covers the concept of exit status and using it to control the flow of your command and scripts. We hope you leave this chapter with an exit status of zero!</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="getting-started.html">GETTING STARTED</a></li>

  <li><a href="beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="standard-files.html">STANDARD FILES</a></li>

  <li><a href="cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="superusers.html">SUPERUSERS</a></li>

  <li><a href="redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="piping.html">PIPING</a></li>

  <li><a href="processes.html">PROCESSES</a></li>

  <li><a href="file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="permissions.html">PERMISSIONS</a></li>

  <li><a href="interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="moving-again.html">MOVING AGAIN</a></li>

  <li><a href="customisation.html">CUSTOMISATION</a></li>

  <li><a href="parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ssh.html">SSH</a></li>

  <li><a href="installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="text-editors.html">TEXT EDITORS</a></li>

  <li><a href="nano.html">NANO</a></li>

  <li><a href="vim.html">VIM</a></li>

  <li><a href="emacs.html">EMACS</a></li>

  <li><a href="kedit.html">KEDIT</a></li>

  <li><a href="gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="scripting.html">SCRIPTING</a></li>

  <li><a href="maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="sed.html">SED</a></li>

  <li><a href="awk.html">AWK</a></li>

  <li><a href="regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">SCRIPTING LANGUAGES</li>

  <li><a href="perl.html">PERL</a></li>

  <li><a href="python.html">PYTHON</a></li>

  <li><a href="ruby.html">RUBY</a></li>

  <li><a href="gnu-octave.html">GNU OCTAVE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="glossary.html">GLOSSARY</a></li>

  <li><a href="command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="outline.html">OUTLINE</a></li>

  <li><a href="credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="checking-exit.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="checking-exit.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

