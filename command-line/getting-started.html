<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="getting-started.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="getting-started.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Getting Started</h1>

  <p>Modern computing is highly interactive, and using the command line is just another form of interaction.&nbsp; Most people use the computer through its desktop or graphical interface, interacting at a rapid pace.&nbsp; They click on an object, drag and drop it, double-click another to open it, alter it, etc.&nbsp;</p>

  <p>Although interactions happen so fast you don't think about it, each click or keystroke is a command to the computer, which it reacts to. Using the command line is the same thing, but more deliberate.&nbsp; You type a command and press the <strong>Return</strong> or <strong>Enter</strong> key.&nbsp; For instance, in my terminal I type:</p>
  <pre>
<strong>date</strong>
</pre>

  <p>And the computer replies with:</p>
  <pre>
Thu Mar 12 17:15:09 EDT 2009
</pre>

  <p>That's pretty computerish.&nbsp; In later chapters we'll explain how to request the date and time in a more congenial format. We'll also explain how working in different countries and with different languages changes the output.&nbsp; The idea is that you've just had an interaction.</p>

  <h2>The Command Line Can Do Much Better</h2>

  <p>The <em>date</em> command, as seen so far, compares poorly with the alternative of glancing at a calendar or clock.&nbsp; The main problem is not the unappetizing appearance of the output, mentioned already, but the inability to do anything of value with the output.&nbsp; For instance, if I'm looking at the date in order to insert it into a document I'm writing or update an event on my online calendar, I have to do some retyping.&nbsp; The command line can do much better.</p>

  <p>After you learn basic commands and some nifty ways to save yourself time, you'll find out more in this book about feeding the output of commands into other commands, automating activities, and saving commands for later use.</p>

  <h2>What Do We Mean By a Command?</h2>

  <p>At the beginning of this chapter we used the word "command" very generally to refer to any way of telling the computer what to do.&nbsp; But in the context of this book, a command has a very specific meaning. It's a file on your computer that can be executed, or in some cases an action that is built into the shell program. Except for the <em>built-in commands</em>, the computer runs each command by finding the file that bears its name and executing that file. We'll give you more details as they become useful.</p>

  <h2>Ways to Enter Commands</h2>

  <p>To follow along on this book, you need to open a command-line interpreter (called a <em>shell</em> or <em>terminal</em> in GNU/Linux) on your computer.&nbsp; Pre-graphical computer screens presented people with this interpreter as soon as they logged in.&nbsp; Nowadays almost everybody except professional system administrators uses a graphical interface, although the pre-graphical one is still easier and quicker to use for many purposes.&nbsp;&nbsp; So we'll show you how to pull up a shell.<br></p>

  <h2>Finding a Terminal</h2>

  <p>You can get a terminal interface from the desktop, but it may be easier to leave the desktop and use the original text-only terminal. To do that, use the &lt;ctrl&gt;&lt;alt&gt;&lt;F1&gt; key combination. You get a nearly blank screen with an invitation to log in. Give it your username and password. You can go to other terminals with &lt;alt&gt;&lt;F2&gt; and so on, and set up sessions with different (or the same) users for whatever tasks you want to do. At any time, switch from one to another by using the &lt;alt&gt;&lt;F#&gt; keystroke for the one you want. One of these, probably F7 or F8, will get you back to the desktop. In the text terminals you can use the mouse (assuming your system has gpm running) to select a word, line or range of lines.&nbsp; You can then paste that somewhere else in that terminal or any other terminal.</p>

  <p>GNU/Linux distributions come with different graphical user interfaces (<em>GUI</em> ) offering different <span class="clickable" onclick="dr4sdgryt(event," ox=""><span class="hg"><span class="hw">aesthetics</span></span></span> and semantic metaphors.&nbsp; Those running on top of the operating system are known as <em>desktop environments</em>.&nbsp; GNOME, KDE and Xfce are among the most widely used ones.&nbsp; Virtually every desktop environment provides a program that mimics the old text-only terminals that computers used to offer as interfaces.&nbsp; On your desktop, try looking through the menus of applications for a program called Terminal.&nbsp; Often it's on a menu named something such as "Accessories", which is not really fair because once you read this book you'll be spending a lot of time in the terminal every day.</p>

  <p>In GNOME you choose <strong>Applications -&gt; Accessories -&gt; Terminal</strong>.</p>

  <p><img title="ubuntu_open" alt="Screenshot_1.png" src="_booki/command-line/static/IntroCommandLine-GettingStarted-Screenshot_1-en.png" height="347" width="500">&nbsp;</p>

  <p>In KDE you choose <strong>K Menu -&gt; System -&gt; Terminal</strong>; in Xfce you choose <strong>Xfce Menu -&gt; System -&gt; Terminal</strong>.&nbsp;<br>
  Wherever it's located, you can almost certainly find a terminal program.</p>

  <p>When you run the terminal program, it just shows a blank window; there's not much in the way of help.&nbsp; You're expected to know what to do--and we'll show you.</p>

  <p>The following figure shows the Terminal window opened on the desktop in GNOME.&nbsp;<br></p>

  <p>&nbsp;<img alt="Screenshot_2.png" src="_booki/command-line/static/CommandLineIntro-GettingStarted-Screenshot_2-en.png" height="449" width="600"><br></p>

  <h2>Running an Individual Command</h2>

  <p>Many graphical interfaces also provide a small dialog box called something like "Run command".&nbsp; It presents a small text area where you can type in a command and press the <strong>Return</strong> or <strong>Enter</strong> key.&nbsp;<br></p>

  <p><img title="run" alt="Screenshot_Run_Application.png" src="_booki/command-line/static/IntroCommandLine-GettingStarted-Screenshot_Run_Application-en.png" height="187" width="520">&nbsp;</p>

  <p>To invoke this dialog box, try typing the <strong>Alt</strong> + <strong>F2</strong> key combination, or searching through the menus of applications.&nbsp; You can use this box as a shortcut to quickly start up a terminal program, as long as you know the name of a terminal program installed on your computer.&nbsp; If you are working on an unfamiliar computer and don't even know the name of the default terminal program, try typing <code>xterm</code> to start up a no-frills terminal program (no fancy menus allowing choice of color themes or fonts).&nbsp; If you desperately need these fancy menus,</p>

  <ul>
    <li>in GNOME the default terminal program should be <code>gnome-terminal</code>;</li>

    <li>in KDE it should be <code>konsole</code>;</li>

    <li>in Xfce you'd try with <code>Terminal</code> or with version specific terminal names: for example in Xfce 4 you should find <code>xfce4-terminal</code>.</li>
  </ul>

  <h2>How We Show Commands and Output in This Book</h2>

  <p>There's a common convention in books about the command-line. When you start up a terminal, you see a little message indicating that the terminal is ready to accept your command. This message is called a <em>prompt</em>, and it may be as simple as:</p>
  <pre>
$
</pre>

  <p>After you type your command and press the <strong>Return</strong> or <strong>Enter</strong> key, the terminal displays the command's output (if there is any) followed by another prompt. So my earlier interaction would be shown in the book like this:</p>
  <pre>
$ <strong>date</strong>
Thu Mar 12 17:15:09 EDT 2009
$
</pre>

  <p>You have to know how to interpret examples like the preceding one. All you type here is <em>date</em>. Then press the <strong>Return</strong> key. The word <em>date</em> in the example is printed in bold to indicate that it's something you type. The rest is output on the terminal.<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="getting-started.html">GETTING STARTED</a></li>

  <li><a href="beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="standard-files.html">STANDARD FILES</a></li>

  <li><a href="cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="superusers.html">SUPERUSERS</a></li>

  <li><a href="redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="piping.html">PIPING</a></li>

  <li><a href="processes.html">PROCESSES</a></li>

  <li><a href="file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="permissions.html">PERMISSIONS</a></li>

  <li><a href="interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="moving-again.html">MOVING AGAIN</a></li>

  <li><a href="customisation.html">CUSTOMISATION</a></li>

  <li><a href="parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ssh.html">SSH</a></li>

  <li><a href="installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="text-editors.html">TEXT EDITORS</a></li>

  <li><a href="nano.html">NANO</a></li>

  <li><a href="vim.html">VIM</a></li>

  <li><a href="emacs.html">EMACS</a></li>

  <li><a href="kedit.html">KEDIT</a></li>

  <li><a href="gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="scripting.html">SCRIPTING</a></li>

  <li><a href="maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="sed.html">SED</a></li>

  <li><a href="awk.html">AWK</a></li>

  <li><a href="regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">SCRIPTING LANGUAGES</li>

  <li><a href="perl.html">PERL</a></li>

  <li><a href="python.html">PYTHON</a></li>

  <li><a href="ruby.html">RUBY</a></li>

  <li><a href="gnu-octave.html">GNU OCTAVE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="glossary.html">GLOSSARY</a></li>

  <li><a href="command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="outline.html">OUTLINE</a></li>

  <li><a href="credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="getting-started.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="getting-started.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

