<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch045_regular-expressions.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch045_regular-expressions.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Regular Expressions</h1>

  <p>When you're looking through files or trying to change text, your needs are often ambiguous or approximate. Typical searches include:</p>

  <ul>
    <li>Finding an indeterminate number of things, such as "one or more zeroes".</li>

    <li>Finding text that can have variants, such as "color" and "colour", or "gray" and "grey".&nbsp;</li>

    <li>Extracting parts of text that forms a pattern. For instance, suppose you have a list of email addresses such as <a href="mailto:somebody@fsf.org">somebody@fsf.org</a> and <a href="mailto:whoever@flossmanuals.net">whoever@flossmanuals.net</a>, and you want to extract the parts after the @ sign (fsf.org and flossmanuals.net, respectively).<br></li>
  </ul>

  <p>To search for such strings and (if you want) make replacements, a special language called <em>regular expressions</em> is invaluable. This section offers a quick introduction to regular expressions. The language can be intimidating at first--but it is not complicated, only terse. You have to use it a bit so that your brain gets used to picking the regular expressions apart.</p>

  <p>The easiest way to learn and practice regular expressions is to use the simple filters provided with the shell, such as grep, Sed, and AWK. The grep command has popped up several times already in this book. In this section we'll use an "extended" version named egrep, because it provides more of the features people use frequently in regular expressions. Sed and AWK were introduced in previous sections; we'll use Sed a lot in this one.</p>

  <p>By the end of this section, you'll understand grep, egrep, and Sed pretty well. You will then be able to move on and use regular expressions in other situations where they are even more powerful. Nearly every modern language, including the other scripting languages mentioned in this book (Perl, Python, and Ruby) offer regular expressions. Even databases offer some form of regular expressions.</p>

  <p>The details of regular expressions vary from one tool to another, and even from one version of a tool to another version of the same tool. We'll show pretty common features, but they don't all work in every tool.<br></p>

  <h2>Plain text</h2>

  <p>A regular expression doesn't have to be fancy. Up to now, the grep commands we've shown look for plain text:</p>
  <pre>
$ <strong>cat color_file</strong>
Primary colors blue and red make the color magenta
Primary colors blue and green make the colour cyan
Primary colors red and green make the colour yellow
Black and white make grey
$ <strong>grep 'colour' color_file</strong>
Primary colors blue and green make the colour cyan
Primary colors red and green make the colour yellow
</pre>

  <p>Because "colour" contains no metacharacters the shell would interpret, we don't need the single quotes, but we're using them to get into the habit. The egrep commands in this section will use lots of metacharacters.</p>

  <h2>Indeterminate quantities</h2>

  <p>One simple application of regular expressions is to search for "any number" of something, or a fuzzy amount such as "3 to 5" hyphens. Here are the metacharacters that support this. For the sake of simplicity, we'll show some in isolation and then use them in some tools.</p>

  <table border="1" cellpadding="1" cellspacing="1">
    <tbody>
      <tr>
        <td>&nbsp;Match zero or more of X</td>

        <td>&nbsp;X*</td>
      </tr>

      <tr>
        <td>&nbsp;Match one or more of X<br></td>

        <td>&nbsp;X+</td>
      </tr>

      <tr>
        <td>&nbsp;Match zero or one of X<br></td>

        <td>&nbsp;X?</td>
      </tr>

      <tr>
        <td>&nbsp;Match from 3 to 5 of X<br></td>

        <td>&nbsp;X{3,5}</td>
      </tr>
    </tbody>
  </table>

  <p>These look a lot like shell (globbing) metacharacters, but there are subtle differences. Focus on what they mean in regular expressions and remember that it is really a separate language from the shell metacharacters.</p>

  <p>Now we can see how to find both "color" and "colour" in one search. We want either zero or 1 "u", so we specify:</p>
  <pre>
$ <strong>egrep 'colou?r' color_file</strong>
Primary colors blue and red make the color magenta
Primary colors blue and green make the colour cyan
Primary colors red and green make the colour yellow
</pre>

  <p>The asterisk (*) is one of the most common and useful metacharacters in regular expressions, but also one of the most confusing and misused. Suppose you wanted to remove zeros from a line. You might try to remove "any number of zeros" through an asterisk:</p>
  <pre>
$ <strong>echo "There are 40006 items" | sed "s/0*/X/"</strong>
</pre>

  <p>But the output is:</p>
  <pre>
XThere are 40006 items
</pre>

  <p>This happened because Sed replaces the first occurrence of the pattern you request. You requested "zero or more" and the first occurrence of that is the beginning of the line!<br></p>

  <p>In this case, you want the plus sign (0+), but many versions of Sed don't support it. You can work around that with:</p>
  <pre>
$ <strong>echo "There are 40006 items" | sed "s/00*/X/"</strong>
There are 4X6 items
</pre>If you put a single digit in the brackets, such as {3}, it means "match this number exactly". If you include the comma without a second digit, such as {3,}, it means "match any number of three or more."

  <h2>Indeterminate Matches, Classes, and Ranges</h2>

  <p>To match any character, just specify a dot or period (.). Thus, the following matches a slash followed by any single character and another slash:</p>
  <pre>
$ <strong>egrep '/./' file</strong>
</pre>

  <p>The dot is commonly combined with one of the fuzzy quantifiers in the previous section. So the following matches any number of characters (but there has to be at least one) between slashes:</p>
  <pre>
$ <strong>egrep '/.+/' file</strong>
</pre>

  <p>The following is the same except that it also finds lines with two slashes in a row (//):</p>
  <pre>
$ <strong>egrep '/.*/' file</strong>
</pre>

  <p>A period is a common character in text, so you often want a dot to mean a dot--not to have its special metacharacter meaning. Whenever you need to search for a character that your tools considers a metacharacter, precede it with a backslash:</p>
  <pre>
$ <strong>egrep '\.' file</strong>
</pre>

  <p>That command finds just a dot. Because the backslash causes the dot character to escape being treated as a metacharacter, we speak of <em>escaping</em> characters through a backslash.</p>

  <p>If you find yourself searching for strings that contain punctuation and you don't want any of the punctuation treated as a metacharacter, it can be tiresome and difficult to escape each character. Consider using fgrep (which stands for "fixed grep") for these strings instead of grep or egrep. The fgrep command looks for exactly what you pass, and doesn't treat anything as a metacharacter. You still have to use single quotes so the shell doesn't treat anything as a metacharacter.<br></p>

  <p>Square brackets let you specify combinations of characters. For instance, to search for both "gray" and "grey" you specify:</p>
  <pre>
$ <strong>egrep "gr[ae]y" color_file</strong>
Black and white make grey
</pre>

  <p>To match the letters that are commonly used as vowels in English, write:</p>
  <pre>
[aeiouy]
</pre>

  <p>The order of characters never matters inside the brackets. We can find lines without vowels by submitting the regular expression to egrep. We'll start with a rather garbled file name <em>letter_file</em>:</p>
  <pre>
This is readable text.
Ths s grbg txt.
This is more readable text.
aaaai
</pre>

  <p>Note that the second line contains no vowels, whereas the last line contains only vowels. First we'll search for a vowel:</p>
  <pre>
$ <strong>grep '[eauoi]' letter_file</strong>
This is readable text.
This is more readable text.
aaaai 
</pre>

  <p>The line without vowels failed to match.</p>

  <p>Now let's look for non-vowel characters. That means not only consonants, but punctuation and spaces. We can <em>invert</em> the character class by putting a caret (^) at the front. In a character class (and nowhere else) the caret means "everything except the following". We'll do that here with five vowels (allowing the "y" to be matched because it can also be a consonant):</p>
  <pre>
$ <strong>grep '[^eauoi]' letter_file</strong>
This is readable text.
Ths s grbg txt.
This is more readable text.
</pre>

  <p>This time only the last line was left out, because it had vowels and nothing else.</p>

  <p>A character class can also contains ranges, which you indicate by a hyphen separating two characters. Instead of [0123] you can specify [0-3]. People often use the following combinations:</p>

  <table border="1" cellpadding="1" cellspacing="1">
    <tbody>
      <tr>
        <td>Any digit<br></td>

        <td>&nbsp;[0-9]</td>
      </tr>

      <tr>
        <td>Any lowercase letter<br></td>

        <td>&nbsp;[a-z]</td>
      </tr>

      <tr>
        <td>Any uppercase letter<br></td>

        <td>&nbsp;[A-Z]</td>
      </tr>

      <tr>
        <td>Any letter<br></td>

        <td>&nbsp;[a-zA-Z]</td>
      </tr>
    </tbody>
  </table>

  <p>As the last example in the table shows, you can combine ranges inside the brackets. You can even combine ranges with other characters, such as the following combination of letters with common punctuation:</p>
  <pre>
[a-zA-Z.,!?]
</pre>

  <p>We didn't have to escape the dot in this example because it isn't treated as a metacharacter inside the square brackets.<br></p>

  <p>Note that a character class, no matter how large, always matches a single character. If you want it to match a lot of characters, use one of the quantifiers from the previous section:</p>
  <pre>
$ <strong>egrep '\([a-zA-Z.,!?]+\)' file</strong>
</pre>

  <p>This matches parentheses that enclose any number of the characters in the character set. We had to escape the parentheses because they are metacharacters--very special ones, it turns out. We'll look at that next.<br></p>

  <h2>Groups</h2>

  <p>Parentheses allow you to manipulate multiple characters at once. Remember that character classes in square brackets always match a single character, even though that single character can be many different things. In contrast, groups can match a sequence. For instance, suppose you want to match quantities of a thousand, a million, a billion, a trillion, etc. You want to match:</p>

  <p>1,000</p>

  <p>1,000,000</p>

  <p>1,000,000,000</p>

  <p>etc.</p>

  <p>You can do that by putting the string ",000" in a group, enclosing it in parentheses. Now anything you apply to it--such as the + character--applies to the whole group:<br></p>
  <pre>
$ <strong>egrep '1(,000)+' file</strong>
</pre>

  <p>But parentheses do even more. They store what they match, which is called <em>capturing</em> it. Then you can refer to it later.<br></p>

  <p>This is a subtle and possibly confusing feature. Let's show it by looking at a file that repeats some sequences of characters:</p>
  <pre>
This bell is a tam-tam.
This sentence doesn't appear in the egrep-generated output.
I want it quick-quick.
</pre>

  <p>The first line contains the word "tam", a hyphen, and then "tam" again. The third line contains "quick", a hyphen, and then "quick" again. These lines don't actually have strings in common, except for the hyphen (which appears in the second line too, so searching for it doesn't distinguish the first and third lines from the second). What the first and third lines share is a pattern: a word followed by a hyphen and itself. So we can grab those two lines by capturing a word and repeating it (the file is named <em>doubles</em>):</p>
  <pre>
$ <strong>egrep ' ([a-z]+)-' doubles</strong>
This bell is a tam-tam.
I want it quick-quick.
</pre>

  <p>Puzzled? The regular expression, broken into pieces, is:</p>

  <table border="1" cellpadding="1" cellspacing="1">
    <tbody>
      <tr>
        <td>&nbsp;(</td>

        <td>&nbsp;Start a group<br></td>
      </tr>

      <tr>
        <td>&nbsp;[a-z]+</td>

        <td>&nbsp;Any number (one or more) of letters<br></td>
      </tr>

      <tr>
        <td>&nbsp;)</td>

        <td>&nbsp;Close the group<br></td>
      </tr>

      <tr>
        <td>&nbsp;-</td>

        <td>&nbsp;Hyphen (a simple match)<br></td>
      </tr>

      <tr>
        <td>&nbsp;</td>

        <td>&nbsp;Repeat the group captured earlier<br></td>
      </tr>
    </tbody>
  </table>

  <p>The  is a special syntax recognized by tools that allow parentheses to capture text. In the first line of the files, it matches "tam" because that's what [a-z]+ matched. In the third line, it matches "quick" because that's what [a-z]+ matched. It says, "whatever you found, I want it again."</p>

  <p>To extract the second part of an email address, such as "fsf.org" from "<a href="mailto:someone@fsf.org">someone@fsf.org</a>", use a regular expression such as:</p>
  <pre>
([a-z._]+)@([a-z._]+)
</pre>

  <p>In this case,  matches the part before the @ sign, while  matches the part after the @. So extract  to get "fsf.org".</p>

  <h2>Alternation</h2>

  <p>We saw that a character class matches only one character at a time. If you have two or more sequences that can appear in the same place, you specify them through <em>alternation</em>. This involves separating them with a vertical bar (|). Thus, the following finds an instance of "FSF" or "Free Software Foundation":<br></p>
  <pre>
FSF|Free Software Foundation
</pre>

  <p>You can put as many alternatives as you want in alternation:</p>
  <pre>
gnu.org|FSF|Free Software Foundation
</pre>

  <p>Because the alternatives are usually embedded in a larger regular expression, you generally need to put them in parentheses to mark where they start and end:</p>
  <pre>
The (FSF|Free Software Foundation)
</pre>

  <h2>Anchoring</h2>

  <p>If you want to match something when it occurs at the beginning of the line, but nowhere else, preface the regular expression with a caret (^). Thus, you can use the following to catch lines that begin with a lowercase letter:<br></p>
  <pre>
^[a-z]
</pre>

  <p>This use of the caret has nothing to do with the caret that we saw before inside of square brackets. A caret means "beginning of line" when it's the first character of a regular expression, but only in that position.</p>

  <p>Similarly, you can match something at the end of a line by adding a dollar sign ($) to the end of the regular expression:<br></p>
  <pre>
[0-9]$
</pre>

  <p>In the phrase "I added 3 and 5 to make 8", the previous regular expression will match the 8 because it's at the end of the line.</p>

  <p>When you're searching for lines that match a regular expression exactly (no extra text at the start or end), use both anchors. For instance, if you want to make sure a line consists only of digits, enter:</p>
  <pre>
^[0-9]+$
</pre>

  <p>The [0-9]+ part specifies "one or more digits", and the ^ and $ ensure that the regular expression takes up the whole line.<br></p>

  <p>We wanted to take you just far enough to get a sense of what regular expressions can do for you. There are many, many more features, but some of them are useful only in programming languages. As we warned before, different tools support different features, so you have to read the documentation for egrep, Sed, or any other tool or language to find out what works there.</p>

  <p>When you're testing regular expressions, you can try lots of online or stand-alone tools (some offered gratis and some for sale) that help debug problems such as mismatched brackets and parentheses. Such tools can help you learn the more complex features and make complex regular expressions easier to write.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="ch004_getting-started.html">GETTING STARTED</a></li>

  <li><a href="ch005_beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="ch006_moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="ch008_basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="ch009_standard-files.html">STANDARD FILES</a></li>

  <li><a href="ch010_cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="ch011_superusers.html">SUPERUSERS</a></li>

  <li><a href="ch012_redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="ch014_multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="ch015_searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="ch016_piping.html">PIPING</a></li>

  <li><a href="ch017_processes.html">PROCESSES</a></li>

  <li><a href="ch018_file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="ch019_command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="ch021_permissions.html">PERMISSIONS</a></li>

  <li><a href="ch022_interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="ch023_checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="ch024_sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="ch025_moving-again.html">MOVING AGAIN</a></li>

  <li><a href="ch026_customisation.html">CUSTOMISATION</a></li>

  <li><a href="ch027_parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="ch028_gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ch029_ssh.html">SSH</a></li>

  <li><a href="ch030_installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="ch031_making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="ch033_text-editors.html">TEXT EDITORS</a></li>

  <li><a href="ch034_nano.html">NANO</a></li>

  <li><a href="ch035_vim.html">VIM</a></li>

  <li><a href="ch036_emacs.html">EMACS</a></li>

  <li><a href="ch037_kedit.html">KEDIT</a></li>

  <li><a href="ch038_gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="ch040_scripting.html">SCRIPTING</a></li>

  <li><a href="ch041_maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="ch042_other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="ch043_sed.html">SED</a></li>

  <li><a href="ch044_awk.html">AWK</a></li>

  <li><a href="ch045_regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">Click here to edit chapter title</li>

  <li><a href="ch047_perl.html">PERL</a></li>

  <li><a href="ch048_python.html">PYTHON</a></li>

  <li><a href="ch049_ruby.html">RUBY</a></li>

  <li><a href="ch050_kjksajdsa.html">kjksajdsa</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch052_glossary.html">GLOSSARY</a></li>

  <li><a href="ch053_command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="ch054_outline.html">OUTLINE</a></li>

  <li><a href="ch055_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch045_regular-expressions.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch045_regular-expressions.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

