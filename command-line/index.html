<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Put Yourself in Command</h1>

  <p>Commands enable you to do all sorts of powerful things. We will demonstrate this by looking at an everyday task you might be familiar with. If you use a digital camera, you probably have a folder full of images on your computer. Imagine you wish to resize the image <em>profile.jpg</em> so it is 300 pixels wide and then save it as a new image called <em>profile_small.jpg</em>.</p>

  <p>Using an image editing software, the steps you need to go through might be as follows:</p>

  <ol>
    <li>Open the image editor from the Applications menu.</li>

    <li>Click <em>File&gt;Open</em>.</li>

    <li>Browse to the folder where you have saved&nbsp;your images.</li>

    <li>Click the image file <em>profile.jpg</em>, and then click <em>Open</em>.</li>

    <li>Click <em>Image&gt;Scale Image</em>&nbsp;to&nbsp;resize&nbsp;the selected image.</li>

    <li>Change the image width to 300 pixels, and then click <em>Scale</em>.</li>

    <li>Click <em>File&gt;Save As</em>&nbsp;to save the image file.</li>

    <li>Enter <em>profile_small.jpg</em> as the new file name, and then click <em>Save</em>.</li>
  </ol>

  <p>&nbsp;<img alt="photo_gimp.png" src="_booki/command-line/static/IntroCommandLine-CommandLineIntro-photo_gimp-en.png" height="519" width="503"></p>

  <p>Using the command line, you can achieve the same&nbsp;result by typing the following:</p>
  <pre>
<strong>convert -resize 300 profile.jpg profile_small.jpg</strong>
</pre>

  <p>That is one&nbsp;step in the command line instead of eight steps using the image editor. Perhaps, you think that the time you spend on reading this book and studying the commands is not worth saving seven steps. But what if there were 30 images to be resized? Do you still want to open each image individually and repeat the process 30 times using the image editor? That would require&nbsp;a total of&nbsp;240 steps. Would you rather just type one command instead and get the job done?</p>

  <p>A single command can&nbsp;do the same thing whether you have&nbsp;30 images, 300, or 3000. This is one of the most compelling reasons to start using the command line. You might start with a slow learning curve, but in the long run, it will save a lot of time. Even more important, learning the command line will open up interesting possibilities and fun ways of working. Let us look at some more reasons why learning the command line is a good idea.<br></p>

  <h2>Advantages of Using Commands</h2>

  <p>Many people who give the command line a try are so amazed by its possibilities that they do not even want to go back to a Graphical User Interface(GUI)! Why? Well, in brief, the command line offers the following main advantages over common graphical software:</p>

  <ul>
    <li><strong>Flexibility-</strong>With graphical programs, you sometimes hit a limit; you just can't do what you want or you need to find cumbersome work-arounds to program limits. With&nbsp;the command line, you can combine commands to yield a virtually infinite range of new and interesting functions. By combining commands creatively, you can make the command line do exactly what you want; it puts you in control of your computer.</li>

    <li><strong>Reliability-</strong>Graphical programs are often immature or even unstable. In contrast, most of the tools that the command line offers are highly reliable. One of the reasons for this reliability is their maturity; the oldest command line programs have been around since the late 1970s. This means that these&nbsp;command lines have been tested for over three decades. They also tend to work the same way across different operating systems, unlike most graphical tools. If you want a Swiss Army knife you can rely on, the command line is for you.</li>

    <li><strong>Speed.</strong> Fancy graphics consume&nbsp;a lot of your hardware's resources, often resulting in slowness or instability. The command line, on the other hand, uses the computer's resources much more sparingly, leaving memory and processing power for the tasks that you actually want to accomplish. The command line is also intrinsically faster; instead of clicking through long chains of graphical menus, you can type commands in a dozen or so keystrokes, and often apply them to multiple files or other objects. If you can type&nbsp;fast, this will enable you to drastically increase your productivity.</li>

    <li><strong>Experience.</strong> Using the command line is a great learning experience. When you use the command line, you communicate with your computer more directly than with the graphical programs, thus learning a lot about its inner workings. Using the command line on a regular basis is <em>the</em> way to becoming a GNU/Linux guru.</li>

    <li><strong>Fun.</strong> Have you ever wanted to be like those cool computer hackers who can make a GNU/Linux machine do things that you have not even dreamed of? Once you&nbsp;learn&nbsp;to use this powerful tool, you will find yourself doing funny and interesting stuff that you have never imagined.</li>
  </ul>

  <h2>The Value of Scripting</h2>

  <p>But wait, there's more! You can also store commands in text files. These text files are called <em>scripts</em> and can be used instead of typing a long series of commands each time. For example, if you store commands in a file called <em>mycommand.sh</em>, you don't have to type out the commands again. Instead, you can&nbsp;simply type:</p>
  <pre>
<strong>mycommand.sh</strong>
</pre>

  <p>In addition, you can combine commands together in simple or sophisticated ways. Further, you can schedule scripts to run&nbsp;at a specific time or specific date or at the occurrence of a specific event on your computer.</p>

  <p>You can also write scripts that&nbsp;accept additional information from you. For example, an image resizing script might ask you to what size the images should be resized before it starts the process.</p>

  <p>Ever tried to do anything remotely like that by using a GUI? Perhaps now you can see how working with the command line interface (CLI) starts to open a whole new world to using your computer.</p>

  <h2>Is my Computer Sick?</h2>

  <p>The&nbsp;command line is also used to check the well-being of your computer. There are many commands you can use to check every facet of your computer's health, from the amount of space left on the hard drive to the temperature of the CPU. If your computer is acting poorly and you do not know what the matter is, a few commands will help you quickly determine whether it is a hardware or a software issue, and help you quickly rectify the problem.<br></p>

  <h2>Spanning the network</h2>

  <p>Another&nbsp;interesting feature of command line interfaces that GUIs can't match is&nbsp;the interaction over a network. Imagine you have a computer in another room and you wish to turn it off. How do you do that? Easy right? Get up, walk to the computer, and click on the "Shutdown" button.</p>

  <p>Well,&nbsp;those who&nbsp;know how to&nbsp;connect to the computer in the next room use the command line and type <code>halt</code> to do the same thing.</p>

  <p>That might seem trivial. Perhaps it's actually better for you to get up off that comfy chair and expend 5 calories walking to the next room. However, what if the computer you wanted to shut down was in another suburb? In another city? Another country?&nbsp; Then, remote control of that computer might&nbsp;be very useful.</p>

  <p>Shutting down a remote computer is just a start. Everything you can do on the command line you can do on the remote computer. That means you can run scripts, execute commands, edit text files, check the diagnostics, and do many other tasks. The world of the command line just got a whole lot bigger.</p>

  <h2>Even graphical programs are commands</h2>

  <p>When you click an icon or menu item to start a program, you are actually running a command. You may require, at times,&nbsp;to understand what commands are you are running. For instance, if you suspect a program is running invisibly in the background and slowing your computer, you can find its command and terminate the program. GUI programs often send more error messages to the command line than to GUI dialog boxes. You can often use these command line messages to diagnose problems more precisely than you can using a graphical interface.<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="getting-started.html">GETTING STARTED</a></li>

  <li><a href="beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="standard-files.html">STANDARD FILES</a></li>

  <li><a href="cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="superusers.html">SUPERUSERS</a></li>

  <li><a href="redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="piping.html">PIPING</a></li>

  <li><a href="processes.html">PROCESSES</a></li>

  <li><a href="file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="permissions.html">PERMISSIONS</a></li>

  <li><a href="interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="moving-again.html">MOVING AGAIN</a></li>

  <li><a href="customisation.html">CUSTOMISATION</a></li>

  <li><a href="parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ssh.html">SSH</a></li>

  <li><a href="installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="text-editors.html">TEXT EDITORS</a></li>

  <li><a href="nano.html">NANO</a></li>

  <li><a href="vim.html">VIM</a></li>

  <li><a href="emacs.html">EMACS</a></li>

  <li><a href="kedit.html">KEDIT</a></li>

  <li><a href="gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="scripting.html">SCRIPTING</a></li>

  <li><a href="maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="sed.html">SED</a></li>

  <li><a href="awk.html">AWK</a></li>

  <li><a href="regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">SCRIPTING LANGUAGES</li>

  <li><a href="perl.html">PERL</a></li>

  <li><a href="python.html">PYTHON</a></li>

  <li><a href="ruby.html">RUBY</a></li>

  <li><a href="gnu-octave.html">GNU OCTAVE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="glossary.html">GLOSSARY</a></li>

  <li><a href="command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="outline.html">OUTLINE</a></li>

  <li><a href="credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="index.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="index.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

