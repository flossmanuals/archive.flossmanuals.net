<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch015_searching-for-files.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch015_searching-for-files.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Searching for Files<br></h1>

  <p>When you first get a computer, you tend to place files in just a couple folders or directories. But as your list of files grows, you have to create some subdirectories and spread the files around in order to keep your sanity. Eventually, you forget where files are. "Where did I store those photos I took in Normandy?"</p>

  <p>You could run <code>ls -R</code>, as in the following section, and start running your finger down the screen, but why? Computers are supposed to be about automation. Let the computer figure out where the file is.</p>

  <p>If you know your file is named "somefile", telling the computer what to do is pretty easy.</p>
  <pre>
$<strong> find . -name somefile -print </strong>
./files/somefile
</pre>

  <p>The <code>find</code> command takes more arguments than the other commands we've seen so far, but if you use it for a while you'll find it becomes natural.&nbsp; Its first argument (the '.') tells find where to start looking: the directory at the top of everything you're searching through. In this case, we're telling <code>find</code> to start looking in whatever directory we're in right now.</p>

  <p>The <code>-name</code> argument tells it to look for a file named <em>somefile</em>.&nbsp; Finally, the <code>-print</code> option tells the command to print out on our screen the location of any file that matches the name it was given.</p>

  <h2><strong>Wildcards with Find</strong></h2>

  <p>What if you don't remember the name of the file you're looking for?&nbsp; You might only remember that it starts with "some".&nbsp; Luckily, <code>find</code> can handle that too.&nbsp;</p>
  <pre>
$<strong> find . -name 'some*' -print </strong>
./dir1/subdir2/files/somefile_other
./some_other_file
./files/somefile 
</pre>

  <p>This time it found a few more files than you were after but it still found the one you wanted.&nbsp; As you can see, the <code>find</code> command can process wildcards in much the same way the shell can.&nbsp; Here you asked it to look for anything that starts with the letters "some".</p>

  <p>The "*", "?", and "[ ]" wildcards can all be used just as they would be in the shell.&nbsp; However, since <code>find</code> is using the wildcards you have to make sure they remain unaltered by the shell.&nbsp; To do this you can surround the name you're searching for, and the wildcards it contains, in single quotes.</p>

  <h2><strong>Trimming The Search Path</strong></h2>

  <p>With just a name and a location, find will begin searching through every directory below its starting point, looking for matches.&nbsp; Depending on how many subdirectories you have where you're searching, <code>find</code> can take a lot of time to look in places you know don't contain the file.</p>

  <p>It is possible, however, to control how far <code>find</code> sinks in the directory tree.</p>
  <pre>
$<strong> find . -maxdepth 1 -name 'some*' -print </strong>
./some_other_file
</pre>

  <p>By using the <code>-maxdepth</code> argument we can tell <code>find</code> to go no lower than the number of directories we specify.&nbsp; A maxdepth of 1 says: don't leave the starting directory. A maxdepth of 3 would allow <code>find</code> to descend 3 directories from where it started, and so on.&nbsp; It's important to note that <code>-maxdepth</code> should immediately follow the start location, or find will complain.</p>

  <h2><strong>Using Criteria<br></strong></h2>

  <p>The <code>find</code> command can search for files based on any criteria the filesystem know about files. For instance, you can search for files based on:</p>

  <ul>
    <li>When they were last modified or accessed (somebody read them)</li>

    <li>How big they are<br></li>

    <li>Who owns them, or what group they are in</li>

    <li>What permissions (read, write, execute) they have</li>
  </ul>

  <ul>
    <li>What type of file (directory, regular file) they are</li>
  </ul>

  <p>and other criteria described in the manual page. Here we'll just show a couple popular options.</p>

  <p>The <code>-mtime</code> option shows the latest modification time. Suppose you just can't remember anything about a file's name, but know that you created or modified it within the past three days. You can find all the files in your home directory that were created or modified within the past three days through:</p>
  <pre>
$ <strong>find ~ -mtime -3 -print</strong>
</pre>

  <p>Notice the minus sign before the 3, for "less than." If you know you created the file yesterday (between 24 and 48 hours ago), you can search for an exact day:<br></p>
  <pre>
$ <strong>find ~ -mtime 1 -print</strong>
</pre>

  <p>&nbsp;To find files that are more than 30 days old (caution: there will be a lot of these), use a plus sign:</p>
  <pre>
$ <strong>find ~ -mtime +30 -print</strong>
</pre>

  <p>Perhaps you want to remove old files that are large, before backing up a directory. Combine <code>-mtime</code> with <code>-size</code> to find these files. The file has to match all the criteria you specify in order to be printed.<br></p>
  <pre>
$ <strong>find directory_to_backup  -mtime +30  -size +500k  -print</strong>
</pre>

  <p>We've specified +500k as our <code>-size</code> option. The plus sign means "greater than" and "500k" means "500 kilobytes in size".<br></p>

  <h2><strong>Using Find To Run a Command on Multiple Files</strong></h2>

  <p>The find command can do much more powerful things than print filenames. You can combine it with any other command you want, so that you can remove files, move them around, look for text in them, and so on. On those occasions, the <code>find</code> command with its <code>-exec</code> option is just what you'll need.</p>

  <p>Because the next example is long, it is divided onto two lines, with a backslash at the end of the first so the shell keeps reading and keeps the two lines as one command. The first line is the same as the command to find old, large files in the previous section.</p>
  <pre>
$ <strong>find directory_to_backup  -mtime +30  -size +500k -print \</strong>
             <strong> -exec rm {} \;</strong>
</pre>

  <p>The <code>-exec</code> option is followed by an <code>rm</code> command, but there are two odd items after it:</p>

  <ul>
    <li>{} is a special convention in the <code>-exec</code> option that means "the current file that was found"</li>

    <li>\; is necessary to tell find what the end of the command is. A command can have any number of arguments. Think of <code>-exec</code> and \; as surrounding the command you want to execute.</li>
  </ul>

  <p>So we find each file, print the name through <code>-print</code> (which we don't have to do, but we're curious to see what's being removed), and then remove it in the <code>-exec</code> option.</p>

  <p>Clearly, a tiny mistake in a find command could lead to major losses of data when used with <code>-exec</code>. Test your commands on throw-away files first!<br></p>

  <p>Using <code>cp</code> you can see how the bracket pairs can be specified multiple times, allowing the file's name to be easily duplicated.</p>
  <pre>
$<strong> find . -name 'file*' -exec cp {} {}.backup \; </strong> 
</pre>

  <p>Experiment and practice!<br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="ch004_getting-started.html">GETTING STARTED</a></li>

  <li><a href="ch005_beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="ch006_moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="ch008_basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="ch009_standard-files.html">STANDARD FILES</a></li>

  <li><a href="ch010_cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="ch011_superusers.html">SUPERUSERS</a></li>

  <li><a href="ch012_redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="ch014_multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="ch015_searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="ch016_piping.html">PIPING</a></li>

  <li><a href="ch017_processes.html">PROCESSES</a></li>

  <li><a href="ch018_file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="ch019_command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="ch021_permissions.html">PERMISSIONS</a></li>

  <li><a href="ch022_interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="ch023_checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="ch024_sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="ch025_moving-again.html">MOVING AGAIN</a></li>

  <li><a href="ch026_customisation.html">CUSTOMISATION</a></li>

  <li><a href="ch027_parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="ch028_gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ch029_ssh.html">SSH</a></li>

  <li><a href="ch030_installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="ch031_making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="ch033_text-editors.html">TEXT EDITORS</a></li>

  <li><a href="ch034_nano.html">NANO</a></li>

  <li><a href="ch035_vim.html">VIM</a></li>

  <li><a href="ch036_emacs.html">EMACS</a></li>

  <li><a href="ch037_kedit.html">KEDIT</a></li>

  <li><a href="ch038_gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="ch040_scripting.html">SCRIPTING</a></li>

  <li><a href="ch041_maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="ch042_other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="ch043_sed.html">SED</a></li>

  <li><a href="ch044_awk.html">AWK</a></li>

  <li><a href="ch045_regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">Click here to edit chapter title</li>

  <li><a href="ch047_perl.html">PERL</a></li>

  <li><a href="ch048_python.html">PYTHON</a></li>

  <li><a href="ch049_ruby.html">RUBY</a></li>

  <li><a href="ch050_kjksajdsa.html">kjksajdsa</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch052_glossary.html">GLOSSARY</a></li>

  <li><a href="ch053_command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="ch054_outline.html">OUTLINE</a></li>

  <li><a href="ch055_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch015_searching-for-files.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch015_searching-for-files.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

