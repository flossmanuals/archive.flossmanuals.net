<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="moving-around.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="moving-around.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Moving Around</h1>

  <p>Anyone who has used a graphical interface has moved between folders. A typical view of folders appears in Figure 1, where someone has opened a home directory, then a folder named "my-stuff" under that, and a folder named "music" under that.</p>

  <p><em>Figure 1 : Folders</em><br></p>

  <p>When you use the command line, folders are called directories. That's just an older term used commonly in computing to refer to collections of things. (Try making an icon that suggests "directory"). Anything you do in a folder on the desktop is reflected in the directory when you're on the command line, and vice versa. The desktop and the command line provide different ways of viewing a directory/folder, and each has advantages and disadvantages.<br></p>

  <p>Files contain your information--whether pictures, text, music, spreadsheet data, or something else--while the directories are containers for files. Directories can also store other directories. You'll be much more comfortable with the command line once you can move around directories, view them, create and remove them, and so on.</p>

  <p>Directories are organized, in turn, into filesystems. Your hard disk has one type of filesystem, a CD-ROM or DVD has another, a USB mass storage device has yet another, and so on. That's why a CD-ROM, DVD, or USB device shows up as something special on the desktop when you insert it. Luckily, you don't have to worry much about the differences because both the desktop and the terminal can hide the differences. But sometimes in this book we'll talk about the information a filesystem has about your files.</p>

  <p>The "first" directory is called the root and is represented by the name / (just a forward slash).&nbsp; You can think of all the directories and files on the system as a tree that grows upside-down from this root (Figure 2):</p>

  <p><img alt="desktop_home_vim.png" src="_booki/command-line/static/desktop_home_vim-en.png" height="557" width="600"></p>

  <p><em>Figure 2 : Root Directory</em><br></p>

  <h2>Absolute and relative paths<br></h2>

  <p>Every file and directory in the system has an "address" called its absolute path or sometimes just its path.&nbsp; It describes the route you have to follow starting from the root that would take you to that particular file or directory.</p>

  <p>For example, suppose you like the vim editor that we'll introduce in a later chapter, and are told you can start it by running the command <code>/usr/bin/vim</code>. This point underlines what we said in an earlier chapter: commands are just executable files. So the vim editor is a file with the path <em>/usr/bin/vim</em>, and if you run that command <code>/usr/bin/vim</code> you will execute the editor. As you can see from these examples, the slash / is also used as a separator between directories.</p>

  <p>Can you find <em>/usr/bin/vim</em> in Figure 2? The pathname can be interpreted as follows:</p>

  <ol>
    <li>Start at the root (/) directory.<br></li>

    <li value="2">Move from / down to a directory named <em>usr</em>.</li>
  </ol>

  <ol>
    <li value="2">Move from <em>usr</em> down to a directory named <em>bin</em>.</li>

    <li value="2"><em>vim</em> is located in that directory.<br></li>
  </ol>

  <blockquote>
    <em>You are just getting used to the command line, and it may feel odd to be typing while reading this book. If you feel any confusion in this section, try scribbling the directory tree in Figure 2 on paper. Draw arrows on the paper as you run the commands in this section, to help orient you to where you are.</em><br>
  </blockquote>

  <p>Note that you can't tell whether something is a file or a directory just by looking at its path.</p>

  <p>When you work with the command line you will be always working "in" a directory.&nbsp; You can find the path of this directory using the command <code>pwd</code> (print working directory), like this:</p>
  <pre>
$ <strong>pwd</strong>
/home/ben
</pre>

  <p>You can see that <code>pwd</code> prints an absolute path.&nbsp; If you want to switch your working directory you can use the command <code>cd</code> (change directory) followed by an argument which points to the target directory:</p>
  <pre>
$ <strong>cd /</strong>
</pre>

  <p>You just changed your working directory to the root of the filesystem!&nbsp; If you want to return to the previous directory, you can enter the command:</p>
  <pre>
$ <strong>cd /home/ben</strong>
</pre>

  <p>As an alternative, you can "work your way" back to <em>/home/ben</em> using relative paths.&nbsp; They are called that because they are specified "in relation" to your current working directory.&nbsp; If you go back to the root directory, you could enter the following commands:</p>
  <pre>
$ <strong>cd /</strong>
$ <strong>cd home</strong>
$ <strong>cd ben</strong>
$ <strong>pwd</strong>
/home/ben
</pre>

  <p>The first command changes your current working directory to the root. The second changes to <em>home</em>, relative to /, making your current working directory <em>/home</em>.&nbsp; The third command changes it to <em>ben</em>, relative to <em>/home</em>, landing you in <em>/home/ben</em>.</p>

  <h3>Good to be back home<br></h3>

  <p>Every user in the system has a directory assigned to him or her, called the home directory.&nbsp; No matter what your current working directory is, you can quickly return to your home directory like this:</p>
  <pre>
$ <strong>cd</strong>
</pre>

  <p>That is, enter the <code>cd</code> command without any arguments.</p>

  <p>All your files and preferences are stored in your home directory (or its subdirectories). Every user of your system with a login account gets her own home directory. Home directories are usually named the same as users' login names, and are usually found in <em>/home</em>, although a few systems have them in <em>/usr/home</em>. When you start your terminal, it will place you in your home directory.</p>

  <p>There's a special shortcut to refer to your home directory, namely the symbol ~ (usually called a tilde, and found near the very left top of most keyboards). You can use it as part of more complex path expressions, and it will always refer to your home directory. For example, <em>~/Desktop</em> refers to the directory called <em>Desktop</em> that usually exists within your home directory.</p>

  <h3>The . and .. directories</h3>

  <p>The entries . and .. are special and they exist in every directory, even the root directory itself (/). The first one is a shorthand for "this directory" while the latter is a shorthand for "the parent directory of this directory."&nbsp; You can use them as a relative path, and you can try and see what happens when you do this:</p>
  <pre>
$ <strong>pwd</strong>
/usr/bin
$ <strong>cd .</strong>
$ <strong>pwd</strong>
/usr/bin
</pre>

  <p>If <em>vim</em> is in <em>/usr/bin</em>, at this point you could run it by typing the relative path:</p>
  <pre>
$ <strong>./vim</strong>
</pre>

  <p>Continuing from the previous example, you can do this:</p>
  <pre>
$ <strong>cd ..</strong>
$ <strong>pwd</strong>
/usr
</pre>

  <p>Since they are actual entries in the filesystem, you can use them as part of more complex paths, for example:</p>
  <pre>
$ <strong>cd /usr/bin</strong>
$ <strong>pwd</strong>
/usr/bin
$ <strong>cd ../lib</strong>
$ <strong>pwd</strong>
/usr/lib
$ <strong>cd ../..</strong>
$ <strong>pwd</strong>
/
$ <strong>cd home</strong>
$ <strong>pwd</strong>
/home
$ <strong>cd ../usr/bin</strong>
$ <strong>pwd</strong>
/usr/bin
</pre>

  <p>The parent directory of the root directory, /.., is root itself.<br></p>

  <p>Try moving around your computer on the command line and you will soon get used to it!</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="getting-started.html">GETTING STARTED</a></li>

  <li><a href="beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="standard-files.html">STANDARD FILES</a></li>

  <li><a href="cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="superusers.html">SUPERUSERS</a></li>

  <li><a href="redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="piping.html">PIPING</a></li>

  <li><a href="processes.html">PROCESSES</a></li>

  <li><a href="file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="permissions.html">PERMISSIONS</a></li>

  <li><a href="interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="moving-again.html">MOVING AGAIN</a></li>

  <li><a href="customisation.html">CUSTOMISATION</a></li>

  <li><a href="parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ssh.html">SSH</a></li>

  <li><a href="installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="text-editors.html">TEXT EDITORS</a></li>

  <li><a href="nano.html">NANO</a></li>

  <li><a href="vim.html">VIM</a></li>

  <li><a href="emacs.html">EMACS</a></li>

  <li><a href="kedit.html">KEDIT</a></li>

  <li><a href="gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="scripting.html">SCRIPTING</a></li>

  <li><a href="maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="sed.html">SED</a></li>

  <li><a href="awk.html">AWK</a></li>

  <li><a href="regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">SCRIPTING LANGUAGES</li>

  <li><a href="perl.html">PERL</a></li>

  <li><a href="python.html">PYTHON</a></li>

  <li><a href="ruby.html">RUBY</a></li>

  <li><a href="gnu-octave.html">GNU OCTAVE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="glossary.html">GLOSSARY</a></li>

  <li><a href="command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="outline.html">OUTLINE</a></li>

  <li><a href="credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="moving-around.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="moving-around.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

