<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="command-history.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="command-history.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Command History Shortcuts</h1>

  <p>The shell lets you bring back old commands and re-enter them, making changes if you want. This is one of the easiest and most efficient ways to cut down on typing, because repeated sequences of commands are very common. For instance, in the following sequence we're going through various directories, listing what's there, deleting files we don't want, and saving certain files under different names:</p>
  <pre>
cd Pictures/                              
ls -l status.log.*
rm status.log.[3-5]
mv status.log.1 status.log.bak

cd ../Documents/
ls -l status.log*                                                            
rm status.log.[2-4]
mv status.log.1 status.log.bak

cd ../Videos/
ls -l status.log*
rm status.log.[2-5]
mv status.log.1 status.log.bak
</pre>

  <p>Eventually, if you had to do this kind of clean-up regularly, you would write a script to automate it and perhaps use a cron job to run it at regular intervals. But for now, we'll just see how to drastically reduce the amount of typing you need while entering the commands manually.</p>

  <p>An earlier chapter showed you how to use arrow keys to move around in your command history as if you were editing a file. This chapter shows a more complicated and older method of manipulating the command history. Sometimes you'll find the methods in this chapter easier, so it's worth practicing them. For instance, suppose you know you entered the <code>mv</code> command you want (or another one very similar to what you want) an hour ago. Pressing the back arrow repeatedly is a lot more trouble than recalling the command using the technique in this section.</p>

  <h2>Recalling a command by a string<br></h2>

  <p>The <em>bang operator</em>, named after the ! character (an exclamation point, or more colloquially "bang"), allows you to repeat recent commands in your history.<br></p>

  <p><em>!string</em> executes the most recent command that starts with <em>string</em>. Thus, to execute the exact same mv command you did before, enter:</p>
  <pre>
<font color="#000000">!mv</font>
</pre>

  <p>What if you don't want the exact same command? What if you want to edit it slightly before executing it? Or just want to look at what the bang operator retrieves to make sure that's the command you want? You can retrieve it without executing it by adding <code>:p</code> (for "print"):<br></p>
  <pre>
<font color="#000000">!mv:p</font>
</pre>

  <p>We'll show you how to edit commands soon.<br></p>

  <p>Perhaps you issued a lot of <code>mv</code> commands, but you know there's a unique string in the middle of the command you want. Surround the string with ? characters, as follows:</p>
  <pre>
<font color="#000000">!?log?</font>
</pre>

  <p>Entering two bangs in a row repeats the last run command. A very useful command history idiom is re-running the last command with superuser privilege:</p>
  <pre>
<font color="#000000">sudo !!</font>
</pre>as we all happen to type commands without the right permissions from time to time.

  <p>While running your last command may seem to have limited use, this method can be modified to select only portions of your last command, as we will see later.<br></p>

  <h2>Recalling a command by number<br></h2>

  <p>The shell numbers each command as it is executed, in order. If you like recalling commands by number, you should alter your prompt to include the number (a later chapter shows you how). You can also look at a list of commands with their numbers by executing the <code>history</code> command:</p>
  <pre>
$ <strong>history</strong>                                                       
...
  502  cd Pictures/
  503  ls -l status.log*
  504  rm status.log.[3-5]
  505  mv status.log.1 status.log.bak
  506  cd ../Documents/
  507  history                                      
$
</pre>

  <p>Here we've shown only the last few lines of output. If you want to re-execute the most recent <code>rm</code> command (command number 504), you can do so by entering:</p>
  <pre>
!504
</pre>

  <p>But&nbsp; the numbers are probably more useful when you think backwards. For instance, if you remember that you entered the <code>rm</code> command followed by three more commands, you can re-execute the <code>rm</code> command through:</p>
  <pre>
!-4
</pre>

  <p>That tells the shell, "start where I am now, count back four commands, and execute the command at that point".<br></p>

  <h3><strong>Repeating arguments</strong></h3>

  <p>You'll often find yourself reusing portions of a previous command, either because you made a typo, or because you are running a sequence of commands for a certain task. We accomplish this using the bang operator with modifiers.</p>

  <p>The three most useful modifiers are: *, !^, and !$, which are shortcuts for all, first, and last arguments respectively. Let's look at these in order.</p>

  <p><em>"commandName *"</em> executes the <em>commandName</em> with any arguments you used on your last command. This maybe useful if you make a spelling mistake. For example, if you typed <em>emasc</em> instead of <em>emacs</em>:</p>
  <pre class="SCREEN">
<font color="#000000">emasc /home/fred/mywork.java /tmp/testme.java</font>
</pre>

  <p>That obviously fails. What you can do now is type:</p>
  <pre class="SCREEN">
<font color="#000000">emacs !*</font>
</pre>

  <p>This executes <code>emacs</code> with the arguments that you last typed on the command-line. It is equivalent to typing:</p>
  <pre class="SCREEN">
<font color="#000000">emacs /home/fred/mywork.java /tmp/testme.java</font>
</pre>

  <p><em>"commandName !^"</em> repeats the first argument.</p>
  <pre class="SCREEN">
emacs /home/fred/mywork.java /tmp/testme.java
svn commit !^    # equivalent to: svn commit /home/fred/mywork.java
</pre>

  <p><em>"commandName !$"</em> repeats the last argument.</p>
  <pre class="SCREEN">
mv /home/fred/downloads/sample_screen_config /home/fred/.screenrc
emacs !$     # equivalent to: emacs /home/fred/.screenrc
</pre>

  <p>You can use these in conjunction as well. Say you typed:</p>
  <pre class="SCREEN">
mv mywork.java mywork.java.backup
</pre>when you really meant to make a copy. You can rectify that by running:
  <pre class="SCREEN">
cp mywork.java.backup mywork.java
</pre>

  <p>But since you are reusing the arguments in reverse, a useful shortcut would be:</p>
  <pre class="SCREEN">
cp !$ !^
</pre>

  <p>For finer-grained control over arguments, you can use the double bang with the <code>:N</code> modifier to select the Nth argument.&nbsp; This is most useful when you are running a command with <code>sudo</code>, since your original command becomes the first argument.&nbsp; The example below demonstrates how to do it.</p>
  <pre class="SCREEN">
sudo cp /etc/apache2/sites-available/siteconfig /home/fred/siteconfig.bak
echo !^ !!:2  # equivalent to echo cp /etc/apache2/sites-available/siteconfig
</pre>

  <p>A range is also possible with <code>!!:M-N</code>.<br></p>

  <h3><strong>Editing arguments</strong><br></h3>

  <p>Often you'll want to re-execute the previous command, but change one string within it. For instance, suppose you run a command on <em>file1</em>:</p>
  <pre>
$ <strong>wc file1</strong>
     443    1578    9800 file1
</pre>

  <p>Now you want to remove <em>file2</em>, which has a name very close to <em>file1</em>. You can use the last parameter of the previous command through "!$", but alter it as follows:</p>
  <pre>
$ <strong>rm !$:s/1/2/</strong>                                    
rm file2
</pre>

  <p>That looks a little complicated, so let's take apart the argument:</p>
  <pre>
!$   :   s/1/2/
</pre>

  <p>The "!$" is followed by a colon and then a "s" command, standing for "substitute". Following that is the string you want to replace (1) and the string you want to put in its place (2) surrounded by slashes. The shell prints the command the way it interprets your input, then executes it.</p>

  <p>Because this kind of substitution is so common, you'll be glad to hear there's a much simpler way to rerun a command with a minor change. You can change only one string in the command through this syntax:<br></p>
  <pre>
$ <strong>wc file1</strong>                                                   
     443    1578    9800 file1
$ <strong>^1^2</strong>                                        
wc file2
</pre>

  <p>We used a caret (^), the string we wanted to replace, another caret, and the string we want to put in its place.<br></p>

  <h2>Searching&nbsp;through&nbsp;the&nbsp;Command&nbsp;History</h2>

  <p>Use the <strong>Ctrl + R</strong> key combination to perform a "reverse-i-search". For example, if you wanted to use the command you used the last time you used <code>snort</code>, start by typing <strong>Ctrl + R</strong>. In the terminal window you'll see:</p>
  <pre>
(reverse-i-search)`':
</pre>

  <p>As you type each letter (s, n, etc.) the shell displays the most recent command that has that string somewhere. When you finish typing "snort", you can use <strong>Ctrl + R</strong> repeatedly to search back through all commands containing "snort." When you find the command you're looking for, you can press the right or left arrow keys to place the command on an actual command line so you can edit it, or just press <strong>Enter</strong> to execute the command.</p>

  <h2>Sharing Bash History</h2>

  <p>The Bash shell saves your history so that you can recall commands from earlier sessions. But the history is saved only when you close the terminal. If you happen to be working in two terminals simultaneously, this means you can't share commands.</p>

  <p>To fix this--if you want the terminal to save each command immediately after its execution--add the following lines to your <em>~/.bashrc</em> file:</p>
  <pre>
shopt -s histappend
PROMPT_COMMAND='history -a'
</pre>

  <p>Learning these shortcuts can save you a tremendous amount of time so please experiment!</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="getting-started.html">GETTING STARTED</a></li>

  <li><a href="beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="standard-files.html">STANDARD FILES</a></li>

  <li><a href="cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="superusers.html">SUPERUSERS</a></li>

  <li><a href="redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="piping.html">PIPING</a></li>

  <li><a href="processes.html">PROCESSES</a></li>

  <li><a href="file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="permissions.html">PERMISSIONS</a></li>

  <li><a href="interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="moving-again.html">MOVING AGAIN</a></li>

  <li><a href="customisation.html">CUSTOMISATION</a></li>

  <li><a href="parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ssh.html">SSH</a></li>

  <li><a href="installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="text-editors.html">TEXT EDITORS</a></li>

  <li><a href="nano.html">NANO</a></li>

  <li><a href="vim.html">VIM</a></li>

  <li><a href="emacs.html">EMACS</a></li>

  <li><a href="kedit.html">KEDIT</a></li>

  <li><a href="gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="scripting.html">SCRIPTING</a></li>

  <li><a href="maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="sed.html">SED</a></li>

  <li><a href="awk.html">AWK</a></li>

  <li><a href="regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">SCRIPTING LANGUAGES</li>

  <li><a href="perl.html">PERL</a></li>

  <li><a href="python.html">PYTHON</a></li>

  <li><a href="ruby.html">RUBY</a></li>

  <li><a href="gnu-octave.html">GNU OCTAVE</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="glossary.html">GLOSSARY</a></li>

  <li><a href="command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="outline.html">OUTLINE</a></li>

  <li><a href="credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="command-history.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="command-history.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

