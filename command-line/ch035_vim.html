<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Introduction to the Command Line</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/command-line/command-line.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/command-line/command-line.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Introduction to the Command Line
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch035_vim.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch035_vim.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>vi and vim</h1>

  <p>Vi is a very powerful command-line text editor. It's used for everything from quick fixes in configuration files to professional programming and even for writing large, complex documents like this book. It's popular on the one hand because it's fast and light-weight, and you can accomplish a lot with a few keystrokes. On the other hand it's also powerful: highly configurable, with many built-in functions.</p>

  <p>Vim is an enhanced version of Vi, offering a lot of features that make life easier for both the novice and expert (Vim stands for "Vi IMproved"). On many modern systems, Vim is installed as the default version of Vi. So if you invoke the <code>vi</code> command you actually run Vim. This is usually not confusing, because everything in Vi works in Vim as well. We will look at Vim in this chapter, but if your system has Vi you can apply these techniques, just replace any reference to <code>vim</code> in the commands to <code>vi</code>.<br></p>

  <p>The best feature about Vim/Vi is that it's shipped with virtually all GNU/Linux variants by default. Once you learn Vim, whenever you are, you can have the power of efficient editing.</p>

  <p>The main drawback of Vim is, as for Emacs (another command line editor), the learning curve. The keyboard short-cuts can be daunting to learn.</p>

  <p>Fortunately, you can work around those drawbacks by using a graphical version of Vim (GVim) with all the buttons and menus for more graphical users. You can also try easy-Vim, with Notepad style editing.</p>

  <p>These simplified versions of Vim reduce the learning curve a lot and expose less advanced users to the power of efficient editing, which in turn increases one's will to learn a more powerful editor.</p>

  <h2>Basic commands</h2>

  <p>To open Vim and begin creating a new text file, get a command-line open and type:</p>
  <pre>
$ <strong>vim</strong>
</pre>

  <p>This presents you with a blank screen, or (if the program running is Vim) a screen of information looking something like this:</p>

  <p><img title="vim" alt="vim" src="_booki/command-line/static/IntroCommandLine-nano-vim-en.png" width="507" height="366"></p>

  <p>If you want to open an existing file, just specify it on the command line as an argument. For instance, the following opens a file called <em>/etc/fstab</em>:<br></p>
  <pre>
$ <strong>vim /etc/fstab</strong>
</pre>

  <p>This file already exists on most GNU/Linux systems, but if you open a non-existent file, you'll get a blank screen. The next section shows you how to insert text; when you're finished you can then save the file.</p>

  <h3>Inserting Text</h3>

  <p>Whether you have a blank screen or a file with text in it, you can add text by entering what's known as <em>edit mode</em>. Just press <strong>i</strong>. You should see this on the bottom of the screen:</p>
  <pre>
-- INSERT --
</pre>

  <p>Whenever this appears on the bottom of the screen, you are in edit mode. Whatever you type becomes part of the file. For instance, try entering "This is line 1." Then press the <strong>Enter</strong> key and enter "This is line 2". Here's what this fascinating contribution to literature looks like in Vim:</p>

  <p><img title="vim_insert" alt="vim_insert" src="_booki/command-line/static/IntroCommandLine-nano-vim_insert-en.png" width="507" height="366"></p>

  <p>When you are finished inserting text, press the <strong>Esc</strong> (Escape) key to leave edit mode; that puts you in normal mode.</p>

  <h3>Only One Place At a Time: The Cursor</h3>

  <p>Vim, like every editor, keeps track of where you are and shows a cursor at that point, which may look like an underline or a box in a different color. In edit mode, you can backspace to remove characters. Vim also allows you to move around using arrow keys and edit the whole document freely. But normal Vi doesn't let you move around; it restricts you to adding text or backspacing to remove it.</p>

  <p>In normal Vi, if you want to go back over what you edited, or move to another place in the file, you must press the <strong>Esc</strong> key, move to the place where you want to insert text, and enter edit mode again. This may seem cumbersome. But Vi provides so many alternate ways of moving around and adding text that you'll find, with some practice, that it's no barrier to productivity. As already mentioned, Vim lets you move around freely and edit the whole file when you're in edit mode--but you'll find yourself leaving edit mode often in order to make use of Vim's powerful commands.<br></p>

  <h3>Basic Movement Commands</h3>

  <p>To practice moving around in a file, you can press the <strong>Esc</strong> key to get out of edit mode. If you have only a small amount of text in the file, you may prefer to find another text file on your system that's larger, and open that. Remember that when you open a file you're in normal mode, not edit mode.</p>

  <p>To move around use the arrow keys.</p>

  <p>To jump to a specific line use the colon button followed by a line number. The following jumps to line 20:</p>
  <pre>
<strong>:20</strong>
</pre>

  <p>You can move quickly up or down a text file by pressing the <strong>PgUp</strong> and <strong>PgDn</strong> keys.</p>

  <p>Search for text by pressing the slash key (/) and then typing the text you want to find:</p>
  <pre>
<strong>/birthday party</strong>
</pre>

  <p>You can simply repeat the <strong>/</strong> key to search for the next occurrence of the string. The search is case-sensitive. To search backward, press the question-mark key (?) instead of the slash key.<br></p>

  <h3>Saving and exiting</h3>

  <p>If you're in edit mode, you can save your changes by pressing <strong>Esc</strong> to go to normal mode, typing <strong>:w</strong> and pressing the <strong>Enter</strong> key. This saves your changes to the file you specified when you opened Vim.</p>

  <blockquote>
    <p>Note: Vim, by default, does not save a backup of the original version of the file. Your <strong>:w</strong> command deletes the old contents forever. Vim can be configured to save backups, though.</p>
  </blockquote>

  <p>If you opened Vim without specifying a file name, you receive an error message when you press <strong>:w</strong>:</p>
  <pre>
E32: No file name
</pre>

  <p>To fix this, specify <strong>:w</strong> with a filename:</p>
  <pre>
<strong>:w</strong> <em><strong>mytestfile.txt</strong></em>
</pre>

  <p>This must also be followed by the <strong>Enter</strong> key.</p>

  <p>To exit Vim, press <strong>:q</strong>. If you have unsaved text, you receive an error message:</p>
  <pre>
E37: No write since last change (add ! to override)
</pre>

  <p>Like most word processors, Vim tries to warn you when you might make a mistake that costs you work.&nbsp; As the message suggests, you can abandon your text and exit by pressing <strong>:q!</strong>. Or use <strong>:w</strong> to save your changes and then enter <strong>:q</strong> again. You can combine writing and quitting through any of the following:</p>
  <pre>
<strong>:wq</strong> (followed by Enter)
<strong>:x</strong> (followed by Enter)
<strong>ZZ</strong>
</pre>

  <h3>Using your mouse with Vim.<br></h3>

  <p>Sometimes it's convenient to use your mouse with Vim, to select text or even just to quickly position the cursor. To enable this mode pass Vim the command:<br></p>
  <pre>
<strong>:set mouse=a</strong> (followed by Enter)
</pre>

  <h3>Opening up several files at the same time</h3>

  <p>Since version 7 Vim has had the (rarely noticed) feature of tabs, that's right, just like those in Firefox or other tabbed applications.</p>

  <p>To use tabs in Vim pass the command :tabnew &lt;file&gt;. For instance, to open up a file foo.txt so that it appears in a tab I would:</p>
  <pre>
<strong>:tabnew /path/to/foo.txt</strong> (followed by Enter)
</pre>

  <p>To move back and forth between this file and the one you were working on previously, use the keys <strong>g</strong> and then <strong>t</strong>. To help remember this key combination you can think of "g" as in goto and "t" as in tab. You can open up as many files as you like into tabs and use <strong>gt</strong> to traverse between them. If you have enabled mouse input (see "Using your mouse with Vim", above) then you can simply click on the tab itself.<br></p>

  <p>You can close a tab in the same way you would a normal file, with <strong>:wq</strong> to commit the changes, or just <strong>:q</strong> to close without committing.&nbsp;</p>

  <h3>Copy, Cut and Paste (A quick introduction to&nbsp;Visual Mode in Vim)</h3>

  <p>Vim uses a mode called&nbsp;<em>Visual Mode</em> to select text for copying to the 'buffer' (think of a buffer as a clipboard) to be used elsewhere.</p>

  <p>To activate Visual Mode, just hit the&nbsp;<strong>v</strong> key on your keyboard and hit enter. Using the cursor keys (arrow keys) select the text you would like to copy or cut.&nbsp;<br>
  <br>
  To copy the selected text, hit the&nbsp;<strong>y</strong> key ('y' stands for "yank"). Now, use the cursor keys and move to a new location in your document. Now press the&nbsp;<strong>p</strong> key ('p' stands for "put" but you can think of it as "paste") to place the text in the given position.</p>

  <p>To cut the selected text, hit the&nbsp;<strong>x</strong> key (who knows what 'x' stands for!). Now move to a new position in your document and press&nbsp;<strong>p</strong> to "put" the text in that place.</p>

  <p>Sometimes it's useful to select text in lines or columns. Experiment with these other forms of Visual Mode by holding <strong>SHIFT+v</strong> to select whole lines or <strong>CTRL+v</strong> to select columns. Before you know it you'll be working just as quickly as you would with a mouse in a word editor!&nbsp;</p>

  <p>Practice!</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">INTRODUCTION</li>

  <li><a href="index.html">INTRODUCTION</a></li>

  <li><a href="ch002_about-this-manual.html">ABOUT THIS MANUAL</a></li>

  <li class="booki-section">BASICS</li>

  <li><a href="ch004_getting-started.html">GETTING STARTED</a></li>

  <li><a href="ch005_beginning-syntax.html">BEGINNING SYNTAX</a></li>

  <li><a href="ch006_moving-around.html">MOVING AROUND</a></li>

  <li class="booki-section">COMMANDS</li>

  <li><a href="ch008_basic-commands.html">BASIC COMMANDS</a></li>

  <li><a href="ch009_standard-files.html">STANDARD FILES</a></li>

  <li><a href="ch010_cut-down-on-typing.html">CUT DOWN ON TYPING</a></li>

  <li><a href="ch011_superusers.html">SUPERUSERS</a></li>

  <li><a href="ch012_redirection.html">REDIRECTION</a></li>

  <li class="booki-section">ADVANCED-ISH</li>

  <li><a href="ch014_multiple-files.html">MULTIPLE FILES</a></li>

  <li><a href="ch015_searching-for-files.html">SEARCHING FOR FILES</a></li>

  <li><a href="ch016_piping.html">PIPING</a></li>

  <li><a href="ch017_processes.html">PROCESSES</a></li>

  <li><a href="ch018_file-structure.html">FILE STRUCTURE</a></li>

  <li><a href="ch019_command-history.html">COMMAND HISTORY</a></li>

  <li class="booki-section">ADVANCED</li>

  <li><a href="ch021_permissions.html">PERMISSIONS</a></li>

  <li><a href="ch022_interactive-editing.html">INTERACTIVE EDITING</a></li>

  <li><a href="ch023_checking-exit.html">CHECKING EXIT</a></li>

  <li><a href="ch024_sub-commands.html">SUB COMMANDS</a></li>

  <li><a href="ch025_moving-again.html">MOVING AGAIN</a></li>

  <li><a href="ch026_customisation.html">CUSTOMISATION</a></li>

  <li><a href="ch027_parameter-substitution.html">PARAMETER SUBSTITUTION</a></li>

  <li><a href="ch028_gnuscreen.html">GNUSCREEN</a></li>

  <li><a href="ch029_ssh.html">SSH</a></li>

  <li><a href="ch030_installing-software.html">INSTALLING SOFTWARE</a></li>

  <li><a href="ch031_making-your-own-interpreter.html">MAKING YOUR OWN INTERPRETER</a></li>

  <li class="booki-section">TEXT EDITORS</li>

  <li><a href="ch033_text-editors.html">TEXT EDITORS</a></li>

  <li><a href="ch034_nano.html">NANO</a></li>

  <li><a href="ch035_vim.html">VIM</a></li>

  <li><a href="ch036_emacs.html">EMACS</a></li>

  <li><a href="ch037_kedit.html">KEDIT</a></li>

  <li><a href="ch038_gedit.html">GEDIT</a></li>

  <li class="booki-section">SCRIPTING</li>

  <li><a href="ch040_scripting.html">SCRIPTING</a></li>

  <li><a href="ch041_maintaining-scripts.html">MAINTAINING SCRIPTS</a></li>

  <li><a href="ch042_other-languages.html">OTHER LANGUAGES</a></li>

  <li><a href="ch043_sed.html">SED</a></li>

  <li><a href="ch044_awk.html">AWK</a></li>

  <li><a href="ch045_regular-expressions.html">REGULAR EXPRESSIONS</a></li>

  <li class="booki-section">Click here to edit chapter title</li>

  <li><a href="ch047_perl.html">PERL</a></li>

  <li><a href="ch048_python.html">PYTHON</a></li>

  <li><a href="ch049_ruby.html">RUBY</a></li>

  <li><a href="ch050_kjksajdsa.html">kjksajdsa</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch052_glossary.html">GLOSSARY</a></li>

  <li><a href="ch053_command-quickie.html">COMMAND QUICKIE</a></li>

  <li><a href="ch054_outline.html">OUTLINE</a></li>

  <li><a href="ch055_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch035_vim.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch035_vim.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

