<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch036_beyond-education.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch036_beyond-education.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Beyond Education</h1>

  <p>&nbsp;Education has a complicated history, including swings between decentralization -those loose associations of students and teachers typifying some early European universities such as Oxford- to centralized control by the state or church. It's easy to imagine that in some of these cases teachers had great freedom to collaborate with each other or that learning might be a collaboration among students and teacher, while in others, teachers would be told what to teach, and students would learn that, with little opportunity for collaboration.&nbsp;</p>

  <p>The current and unprecedented wealth in the Global North has brought near universal literacy and enrollment in primary education, along with impressive research universities and increasing enrollment in university and graduate programs. This apparent success masks that we are in an age of unprecedented inequality, centralized control, driven by standards politically determined at the level of large jurisdictions, and a model in which teachers teach how to take tests and both students and teachers are consumers of educational materials created by large publishers. Current educational structures and practices do not take advantage of the possibilities offered by collaboration tools and methods and in some cases are in opposition to use of such tools.&nbsp;</p>

  <p>Much as the disconnect between the technological ability to access and build upon and the political and economic reality of closed access in scientific publishing created the Open Access (OA) movement, the disconnect between what is possible and what is practiced in education has created collaborative responses.</p>

  <h2>Open Educational Resources</h2>

  <p>The Open Educational Resources (OER) movement encourages the availability of educational materials for free use and remix—including textbooks and also any materials that facilitate learning. As in the case of OA, there is a strong push for materials to be published under liberal Creative Commons licenses and in formats amenable to reuse in order to maximize opportunities for latent collaboration, and in some cases to form the legal and technical basis for collaboration among large institutions.&nbsp;</p>

  <p>OpenCourseWare (OCW) is the best known example of a large institutional collaboration in this space. Begun at MIT, over 200 universities and associated institutions have OCW programs, publishing course content and in many cases translating and reusing material from other OCW programs.</p>

  <p>Connexions, hosted by Rice University, is considered by many as an example of an OER platform facilitating large scale collaborative development and use of granular “course modules” which currently number over 15,000. The Connexions philosophy page is explicit about the role of collaboration in developing OER:</p>

  <blockquote>
    <p>“Connexions is an environment for collaboratively developing, freely sharing, and rapidly publishing scholarly content on the Web. Our Content Commons contains educational materials for everyone—from children to college students to professionals—organized in small modules that are easily connected into larger collections or courses. All content is free to use and reuse under the Creative Commons “attribution” license.</p>

    <dl>
      <dt>Content should be modular and non-linear</dt>

      <dd>Most textbooks are a mass of information in linear format: one topic follows after another. However, our brains are not linear—we learn by making connections between new concepts and things we already know. Connexions mimics this by breaking down content into smaller chunks, called modules, that can be linked together and arranged in different ways. This lets students see the relationships both within and between topics and helps demonstrate that knowledge is naturally interconnected, not isolated into separate classes or books.</dd>

      <dt>Sharing is good</dt>

      <dd>
        Why re-invent the wheel? When people share their knowledge, they can select from the best ideas to create the most effective learning materials. The knowledge in Connexions can be shared and built upon by all because it is reusable:

        <ul>
          <li><strong>technologically:</strong> we store content in XML, which ensures that it works on multiple computer platforms now and in the future.</li>

          <li><strong>legally:</strong> the Creative Commons open-content licenses make it easy for authors to share their work—allowing others to use and reuse it legally—while still getting recognition and attribution for their efforts.</li>

          <li><strong>educationally:</strong> we encourage authors to write each module to stand on its own so that others can easily use it in different courses and contexts. Connexions also allows instructors to <em>customize</em> content by overlaying their own set of links and annotations. Please take the Connexions Tour and see the many features in Connexions.</li>
        </ul>
      </dd>

      <dt>Collaboration is encouraged</dt>

      <dd>Just as knowledge is interconnected, people don’t live in a vacuum. Connexions promotes communication between content creators and provides various means of collaboration. Collaboration helps knowledge grow more quickly, advancing the possibilities for new ideas from which we all benefit.”</dd>
    </dl>

    <p>—Connexions—Philosophy, CC BY, &lt;<a href="http://cnx.org/aboutus/">cnx.org/aboutus/</a>&gt;<br></p>
  </blockquote>

  <p>However, one major question with Connexions that needs to be asked is whether it really is a collaborative platform or is it a platform for sharing content. Authors that make their content available as “modules” for others to remix into collections, for example, might not be considered a collaborative activity. Although popularly considered collaborative it probably is better seen as a successful mechanism for sharing individually authored OER materials. Discussions and connections between contributors might be made around this shared content which might turn into collaborations that occur outside of the platform but Connexions itself as platform for collaboration is mostly aspirational.&nbsp;</p>

  <p>There is also P2PU &lt;p2pu.org/&gt; and Wikiversity <a href="http://en.wikiversity.org/">&lt;en.wikiversity.org&gt;</a> which are trying to establish remote and collaborative pegagogical strategies, and another interesting group producing OER materials is the Teaching Open Source group. &lt;<a href="http://www.teachingopensource.org/">www.teachingopensource.org</a>&gt;.<br></p>

  <h2>Beyond the institution</h2>

  <p>OER is not only used in an institutional context—it is especially a boon for self-learning. OCW materials are useful for self-learners, but OCW programs generally do not actively facilitate collaboration with self-learners. A platform like Connexions is more amenable to such collaboration, while wiki-based OER platforms have an even lower barrier to contribution that enable self-learners (and of course teachers and students in more traditional settings) to collaborate directly on the platform. Wiki-based OER platforms such as Wikiversity and WikiEducator make it even easier for learners and teachers in all settings to participate in the development and repurposing of educational materials.&nbsp;</p>

  <p>Self-learning only goes so far. Why not apply the lessons of collaboration directly to the learning process, helping self-learners help each other? This is what a project called Peer 2 Peer University has set out to do:</p>

  <blockquote>
    “The mission of P2PU is to leverage the power of the Internet and social software to enable communities of people to support learning for each other. P2PU combines open educational resources, structured courses, and recognition of knowledge/learning in order to offer high-quality low-cost education opportunities.&nbsp;It is run and governed by volunteers.”
  </blockquote>

  <h2>Scaling educational collaboration</h2>

  <p>As in the case of science, delivering the full impact of the possibilities of modern collaboration tools requires more than simply using the tools to create more resources. For the widest adoption, collaboratively created and curated materials must meet state-mandated standards and include accompanying assessment mechanisms.</p>

  <div class="glossary">
    <hr>

    <h3>Educational Intervention</h3>

    <p>The book sprint might be applied to PhD dissertations. The book sprint could be extended to a thesis written over two weeks by 20 people. Is there a venture capitalist out there who would like to found “SprintMyDissertation.com”?</p>
    <hr>
  </div>

  <p>While educational policy changes may be required, perhaps the best way for open education communities to convince policymakers to make these changes is to develop and adopt even more sophisticated collaboration tools, for example reputation systems for collaborators and quality metrics, collaborative filtering and other discovery mechanisms for educational materials. One example are “lenses” at Connexions &lt;<a href="http://cnx.org/lenses">cnx.org/lenses</a>&gt;, which allow one to browse resources specifically endorsed by an organization or individual that one trusts.</p>

  <p>Again, similar to science, clearing the external barriers to adoption of collaboration may result in general breakthroughs in collaboration tools and methods.</p>

  <p>&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch036_beyond-education.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch036_beyond-education.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

