<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch003_a-brief-history-of-collaboration.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch003_a-brief-history-of-collaboration.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>A Brief History of Collaboration</h1>

  <blockquote>
    <p>Whenever a communication medium lowers the costs of solving collective action dilemmas, it becomes possible for more people to pool resources. And “more people pooling resources in new ways” is the history of civilization in…seven words.</p>

    <p>—Marc Smith, Research sociologist at Microsoft</p>
  </blockquote>

  <div class="captionBox" about="/collaborativefutures/static/openness.jpg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/terms/" align="center">
    <a href="http://thewebisagreement.com"><img src="_booki/collaborative-futures/static/openness.jpg" style="border: medium none;" title="The Web is Agreement" width="347" height="406"></a>

    <p>Detail of <a href="http://thewebisagreement.com" rel="cc:attributionURL" property="dc:title">The Web is Agreement</a> / <span property="cc:attributionName">Paul Downey</span> / <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY</a></p>
  </div>

  <p>This book is about the future of collaboration; to get there, it is necessary to understand collaboration’s roots.</p>

  <p>It is impossible to give a full history in the context of this book; we instead want to highlight a few key events in the development of collaboration that directly inform the examples we will be looking at.</p>

  <p>Most of these stories are well known, so we decided to keep them short. They are all very well documented, so these descriptions should be great starting points for further research.</p>

  <h2>Anarchism in the Collaboratory</h2>

  <p>Anarchist theory provides some of the background for our framing of autonomy and self organization.</p>

  <p>This is recapitulated by Yochai Benkler, one of the leading modern theorists of open collaboration, in his book The Wealth of Networks: How Social Production Transforms Markets and Freedom:</p>

  <blockquote>
    <p>“The networked information economy improves the practical capacities of individuals along three dimensions: (1) it improves their capacity to do more for and by themselves; (2) it enhances their capacity to do more in loose commonality with others, without being constrained to organize their relationship through a price system or in traditional hierarchical models of social and economic organization; and (3) it improves the capacity of individuals to do more in formal organizations that operate outside the market sphere. This enhanced autonomy is at the core of all the other improvements I describe. Individuals are using their newly expanded practical freedom to act and cooperate with others in ways that improve the practiced experience of democracy, justice and development, a critical culture, and community.</p>

    <p>…</p>

    <p>[M]y approach heavily emphasizes individual action in nonmarket relations. Much of the discussion revolves around the choice between markets and nonmarket social behavior. In much of it, the state plays no role, or is perceived as playing a primarily negative role, in a way that is alien to the progressive branches of liberal political thought. In this, it seems more of a libertarian or an anarchistic thesis than a liberal one. I do not completely discount the state, as I will explain. But I do suggest that what is special about our moment is the rising efficacy of individuals and loose, nonmarket affiliations as agents of political economy.”</p>
  </blockquote>

  <div class="glossary">
    <hr>

    <h3>Non-human Collaboration</h3>

    <p>Why do we imagine it is only humans who act, react and enact the world? What if plants, animals, things, forces and systems can also exert agency? Who are we collaborating with and through? Living things and inert matter. Organisms of all kinds could be included in forms and assemblages of collaboration. The agents: human and non-human entities, plants, objects, systems, histories. This thinking proposes an inclusive model.<br></p>
    <hr>
  </div>

  <h2>Science to Software</h2>

  <p>Although the history of science is intertwined with that of states, religions, commerce, institutions, indeed the rest of human history, it is on a grand scale the canonical example of an open collaborative project, always struggling for self-organization and autonomy against pressure from state, religion, and market, in a quest for a common goal: to discover the truth. Collaboration in science also occurs at all timescales and levels of coupling, from deeply close and intentional collaboration between labs to opportunistic collaboration across generations as well as problematic collaborations been researchers and industry.</p>

  <div class="glossary">
    <hr>

    <h3>Progress</h3>

    <p>Understood as a belief system. An inheritance of the Enlightenment akin to the idea of a singular, objective "truth" out there awaiting human discovery. Belief in the perpetual improvement of things (an easier way of living and acting, or society as such) via the development of technology. Progress as the fetishism of change and constant transformation. See also Speed and Acceleration/Deceleration.</p>
    <hr>
  </div>

  <p>The last half millennium produced innumerable examples of interesting collaboration in addition to the great scientific endeavor. Within the technological sphere, none is as cogent in informing and driving contemporary collaboration as the Free Software movement, which provides much of the nuts and bolts immediate precedent for the kinds of collaborations we are talking about—and often provides the virtual nuts and bolts of these collaborations!&nbsp; The story goes something like this: Once upon a time all software was open source. Users were sent the code, and the compiled version, or sometimes had to compile the code themselves to run on their own specific machine. In 1980 MIT researcher Richard Stallman was trying out one of the first laser printers, and decided that because it took so long to print, he would modify the printer driver so that it sent a notice to the user when their print job was finished. Except this software only came in its compiled version, without source code. Stallman got upset—Xerox would not let him have the source code. He founded the GNU project and in 1985 published the GNU Manifesto. One of GNU’s most creative contributions to this movement was a legal license for free software called the GNU Public License or GPL. Software licensed with the GPL is required to maintain that license in all future incarnations; this means that code that starts out freely licensed has to stay freely licensed. You cannot close the source code. This is known as a Copyleft license.</p>

  <h2>Mass Collaborations</h2>

  <p>Debian is the largest non-market collaboration to emerge from the free software movement.</p>

  <p>Beginning in 1993, thousands of volunteer developers have maintained a GNU/Linux operating system distribution, which has been deeply influential well beyond its substantial deployments.</p>

  <p>Debian has served as the basis for numerous other distributions, including the most popular for the past several years, Ubuntu. Debian is also where many of the pragmatics of the free software movement were concretized, including in the Debian Free Software Guidelines in 1997, which served as the basis of the Open Source Definition in 1998.</p>

  <p>In 1995 Ward Cunningham created the first wiki, a piece of software that allowed multiple authors to collaboratively author documents. This software was used especially to hold meta-discussions of collaboration, in particular on MeatballWiki. Dozens of wiki systems have been developed, some with general collaboration in mind, others with specific support for domain-specific collaboration, for example Trac for supporting software development. In 2001 Wikipedia was founded, eventually becoming by far the most prominent example of massive collaboration.</p>

  <h2>Web 2.0 is bullshit</h2>

  <p>Although they have a fairly distinct heritage, Wikipedia and wikis in general are often grouped with many later sites under the marketing rubric “Web 2.0”.</p>

  <p>While many of these sites have ubiquitous “social” features and in some cases are very interesting collaboration platforms, particularly when considering their scale, all have extensive precedents.</p>

  <p>The Web 2.0 term is directly borrowed from software release terminology.</p>

  <p>It implies a major “dot release” of the web—an all encompassing new version, headed by the proprietary new media elite (the likes of Google, NewsCorp, Yahoo, Amazon) that passive web users, still using the old “1.0 version”, should all upgrade to. “Web 2.0” also gave birth to the use of “Web 1.0” which stands for conservative approaches to using the web that are merely attempting to replicate old offline publishing models.</p>

  <p>More than anything else this division of versions implies a shift in IT business world—an understanding that a lot of money can be made from web platforms based on user production.</p>

  <p>This new found excitement of the business sector has brought a lot of attention to these platforms and indeed produced some excellent tools.</p>

  <p>But the often too celebratory PR language of these platforms has affected their functionality, reducing our social life and our peer production to politically correct corporate advertising. Sharing, friendship, following, liking, poking, democratizing… collaborating.</p>

  <p>These new platforms use a pleasant social terminology in an attempt to attract more users.</p>

  <p>But this polite palette of social interactions misses some of the key features that the pioneering systems were not afraid to use.</p>

  <p>For example, while most social networks only support binary relationships, Slashcode (the software that runs Slashdot.org, a pioneer of many features wrongly credited to “Web 2.0”) included a relationship model that defined friends, enemies, enemies-of-friends, etc.</p>

  <p>The reputation system on the Advogato publishing tool supported a fairly sophisticated trust metric, while most of the more contemporary blog platforms support none.</p>

  <h2>Web 3.0 is also bullshit</h2>

  <blockquote>
    “The future is already here—it is just unevenly distributed.”<br>
    —William Gibson
  </blockquote>

  <p>One might argue that Web 2.0 has popularized collaborative tools that have&nbsp;earlier been accessible only to a limited group of geeks. It is a valid point to make.</p>

  <p>Yet the early social platforms like IRC channels, Usenet and e-mail have been protocol based and were not owned by a single proprietor.</p>

  <p>Almost all of the current so called Web 2.0 platforms have been built on a centralized control model, locking their users into dependence on a commercial tool.</p>

  <p>We do see a turn against this lock-in syndrome.</p>

  <p>The past year has seen a shift in attention towards open standards, interoperability and decentralized network architectures.</p>

  <div class="glossary">
    <hr>

    <h3>Google Wave</h3>

    <p>In May 2009, Google announced the Google Wave project as its answer to the growing need for more user autonomy in online services and to further its own data-omnivorous agenda. Wave was declared an open source platform for web services that (like email) works in a federated model. What this means is that the user can choose to use a Wave service (on either a Google or a non-Google owned server) through which to channel her data and interaction in an online service.</p>

    <p>For example, Alice is using her Google-hosted Wave account to schedule an armed bank robbery with Bob. She is using a Wave-enabled collaborative scheduling application which, in Wave terms, is called a ‘Robot’. Bob is using his own encrypted Wave server hosted somewhere in a secret location. Alice is happy since she didn’t have to work hard to get the wonderful collaborative functionality of Wave from Google. Bob is happy since he doesn’t have to store all his data with Google and can still communicate with his co-conspirator Alice and feel like his data is safe. Google is happy since the entire conversation between the two outlaws is available for it to index and it could have easily targeted them with ads for weapons, ski masks, drilling equipment and vans. In the end, Wave or something like it could be another way in which Open Source software and open standards makes for happy collaboration.</p>
    <hr>
  </div>

  <p>The announcement of Google Wave was probably the most ambitious vision for a decentralized collaborative protocol coming from Silicon Valley. It was launched with the same celebratory terminology propagated by the self-proclaimed social media gurus, only to be terminated a year later when the vision could not live up to the hype.</p>

  <p>Web 3.0 is also bullshit. The term was initially used to describe a web enhanced by Semantic Web technologies. However, these technologies have been developed painstakingly over essentially the entire history of the web and deployed increasingly in the latter part of the last decade.<br></p>

  <p>Many Open Source projects reject the arbitrary and counter-productive terminology of “dot releases” the difference between the 2.9 release and the 3.0 release should not necessarily be more substantial than the one between 2.8 and 2.9.</p>

  <p>In the case of the whole web we just want to remind Silicon Valley: “Hey, we’re not running your ‘Web’ software. Maybe it’s time for you to upgrade!”</p>

  <h2>Free Culture and Beyond</h2>

  <p>The Free Culture movement and Creative Commons are built on top of the assumption that there is a deep analogy between writing code and various art forms.&nbsp; This assertion is up for debate and highly contested by some collaborators on this project.&nbsp; (For more on this topic see the chapter “Can Design By Committee Work?”)</p>

  <p>No doubt software is a cultural form, but we should be aware of the limits of the comparison to other creative modes.&nbsp; After all, software operates according to various objective standards. &nbsp; Successful software works; clean code is preferable; good code executes. What does it mean for a cultural work to “execute”?&nbsp; Where code executes, art expresses. Indeed, many forms of art depend on ambiguity, layered meanings, and contradiction.&nbsp; Code is a binary language, whereas the words used to write this book, even though they are in English, will be interpreted in various unpredictable ways. Looking at all of creativity through the lens of code is reductive.<br></p>

  <p>We also wonder if collaboration is possible or desirable for a project that is deeply personal or subjective.&nbsp; Would I necessarily want to collaborate on a memoir, a poem, a painting?&nbsp; We also wonder if we can ever not collaborate, in the sense that we are always in relationship to our culture and environment, to the creations that proceeded ours, to the audience.&nbsp; Or, to make matters stranger, can we ever not collaborate, even when it seems that we are alone?&nbsp; As musician David Byrne wrote on his blog:</p>

  <blockquote>
    <p>“But one might also ask: Is writing ever NOT collaboration? Doesn’t one collaborate with oneself, in a sense? Don’t we access different aspects of ourselves, different characters and attitudes and then, when they’ve had their say, switch hats and take a more distanced and critical view—editing and structuring our other half’s outpourings? Isn’t the end product sort of the result of two sides collaborating? Surely I’m not the only one who does this? ”</p>

    <p>—David Byrne &lt;<a href="http://journal.davidbyrne.com/2010/03/031510-collaborations.html%20">journal.davidbyrne.com/2010/03/031510-collaborations.html</a>&gt;<br></p>
  </blockquote>

  <p>For those who believe that the code and culture analogy is deeply insightful, the free culture movement is an attempt to translate the ethics and practices of free software to other fields, some closely tied to technology changes (including wikis and social media sites mentioned above) allowing more access and capability to share and remix materials. Creative Commons, founded in 2001, provides public licenses for content akin to free software licenses, including a copyleft license roughly similar to the FDL (Free Documentation License) that is used by Wikipedia. These licenses have been used for blogs, wikis, videos, music, textbooks, and more, and have provided the legal basis for collaborations often involving large institutions, for example publishing and re-use of Open Educational Resources, most famously the OpenCourseWare project started at MIT as well as many-to-many sharing with extensive latent collaboration, often hosted on sites like Flickr.</p>

  <div class="glossary">
    <hr>

    <h3>Art++</h3>

    <p>Aesthetic production can form a coalition with open source and networked culture, with real life as lived outside the art gallery/space/system, and with political concerns such as the Commons, social justice and sustainability. Let us not discount the productive alliances that can be forged amongst areas of experimental cultural practice.</p>
    <hr>
  </div>

  <p>There is still much to learn from historical examples of collaborative theory and practice—and some of these in turn have lessons to learn from current collaboration practices—for anarchist theory, see the Solidarity chapter, for science, see the Science 2.0 chapter. Even the term autonomy may have a useful contribution to contemporary discussion of collaboration, for example resolving the incompleteness and vagueness present in both “free” and “open” terminology.</p>

  <div class="glossary">
    <hr>

    <h3>Open</h3>

    <p>There are different levels of openness. Being more or less open implies a level of agency, being more or less able to act, and/or being part of something (e.g. a group, debate, project), and/or having the power of access or not. There are logics of control and networks in any collaborative system—but it becomes important to imagine control other than relating to a totality. In their book “The Exploit: A Theory of Networks”(2007), Alexander R. Galloway &amp; Eugene Thacker suggest imagining networks outside an “abstract whole”, networks that are not controlled in a total way. They further argue that open source fetishizes all the wrong things. The opposition between open and closed is flawed. An open collaboration in comparison to a less open (almost closed) collaboration suggests the possibility for shifting hierarchies within the collaboration. A closed collaboration can be understood as a defined micro-community that has gathered for particular reasons, and that remains as group intact for the duration of the project. &nbsp;</p>

    <p>Collaboration as in Collaborative Futures might be in-between open and closed. The question of a collaborative future is a projection. Using imagination as a tool, a collaborative future is as open as possible confirming the variability of a system - dissent, multiplicity, and possible failure allow agency in its proper sense.</p>
    <hr>
  </div>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch003_a-brief-history-of-collaboration.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch003_a-brief-history-of-collaboration.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

