<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>P2P : The Unaccepted Face of Cultural Industry?</h1>

  <p>Napster’s launch and popularization in 1999 transformed the unauthorized online circulation of media from an activity of adepts to a mass phenomena. In its aftermath, the public discourse ramped up, pivoting around the denomination and vilification of P2P as ‘piracy’. But this language was nothing new; it represented a quantitative expansion and escalation in a longer struggle over cultural production. Campaigns against home taping and the litigation against the VCR are just two instances in that history. &nbsp;<br></p>

  <p>The precarious legal situation of P2P communities make them difficult to discuss. Legal campaigns have propelled a bifurcation into two species: public and private. The former, exemplified by the Pirate Bay with a user base of millions, are predominantly (all though not solely) used for the distribution of mainstream content. ‘Oink’ exemplifies a private community: dedicated to sharing music, it was closed by police in autumn 2007 and had 180,000 members at the time of its death. The site's founder was subsequently tried and acquitted on charges of conspiracy to defraud; four site administrators were charged and convicted of copyright infringement.&nbsp;</p>

  <p>What is of interest here however is the collaborative productivity of these communities, and the way in which their growth has produced a new method of distribution for works more generally.</p>

  <h2>A New Type of Archive</h2>

  <p>Private P2P communities are generating new types of archives based on federating the sum of their users’ contributions. Whereas media collections in the analogue period were private libraries, in the digital they coagulate into decentralized archives, whose communities assemble databases of metadata and information about the items shared. Sub-communities form around genres or individual producers, seeking out lost works, and aggregating literature and other material considered important to contextualize their presentation.&nbsp;</p>

  <p>In the case of film communities, large numbers of works are translated and subtitled by participants <em>ab initio</em>. Networked cinephilia is also about attempting to extend, or create, an audience for undervalued films by reintroducing them via virtual channels. The low cost of digital distribution enables the release of works preciously regarded as too economically marginal to justify commercial release. Here we are generally not speaking about works belonging to Hollywood or major record labels. P2P community participants capture recordings from television and radio or digitize existing analogue versions for distribution. In many cases, there is no possibility to effectively access these works through normal channels, and this functions as both an attraction and a justification, as users feel that their activity is not parasitic, but rather assists in the preservation of otherwise abandoned works.&nbsp;</p>

  <p>This activity is in part driven by the proselytizing instincts of enthusiasts, but is also nurtured by the technical framework within which such sites function. Users are expected both to take things they like and to reciprocate by making their own contributions. This expectation is enforced through the application of ratio rules, which limit the amount one can download in proportion with the amount uploaded. Ratio functions both as a proxy for currency and as a reputation index. Due to their contested legality, access to these communities is normally by invitation only, and invitations are provided only to those who have demonstrated a willingness to observe the rules of the game by maintaining an acceptable ratio.&nbsp;</p>

  <p>Hackers affiliated with these communities develop software required to accomplish many of the tasks outlined above: subtitle authoring and synchronization; transcoding; tools for working with audio and video streams; database management; improved distribution protocols and clients.</p>

  <p>Lastly the availability of large amounts of unrestricted digital works enable further downstream creation, by making available materials which can be recycled into new works, be they fan-films, criticism, pastiche or parody. Barriers to participation in this subsequent layer of production are diminished through the provision of images and music in readily manipulable form.</p>

  <div class="glossary">
    <hr>

    <h3>Non-Documents&nbsp;</h3>

    <p>The obsession and notoriety to document a process, a thinking and action, produce an eruption of belief in materialities. It is important to emphasize that outside the Western context there is not necessarily such a desire to build a mega-archive. Can we learn to non-document, or to create more virtual documents in the form of brain images, memory data, and partially return to oral histories? How would that affect our relation to the elderly and socially (technologically) excluded people? &nbsp;</p>
    <hr>
  </div>

  <h2>Independent Channels for Distribution</h2>

  <p>It may have been the lure of free or rare goods that initially motivated users to install p2p clients, but in the process something new has been created: an unowned distribution infrastructure. Until the emergence of p2p networks, serving large amounts of media content was expensive, posing a dilemma for independent producers who wanted to distribute works online. Media enterprises invest large amounts of resources, and employ expensive service companies, to ensure rapid data delivery to viewers and finesse the user experience, but the costs involved are beyond the reach of most independents. Video platforms such as Youtube and Vimeo provide such facilities for serving video, but exact control in terms of framing, content 'suitability', or commercial exploitation - advertising - in return.</p>

  <p>The installed base of p2p clients, dispersed amongst the population, allows independents to shift the burden of data transfer to fans and supporters. Large file-sharing sites such as the Pirate Bay and Mininova periodically donate promotional space to encourage users to download works made available voluntarily by their producers. In so doing, a sort of diffuse ‘channel’ is created, which has no-one point of entry, as anyone can use their web-site to disseminate links to items available in these networks, be it on Bit Torrent, eMule, Gnutella etc. Once injected into these networks the work is effectively impossible to censor, although its level of visibility, and the performance of the network as a distribution platform, will vary according to the level of user mobilization and enthusiasm behind it.</p>

  <p>&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

