<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch014_coordinating-mechanisms.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch014_coordinating-mechanisms.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Coordinating Mechanisms create Contexts</h1>

  <p>Contributions such as edits to a wiki page, or “commits” to a version control system, cannot exist outside of the context in which they are made. A relationship to this context requires a coordinating mechanism that is an integral part of the initial production process. These mechanisms of coordination and governance can be both technical and social.</p>

  <h2>Technical Coordination and Mediation</h2>

  <p>Wikipedia uses several technical coordination mechanisms, as well as strong social mechanisms. The technical mechanism separates each contribution, mark it chronologically and attribute it to a specific username or IP address. If two users are editing the same paragraph and are submitting contradicting changes, the MediaWiki software will alert these users about the conflict, and requires them to resolve it. Version control systems use similar technical coordination mechanisms, marking each contribution with a timestamp, a user name, and requiring the resolution of differences between contributions if there are discrepancies in the code due to different versions.&nbsp;</p>

  <p>The technical coordination mechanisms of the Wiki software lowers the friction of collaboration tremendously but it doesn't take it away completely. It makes it much harder to create contributions that are not harmonious with the surrounding context. If a contribution is deemed inaccurate, or not an improvement, a user can simply revert to the previous edit. This new change is then preserved and denoted by the time and user who contributed it.&nbsp;</p>

  <h2>Social Contracts and Mediation</h2>

  <p>Academic research into the techno-social dynamics of Wikipedia shows clear emergent patterns of leadership. For example the initial content and structure outlined by the first edit of an article are often maintained through the many future edits years on. (A Kittur, RE Kraut; Harnessing the Wisdom of Crowds in Wikipedia: Quality through Coordination) The governance mechanism of the Wiki software does not value one edit over the other. Yet, what is offered by the initial author is not just the initiative for the collaboration, it is also a leading guideline that implicitly coordinates the contributions that follow.</p>

  <p>Much like a state, Wikipedia then uses social contracts to mediate the relationship of contributions to the collection as a whole. All edits are supposed to advance the collaborative goal—to make the article more accurate and factual. All new articles are supposed to be on relevant topics. All new biographies need to meet specific guidelines of notability. These are socially agreed upon contracts, and their fabric is always permeable. The strength of that fabric is the strength of the community.</p>

  <blockquote>
    “If you're going against what the majority of people perceive to be reality, you're the one who's crazy” &nbsp;<br>
    Stephen Colbert<br>
    &lt;<a href="http://www.colbertnation.com/the-colbert-report-videos/72347/july-31-2006/the-word---wikiality">www.colbertnation.com/the-colbert-report-videos/72347/july-31-2006/the-word---wikiality</a>&gt;
  </blockquote>

  <p>An interesting example of leadership and of conflicting social pacts happened on the Wikipedia Elephant article. In the TV show <em>The Colbert Report</em> Stephen Colbert plays a satirical character of a right wing television host dedicated to defending Republican ideology by any means necessary. For example he constructs ridiculous arguments denying climate change. He is not concerned that this completely ignores reality, which he claims “has a Liberal bias”.</p>

  <p>On July 31st, 2006, Colbert ironically proposed the term "Wikiality" as a way to alter the perception of reality by editing a Wikipedia article. Colbert analyzed the interface in front of his audience and performed a live edit to the Elephants page, adding a claim that the Elephant population in Africa had tripled in the past 6 months.&nbsp;</p>

  <p>Colbert proposed his viewers follow a different social pact. He suggested that if enough of them helped edit the article on Elephants to preserve his edit about the number of Elephants in Africa, then that would become the reality, or the Wikiality—the representation of reality through Wikipedia. He also claimed that this would be a tough “fact” for the Environmentalists to compete with, retorting “Explain that, Al Gore!”</p>

  <p>It was great TV, but created problems for Wikipedia. So many people responded to Colbert’s rallying cry that Wikipedia locked the article on Elephants to protect it from further vandalism. &lt;<a href="http://en.wikipedia.org/wiki/Wikipedia:Wikipedia_Signpost/2006-08-07/Wikiality">http://en.wikipedia.org/wiki/Wikipedia:Wikipedia_Signpost/2006-08-07/Wikiality</a>&gt; Furthermore, Wikipedia banned the user Stephencolbert for using an unverified celebrity name (a violation of Wikipedia’s terms of use &lt;<a href="http://en.wikipedia.org/wiki/User:Stephencolbert">en.wikipedia.org/wiki/User:Stephencolbert</a>&gt;.<br></p>

  <p>Colbert and his viewers’ edits were perceived as mere vandalism that was disrespectful of the social contract that the rest of Wikipedia adhered to, thus subverting the underlying fabric of the community. Yet they were following the social contract provided by their leader and his initial edit. It was their own collaborative social pact, enabled and coordinated by their own group. Ultimately, Wikipedia had to push one of its more obscure rules to its edges to prevail against Stephen Colbert and his viewers. The surge of vandals was blocked but Colbert gave them a run for the money, and everyone else a laugh, all the while, making a point about how we define the boundaries of contribution.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch014_coordinating-mechanisms.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch014_coordinating-mechanisms.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

