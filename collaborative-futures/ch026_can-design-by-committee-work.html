<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch026_can-design-by-committee-work.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch026_can-design-by-committee-work.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Can Design By Committee Work?</h1>

  <p>In the excited celebration of the merits of Free Software and radically networked production methods, an important truth is left unspoken. Networked collaboration shines at the low levels of network protocols, server software and memory allocation, but has consistently been a point of failure when it comes to user interfaces. How come the methods that transformed code production and encyclopedia writing fails to extend into graphic and interface design?&nbsp;</p>

  <p>What follows is an investigation into the difficulties of extending the FLOSS collaboration model from coding to its next logical step, namely interface design. While it dives deep into the practical difference between these two professional fields, it might also serve as a cautionary tale to consider before prematurely declaring the rise of “Open Source Architecture”, “Open Source University”, “Open Source Democracy”…<br></p>

  <h2>The Challenges</h2>

  <h3>Scratching an Itch</h3>

  <p>Coders are fulfilling their own need to change software, to make it their own. They might have diverging motivations but if you’re already modifying something for yourself it is really easy to answer the “why share?” question with “why not?”. By the time the code executes correctly the immediate users of the software, the coders themselves, are already familiar with the software and can operate it even without a delicately crafted user interface.<br></p>

  <p>Therefore the motivation to take an extra step and to invest in a usable interface that would extend the user base beyond the original geek-pool is not obvious. This is already working for me, so what itch am I scratching when I work hard to make it usable by others who can't help me code it?&nbsp;</p>

  <p>And for designers themselves, what is the incentive to make the design process more collaborative? Will others make my design better? Would they be able to better communicate my thoughts than I can?</p>

  <p>Beyond that, FLOSS interface design suffers from a chicken and egg problem; Most designers don’t use FLOSS tools, and so they are not aware that they could make the software better. As a result FLOSS often suffers from inferior interfaces that makes designers shy away from it and stick to their proprietary tools. The cycle continues…</p>

  <h3>Granularity</h3>

  <p>Both software and wikis are made of granular building blocks, namely the character. This makes every typo an invitation to collaborate. My first Wikipedia edit was a typo correction, the second was an additional reference link, the third was actually a whole paragraph and that lead to more substantial contributions like adding a whole new article and so on.</p>

  <p>Each granular step gets you closer to the next granular step. This ladder of participation makes every next step easier. It also allows easy comparison of changes which provide transparency, accountability, moderation and an open license to try and possibly fail knowing that you can always revert to the previous version.</p>

  <p>You don't have that with design as the changes are not granular and are not as easily traceable. The first step is steep and a ladder is nowhere to be found.&nbsp;</p>

  <h3>Encoding / Decoding</h3>

  <p align="center"><img style="width: 519px;" src="_booki/collaborative-futures/static/osd_comcycle_0_2.png">&nbsp;<br></p>

  <p>In his 1980 article “Encoding / Decoding” cultural theorist Stuart Hall defines communication in terms of code. To describe it briefly let’s imagine a spoken conversation between Alice and Bob. Alice encodes her framework of knowledge into the communicable medium of speech. Assuming Bob can hear the sounds and understand the spoken language, he then decodes the sounds into a framework of knowledge.</p>

  <p>Both encoding and decoding are creative processes. Ideas are transformed into messages that are then transformed into ideas again. The code Alice uses for encoding is different than the one used by Bob for decoding. Alice could never just telepathically “upload” ideas into Bob’s brain. We would all agree that is a good thing.</p>

  <p>Let's entertain Hall’s ideas of encoding and decoding in software. Alice is an FLOSS hacker, Bob is collaborating with her as a designer. Alice is writing software code, she knows when it executes and when it doesn't as the program communicates that through error messages. When she is happy with the result she uploads the code to an online repository.&nbsp;</p>

  <p>Bob then downloads the code to his computer and since it executed on Alice’s computer, it also executes on his. When Alice and Bob collaborate through programming language they are literally using the same code for encoding and decoding.</p>

  <p align="center">&nbsp;<img style="width: 572px;" src="_booki/collaborative-futures/static/osd_comcycle_8_1.png"><br></p>

  <p>Alice always chooses one of her three favorite programming languages. Being a designer, to communicate a message visually Bob starts by defining a visual language—graphics, color, layout, animation, interaction… If Alice or any other developer had to reinvent a new programming language on every single project we would not be speaking about FLOSS now.</p>

  <p>Bob needs to define a graphic language, a standard for the collaboration. Doing that is already a major part, possibly the most important part of the creative work. Whoever works with Bob will need to accept and follow these standards, relinquish control and conform to Bob’s predefined graphic language. These artificial constraints are harder to learn and conform to than the constraints of a programming language. While constraints and standards in technology are the mother of creativity, in design they can often feel artificial and oppressive.<br></p>

  <p>Beyond that, within a collaboration, when Bob tries to argue for the merits of his design, unlike in the case of Alice’s code he cannot prove that it executes flawlessly, or that it is faster or more resource efficient. The metrics are not as clear.</p>

  <p>It is important to remember, in collaboration on code Alice and Bob have a third collaborator, one that cannot be reasoned with - the computer. This collaborator will simply not execute anything that doesn't fit its way of work. On the other hand, as long as it is syntactically correct and satisfies the inflexible collaborator even “ugly code” executes and muddles through.&nbsp; And so, the different voices expressed in code are flattened into a single coherent executed application.</p>

  <p>For better or worse, we lack this inflexible collaborator in design. It doesn't care about our communicative message and it doesn't level the playing field for communicative collaboration. And so, the different voices in design simply spell inconsistent multiplicity that dilutes the communicative message.</p>

  <p>One might turn to Wikipedia as a testament to successful non-code-based collaboration, but Wikipedia enforces very strict and rational guidelines. There is no room for poetry or subjectivity within its pages.</p>

  <h2>So is it simply impossible?</h2>

  <p>Not necessarily. If we step out of the technical construct of the FLOSS methodology we can identify quite a few networked collaborations that are transforming and often improving the design process.</p>

  <p>It is tempting to see free culture and the free sharing of media as evidence of collaboration, but the availability of work for remixing and appropriation does not necessarily imply a collaboration. Sharing is essential to collaboration but it is not enough.</p>

  <p>WordPress, the leading Free Software blogging tool is an interesting example. Looking to redesign the WordPress administration interface, Automattic, the company leading the Wordpress community has hired HappyCog, a prominent web-design firm. And indeed in March 2008, WordPress 2.5 launched with a much improved interface. Through a traditional design process HappyCog developed a strong direction for the admin interface. Eight months later Automattic released another major revision to the design that relied on HappyCog’s initial foundations but extended them far beyond.</p>

  <p>One of the interesting methodologies used to involve the WordPress community in the design process was a call for icon designers to provide a new icon set for the interface. Within two weeks six leading icon sets were up for vote by the community. But rather than just asking for a blanket like/dislike vote, they were invited to provide detailed assessments of consistency, metaphor coherence and so on. Some of the icons designers ended up acknowledging the superiority of other contributions and voting against their own sets. But the final icon set was indeed a collaborative effort, as some of the icons were altered based on inspiration from the other sets.</p>

  <p>Another example is the evolution of grid systems for web design. Half a century after the rise of Swiss style graphic design, some design bloggers suggested some of its principles could apply to web design. Those suggestions evolved into best practices and from there into Blueprint CSS, an actual style sheet framework. The popularity of that framework inspired other frameworks like 960.gs and others. Similar processes happen in interaction design as well, with the pop-up window evolving into elegant lightbox modules and then being repeatedly changed and modified as open source code libraries.</p>

  <p>Other design minded experiments in Free Software like the ShiftSpace platform challenge the web interface power structure. ShiftSpace allows users to interact with websites on their own terms by renegotiating the interface and proposing different interactions on top of the page. Projects like ShiftSpace aim to expand the limited participatory paradigm of the web beyond user generated content to also include user generated interfaces.</p>

  <h2>Make it happen!</h2>

  <p>There are ways to make Open Source design work without falling into the traps often characterized as “design by committee”. We are already seeing <strong>designers scratching their own itches</strong> and contributing creative work to the commons.</p>

  <p>Lecturing designers (or any users) and demanding they use bad tools for ideological reasons is counter productive. Designers often use free tools (or make unauthorized use of proprietary tools) only because they are free as in free beer. So to win any new user, Free Software should be pitched on the full range of its merits rather than ethics alone. While the ethics of “free as in free speech” are very convincing for those who can “speak” code, for those who do not have the skills to modify it the openness of the source is not such a compelling virtue.</p>

  <p>Free Software tools have won on their broad merits many times, not only on the low-level system and network fronts. Wikis and blogging software are interaction and communication tools that have been invented by the Free Software community and have maintained a lead over their proprietary competitors. Networking and collaboration are the bread and butter of Free Software, and these are advantages the community should leverage.<br></p>

  <p>In the same way that Wikipedia extends the Free Software collaboration model by leveraging the <strong>granularity of the character</strong>, so can design. From a collaboration standpoint, where possible, it is preferable to use code to implement design (like HTML, CSS). Beyond that, collaborators should adopt distributed version control systems for both code and image files. Rather than trying to compete with proprietary software by creating open clones, the Free Software community can leverage its experience as an advantage and focus on new collaborative paradigms for version control and collaboration.&nbsp;</p>

  <h3>Encoding / Decoding</h3>

  <div class="glossary">
    <hr>

    <h3>&nbsp;Bike-Shedding</h3>

    <blockquote>
      <p>“Why Should I Care What Color the Bikeshed Is?</p>

      <p>The really, really short answer is that you should not. The somewhat longer answer is that just because you are capable of building a bikeshed does not mean you should stop others from building one just because you do not like the color they plan to paint it. This is a metaphor indicating that you need not argue about every little feature just because you know enough to do so. Some people have commented that the amount of noise generated by a change is inversely proportional to the complexity of the change.”</p>

      <p>&lt;<a href="http://www.freebsd.org/doc/en_US.ISO8859-1/books/faq/misc.html#BIKESHED-PAINTING">www.freebsd.org/doc/en_US.ISO8859-1/books/faq/misc.html#BIKESHED-PAINTING</a>&gt;</p>
    </blockquote>
    <hr>
  </div>

  <p>Finally, There are ways for us to better analyze the <strong>encoding and decoding</strong> of the communicative message. We can formalize processes of <strong>collaborative encoding</strong>. We can start by conducting networked design research using existing research tools, that way we might come up with design decisions collaboratively. We can define modular and extensible languages that embody the design decisions and still allow for flexibility and special cases (like Cascading Style Sheets). We should also learn how to document these design decisions we take so they serve the rest of the collaborators. Designers have been doing it for many years in more traditional and hierarchical design contexts, compiling documents like a brand book or a design guide.</p>

  <p>For the <strong>decoding</strong> part, we should realize that many design patterns are rational or standardized and can leverage a common-ground without compromising the creative output. For example <u>underlined text</u> in a sentence on the web almost always implies a hyperlink. We can choose to communicate a link otherwise but if we try to use this underline styling as a sort of emphasis (for example) we can expect users will try to click on it.</p>

  <p>User experience research, technical aspects of design, best practices in typography, icon use, interaction paradigm, these are all aspects of design that can be researched and assessed according to measurable parameters. Thorough research of these can provide a basic consensus for shared expectations of how a message will be interpreted. A lot of this work is already taking place on design blogs that over the past few years have been publishing a lot of research on the subject.</p>

  <p>Finally, the substantial parts of design that still cannot be easily quantified or assessed on a unified rational ground, should be managed through trust and leadership. In the absence of any convenient meter of coding meritocracy, a resilient community of practice must develop design leadership whose work and guidance is respected and appreciated.<br></p>

  <h2>Scaling Subjectivity</h2>

  <p>It comes down to the deep paradox at the heart of design (interface, architecture, product…). We are trying to create a subjective experience that would scale up—a single personal scenario that can be multiplied again and again to fit a wide array of changing needs by a vast majority of users. The thing is subjectivity cannot be scaled, that's what makes it subjective, and therefore the attempts to create a one size fits all solution are bound to fail, and the attempts to customize the solution to every individual user in every individual use case are also bound to fail.&nbsp;</p>

  <p align="center"><img src="_booki/collaborative-futures/static/osd_mice.png" width="450"><br></p>

  <p>Chris Messina gives a great example of this paradox in a comparison between Apple's Magic Mouse and the Open Office Mouse &lt;<a href="http://factoryjoe.com/blog/2009/11/07/open-source-design-and-the-openofficemouse/">factoryjoe.com/blog/2009/11/07/open-source-design-and-the-openofficemouse/</a>&gt;. While Apple's solution is a slick and clean one-button device, the OOMouse has “<em>18 programmable mouse buttons with double-click functionality; Analog Xbox 360-style joystick with optional 4, 8, and 16-key command modes; 63 on-mouse application profiles with hardware, software, and autoswitching capability;</em>” and more… While Apple's Magic Mouse embodies the company's commitment to design leadership at the price of user choice, the OOMouse embodies the Free Software's preference of openness and customization over unified leadership.&nbsp;</p>

  <p>Successful FLOSS projects have always benefited from a mix of the two approaches, a combination of openness and leadership. Finding a similar nuanced approach in other fields is required if we ever hope to extend the FLOSS model beyond code. We cannot just sprinkle the pixie dust on everything and expect wonders. This applies also to design. But hopefully we can make some progress by demystifying the process, make sure we apply collaboration wisely when it does makes sense, and come up with new ways when it doesn't.&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch026_can-design-by-committee-work.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch026_can-design-by-committee-work.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

