<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch027_multiplicity-and-social-coding.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch027_multiplicity-and-social-coding.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Multiplicity and Social Coding</h1>

  <p>The Linux kernel, arguably one of the most important FLOSS projects, was managed without a version control system until 2002. Linus Torvalds, the project leader, disliked centralized version control systems, which he considered unsuitable for kernel development. The Linux kernel is a very large and complex software project, has extraordinary quality demands and also attracts thousands of developers. Changes were meticulously tracked through a distributed hierarchy of delegates, but the system was showing strain.</p>

  <p>In 2002 Linus finally decided that a “distributed version control system” (DVCS) would match the project's needs. The Linux kernel was migrated to the proprietary BitKeeper version control system, a selection which sparked great controversy because of its closed license. In 2005, licensing disputes eventually led to the creation of freely licensed distributed version control system and the DVCS named Git was created.&nbsp;</p>

  <p>Distributed Version Control Systems operate on a different model than repositories managed by a centralized, client-server system. The DVCS model is peer-to-peer and, while it can be configured to resemble traditional client-server transactions, it can also support more complex interactions. In a DVCS system, every developer works locally with a complete revision history, and changes can be pushed and pulled from any other peer repository. The version control system has vastly improved support for merging across multiple repositories, and all working checkouts are effectively forks, until they are merged back onto a canonical trunk.&nbsp;</p>

  <p>The demands on the Linux kernel project prefigured demands on other projects. In the past few years distributed version control systems have dramatically increased in popularity. Mercurial, Bazaar, and Git have emerged as the most popular open source DCVS systems, and hosting services have launched offering each of these systems free of charge for open source projects. Google Code began supporting Mercurial, alongside Subversion, repositories in mid 2009 (Paul, 2009b). Cannonical, the company which sponsors the Ubuntu GNU/Linux distribution, offers free Bazaar hosting to open source projects on Launchpad.net. In February 2008 GitHub.com launched, a “social coding” site which provides Git hosting and rich social networking tools to all developers using the site, gratis for FLOSS code. Bitbucket.org offers similar social networking tools around Mercurial, and describes itself as “leading a new paradigm of working with version control”.</p>

  <p>The centralized hosts of peer-to-peer protocols broker a new balance between centralization and federation. They facilitate coordination, but do not mandate it. A site like GitHub can track and aggregate multiple branches of development, but branching does not require any permission or upfront coordination. Instead of requiring an upfront investment of attention and energy to coordinate development activities, DVCS concentrate on improving the mechanisms for developers to track, visualize, and merge changes. The costs of coordinating collaboration is deferred, and the communication overhead required to synchronize and align different branches of code is (hopefully) reduced.</p>

  <p>There is a fascinating culture emerging around DVCS, facilitated by software, but responding to (and suggesting) shifts in collaboration styles. As one developer explains:</p>

  <blockquote>
    “SourceForge is about projects. GitHub is about people… A world of programmers forking, hacking and experimenting. There is merging, but only if people agree to do so, by other channels… GitHub gives me my own place to play. It lets me share my code the way I share photos on Flickr, the same way I share bookmarks on del.icio.us. Here’s something I found useful, for what it's worth… Moreover, I'm sharing my code, for what it's worth to me to share my code… I am sharing my code. I am not launching an open source project. I am not beginning a search for like minded developers to avoid duplication of efforts. I am not showing up at someone else’s door hat in hand, asking for commit access. I am not looking to do battle with Brook’s Law at the outset of my brainstorm” &nbsp;<br>
    —Gutierrez, 2008
  </blockquote>

  <p>Sometimes developers simply want to publish and share their work, not start a social movement. Sometimes they want to contribute to a project without going through masonic hazing rituals. DVCS facilitates these interactions far more easily than traditional centralized version control systems and the hierarchical organizations which tend to accompany them. Part of what makes this all work smoothly are very good tools to help merge disparate branches of work. This all sounds chaotic and unmanageable, but so did concurrent version control when it first became popular.</p>

  <div class="glossary flip">
    <hr>

    <h3>Distribution</h3>

    <p>An inquiry into the channels and formats that disperse, publicize and create audiences for ideas, objects, and data. Online / offline / at the thresholds. Distribution via a particular channel is not always the end point. Distributed material may possibly be adapted, modified, hacked, remixed and re-distributed by a user or group. This modification may or may not be legal depending on intellectual property and licensing issues. But the user might do it anyway.</p>
    <hr>
  </div>

  <p>In anecdotal accounts of switching to DVCS, developers describe an increase in the joy of sharing—the tools help reduce the focus on perfecting software for an imagined speculative use and the overhead of coordinating networks of trusted contributors. The practice really emphasizes the efficient laziness of agile programming, and helps people concentrate on the immediate requirements, rather than becoming preoccupied with endless planning and prognostications.</p>

  <p>&nbsp;In some respects, this emerging style of collaboration is more freewheeling than an anonymously editable wiki, since all versions of the code can simultaneously exist—almost in a state of superposition. Wikis technically support the preservation of diversity in a page's history, but in a centralized wiki the current page is the (ephemeral) final word. DVCS are developing richer interfaces to simultaneously represent diversity, and facilitate the cherry picking of features from across a range of contributors. The expression of a multiplicity of heterarchical voices is explicitly encouraged, although there is a hidden accumulation of technical debt that accrues the longer a merger of different branches of work is delayed. Of course, sometimes you may actually want to start a community or social movement around your software, and that is still possible but is now decoupled, and needs to be managed with purposeful intent.&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch027_multiplicity-and-social-coding.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch027_multiplicity-and-social-coding.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

