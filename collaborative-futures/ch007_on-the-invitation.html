<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch007_on-the-invitation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch007_on-the-invitation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>On The Invitation</h1>

  <p>A shadow hangs over every discussion of social life today: the shadow of economism. In its darkness, light emanates only from productivity, efficiency, incentives and profits. To the epithet of ‘dismal’ famously ascribed to economics, on account of its pessimistic commitment to the inevitability of scarcity for the many, one could also add ‘dark’. The dimness of the prospect derives from the poverty of its instruments of measurement, or rather its reliance on the measurable as a sole index of the ‘good’. But the edifice of economic organization sits atop a substrata of generalized cooperation that conditions the very possibility of its existence.</p>

  <blockquote>
    <p>“The virtually preponderant importance henceforth recognized of externalities bears witness to the limits of the market economy and puts its categories in crisis. It makes visible that the primary sources of wealth, virtual source and condition for all the others, are not manufacturable by any business, accountable in any form of currency, exchangeable against any equivalent. It reveals that the visible economy, so-called formal, is just a relatively small part of the total economy. Its domination over the latter has rendered invisible the existence of a primary economy made up of non-market activities, exchanges and relations through which meaning is produced, the capacity to love, to cooperate, to feel, to link oneself to others, to live in peace with one’s body and nature.</p>

    <p>It is in this other economy that individuals produce themselves as humans, at the same time mutually and individually, and produce a common culture. The recognition of the primacy of external wealth to the economic system implicates the necessity for an inversion of the relationship between between the production of market “value” and the production of wealth “unexchangeable, unappropriable. intangible, indivisible and unconsumable”: the former should be subordinated to the latter.</p>—Andre Gorz, <em>The Immaterial, p.80 (translated by Alan Toner)</em>
  </blockquote>

  <p>Individuals can only produce in connectedness with society, and they do so in manifold ways rarely acknowledged as ‘productive’. And it is their horizontal interrelation that enables everything else to function; social production is care outside of crèche, clinic, hospice and hospital; the force for social peace infinitely more powerful than police on the street; the space of educational and cultural development outside of the University and school; the source of skills in language and communication. It is produced every time we ask for directions in the street, recommend a book, help someone carry a pram in the subway, advise on how to fix a pipe, popularize a fashion or lifestyle…&nbsp;</p>

  <p>To describe these things as ‘productive’ seems absurd or even repugnant, because we know and expect that people do not always behave instrumentally. But the temptation to reframe them stems from frustration at the way in which these things are otherwise magically discounted as ‘externalities’ in a society where economic frameworks and categories are treated as paramount, to the point of rendering anything beyond productivity and profit invisible.</p>

  <p>From this perspective, the attention now given to collaboration poses both an opportunity and a problem. The positive aspect is that it represents the emergence from invisibility of that social wealth which comes from beyond market or state, liberating activity from subsumption to market logic. The danger resides in the threat that its recognition <em>as</em> productivity may lead to a further reduction of life to economistic categories.</p>

  <p>Current hype around collaboration tends to discuss only that part of social production understood as having direct economic impact (because it has been seen to produce substitutes for goods and services previously generated only within the market or the firm). But this aspect is just the tip of the iceberg: the critical project is to bring the rest of the ice into view.</p>

  <h2>Come in Everybody?</h2>

  <div class="glossary flip">
    <hr>

    <h3>Invitation</h3>

    <p>Specific methods by which a project invites the outside in. Linked to questions of having/getting access, entries, hospitality, social bonds, fake bonds.</p>
    <hr>
  </div>

  <p>Like a conversation, creating with others can only take place within a context of willingness: to listen, share, exchange, be contradicted, learn. But what determines how, where and with whom this happens?</p>

  <p>Approach a stranger on the street: instead of asking them for directions, ask them if they’ll come to the cinema, discuss the film afterwards and jointly compose a review. It is likely that the request will be interpreted as an intrusion, even an aggression, and be rebuffed.</p>

  <p>Access to people, spaces and activities is limited by obstacles of class, gender, cultural capital, place and acquaintance. Protocols of participation evolve based on assumptions and norms which are naturalized rather than problematicised. Whilst these modes remain unchallenged their unexamined assumptions are reproduced.&nbsp;</p>

  <p>This vantage point allows us to reconsider the nature of the self-selective quality of online participation. When an expanded range of potential contexts is enabled through the attenuation of place as a condition to engagement, other barriers to participation become more visible: you may want to throw yourself into a collective project, but maybe they don’t want you. In some cases this may be a matter of pragmatic requirements: thresholds of commitment needed for trust; expectations of responsibility; safety considerations (as in the case of pirate communities or Debian). But there are also exclusions resulting from social stratification or plain cliquishness.</p>

  <p>Alternatively invitations can be an expression of a group’s willingness to challenge its own composition and practices, making itself available for change.</p>

  <p>The invitation is thus an important tool for reproducing and transforming the community. Existing members function as portals through which others can be introduced and vouched for.<br></p>

  <p>But who can accept the invitation? Only those who have spare time, energy and resources, and this is the key limiting factor to any putative hope for ‘open participation’. Each participant is circumscribed by their economic and cultural resources. Those who labour at the margins just to survive have little latitude, or energy, to engage in activities geared to enriching their lives in non-material ways.</p>

  <h2>On Economism and Incentives</h2>

  <p>If the above addresses the ‘with whom’ and ‘where’ can we collaborate, we can now return to the why. But it is an inquiry inflected by the initial comments on social production above. We are always producing with others, and why we do so has varied explanations not all of which can be explained in the language of incentives. Eben Moglen captures this well in his discussion of the energy behind creativity in general and free software in particular:</p>

  <blockquote>
    <p>“According to the econodwarf’s vision, each human being is an individual possessing “incentives,” which can be retrospectively unearthed by imagining the state of the bank account at various times. So in this instance the econodwarf feels compelled to object that without the rules [copyright] I am lampooning, there would be no incentive to create the things the rules treat as property: without the ability to exclude others from music there would be no music, because no one could be sure of getting paid for creating it.</p>

    <p>…</p>

    <p>The dwarf's basic problem is that “incentives” is merely a metaphor, and as a metaphor to describe human creative activity it's pretty crummy. I have said this before… but the better metaphor arose on the day Michael Faraday first noticed what happened when he wrapped a coil of wire around a magnet and spun the magnet. Current flows in such a wire, but we don't ask what the incentive is for the electrons to leave home. We say that the current results from an emergent property of the system, which we call induction. The question we ask is “what's the resistance of the wire?” So Moglen's Metaphorical Corollary to Faraday's Law says that if you wrap the Internet around every person on the planet and spin the planet, software flows in the network. It's an emergent property of connected human minds that they create things for one another's pleasure and to conquer their uneasy sense of being too alone. &nbsp;</p>

    <p><br>
    —Eben Moglen, <em>Anarchism Triumphant: Free Software and the Death of Copyright&nbsp;</em></p>
  </blockquote>

  <p>The dogma of monetary incentives, with which Moglen quarrels, is rooted in a philosophical history which reached its apogee in the work of Jeremy Bentham. According to his prescription, individuals act to attain that which is good for them—the useful. Bentham believed utility could be universally defined and function as a guide for social organization. Although this belief faded with the acceptance that utility was subjectively defined, henceforth the new utilitarians would elevate the market as decentralized arbiter and archive of the useful. With only minor modifications this model remains hegemonic today in the form of neoclassical economics. In its liturgy, <em>homo economicus</em> is a rational agent performing countless cost-benefit calculations, and undertaking instrumental action to reach logical ends. As Alain Caillé puts it:</p>

  <blockquote>
    <p>“... one can characterize utilitarianism as all purely instrumental conceptions of existence, which organize life as a function of a calculation or a systematic logic of means and ends, in which an action is always carried out for some other purpose than itself,&nbsp; tied to the sole individual subject, supposedly closed in himself and sole master, addressee and beneficiary of his acts.” &nbsp;</p>

    <p>—Alain Caillé, <em>De L’anti-utilitarisme, 2006</em></p>
  </blockquote>

  <p>Much critical thought accepts substantial parts of the utilitarian perspective, finding in it a clear logic that allows both an understanding of the motor of human behavior, and the possibility to intervene in and transform it. Of course the explanatory power of this framework lies in its account of what people do to satisfy the ubiquitous need for money: food, clothing, shelter are just the least controversial of the needs usually offered only in return for payment. Failing congenital riches (or a social welfare state), one must earn money, and this need for income constitutes the main reason why most people work where they do: in contexts not freely selected by them.</p>

  <p>Once the set of narrowly utilitarian explanations have been re-dimensioned as capturing just a subset of what drives action, their explanatory power can be more usefully harnessed.</p>

  <p>Utilitarian belief was bolstered by studies of situations where the contingency of rewards on performance—incentives—does indeed increase/improve output. Social psychologists class such responsiveness to rewards—or threats—as evidence of ‘extrinsic motivation’. But in recent years extensive research has demonstrated that monetary incentives can be counter-productive; over time they can ‘crowd out’ important non-monetary drives. Humans are not coin-operated machines, and need meaning, recognition and fulfillment.<br></p>

  <h2>Beyond the Carrot and the Stick</h2>

  <p>We do not do anything and everything solely for money. Even in the labor market we often trade off material gain against the reconciliation of other desires. These drives are classified by psychologists as ‘intrinsic motivations’. The original definition comes from Deci:</p>

  <blockquote>
    “One is said to be intrinsically motivated to perform an activity when one receives no apparent reward except the activity itself.”<br>
    —Deci 1971, p.105
  </blockquote>

  <p>Amongst the internal motivations identified as important are: the cultivation of self-determination (control over action); enhancement of self-esteem; performance of the self. What unites these elements is their importance in the formation of identity.<br></p>

  <p>Deci’s definition is problematic because it proposes a segregation of motivations which rarely exists; instead the quest for external reward and personal gratification co-exist in varying measures. This is true both for payed and voluntary labor (where it might be the quest for prestige). But intrinsic factors are plainly more important in voluntary pursuits, for the obvious reason that there is neither remuneration as compensation for doing something dissatisfying, nor monetary sanction for doing something badly.&nbsp;</p>

  <p>Collaboration can provide a context where ability and commitment find acknowledgment, and a stage (especially online) on which we can choose to present ourselves in ways unavailable in the spaces of work or the local physical community. This concern with the self may help explain why attribution appears important to many participants. Because it is possible to get more done working with others, collaboration also offers the possibility of empowerment, and the chance to learn through interaction. On a more hedonistic level, these contexts also offer a space in which to play, unleash curiosity, engage in gestures of reciprocity and kindness, or indulge the sheer pleasure of sociality through participatory community.&nbsp;</p>

  <p>Distinctions such as extrinsic and intrinsic, whilst schematic, help to parse and appreciate the different levels. The drives behind our actions are multifaceted, complex and resistant to exhaustive or exact dissection.</p>

  <h2>The Right Job…</h2>

  <blockquote>
    <p>"...you will probably be very much surprised when I say that there is really no such thing as laziness. What we call a lazy man is generally a square man in a round hole. That is, the right man in the wrong place."</p>

    <p>—Alexander Berkman, <em>What is&nbsp; Anarchism?</em><br></p>
  </blockquote>

  <p>Implicit in all this is the fundamental importance of the nature of the task to be performed; motivations do not exist unanchored from specifics, but vary according to the nature and circumstances of what is to be done. Mundane, repetitive chores, or those with no graspable outcomes, are inherently less appealing. They neither challenge intelligence or catalyze learning, nor deliver a sense of making a meaningful contribution. On the other hand an objective that requires development of new skills, creativity, and that affords control over the nature and style of our contribution is more attractive. Surveys of the FLOSS community, for example, have repeatedly borne out that problem solving, pleasure in ‘making things’, and delight in the aesthetics of code are all attractive to contributors (Ghosh, Lakhani).&nbsp;</p>

  <p>In his book, “Here Comes Everybody,” Clay Shirky describes editing a Wikipedia entry on the fractal nature of snowflakes. He asks himself why he did it, and comes up with three answers. First he says it “was a chance to exercise some unused mental capacities—I studied fractals in a college physics course in the 1980s.” The second reason is vanity: “the “Kilroy was here” pleasure of changing something in the world, just to see my imprint on it.” The third motivation is simply “the desire to do a good thing. This motivation of all of them, is both the most surprising and the most obvious.” (Shirky, p. 132)</p>

  <h2>… for the Right Person: Self-Selection</h2>

  <p>Where involvement is purely voluntary, people assume tasks which they feel suited to, or whose challenges attract them. This <em>self-selecting</em> character of participation distinguishes relations and activities in these projects from the mobilization of labour in the firm. As a result intrinsic motivations have a greater importance in these contexts, but this self-directed orientation is often alloyed with extrinsic motivations.</p>

  <p>Online, the ease of sharing information erodes the obstacles between doing something for oneself and socializing it for others. As a software developer wrote in one weblog:</p>

  <blockquote>
    <p>“The fact is, in the Free Software world… the developer is the consumer. Applications are not programmed for some mythical “average consumer” but rather for real world applications. For example, if I need a new image viewer because none of the current ones (xv, ee, eog, etc.) meet my needs I simply sit down and program one to what I exactly want. Many companies already do this internally and create proprietary software. However, in the Free Software community that software which would otherwise be proprietary is made public.</p>

    <p>… This is what I call productive selfishness. (Successful) Projects are created for their developers own personal reasons and are given to the community simply because someone else might have need of it and might want to extend it.”</p>

    <p>—'Penguinhead’, <em>Linux Today</em><br></p>
  </blockquote>

  <p>His formulation reminds us that the relationship between satisfying our own needs and helping others can often be an <em>encounter</em> rather than a collision.</p>

  <p>Some free software programmers get paid to do what they love. For others money may not be immediately present, but reputation and prestige are; learning may be fun but it’s also useful to guarantee ongoing employability; contact with others in a creative context may help appease self-doubt, but may also be the vector for the next job.</p>

  <p>But in addition to these ambivalent convergences, there is always something more.</p>

  <h2>Interconnectedness<br></h2>

  <p>There is no individual universe available to inhabit: the responses, opinions and reactions of others are always in play and often important to us. Human interconnectedness is embedded, and manifests itself in the search for recognition, not the micro-celebrity of the online media, but rather a primary acknowledgment of being, dignity and worth. &nbsp;<br></p>

  <p>Ultimately the motivations are myriad in a galaxy of online communities composed of a universe of subjectivities: forums filled with the argumentative, those looking for information and occasionally donating some themselves, the lonely seeking community, the vain, the depressed seeking support. Projects made of apprentices and mentors, practical people, proselytisers, entrepreneurial types, dreamers. In sum, a plurality, who one way or the other have built sustained communities.<br></p>

  <p><br></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch007_on-the-invitation.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch007_on-the-invitation.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

