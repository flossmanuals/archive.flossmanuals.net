<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch031_forks-vs-knives.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch031_forks-vs-knives.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Forks vs. Knives</h1>

  <p>When you face a simple task and have all the capability and know-how necessary to accomplish it by yourself, there is really no reason for you to collaborate. And that's OK.&nbsp;</p>

  <p>But when achieving the goal is hard, and the tools you have are insufficient, there is room for collaboration. In some cases collective action will be a part of the task itself—meaning the mobilization of group collaboration is a part of the task. Often this mobilization is a by product of the principal objective.</p>

  <p>We defined intentionality and coordination of contributions as key to collaboration. But both intention and coordination actually raise the cost of collaboration, and in some cases makes it not worth the trouble.</p>

  <blockquote>
    <p>“Sometimes developers simply want to publish and share their work, not start a social movement.&nbsp; Sometimes they want to contribute to a project without going through masonic hazing rituals.”</p>
  </blockquote>

  <p>This quote from the Multiplicity and Social Coding chapter refers to Distributed Version Control Systems. The loose coordination enabled by these systems attempts to lower the cost of collaboration. By using these distributed collaborative tools the overhead inherent in establishing intention and coordination is reduced. In fact these system allow for a completely individualized practice. A Git user can work alone for years. By publishing her files online under a Free Software license she opens the door to a potential reappropriation or even future collaboration. She does not have to commit to contribute, she does not have to coordinate with anyone. She is not collaborating (yet).&nbsp;</p>

  <p>This approach is similar to the principles advocated by the Free Culture movement. Share first and maybe collaborate later, or have others use your work, be it in an individual or a collaborative manner. But while distributed sharing platforms are common, DVCS excels in constantly switching between a coordinated action and an individuated one.</p>

  <p>At any point of the development process Alice, a Git user, can inspect Bob's code repository and choose to fork (essentially duplicate) his code base to work on it separately. No permission is required, no coordination needed. At any point, Bob can pull Alice's changes and merge them back into his own repository.&nbsp;</p>

  <p>This might seem trivial, but it's not. Centralized version control systems can make it technically easy to fork but are usually not sophisticated enough to make merging back easy. This has turned forking into a highly contested practice, as forking the code meant forking the project and dividing the community. DVCS makes forking and merging trivial and lowers the cost of collaboration.&nbsp;</p>

  <p>But whilst distributing and individuating the process minimizes the need for intent and coordination, it may result in deemphasizing the collaborative act. By ensuring that ‘you don’t have to start a social movement’, does it divorce itself from the social ideals of collaboration celebrated by many Free Software activists?</p>

  <p>We argue it does not. You can still start a social movement if you like. You might actually have better tools to do so too, as the distributed process allow a larger autonomy for the individual members and less friction in governance and control.</p>

  <div class="glossary">
    <hr>

    <h3>Architecture</h3><img style="width: 406px; height: 557px;" src="_booki/collaborative-futures/static/mov.architecture-cropped.png">
    <hr>
  </div>

  <h2>From Splicing Code to Assembling Social Solidarities</h2>

  <p>We can already see early examples of these approaches outside of Free Software. One of them is the Twitter Vote Report used in the 2008 US presidential elections &lt;<a href="http://twittervotereport.com">twittervotereport.com</a>&gt; and its later incarnation as SwiftRiver, a tool for crowdsourcing situational awareness:</p>

  <blockquote>
    <p>“Swift hopes to expand [Twitter Vote Report’s] approach into a general purpose toolkit for crowdsourcing the semantic structuring of data so that it can be reused in other applications and visualizations.&nbsp; The developers of Swift are particularly interested in crisis reporting (Ushahidi) and international media criticism (Meedan), but by providing a general purpose crowdsourcing tool we hope to create a tool reusable in many contexts.&nbsp; Swift engages self-interested teams of “citizen editors” who curate publicly available information about a crisis or any event or region as it happens”<br>
    —Swift Project, 2010 &lt;<a href="http://github.com/unthinkingly/swiftriver_rails">http://github.com/unthinkingly/swiftriver_rails</a>&gt;</p>
  </blockquote>

  <p>These activist hacker initiatives are realizing the potential of loosely coordinated distributed action. Its political power is entangled with its pragmatism, allowing the collaboration to fluidly shift between individual and collective action. In January 2010, as the horrors of the Haiti earthquake were unraveling, hackers around the world were mobilizing in unconference-style “CrisisCamps”. These hackathons gathered individuals in physical space to “create technological tools and resources for responders to use in mitigating disasters and crises around the world.” &lt;<a href="http://crisiscommons.org/about-us/">crisiscommons.org/about-us</a>&gt;</p>

  <h2>Contesting the Shock Doctrines</h2>

  <p>This type of external interest and action was previously reserved to human rights organization, media companies, governments and multinational corporations—all organizations that work in a pretty hierarchical and centralized manner. Now we see a new model emerge—a distributed networked collaboration of interested individuals contributing digital labor, not just money.</p>

  <p>The political vacuum presented by these natural or man made crises leave room for a strong active force that often enforces a new political and economic reality. In her book titled The Shock Doctrine, author Naomi Klein describes how governments and businesses have exploited instances of political and economic instabilities in recent decades to dictate a neo-liberal agenda. In each case the interested powers were the first on the scene, imposing rigid rules of engagement and coordination, and justifying enforcement by the need to restore order.</p>

  <p>In contrast, the activists are providing the tools and the know how for data production and aggregation. They are then actively assembling them into actionable datasets:</p>

  <blockquote>
    <p>“People on the ground need information, desperately. They need to know which symbols indicate that a house has already been searched, where the next food/water/medicine drop will be, and that the biscuits are good, and not expired.&nbsp; They also need entertainment, and news—à la Good Morning Vietnam. And messages of consolation, emotional support, solidarity, and even song and laughter. ”<br></p>

    <p>—Jonnah Bossowitch<br></p>
  </blockquote>

  <p>The model of individual autonomy and free association that enables the hackers coding is embedded into the assistance they propose, empowering communities on the ground. One of the hackers from the NYC CrisisCamp jokingly declares: “<em>Two sides get to play the shock doctrine game”</em>. It is obviously a drop in the ocean in comparison to the scale of the disaster and the years it would take to heal. Nations and corporations have long term interests and the resources that will probably keep them in the picture long after the networked effort will evaporate.</p>

  <p>These are brave yet very early experiments in new political association. They are widely informed by experiments in collaboration and control in information economies. Most of them will not automatically translate to meat space. Especially at times of natural disaster, when food and medicine shortage occupies much of the human rights debate.</p>

  <h2>Open Leaders Failing Forward</h2>

  <p>The abundance of information technologies have also lowered the price of failure and made it an inherent part of the process. It's not about whether you will fail, it's about how you will fail. What will you learn from failure? How will you do things differently next time. This is what some call “failing forward”.&nbsp;</p>

  <p>The lowering costs of failure in distributed networked production allows for a more open emergence of leadership. One may provide leadership for a while and then get stuck, lose the lead, and be&nbsp;replaced by another one forking and leading in a different direction. This algorithmic logic justifies open access to knowledge and distribution of power that favors merit over entitlement. This is not a democracy, but a meritocracy. A meritocracy that favors technical expertise, free time, persistence and social skills.&nbsp; All traits that are definitely not evenly distributed.</p>

  <p>Initiatives like FLOSS Manuals have acknowledged the importance of documentation for the collaborative process. To take real advantage of the network effect, we should learn to document failure, not only success.</p>

  <p>In the past, experiments in alternative social organizations were hampered by limitations on the resources available within individual projects, and isolated by the costs of communication and coordination with kindred efforts. This was the case of the Cooperative movement, communes, the occupied factories in Argentina and other similar alternative social experiments. If we extend the notion of failing forward beyond the production of information, future results might look different.</p>

  <p>New models of collaboration will continue to inform and alter our social relations. These political experiments are free for us to assess, free for us to fork and to try something different. Then, in the future after more development is done, and the commits have been tested, we will also be free to merge them back.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch031_forks-vs-knives.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch031_forks-vs-knives.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

