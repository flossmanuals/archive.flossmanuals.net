<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch010_participation-and-process.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch010_participation-and-process.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Participation and Process</h1>

  <p>Our conception of what constitutes fair treatment vary according to context, as do our reactions to being treated unfairly. If someone jumps the queue in front of us in a shop, it's annoying but quickly forgotten. But if we contribute time or money to establishing a collective enterprise, and instead it is subverted for other ends, then we feel angry and betrayed.&nbsp;</p>

  <p>Our expectations and emotional intensity vary according to the degree we feel ourselves invested in, and part of, a shared project. As the intensity increases so does a need for procedural guarantees, transparency, fairness in terms of the division of benefits and acknowledgment.&nbsp;</p>

  <p>Where participation is limited to small or occasional contributions, we may not even want to be drawn into time-consuming discussions about goals and methods. Likewise where our involvement is driven purely by whims of gratification rather than any desire to attain distant objectives. NASA's ClickWorkers project asks users to identify and count craters on Mars, and the combined inputs allow them to rationalize use of their internal research resources. While it is impossible to guess all the motivations which drive people to contribute, it is obvious that no one expects to be able to actively influence NASA's overall agenda by contributing, nor to control the organization directly.&nbsp;</p>

  <p>Sustained involvement requiring a substantial expenditure of effort, or active engagement to create or promote something deemed of worth or importance, demands a more careful framework. Care is required because participation implicates our sense of identity. Defection by others, a sense of betrayal, anger at manipulation or exploitation are destructive not only to the immediate project but to willingness to collaborate in the future. On the other hand every collaboration needs room also to change, and a breathing space which acknowledges the different levels of commitment of its participants, which themselves will vary over time.</p>

  <p>While an explicit process is no panacea to the problems that arise when we deal and work with others, it can anticipate and mitigate the most damaging consequences when things go awry, whilst protecting the flexibility necessary to adapt.</p>

  <h2>Productive dissent</h2>

  <p class="">Any collaborative endeavor (paid, unpaid or underpaid as the case may be) risks becoming an echo chamber—a shared space in which participants operate from a similar world-view, mostly agree with each other, and quibble over details and technicalities without being about to raise larger, riskier questions about the work and why or whether it needs to exist.</p>

  <p class="">Friendship, love and attachment can be crucial to collaborative work. With friendship comes responsibility and good friends can (should) challenge each other. But “open” collaborations are quite susceptible to the inadvertent suppression of dissent because of the convergence of like-minded thinkers and the lack of formal processes for recruitment of participants, voicing of disagreements, and raising of fundamental ontological issues (see the Interlude: 'Tyranny of Structurelessness' section).&nbsp;</p>

  <p class="">Particularly if they are meant for an audience beyond the creators, collaborative projects should always take heed of their “outside” and engineer a process for productive antagonism. This might take the form of invitations to participate or partner, public forums, consultation over beer, training processes for newbies or provocative blog posts, among many other possible formats.</p>

  <div class="glossary">
    <hr>

    <h3>Dissent</h3>

    <p>Necessary as a constituting element for any debate, agency, educational turn, and (micro)community to emerge. In an effort to reduce the “echo chamber” effect, collective efforts need to engineer methods of collective listening as a means of gathering productive dissent.</p>
    <hr>
  </div>

  <p class="">Essentially, collaborative projects need to develop modes of collective listening to the environment outside their boundaries. Can collaborations learn to be more sensitive and receptive?</p>

  <p class="">It is challenging to be sensitive and it is hard to listen. It may not come naturally. Hence it is an engineering problem—something to be purposefully designed.&nbsp;</p>

  <p>Architect and theorist Markus Miessen eloquently interrogates the consensus-seeking rhetoric around calls for “participation”, an insight that can be extended to collaboration. All too often a romantic view of “harmony and solidarity” is assumed. Miessen writes that he “would like to promote an understanding of conflictual participation, one that acts as an uninvited irritant”.&nbsp;<!--EndFragment--> In the writing of this book we have seen first hand how easy it is to subsume dissent within a collaborative framework (because, in many ways, it takes more work to disagree, to communicate, to reconcile or part ways). Collaborations may actually encourage a sort of passivity; pre-existing projects have a logic and inertia that can be impossible for outsiders to crack, to truly intervene on, even if they are encouraged by originators to do so. In a way, collaboration produces subjects—one must submit to the project, become subjected to it—it order to engage.</p>

  <h2>Authority in Distributed Creation</h2>

  <blockquote>
    <p>“We reject: kings, presidents and voting.</p>

    <p>We believe in: rough consensus and running code.”</p>

    <p>—David Clark, “A Cloudy Crystal Ball—Visions of the Future” (1992)</p>
  </blockquote>

  <p>Online communities are not organized as democracies. The most accountable of them substitute a deliberative process of discussion for majority-based voting. This derives from the fact that the original initiative emanated from one or a couple of people, and because participants are there of their own volition. Majority rule is not seen as inherently good or useful. The unevenness of contributions highlights the fact that a disproportionate part of the work in a project is done by a smaller sub-group. Within a political sphere that privileges production this trends towards the valuing of ability and commitment, sometimes phrased in the language of ‘meritocracy'. (That said, as we think about these structures and arrangements outside the software model it gets more tricky. How do we apply the concept of “meritocracy” to the realm of art making, where there are fewer objective standards?&nbsp; Yes, a person may be very proficient with video editing software, but that doesn't necessarily make them a “good editor.”&nbsp; To continue with this example, there are no objective criteria for things like narrative sensibility, pace, style of cutting, and so on—only conventions and taste. What and who defines merit?)</p>

  <p>Founders particularly have considerable power, derived from the prestige accruing from a successful project, recognized ability, and their network centrality—having had most opportunity to forge relations with newcomers, and an overview of the technical structures and history of the project. These factors give them authority. This hierarchical element is nonetheless diffused by the modular nature of productive organization: sensible structures devolve authority over their parts so as to maximize the benefits of voluntary contribution.&nbsp;</p>

  <p>This architectural enabling of autonomy extends also to newer users, who can take initiative free from having to continuously seek permission and endorsement. However their contributions may not be incorporated if considered substandard or unnecessary, but such decisions arise out of a dialogue which must have some basis in efficiency, aesthetics or logic. Arbitrary dismissal of others in a community environment risks alienating others, which if generalized and persistent may place the whole edifice under strain, or even spark a fork or split.</p>

  <p>Longstanding projects have also tended to give themselves defined legal forms at some point, thus the prevalence of foundations behind everything from Wikipedia to Apache. These structures often have charters, and sometimes hold elections to decide on the entry of the new members or appoint totemic figures.</p>

  <h2>Reputation and Trust</h2>

  <p>Influence derives from reputation—a substitute for trust in the online environment—which is accumulated and assessed through the use of persistent avatars, user names or real names. In addition to demonstrated aptitude, quantitative measures of commitment are also relied upon. Initial promotion of an editor's status on Wikipedia, for example, relies upon the length of time since the first edit, and the number of edits effected. Thereafter advancement also entails a qualitative evaluation of an editor's performance by their peers.</p>

  <p>Higher user status allows the individual greater power over the technical tools that co-ordinate the system, and require confidence on the part of others that access will not abused. This threat is higher in software projects where hostile infiltration poses a real security risk given that the code will be publicly distributed. A variety of methods for vouching for each other are thus cultivated, new developers may require sponsors. In the case of Debian physical encounters between developers are used to sign each others' encryption keys, which are then used to authenticate the package management process, adding a further layer of robustness.</p>

  <h2>Process Fetishism</h2>

  <p>There's a risk of making a fetish of process over product, of the act of collaboration over the artifact that results from it. How important is it that a product was produced through an open, distributed network if, in the end, it serves the interests of the status quo? If it's just another widget, another distraction, an added value that some giant conglomerate can take advantage of, as in some cases of crowdsourcing? Does open collaboration serve a purpose or is it more like a drum circle, way more fun and interesting for the participants than for those who are forced to listen to it?</p>

  <p>Collaboration is fundamental to human experience. It should be no surprise that collaboration also occurs online. The important question is what goals these new opportunities for cooperation and creation across space and time are put in service of.</p>

  <p>In placing emphasis on process—on activity that is ongoing—there is also an implicit elevation of change over stasis, movement over stillness, speed over slowness, fluidity over fixity. 'Nowness', newness, and real time are heralded as superior to a long view.&nbsp; These attributes must be questioned as goods in and of themselves.</p>

  <p>It is true that digital media needs to move, to be updated, to stay relevant—but we should pause to critically reflect on why this is necessarily the case. After all, hardware and software manufacturers, using the principles of “planned obsolescence” push consumers to buy new, supposedly improved devices every season. Or worse, they design them to malfunction, forcing consumers to purchase replacements, as Giles Sade's <em>Made To Break: Technology and Obsolescence in America</em> makes fascinatingly clear.</p>

  <p>This mindset was humorously visible early one morning as two of us walked passed an Apple Store in New York City on the way to work on this text. A line of people holding iPhones wrapped around the block, all of them waiting in eager anticipation of—you guessed it—the revamped iPhone.&nbsp; We question whether this impatient acquisitiveness, this obsession with the “latest and greatest,” is an attitude we want to export to other modes of creative production. In a society committed to growth at any cost—to more, newer, faster, bigger, better—it's tempting to assume that change is progress, when there is in fact no such guarantee. And so, while we believe that open collaboration can be a valuable paradigm in certain circumstances, it should not be mistaken for a panacea.&nbsp; Alongside collaboration we must carve out space for projects that are no longer upgraded or made “new and improved,” that are no longer in process, and as such provide counterbalance to the tyranny of the new and the now.</p>

  <div class="glossary">
    <hr>

    <h3>Acceleration/Deceleration</h3>

    <p>A mode of speed or slowness that describes a changing intensity of movement. Data as well as our lives seem to be measure-able by modes of movement. The still or fixed position is perhaps a way of indicating a counter-position in this constant claim for moving and taking part in the seamless flux. Becoming slow and still and silent is possibly a radical, risky, more complex and therefore more attractive act.</p>
    <hr>
  </div>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch010_participation-and-process.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch010_participation-and-process.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

