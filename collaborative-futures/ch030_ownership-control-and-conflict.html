<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch030_ownership-control-and-conflict.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch030_ownership-control-and-conflict.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Ownership, Control, Conflict</h1>

  <blockquote>
    <em>“Free cooperation has three definitions: It is based on the acknowledgment that given rules and given distributions of control and possession are a changeable fact and do not deserve any higher objectifiable right. In a free cooperation, all members of the cooperation are free to quit, to give limits or conditions for its cooperative activity in order to influence the rules according to their interests; they can do this at a price that is similar and bearable for all members; and the members really practice it, individually and collectively.”</em><br>
    —Christoph Spehr, <em>The Art of Free Cooperation</em><br>
  </blockquote>

  <h2>Ownership</h2>

  <p>In the industrial information economy the outputs are owned by the company that produces them. Copyrights and patentable inventions produced by employees are transferred to the corporate owner, either directly or via the ‘work for hire’ doctrine, which treats the acts of individuals as extensions of the employers will.</p>

  <p>Collaborations amongst small groups often use similar proprietary methods to control the benefits arising from their outputs, but parcel out ownership amongst collaborators. In larger-scale settings, however, the very concept of ownership is turned on its head: where proprietary strategies seek to exclude use by others, these approaches prevent exclusion of others from using.</p>

  <p>In the traditional workplace, the labor relationship was set out in a legally binding manner, whose terms were clear although imbalanced. In the digital era however the distinction between the time and space of work and that of play is ambivalent, and those dedicating their energy are often not employees. Licenses play a role partially analogous to that of the labor contract. Cases where volunteer contributions were subsequently privatized, such as Gracenote's enclosure of the Compact Disk Database (CDDB), have demonstrated the risks inherent in not confronting the ownership question (the takeover and commercialization of the IMDB is another less dramatic example).&nbsp;</p>

  <p>The two principal licensing schemes used in free software and free culture production today reflect this. The GNU Public License, stewarded by the Free Software foundation, guarantees the rights to use, distribute, study and modify, <em>provided</em> the user agrees to abide by the same terms with any downstream outputs. Creative Commons (CC) licenses are more diverse, but that most commonly employed within large scale collaborations, the BY-SA (Attribution and ShareAlike) license, functions in the same manner. However, amongst individual users and small-team production there continues to be wide use of CC licenses, which permit distribution but bar commercial use.</p>

  <h2>Conflict</h2>

  <p>This licensing approach creates a system where rich repositories of data artifacts are available for reuse, also commercially, for those who abide by the rules: commons on the inside, property to the outside. Following Spehr, we can see that this strategy of preventing exclusive ownership allows anyone disagreeing with the direction taken by a project to leave <em>without</em> having to sacrifice the work that they have invested, because they can bring it with them and take up where they left off.</p>

  <p>Such configurations are useful for a second reason. In traditional proprietary organizations the disgruntled have three options: <em>exit</em>, <em>loyalty</em>—putting up with it-, or <em>voice</em>—speaking out in opposition (the terminology is Albert Hirschman’s). Because speaking out often incurs awkward conflicts and the possibility of stigmatization or expulsion, it is heavily disincentiv. Once the power of ownership is contained, however, one can leave the collaboration without abandoning the project, and the pressure to withhold criticism and disagreement is correspondingly attenuated. This can encourage conflicts to be played out in a potentially useful manner within a project, and makes exit an act of last resort.&nbsp;</p>

  <p>Although this licensing protects participants’ access to the outputs, there is always a cost to leaving: loss of any recognition and visibility already attained, technical infrastructure, and the consumption of energy through acrimony.</p>

  <h2>Forking and Merging</h2>

  <p>There have been many successful software forks over the years, demonstrating that the guarantees actually work. In some cases the second project supersedes the original, in others a period of separation is sufficient to cool tempers and reconcile differences, culminating in a reunification around new terms.<br></p>

  <div class="glossary">
    <hr>

    <h3>Fork</h3>

    <ol>
      <li>As a piece of cutlery or kitchenware, a fork is a tool consisting of a handle with several narrow tines (usually two, three or four) on one end. The fork, as an eating utensil, has been a feature primarily of the West. &lt;<a href="http://en.wikipedia.org/wiki/Fork">en.wikipedia.org/wiki/Fork</a>&gt;<br></li>

      <li>(software) When a piece of software or other work is split into two branches or variations of development. In the past, forking has implied a division of ideology and a split of the project. With the advent of distributed version control, forking and merging becomes a less precipitous, divisive action. &lt;<a href="http://en.wikipedia.org/wiki/Fork_(software_development)">en.wikipedia.org/wiki/Fork_%28software_development%29</a>&gt;</li>
    </ol>
    <hr>
  </div>

  <p>The disruptive force of forking is greater in an environment whose default is to maintain code in centralized, collaboratively maintained repositories such as Subversion. Entry and exit in the project implicate both a division of participants and the need to erect new infrastructural support. The popularization of distributed version control systems such as GIT, Bazaar and Mercurial is changing this default (as discussed above in <em>Multiplicity and Social Coding</em>), and creating more situations where the autonomous development of code, and the possibility of its repeated collaborative merging are rendered more explicit. One could say that the future is one where the fork, a separated initiative, is the basic state, always awaiting its moment of reintegration.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch030_ownership-control-and-conflict.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch030_ownership-control-and-conflict.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

