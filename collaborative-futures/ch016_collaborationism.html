<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch016_collaborationism.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch016_collaborationism.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Collaborationism</h1>

  <p>As collaborative action can have more than one intent, it can also have more than one repercussion. These multiple layers are often a source of conflict and confusion. A single collaborative action can imply different and even contrasting group associations. In different group context, one intent might incriminate or legitimize the other. This group identity crisis can undermine the legitimacy of collaborative efforts altogether.</p>

  <h2>Collaboration with the enemy</h2>

  <p>In a presentation at the <em>Dictionary of War</em> conference at Novi Sad, Serbia in January 2008, Israeli curator Galit Eilat described the joint Israeli/Palestinian project <em>Liminal Spaces</em>:</p>

  <blockquote>
    <p>“…When the word “collaboration” appeared, there was a lot of antagonism to the word. It has become very problematic, especially in the Israeli/Palestinian context. I think from the Second World War the word “collaboration” had a special connotation. From Vichy government, the puppet government, and later on the rest of the collaborations with Nazi Germany.</p>

    <p>Galit Eilat, Dictionary of War video presentation ”<br>
    &lt;<a href="http://dictionaryofwar.org/concepts/Collaboration_(2)">dictionaryofwar.org/concepts/Collaboration_(2)</a>&gt;</p>
  </blockquote>

  <p>While there was no doubt that <em>Liminal Spaces</em> was indeed a collaboration between Israelis and Palestinians, the term itself was not only contested, it was outright dangerous.</p>

  <p>I remember one night in 1994 when I was a young soldier serving in an Israeli army base near the Palestinian city of Hebron, around 3:30am a car pulled off just outside the gates of our base. The door opened and a dead body was dropped from the back seat on the road. The car then turned around and rushed back towards the city. The soldiers that examined the body found it belonged to a Palestinian man. Attached to his back was a sign with the word “Collaborator”.</p>

  <h2>Context and conflict</h2>

  <p>This grim story clearly illustrates how culturally dependent and context-based a collaboration can be. While semantically we will attempt to dissect what constitutes the context of a collaboration, we must acknowledge the inherit conflict between individual identity and group identity. An individual might be a part of several collaborative or non-collaborative networks. Since a certain action like SEO optimization can be read in different contexts, it is often a challenge to distill individual identity from the way it intersects with group identities.</p>

  <blockquote>
    <p>“The nonhuman quality of networks is precisely what makes them so difﬁcult to grasp. They are, we suggest, a medium of contemporary power, and yet no single subject or group absolutely controls a network. Human subjects constitute and construct networks, but always in a highly distributed and unequal fashion. Human subjects thrive on network interaction (kin groups, clans, the social), yet the moments when the network logic takes over—in the mob or the swarm, in contagion or infection—are the moments that are the most disorienting, the most threatening to the integrity of the human ego.”</p>

    <p>The Exploit: A Theory of Networks<br>
    <em>by Alexander R. Galloway and Eugene Thacker</em></p>
  </blockquote>

  <p>The term “group identity” itself is confusing as it obfuscates the complexity of different individual identities networked together within the group. This inherent difficulty presented by the nonhuman quality of networks means that the confusion of identities and intents will persist. Relationships between individuals in groups are rich and varied. We cannot assume a completely shared identity and equal characteristics for every group member just by grouping them together.</p>

  <p>We cannot expect technology (playing the rational adult) to solve this tension either, as binary computing often leads to an even further reduction (in the representation) of social life. As Ippolita, Geert Lovink, and Ned Rossiter point out</p>

  <blockquote>
    <p>“We are addicted to ghettos, and in so doing refuse the antagonism of ‘the political’. Where is the enemy? Not on Facebook, where you can only have ‘friends’. What Web 2.0 lacks is the technique of antagonistic linkage.”</p>

    <p>The Digital Given—10 Web 2.0 Theses<br>
    by Ippolita, Geert Lovink &amp; Ned Rossiter<br>
    &lt;<a href="http://networkcultures.org/wpmu/geert/2009/06/15/the-digital-given-10-web-20-theses-by-ippolita-geert-lovink-ned-rossiter/">networkcultures.org/wpmu/geert/2009/06/15/the-digital-given-10-web-20-theses-by-ippolita-geert-lovink-ned-rossiter/</a>&gt;</p>
  </blockquote>

  <p>The basic connection in Facebook is referred to as friendship since there is no way for software to elegantly map the true dynamic nuances of social life. While friendship feels more comfortable, its overuse is costing us richness of our social life. We would like to avoid these binaries by offering variation and degrees of participation.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch016_collaborationism.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch016_collaborationism.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

