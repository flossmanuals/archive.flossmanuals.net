<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>Collaborative Futures</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/collaborative-futures/collaborative-futures.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/collaborative-futures/collaborative-futures.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Collaborative Futures
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch033_free-vs-gratis-labor.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch033_free-vs-gratis-labor.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Free vs. Gratis Labor</h1>

  <p>What makes a collaboration “open”? Open appears to be an assertion of—though it is more accurately an aspiration towards—egalitarianism, inclusion, non-coerciveness, freedom.</p>

  <p>But what kind of freedom? Free as in unscripted and improvisatory, free as in freely chosen, free as in unpaid, or free as in it won't tie you down?&nbsp;</p>

  <p>As books like Jeff Howe’s <em>Crowdsourcing: Why The Power of the Crowd is Driving the Future of Business</em> show, corporate America is ready to collaborate. They want to have an open relationship with their workforce, because who can beat free? And by turning their consumers into collaborators, the bond between the company and their customers is made even stronger. Meanwhile, everyday people are happy to help. Why? Howe says people do it for fun and for the “cred”, otherwise known as the “emerging reputation economy.”&nbsp;</p>

  <p>In her oft-referenced essay “Free Labor: Producing Culture for the Digital Economy,” Tiziana Terranova discusses free labor's complex relationship to capitalism.&nbsp;</p>

  <blockquote>
    Free labor is a desire of labor immanent to late capitalism, and late capitalism is the field that both sustains free labor and exhausts it. It exhausts it by subtracting selectively but widely the means through which that labor can reproduce itself: from the burnout syndromes of Internet start-ups to underretribution and exploitation in the cultural economy at large. Late capitalism does not appropriate anything: it nurtures, exploits, and exhausts its labor force and its cultural and affective production. In this sense, it is technically impossible to separate neatly the digital economy of the Net from the larger network economy of late capitalism. Especially since 1994, the Internet is always and simultaneously a gift economy and an advanced capitalist economy. The mistake of the neoliberalists (as exemplified by the Wired group), is to mistake this coexistence for a benign, unproblematic equivalence.
  </blockquote>

  <p>As Terranova shows, we cannot pretend the gift economy of free (unpaid) collaboration exists totally apart from, untainted by, neoliberalism. The two are deeply interconnected, and becoming more so. Perhaps this is what our collaborative future really looks like.</p>

  <p>Power exists after decentralization and neoliberalism operates in a distributed fashion—by smashing institutions (healthcare, schools, social welfare) and setting workers “free”. Freedom, crowdsource, outsource, choice, autonomy, independence, innovation. We should not forget that these are also words central to the vocabulary of the market.&nbsp;</p>

  <div class="glossary">
    <hr>

    <h3>Free</h3>

    <p>Free as in free speech? Free as in free beer? Free as in free world? Free as in free markets? Free as in free labor? Using the word “free” is ambiguous as it can be used to celebrate the excesses of neoliberal capitalism and/or imply a confrontational position to this regime. It is important to clarify what we mean when we say ‘free’. Who is free and what exactly are they free/freed from? Free is not always good and structure is not always bad (see <em>The Tyranny of Structurelessness</em>). Freedom and structure are not necessarily oppositional. As Deleuze and Guattari caution, “Never believe that a smooth space will suffice to save us.” Freedom might not be such a great thing if it means, like anonymous (see first section in the book), that collective action produces terrorism, renunciation of social responsibility and intolerance or, like free markets, that openness produces extreme social and economic inequality. Perhaps it is important to rethink our relation to freedom. &nbsp;</p>
    <hr>
  </div>

  <p>In this new world we are no longer bound to jobs that are required by law to stay with us, to support us; instead we're free to reinvent ourselves, to flit from project to project. This is the freedom of not being tied down, though the downside is that we're on our own. It's an open relationship, after all. We are freer, in many respects, but also replaceable. The crowd can take over; the crowd is cheaper, more efficient, less demanding.&nbsp;</p>

  <p>These days, as the precarity of populations increases, we should ask ourselves, who has the free time to collaborate? Time is not an evenly distributed resource, as women working the “second shift” know too well. Who is able to work for “reputation,” a substance invoked not just by Howe but also in the pages of this book? Only those who have enough to cover the basics.</p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Anonymous</a></li>

  <li><a href="ch002_how-this-book-is-written.html">How This Book is Written</a></li>

  <li><a href="ch003_a-brief-history-of-collaboration.html">A Brief History of Collaboration</a></li>

  <li><a href="ch004_this-book-might-be-useless.html">This Book Might Be Useless</a></li>

  <li class="booki-section">Background Concepts</li>

  <li><a href="ch006_assumptions.html">Assumptions</a></li>

  <li><a href="ch007_on-the-invitation.html">On the Invitation</a></li>

  <li><a href="ch008_social-creativity.html">Social Creativity</a></li>

  <li><a href="ch009_open-relationships.html">Open Relationships</a></li>

  <li><a href="ch010_participation-and-process.html">Participation and Process</a></li>

  <li><a href="ch011_limits-of-participation.html">Limits of Participation</a></li>

  <li class="booki-section">What is collaboration anyway?</li>

  <li><a href="ch013_first-things-first.html">First Things First</a></li>

  <li><a href="ch014_coordinating-mechanisms.html">Coordinating Mechanisms</a></li>

  <li><a href="ch015_does-aggregation-constitute-collaboration.html">Does Aggregation Constitute Collaboration?</a></li>

  <li><a href="ch016_collaborationism.html">Collaborationism</a></li>

  <li><a href="ch017_criteria-for-collaboration.html">Criteria for Collaboration</a></li>

  <li><a href="ch018_continuum-sets-for-collaboration.html">Continuum Sets for Collaboration</a></li>

  <li><a href="ch019_non-human-collaboration.html">Non-Human Collaboration</a></li>

  <li class="booki-section">Case Studies</li>

  <li><a href="ch021_boundaries-of-collaboration.html">Boundaries of Collaboration</a></li>

  <li><a href="ch022_p2p-the-unaccepted-face-of-cultural-industry.html">P2P: The Unaccepted Face of Cultural Industry?</a></li>

  <li><a href="ch023_anonymous-collaboration-ii.html">Anonymous Collaboration II</a></li>

  <li><a href="ch024_problematizing-attribution.html">Problematizing Attribution</a></li>

  <li><a href="ch025_asymmetrical-attribution.html">Asymmetrical Attribution</a></li>

  <li><a href="ch026_can-design-by-committee-work.html">Can Design By Committee Work?</a></li>

  <li><a href="ch027_multiplicity-and-social-coding.html">Multiplicity and Social Coding</a></li>

  <li class="booki-section">The Present</li>

  <li><a href="ch029_crowdfunding.html">Crowdfunding</a></li>

  <li><a href="ch030_ownership-control-and-conflict.html">Ownership, Control and Conflict</a></li>

  <li><a href="ch031_forks-vs-knives.html">Forks vs. Knives</a></li>

  <li><a href="ch032_the-tyranny-of-structurelessness.html">The Tyranny of Structurelessness</a></li>

  <li><a href="ch033_free-vs-gratis-labor.html">Free vs. Gratis Labor</a></li>

  <li><a href="ch034_other-peoples-computers.html">Other People’s Computers</a></li>

  <li><a href="ch035_science-20.html">Science 2.0</a></li>

  <li><a href="ch036_beyond-education.html">Beyond Education</a></li>

  <li><a href="ch037_how-would-it-translate.html">How Would It Translate?</a></li>

  <li><a href="ch038_death-is-not-the-end.html">Death is Not the End</a></li>

  <li class="booki-section">Futures</li>

  <li><a href="ch040_free-as-in-free-world.html">Free as in Free World</a></li>

  <li><a href="ch041_networked-solidarity.html">Networked Solidarity</a></li>

  <li><a href="ch042_free-culture-in-cultures-that-are-not-free.html">Free Culture in Cultures that are Not Free</a></li>

  <li class="booki-section">Epilogue</li>

  <li><a href="ch044_anatomy-of-the-first-book-sprint.html">Anatomy of the First Book Sprint</a></li>

  <li><a href="ch045_2-words-vs-33000.html">2 Words vs. 33,000</a></li>

  <li><a href="ch046_knock-knock.html">Knock Knock</a></li>

  <li><a href="ch047_are-we-interested.html">Are We Interested?</a></li>

  <li><a href="ch048_chat-samples.html">Chat Samples</a></li>

  <li><a href="ch049_looking-in-from-the-outside.html">Looking In From the Outside</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="ch051_things-we-ended-up-not-including.html">Things We Ended Up Not Including</a></li>

  <li><a href="ch052_write-this-book.html">Write This Book</a></li>

  <li><a href="ch053_credits.html">Credits</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch033_free-vs-gratis-labor.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch033_free-vs-gratis-labor.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

