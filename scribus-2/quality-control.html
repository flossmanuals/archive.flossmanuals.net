<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>Scribus</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/scribus-2/scribus-2.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/scribus-2/scribus-2.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	Scribus
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="quality-control.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="quality-control.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Quality Control</h1>

  <p>The purpose of quality control is to make sure that your layout will be exported to PDF following certain limitations and free of errors of various types, which is essential for graphic design production. There are three tools which help us: <strong>Preflight Verifier</strong>, <strong>PDF Export</strong>, and the <strong>Search-Replace</strong> function. The Preflight Verifier is configurable in <em>File &gt; Preferences &gt; Preflight Verifier</em>, and it looks for the following:</p>

  <ul>
    <li>Missing characters</li>

    <li>Objects which are placed somewhere off the page</li>

    <li>Text overflow</li>

    <li>Transparency</li>

    <li>Missing images<br></li>

    <li>Resolution of images</li>

    <li>PDF files contained in the layout</li>

    <li>Presence of GIF images</li>

    <li>Verification of PDF Annotations and Fields</li>

    <li>Ignoring unprintable layers</li>

    <li>Checking the difference between display and printing of layers</li>
  </ul>

  <p>Preflight Verifier displays a dialog which will show each error it detects, along with the relevant page and object. The advantage is that it very easily will detect a host of errors your visual review may easily miss, and takes at most a few seconds to complete.</p>

  <p>Once you are past this initial screen, you then proceed to the next step, <strong>PDF Export</strong>. The various options are explained in the chapter <em>The Final Output</em>. For our purposes here, we will say that this takes a few seconds to run the export function, and in some cases it may need to be repeated. This might come about after you examine the PDF on screen or in printed form.</p>

  <h2>What should you check?</h2>

  <p>Everything! Quality control requires a keen eye and for you to be mentally sharp. If you keep in mind that a Scribus document is really a set of instructions on how to build a PDF from various sources, you can realize the importance of having at hand all the materials you need. You are also mindful of the technical specifications for your layout, and you have already been checking, rechecking, and re-rechecking all of this as you create the layout. In this final check we are once again reviewing the technical specifications, on a practical level the geometry of the pages and the content. This includes the following:</p>

  <ol>
    <li>The number of pages</li>

    <li>Document format, with attention to the measurement units</li>

    <li>Margins</li>

    <li>Safety margins, allowing for the type of finishing, trimming, binding, etc.*</li>

    <li>Bleed</li>

    <li>The color space</li>

    <li>The number of colors</li>

    <li>The actual colors (plates)</li>

    <li>Resolution of images</li>

    <li>Ink coverage</li>

    <li>Black**</li>

    <li>Output profiles</li>

    <li>The presence of all the sources</li>

    <li>Typography – applying styles and typographic formatting, gaps, widow and orphans, typographical gray, hyphenation, and spaces</li>

    <li>Entering corrections</li>

    <li>Compliance with the stylebook as appropriate</li>

    <li>Compliance with all particular requirements of the project</li>
  </ol>

  <p>If the document is to be printed, the final true test is viewing the actual printed output. A document created for the screen can only be checked on screen.</p>

  <p>* Safety margins are not to be confused with document margins. This includes the tolerance of the finishing equipment, trimming and binding, gluing, and perforating. This will depend on the specific printing equipment, and should have been checked at the beginning of the design process, since it may affect the design and layout of page elements. More specifically, you don't want important content in areas susceptible to loss during finishing.</p>

  <p>** There are many kinds of black, and you may in fact use different blacks in the same document and on the same page. Once your document has been printed out in quantity, these different blacks may be incompatible and at that point it's too late. Imagine you have a photo with a margin which fades to black, yet this turns out to be a different black than the background around it. An attempt at a sublime transition becomes ugly.</p>

  <h2>Proofreading</h2>

  <p><strong>Copyediting</strong> and <strong>proofreading</strong> are related tasks. The former applies to reviewing the source materials for accuracy, grammar, and content. The latter, proofreading, as the name implies, happens <em>after</em> you have generated a proof of your work for a final assessment, not only of the issues checked during copyediting, but also fonts, layout, and all the visual aspects of the end product. You begin on bad footing with your reader if your work isn't coherent, concise, and pleasing to view and read. As it implies, copyediting is best done before your create your layout, set and adjust your styles, since this is the time when editing of the content is easiest. It also avoids creating other problems which might change the layout, such as when you significantly add or remove material.</p>

  <p>This having been said, you may still find yourself changing the content in the proofreading stage. The choices for hyphenation may be inappropriate and need editing. You simply may miss some grammatical errors until this final stage. It may be that once you see the layout a change in the text content may improve the flow of text or deal with some ugliness like rivers or gaps in the lines of text. Thus, proofreading is also a very essential part of the editing process.</p>

  <h3>What about spell-checking?</h3>

  <p>Scribus has a built-in spell-checker [<em>Item &gt; Check Spelling</em>], though it admittedly is a bit cumbersome. Typically, your word processing software will have a much easier to use and better spell-checker. Even so, as good as these are, they are not error-free, so it is up to you to find mistakes these automated utilities leave or create.</p>

  <h2>Entering corrections</h2>

  <p>Once errors are identified, they must be corrected. On a simple operative level, this only requires identifying an error, going to <em>Edit Contents</em> mode, and then going to and fixing the error. This seems simple enough, but requires rapt attention and efficient visual scanning.</p>

  <p>You have at your disposal another tool in Scribus for this task: <strong>Search-Replace</strong>. Once you have selected a text frame, select <em>Edit &gt; Search/Replace.</em></p>

  <p>The advantages include the fact that your errors will be found very quickly and without mistakes. You can start by simply entering the text to search for – it's not necessary to fill in all these fields. Immediately you will be presented with the appropriate location, then you may choose to enter the text to replace the error, and choose to replace one at a time or all subsequent errors also.</p>

  <p align="center"><img src="_booki/scribus-2/static/Scribus_en-qualitycontrol-Search.png"></p>

  <p align="center"><img src="_booki/scribus-2/static/Scribus_en-qualitycontrol-Replace.png"></p>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Introduction</a></li>

  <li><a href="requirements.html">Requirements</a></li>

  <li><a href="hands-on.html">Hands-on</a></li>

  <li><a href="examples-using-scribus.html">Examples using Scribus</a></li>

  <li><a href="about-this-book.html">About this book</a></li>

  <li class="booki-section">Create</li>

  <li><a href="begin-at-the-end.html">Begin at the end</a></li>

  <li><a href="editorial-and-graphical-workflows.html">Editorial and graphical workflows</a></li>

  <li><a href="preparing-your-sources.html">Preparing your sources</a></li>

  <li><a href="document-format.html">Document format</a></li>

  <li><a href="choosing-colors.html">Choosing colors</a></li>

  <li><a href="selecting-fonts.html">Selecting fonts</a></li>

  <li class="booki-section">Organize</li>

  <li><a href="organizing-your-files.html">Organizing your files</a></li>

  <li><a href="master-pages.html">Master Pages</a></li>

  <li><a href="composing.html">Composing</a></li>

  <li><a href="styles.html">Styles</a></li>

  <li><a href="scripter-and-scripts.html">Scripter and scripts</a></li>

  <li class="booki-section">Produce</li>

  <li><a href="importing-text-and-images.html">Importing text and images</a></li>

  <li><a href="typography.html">Typography</a></li>

  <li><a href="text-flow-and-typography.html">Text flow and typography</a></li>

  <li><a href="color-management.html">Color management</a></li>

  <li><a href="quality-control.html">Quality Control</a></li>

  <li><a href="archiving-documents.html">Archiving documents</a></li>

  <li><a href="the-final-output.html">The final output</a></li>

  <li class="booki-section">Appendix</li>

  <li><a href="frequent-problems.html">Frequent problems</a></li>

  <li><a href="installation.html">Installation</a></li>

  <li><a href="glossary.html">Glossary</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="quality-control.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="quality-control.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

