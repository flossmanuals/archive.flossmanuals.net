<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
  <head>
    <title>OpenMRS Developers' Guide</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/openmrs-developers-guide/openmrs-developers-guide.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/openmrs-developers-guide/openmrs-developers-guide.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	OpenMRS Developers' Guide
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="architecture.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="architecture.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Architecture</h1>

  <p>This chapter contains an in-depth view of the architecture of the system. If you don't understand everything on the first reading, don't fret! Understanding how the basic system fits together is the most important thing you need for now.</p>

  <h2>Technical overview&nbsp;</h2>

  <h3><span style="font-weight: normal;"><font size="2">OpenMRS is a framework built upon Java and other related frameworks. It is based on a modular architecture which consists of a core application and optional modules which provide additional functionality to the core workflows.</font></span></h3>

  <p><font face="tahoma, arial, helvetica, sans-serif" size="2">The key architectural components of the OpenMRS core can be depicted as follows:</font></p>

  <p><img src="_booki/openmrs-developers-guide/static/Application-layers.png" alt="" align="" width="600" height="517" class=""></p>

  <p><em>An overview of OpenMRS</em></p>

  <p>The backbone of OpenMRS lies in its core API.&nbsp;The OpenMRS API has methods for all of the basic functions such as adding/updating a patient, encounter, observation, etc. Methods which enable this functionality are provided in service layer classes.&nbsp;</p>

  <h2>The source code structure</h2>

  <p>In OpenMRS framework and modules, there are different levels in the code architecture. The OpenMRS source code is divided into three main segments: the&nbsp;User Interface, Service, and Data Access layers. This layering isolates various system responsibilities from one another, to improve both system development and maintenance.</p>

  <h3>The Data Access layer</h3>

  <p>The Data Access layer is an abstraction layer from the actual data model and its changes. It uses Hibernate as the Object Relational mapping tool, and Liquibase to manage relational database changes in a database-independent way.</p>

  <p>The relationships between our domain objects and database tables are mapped using a mixture of Hibernate annotations and XML mapping files. The data access layer is exposed to the service layer through interfaces, thereby shielding it from from implementation details such as which object relational mapping tool is being used.</p>

  <h3>The Service layer</h3>

  <p>The Service layer is responsible for managing the business logic of the application. It is built around the Spring framework. The OpenMRS service layer classes make extensive use of the Spring framework for a number of tasks including the following:&nbsp;</p>

  <ul>
    <li>Spring Aspect Oriented Programming (AOP) is used to provide separate cross cutting functions (for example: authentication, logging).</li>
  </ul>

  <ul>
    <li>Spring Dependency Injection (DI) is used to provide dependencies between components.</li>
  </ul>

  <ul>
    <li>Spring is used to manage transactions in between service layer classes</li>
  </ul>

  <h3>User Interface layer</h3>

  <p>The User Interface layer for the legacy application is built upon Spring MVC, Direct Web Remoting (DWR), JSP and JavaScript. DWR is used for AJAX functionality and it provides the mapping between our Java objects and methods to JavaScript objects and methods respectively. JQuery is used to simplify the interactions with Javascript and the browser. Spring MVC is used to provide the Model-View-Controller design pattern. Our domain objects serve as the Model. We have a mixture of controllers that subclass Spring's SimpleFormControllers and those which use Spring's @Controller annotation. For the new reference application user interface, we no longer use Spring MVC, DWR or JSP, but heavily use Groovy, JQuery, AngularJS, and more.</p>

  <h2>The Modular Architecture</h2>

  <p>At the heart of OpenMRS is a custom module framework which lets you extend and modify the default functionality of the OpenMRS core in accordance to your needs. Modules are also&nbsp;structured like the OpenMRS core, and consist of user interface, data access and service layers.</p>

  <p><span style="font-size: 12px; line-height: 17px;">Some OpenMRS functionality is pulled out into modules instead of being written into the core application. This allows users to upgrade the content in those modules without having to wait for the next OpenMRS release. Currently, the only core module used in OpenMRS is the Logic Module.</span>&nbsp;</p>

  <h2>Associated Frameworks and Technology Stacks</h2>

  <h3>Hibernate</h3>

  <p>Hibernate is the Object Relational Mapper used by OpenMRS. It allows users to describe the relationship between database tables and domain objects using just xml or annotations.</p>

  <p>Hibernate is also useful in managing dependencies between classes. As an example, the concept domain in the data model consists of tables named <em>concept</em>, <em>concept_answer</em>, <em>concept_set</em> and <em>concept_name</em>. It would be very difficult to keep up with where to store each part of the concept object and the relations between them if a user decides to update each table individually. However, using Hibernate, developers only need to concern themselves with the Concept object, and not the tables behind that object. The <em>concept.hbm.xml</em> mapping file does the work of knowing that the Concept object contains a collection of ConceptSet objects, a collection of ConceptName objects, etc.</p>

  <p>However, also note that Hibernate enforces lazy loading - it will not load all associated objects until they are needed. For this reason, you&nbsp;must&nbsp;either fetch/save/manipulate your object in the same session (between one open/closeSession) or you must hydrate all object collections in the object by calling the getters (getConceptAnswers, getConceptNames, getSynonyms, etc).</p>

  <h3>Spring MVC</h3>

  <p>OpenMRS strongly subscribes to the Model-View-Controller pattern. Most controllers included in the OpenMRS core will be SimpleFormControllers and be placed in the <em>org.openmrs.web.controller</em>&nbsp;package. However, some controllers have been rewritten to use Spring 2.5+ annotations, and we recommend that you use these in the future. The model is set up in the controller's formBackingObject, and processed/saved in the processFormSubmission and onSubmit methods. The jsp views are placed in <em>/web/WEB-INF/view</em>.</p>

  <p>Furthermore, not all files served by the webapp are run through Spring. The <em>/web/WEB-INF/web.xml</em>&nbsp;file maps certain web page extensions to the SpringController. All *.form, *.htm, and *.list pages are mapped. The SpringController then uses the mappings in the <em>openmrs-servlet.xml</em>&nbsp;file to know which pages are mapping to which Controller.</p>

  <p>There are no .jsp pages that are accessed directly. If a page's url is <em>/admin/patients/index.htm</em>, the jsp will actually reside in <em>/web/WEB-INF/view/admin/patients/index.jsp</em>. This is necessary so that we can do the redirect with the SpringController. Because the file being accessed ends with .htm, the SpringController is invoked by the web server. When the SpringController sees the url, it simply replaces .htm with .jsp and looks for the file in <em>/web/WEB-INF/view/</em> according to the jspViewResolver bean in <em>openmrs-servlet.xml</em>. If the page being accessed was patient.form, the mapping in the urlMapping bean would have told spring to use the PatientFormController and the patientForm.jsp file.&nbsp;</p>

  <h2>Authentication and Authorization</h2>

  <p>OpenMRS has a very granulated permissions system. Every action is associated with a Privilege, which in turn can be grouped into Roles. Examples of such privileges are "Add Patient", "Update Patient", "Delete Patient", "Add Concept", "Update Concept", and more. A Role can also point to a list of inherited roles. The role inherits all privileges from that inherited role. In this way, hierarchies of roles are possible. A User contains only a collection of Roles, not Privileges. These privileges are enforced in the service layer using AOP annotations.</p>

  <p><span style="font-family: Arial, verdana, sans-serif; font-size: 18px; font-weight: bold;">Build management</span></p>

  <p>OpenMRS uses Apache Maven for build management of the OpenMRS core and modules.&nbsp;</p>

  <p>All information regarding the module being built, its dependencies on other external modules and components, the build order, directories, and required plug-ins&nbsp;are stored in the modules' <em>pom.xml</em> file.&nbsp;</p>

  <p>Following release, these build artifacts are uploaded and maintained in a maven repository manager. A maven repository manager is used for this purpose due to a number of advantages that it provides. These advantages include:</p>

  <ul>
    <li>Faster and more reliable builds</li>

    <li>Improved collaboration</li>

    <li>Component usage visibility</li>

    <li>Enforcement of component standards&nbsp;</li>
  </ul>The Maven Repository used by OpenMRS is SonaType Nexus, which can be accessed at <a href="http://mavenrepo.openmrs.org/nexus/">http://mavenrepo.openmrs.org/nexus/</a>.<br>

  <p>Artifacts maintained in the OpenMRS repository are:</p>

  <p><strong>Releases</strong></p>

  <ul>
    <li>Maven built releases (1.8.0 and later)</li>

    <li>Ant built releases (1.5.0 up to 1.7.X)</li>
  </ul>

  <p><strong>Snapshots</strong></p>

  <ul>
    <li>Maven development versions</li>
  </ul>

  <p><strong>Modules</strong></p>

  <ul>
    <li>Module releases</li>
  </ul>

  <p><strong>3rd party artifacts</strong></p>

  <ul>
    <li>Libraries not found in other Maven repositories (HAPI)</li>

    <li>Modified libraries (DWR, Hibernate, Liquibase, Simple XML)</li>

    <li>Custom Maven plugins (OpenMRS omod plugin)</li>
  </ul>

  <h2>&nbsp;Summary</h2>

  <p>As you read the next section, keep in mind the important parts from this chapter:</p>

  <ul>
    <li>OpenMRS consists of a core system, with a modular architecture to extend its functionality.</li>

    <li>There are three main layers to the system:&nbsp;<span style="font-family: Trebuchet, verdana, sans-serif;">User Interface, Service Layer and Data Access Layer.</span></li>

    <li>openMRS makes extensive use of a number of frameworks including Spring and Hibernate.</li>

    <li>We use Apache Maven for build management, JIRA for issue management and Github for version control.</li>
  </ul>
</div>

<ul class="menu-goes-here">
  <li class="booki-section">Introduction</li>

  <li><a href="index.html">Welcome to OpenMRS!</a></li>

  <li class="booki-section">Saving Lives With Software</li>

  <li><a href="the-need-for-health-it.html">The Need for Health IT</a></li>

  <li><a href="story-of-openmrs.html">Our Response</a></li>

  <li><a href="state-of-openmrs.html">OpenMRS Today</a></li>

  <li class="booki-section">Community</li>

  <li><a href="why-how-we-communicate.html">Working Cooperatively</a></li>

  <li><a href="collaboration-tools.html">Collaboration Tools</a></li>

  <li class="booki-section">Technology</li>

  <li><a href="architecture.html">Architecture</a></li>

  <li><a href="data-model.html">Data Model</a></li>

  <li><a href="development-process.html">Development Process</a></li>

  <li><a href="get-set-up.html">Get Set Up</a></li>

  <li class="booki-section">Case Study</li>

  <li><a href="hello-world-module.html">Creating Your First Module</a></li>

  <li class="booki-section">What's Next?</li>

  <li><a href="get-involved.html">Get Involved</a></li>

  <li><a href="support.html">Get Support</a></li>

  <li><a href="checklist.html">Developer Checklist</a></li>

  <li class="booki-section">Appendices</li>

  <li><a href="appendix-a-learning-resources.html">Appendix A: Learning Resources</a></li>

  <li><a href="appendix-b-glossary.html">Appendix B: OpenMRS Glossary</a></li>

  <li><a href="appendix-c-troubleshooting-flow-chart.html">Appendix C: Troubleshooting</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="architecture.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="architecture.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

