<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="LTR">
  <head>
    <title>VLC</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="_templates/fm_resources/fl2.ico" type="image/x-icon" />
    <link rel="stylesheet" href="_templates/fm_resources/fm_book.css" type="text/css" />
   <link type="text/css" href="_templates/prettify/src/prettify.css" rel="Stylesheet" >
<script type="text/javascript" src="_templates/prettify/src/prettify.js"></script>



    <script type="text/javascript" src="_templates/jquery/js/jquery-1.6.1.min.js"></script>

    <script type="text/javascript" src="_templates/fm_resources/fm_book.js"></script>
</head>
<body onload="prettyPrint();">
<div id="home">
  <a href="../index.html"><img src="_templates/fm_resources/top_read_back.gif" border="0" alt="" /></a>
</div>

<div id="pdf">
  <a href="../_booki/vlc/vlc.epub"><img src="_templates/fm_resources/epub.gif" border="0" alt="" /></a>
  <a href="../_booki/vlc/vlc.pdf"><img src="_templates/fm_resources/pdf.gif" border="0" alt="" /></a>
  <a href="_all.html"><img src="_templates/fm_resources/print.gif" border="0" alt="" /></a>
</div>

<div id="index-book-title">
	VLC
</div>
<div id="main">
  <div class="navbar">
    <div class="arrow top left"><a href="ch009_playing-video.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow top right"><a href="ch009_playing-video.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
<!--    <div id="edit-links">
      <a href='http://www.booki.cc/'>Edit this Book</a>
    </div>
-->
  </div>

  <div class="fm-content">
    <div id="main-content">
  <h1>Playing Video&nbsp;&nbsp;&nbsp;</h1>

  <p><strong>Software name :</strong> VLC<br>
  <strong>Software version&nbsp; :</strong> 1.0.3</p>

  <p class="MsoNormal"><span>VLC supports a wide range of video file formats including: MPEG,</span> <span>WMV, DIVX, H.264, and Real Video, making it one of the most versatile media players out there. VLC can also play network streams and most commercial DVDs with ease.</span></p>

  <p class="MsoNormal"><span>In this chapter, we’ll focus on two common tasks: playing video files and playing DVDs.</span></p>

  <p class="MsoNormal"><span>Let’s take a look at the interface in Windows 7:</span></p>

  <p><img alt="1.png" src="_booki/vlc/static/VLC-Playing_Video-1-en.png" height="310" width="441">&nbsp;</p>

  <p class="MsoNormal"><span>VLC features most standard playback options and more. For instance, you can slow or speed playback using the <strong>Slower</strong> and <strong>Faster</strong> buttons. The <strong>Toggle Fullscreen</strong> button allows you to switch back and forth between fullscreen and normal view modes. The <strong>Playlist</strong> button opens the <strong>Playlist</strong> menu which allows you to drag-and-drop video files into a playlist for continuous playing. The <strong>Extended Settings</strong> button opens a tab-based menu that allows you to adjust audio and video effects during playback. For instance, the brightness and contrast levels can be adjusted to suit your needs.</span></p>

  <p>&nbsp;</p>

  <p class="MsoNormal"><strong><span>Playing video files</span></strong></p>

  <p class="MsoNormal"><span>The easiest way to play (open) video files is the method of drag-and-drop directly into VLC.</span></p>

  <p class="MsoNormal"><span>In this example, we’re opening a sample video file of ‘Keyboard Cat’ on the Windows 7 desktop. Both the video file and the icon to launch VLC are on the desktop. This particular video file is a .WMV file type, which is supported by VLC.</span></p>

  <p>&nbsp;</p>

  <p class="MsoListParagraph"><strong><span>To drag-and-drop a video file into VLC:</span></strong></p>

  <p class="MsoListParagraph"><span>1. Hover the mouse pointer over the video file.</span></p>

  <p class="MsoListParagraph"><span><span>2.</span></span> <span>Click and hold the left mouse button.</span></p>

  <p>3. &nbsp;<span>Still holding the left mouse button, move the pointer to the VLC icon and release the left mouse button.</span>&nbsp;</p>

  <p><img alt="2.png" src="_booki/vlc/static/VLC-Playing_Video-2-en.png" height="144" width="224">&nbsp;</p>

  <p class="MsoNormal"><span>The video file begins playback.</span></p>

  <p><br></p>

  <p class="MsoNormal"><span>Another method of opening a video file is through the menu.</span></p>

  <p>&nbsp;</p>

  <p class="MsoNormal"><strong><span>To open a video file using the menu:</span></strong></p>

  <p><span><span>1.<span>&nbsp;</span></span></span> <span>On the menu, click <strong>Media</strong> &gt; <strong>Open File…</strong>.</span>&nbsp;</p>

  <p><img alt="3.png" src="_booki/vlc/static/VLC-Playing_Video-3-en.png" height="332" width="307">&nbsp;</p>

  <p>&nbsp;</p>

  <p><span><span>2. <span>&nbsp;</span></span></span><span>Browse to the directory of your desired video file.</span>&nbsp;</p>

  <p><img alt="4.png" src="_booki/vlc/static/VLC-Playing_Video-4-en.png" height="365" width="521">&nbsp;</p>

  <p>3. Double-click the video file.</p>

  <p>&nbsp;</p>

  <p class="MsoNormal"><span>The video file begins playback.</span></p>

  <p><img alt="5.png" src="_booki/vlc/static/VLC-Playing_Video-5-en.png" height="367" width="298">&nbsp;</p>

  <p>&nbsp;</p>

  <p class="MsoNormal"><span>Playing a commercial DVD with VLC is nearly the same process as playing a video file. While drag-and-drop can be used, the fastest method is using the menu. Before we begin, make sure your computer can play DVDs, which requires a DVD-ROM drive installed on your computer.</span></p>

  <p><br></p>

  <p class="MsoNormal"><strong><span>To play a DVD:</span></strong></p>

  <p><span>1. With a DVD inserted into the DVD-ROM drive, click <strong>Media</strong> &gt; <strong>Open Disc…</strong></span><br></p>

  <p><img alt="6.png" src="_booki/vlc/static/VLC-Playing_Video-6-en.png" height="348" width="321">&nbsp;</p>

  <p>&nbsp;</p>

  <p class="MsoNormal"><span>The <strong>Disc</strong> tab is now selected.</span></p>

  <p><img alt="7.png" src="_booki/vlc/static/VLC-Playing_Video-7-en.png" height="456" width="470">&nbsp;</p>

  <p><span>2. Make sure the <strong>Disc device</strong> box has the DVD-ROM drive selected. By default, VLC selects the most common location, but you may need to browse and find it.</span></p>

  <p>3. Click the <strong>Play</strong> button.&nbsp;</p>

  <p>The DVD opens and begins playback.</p>

  <p><br>
  While in play mode, DVD menu items can be accessed with the pointer, but this ability varies with DVD.&nbsp;</p>
</div>

<ul class="menu-goes-here">
  <li><a href="index.html">INTRODUCTION</a></li>

  <li class="booki-section">INSTALLING</li>

  <li><a href="ch002_ubuntu.html">UBUNTU</a></li>

  <li><a href="ch003_osx.html">OSX</a></li>

  <li><a href="ch004_windows.html">WINDOWS</a></li>

  <li><a href="ch005_linux-advanced.html">LINUX (advanced)</a></li>

  <li class="booki-section">GETTING STARTED</li>

  <li><a href="ch007_interface.html">INTERFACE</a></li>

  <li class="booki-section">PLAYING CONTENT</li>

  <li><a href="ch009_playing-video.html">PLAYING VIDEO</a></li>

  <li><a href="ch010_playing-streams.html">PLAYING STREAMS</a></li>

  <li><a href="ch011_viewing-subtitles.html">VIEWING SUBTITLES</a></li>

  <li><a href="ch012_playing-video_ts-files.html">PLAYING VIDEO_TS FILES</a></li>

  <li class="booki-section">STREAMING</li>

  <li><a href="ch014_what-is-streaming.html">WHAT IS STREAMING?</a></li>

  <li><a href="ch015_streaming-to-icecast.html">STREAMING TO ICECAST</a></li>

  <li><a href="ch016_streaming-wizard.html">STREAMING WIZARD</a></li>

  <li class="booki-section">ENCODING AUDIO</li>

  <li><a href="ch018_ripping-cds.html">RIPPING CDS</a></li>

  <li class="booki-section">ENCODING VIDEO</li>

  <li><a href="ch020_basic-encoding.html">BASIC ENCODING</a></li>

  <li><a href="ch021_grabbing-stills.html">GRABBING STILLS</a></li>

  <li class="booki-section">APPENDICES</li>

  <li><a href="ch023_glossary.html">GLOSSARY</a></li>

  <li><a href="ch024_links.html">LINKS</a></li>

  <li><a href="ch025_credits.html">CREDITS</a></li>
</ul>

  </div>
<br><br>
  <div class="navbar">
    <div class="arrow bottom left"><a href="ch009_playing-video.html#"><img src="_templates/fm_resources/lefttext.png" style="height:20px !important;" alt="" /></a></div>
    <div class="arrow bottom right"><a href="ch009_playing-video.html#"><img src="_templates/fm_resources/righttext.png" style="height:20px !important;" alt="" /></a></div>
  </div>
</div>

</body>
</html>

